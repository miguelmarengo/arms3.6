# coding=utf-8

# para generar los .py del QT en terminal:
# pyuic5 -x mainwindow.ui > mainwindow.py
# pyrcc5 img5.qrc -o img5_rc.py

import threading


from PyQt5 import QtWebEngineWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox

from ARMSui.mainwindow import *

from Base.GeneradorRutas.Optimiza.tablas_drag_drop import TablasDragRows
from Base.DataScience.Dashboard.Dashboard import *
from Base.GeneradorRutas.Datos.Datos import *
from Base.GeneradorRutas.Despegue.Despegue import Despegue
from Base.GeneradorRutas.Despegue.Predespegue import Predespegue
from Base.Arcos.Service import DistanceService
from Base.GeneradorRutas.Optimiza.funciones import *
from Base.GeneradorRutas.Motor.ejecucionOpta import OptaPlanner
from Base.GeneradorRutas.Datos.Planes import *
from Base.GeneradorRutas.Optimiza.OptimizaAutomatico import *
from Base.GeneradorRutas.Optimiza.OptimizaManual import *
from Base.GeneradorRutas.Optimiza.AnadirPedidos import *
from IO.Apis.json2df import *
from IO.Apis.call_motor_optimizado import CallMotorOptimizado
from Base.IniciarSesion.Acceso.IniciarSesion import *
from pathlib import Path
from Base.GeneradorRutas.Asignar.Asignar import *
from IO.validaciones.checkssformat import *

from Base.mis_trabajadores.trabajador_Motor import *
from Base.mis_trabajadores.trabajador_generico import *
from IO.Apis.login import Login
    # ==========================================================
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        # prepara los hilos de los trabajadores
        self.hiloTrabajadorMotor = QThreadPool()
        self.hiloTrabajadorGenerico = QThreadPool()

        #abre arcos
        #DistanceService.synchronizeFile()

        #intentar logear test
        #response = Login.validate(self,"SILODISA","avazquez@arete.ws","avazquez")
        

        try:
            path = str(Path.home())
            pathFolder = path+"/apps/arms/SILODISA/SILODISA/arcos"
            pathFile   = pathFolder+"/arcos.txt"
            config.A = pd.read_csv(pathFile, sep='\t',
                                names=['ORIGEN', 'DESTINO', 'DISTANCIA', 'TIEMPO', 'LATITUD_DESTINO_A',
                                        'LONGITUD_DESTINO_A', 'LATITUD_DESTINO_B', 'LONGITUD_DESTINO_B',
                                        'FECHA_CALCULO'])
        except Exception as ex:
            print("no existe el archivo de arcos localmente")
        
        
        # Widgets
        self.initUI(self)
        if len(config.usuario) != 0:
            # Datos
            self.datos = Datos(self)
            self.datos.cargaDatos()
            # Planes
            self.plan = Planes(self)
            self.plan.cargaPlanesCreados()
        llena_lista_pedidos(self)

        # optimizador
        self.optimizaAutomatico = OptimizaAutomatico(self)

        # Motor
        # Inicia una instancia de OptaPlanner (java) en un hilo de ejecución separado
        self.optaPlanner = OptaPlanner(self)
        self.optaPlanner.generaDirectorios()
        self.optaPlanner.iniciaInstancia()
        self.activabtnIniciar()

        # Despegue
        # self.despegue = Despegue(self)
        # self.despegue.manejador()
        #
        # #Asignador
        self.asignar = Asignador(self)
        #
        # # Predespegue
        # self.predespegue = Predespegue(self)
        #
        # # dashboards
        showdashboardsfun(self)



    def initUI(self, ui):
        # -- STATUS BAR -------------------------------------------------
        # prepara el statusbar
        self.progressBar = QProgressBar(ui)
        self.labelEmpresa = QLabel(ui)
        self.labelCEDI = QLabel(ui)
        self.labelUser = QLabel(ui)
        self.lblMensaje = QLabel(ui)
        s = 'QProgressBar{border: 0px solid grey;border-radius: 5px;text-align: center}'
        s1 = ' QProgressBar::chunk {background-color: rgba(106, 193, 140, 55);width: 10px;margin: 1px;}'
        self.progressBar.setStyleSheet(s+s1)
        # añadelos permanente al status bar
        ui.statusBar.addPermanentWidget(self.progressBar, 4)
        ui.statusBar.addPermanentWidget(self.lblMensaje, 3)
        ui.statusBar.addPermanentWidget(self.labelEmpresa, 1)
        ui.statusBar.addPermanentWidget(self.labelCEDI, 1)
        ui.statusBar.addPermanentWidget(self.labelUser, 1)
        # # -- DRAG and DROP -------------------------------------------------
        self.gridPlanManual = TablasDragRows()
        self.gridV1Manual = TablasDragRows()
        self.gridV2Manual = TablasDragRows()
        self.layoutPlan.addWidget(self.gridPlanManual)
        self.layoutV1.addWidget(self.gridV1Manual)
        self.layoutV2.addWidget(self.gridV2Manual)

        self.gridPlanManual.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.gridPlanManual.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.gridV1Manual.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.gridV1Manual.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.gridV2Manual.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.gridV2Manual.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

        #self.gridPlanManual.setVerticalHeaderItem()

        # self.gridPlanManual.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        # self.gridV1Manual.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        # self.gridV2Manual.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)

        # self.gridPlanManual.resizeColumnsToContents()
        # self.gridPlanManual.resizeRowsToContents()
        # self.gridV1Manual.resizeColumnsToContents()
        # self.gridV1Manual.resizeRowsToContents()
        # self.gridV2Manual.resizeColumnsToContents()
        # self.gridV2Manual.resizeRowsToContents()

        self.gridPlanManual.verticalHeader().setDefaultSectionSize(14);
        self.gridV1Manual.verticalHeader().setDefaultSectionSize(14);
        self.gridV2Manual.verticalHeader().setDefaultSectionSize(14);

        self.gridPlanManual.setStyleSheet('background:transparent;')
        self.gridV1Manual.setStyleSheet('background:transparent;')
        self.gridV2Manual.setStyleSheet('background:transparent;')

        self.gridPlanManual.horizontalHeader().setDefaultSectionSize(75);
        self.gridV1Manual.horizontalHeader().setDefaultSectionSize(75);
        self.gridV2Manual.horizontalHeader().setDefaultSectionSize(75);

        self.gridPlanManual.horizontalHeader().setMinimumSectionSize(75)
        self.gridV1Manual.horizontalHeader().setMinimumSectionSize(75)
        self.gridV2Manual.horizontalHeader().setMinimumSectionSize(75)

        self.gridPlanManual.setFixedWidth(600)

        # connect's
        self.gridPlanManual.clicked.connect(self.slotGridtablaPlanClicked)
        self.gridV1Manual.clicked.connect(self.slotGridtablaV1Clicked)
        self.gridV2Manual.clicked.connect(self.slotGridtablaV2Clicked)

        self.dashboard_1 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar1)
        self.dashboard_2 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar2)
        self.dashboard_3 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar3)
        self.dashboard_4 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar4)
        self.dashboard_5 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar5)
        self.dashboard_6 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar6)
        self.dashboard_7 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar7)
        self.dashboard_8 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar8)
        self.dashboard_9 = QtWebEngineWidgets.QWebEngineView(ui.tab2Analizar9)



        # -- TOOL BAR -------------------------------------------------
        # slot_recarga_ss = QAction(QIcon(':/IMG4/1REFRESH.png'), "Recarga Plan", self)
        # slot_guarda_ss = QAction(QIcon(':/IMG4/1GUARDA.png'), "Guarda el Plan", self)
        # self.toolBar.addAction(slot_recarga_ss)
        # self.toolBar.addAction(slot_guarda_ss)
        # self.toolBar.addSeparator()
        # self.toolBar.actionTriggered[QAction].connect(self.toolBarBtnPressed)
        # -- conectar conlas drag tablas que hice -------------------------------------------------
        # self.tablaPlan.cellChanged['int','int'].connect(self.slot_plan_abierto_administrar_manual)

    def keyPressEvent(self, e):

        print(e.key())

        if e.key() == Qt.Key_Shift:
            config.Key_Shift = True
            print('Qt.Key_Shift down')

        if e.key() == Qt.Key_Alt:
            config.Key_Alt = True
            print('Qt.Key_Alt down')

        if e.key() == Qt.Key_Control:
            config.Key_Control = True
            print('Qt.Key_Control down')

        if e.key() == Qt.Key_F1:
            print('Qt.Key_F2')
        if e.key() == Qt.Key_F2:
            print('Qt.Key_F2')
        if e.key() == Qt.Key_F3:
            print('Qt.Key_F3')
        if e.key() == Qt.Key_F4:
            print('Qt.Key_F4')
        if e.key() == Qt.Key_F5:
            print('Qt.Key_F5')
        if e.key() == Qt.Key_F6:
            print('Qt.Key_F6')
        if e.key() == Qt.Key_F7:
            print('Qt.Key_F7')
        if e.key() == Qt.Key_F8:
            print('Qt.Key_F8')
        if e.key() == Qt.Key_F9:
            print('Qt.Key_F9')
        if e.key() == Qt.Key_F10:
            print('Qt.Key_F10')
        if e.key() == Qt.Key_F11:
            print('Qt.Key_F11')
        if e.key() == Qt.Key_F12:
            print('Qt.Key_F12')

    def keyReleaseEvent(self, e):

        if e.key() == Qt.Key_Shift:
            config.Key_Shift = False
            print('Qt.Key_Shift up')

        if e.key() == Qt.Key_Alt:
            config.Key_Alt = False
            print('Qt.Key_Alt up')

        if e.key() == Qt.Key_Control:
            config.Key_Control = False
            print('Qt.Key_Control up')

    def resizeEvent(self, event):
        resizewindow_dash(self)

    def activabtnIniciar(self):
        self.btnMotorIniciar.setEnabled(True)
        self.btnMotorIniciar.setStyleSheet("color: rgba(125,201,159);\n"
                                              "border-color: rgba(125,201,159);\n"
                                              "")

        self.btnMotorDetener.setEnabled(False)
        self.btnMotorDetener.setStyleSheet("color: gray;\n"
                                              "border-color: gray;\n"
                                              "")

    def desactivabtnIniciar(self):
        self.btnMotorIniciar.setEnabled(False)
        self.btnMotorIniciar.setStyleSheet("color: gray;\n"
                                              "border-color: gray;\n"
                                              "")

        self.btnMotorDetener.setEnabled(True)
        self.btnMotorDetener.setStyleSheet("color: rgba(125,201,159);\n"
                                              "border-color: rgba(125,201,159);\n"
                                              "")


    #region  ==== REGION Trabajador Motor

    def trabjadorMotorCachaSenalProgreso(self, diccionario):
        try:
            self.progressBar.setValue(diccionario['progressBar'])            
            self.lblMensaje.setText("{}".format(diccionario['lblMensaje']))
            #Indicadores derecha
            self.lblMotorTotalCosto.setText('$ {:0,.0f} total'.format(diccionario['TotalCosto']))
            self.lblMotorViajes.setText(str(diccionario['Viajes']) + " viajes")
            self.lblMotorKm.setText('{:0,.0f} km'.format(diccionario['KmTotales']))
            self.lblMotorSegundos.setText(str(diccionario['tiempoEjecucion']) + " segundos")

            # Indicadores de abajo
            self.lblMotorCostoPorViaje.setText('$ {:0,.2f}/Viaje'.format(diccionario['CostoXViaje']))
            self.lblMotorCostoPorDestino.setText('$ {:0,.2f}/Destino'.format(diccionario['CostoXDestino']))
            self.lblMotorCostoPorPedido.setText('$ {:0,.2f}/Pedido'.format(diccionario['CostoXPedido']))
            self.lblMotorCostoPorKg.setText('$ {:0,.2f}/Kg'.format(diccionario['CostoXKg']))
            self.lblMotorCostoPorPieza.setText('$ {:0,.2f}/Pzas'.format(diccionario['CostoXPieza']))
            self.lblMotorCostoPorM3.setText('$ {:0,.2f}/m3'.format(diccionario['CostoXM3']))
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def trabjadorMotorCachaSenalResultado(self, s):
        try:
            # Estamos analizando qué podríamos utilizar aquí en este método
            #self.label_33.setText('************ trabjadorMotorCachaSenalResultado ' + str(s))
            print("trabjadorMotorCachaSenalResultado se ha ejecutado")
            #self.activabtnIniciar()
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def trabjadorMotorCachaSenalFinalizado(self):
        try:
            print("trabjadorMotorCachaSenalFinalizado se ha ejecutado")
            #self.activabtnIniciar()
            # if self.optaPlanner.guardaSolucion():
            #     print("Recuperando a DataFrame el JSON generado por el motor...")
            #     try:
            #         config.dfPlan = json2df_motor('plan.json')
            #         self.plan.cargar_grid_tablero()
            #         fnPutGrid(config.dfPlan, self.gridElPlan, config.campos_ssPlan)
            #         config.TableroOptimizado = calculate_metrics(config.dfPlan)
            #         fnPutTablero(self.tableroPlan, solo_original=False)
            #         fnPutTablero(self.tableroOptimizaAuto, solo_original=False)
            #         fnPutTablero(self.tableroOptimizaManual, solo_original=False)
            #         self.tabWidget1Principal.setCurrentIndex(2)
            #         self.lblMensaje.setText("Motor finalizado")
            #
            #         # Desactivo botón de "Detener"
            #         self.btnMotorDetener.setEnabled(False)
            #     except Exception as e:
            #         exc_type, exc_obj, exc_tb = sys.exc_info()
            #         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #         error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #         # config.b('**** ERROR **** {}'.format(error))
            #         print("Error al recuperar JSON a DataFrame")
            #         print(error)
            #         QMessageBox.critical(self, "Imposible continuar", "No se pudo cargar la solución guardada",
            #                             QMessageBox.Ok)
            # else:
            #     print("No hay una solución qué guardar")
        except Exception as e:
        # ----------------------------------------------------
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def trabjadorMotorCachaSenalError(self, a):
        try:

            #self.label_40.setText('************ trabjadorMotorCachaSenalError')
            print ('errrroooorrr', a)

            #self.label_40.setText('************ trabjadorMotorCachaSenalError')
            print("trabjadorMotorCachaSenalFinalizado se ha ejecutado")
            #print ('errrroooorrr', a, b, c)
            #self.activabtnIniciar()

        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    pass
    #endregion

    # region  ==== REGION Trabajador Genérico

    def trabajadorGenericoCachaSenalProgreso(self, diccionario):
        try:
            self.progressBar.setValue(diccionario['progressBar'])
            self.lblMensaje.setText('{}'.format(diccionario['lblMensaje']))
            print('Progreso:', diccionario['lblMensaje'])
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Error {}'.format(error))
        return False

    def trabajadorGenericoCachaSenalResultado(self, s):
        try:
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Resultado {}'.format(s))
            print(s)
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Error {}'.format(error))
        return False

    def trabajadorGenericoCachaSenalFinalizado(self):
        try:
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Finalizado')
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Error {}'.format(error))
            return False

    def trabajadorGenericoCachaSenalError(self, a):
        try:
            self.progressBar.setValue(50)
            self.lblMensaje.setText('Error {}'.format(a))
       # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            self.progressBar.setValue(100)
            self.lblMensaje.setText('Error {}'.format(error))
            return False

    # endregion


    # region ==== REGION Slots para trabajadores

    def slot_btnMotorIniciar(self):
        print('slot_btnMotorIniciar')

        try:
            # trabajador
            trabajadorMotor = Trabajador(fnEjecutaTrabajoMotor)
            # señales
            trabajadorMotor.signals.progreso.connect(self.trabjadorMotorCachaSenalProgreso)
            trabajadorMotor.signals.resultado.connect(self.trabjadorMotorCachaSenalResultado)
            trabajadorMotor.signals.finalizado.connect(self.trabjadorMotorCachaSenalFinalizado)
            trabajadorMotor.signals.error.connect(self.trabjadorMotorCachaSenalError)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorMotor.start(trabajadorMotor)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def slot_btnMotorDetener(self):
        print('slot_btnMotorDetener')

        if self.optaPlanner.instanciaEsActiva():
            self.optaPlanner.detenerEjecucion()
            data = self.optaPlanner.guardaSolucion()
            if data["status"]:
                print("Recuperando a DataFrame el JSON generado por el motor...")
                try:
                    config.dfPlan = json2df_motor(data["data"])
                    self.plan.cargar_grid_tablero(tableroOriginal=False)
                    config.TableroOptimizado = calculate_metrics(config.dfPlan)
                    self.tabWidget1Principal.setCurrentIndex(2)
                    self.lblMensaje.setText("Motor finalizado")

                    # Desactivo botón de "Detener"
                    self.activabtnIniciar()
                except Exception as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
                    # config.b('**** ERROR **** {}'.format(error))
                    print("Error al recuperar JSON a DataFrame")
                    print(error)
                    #QMessageBox.critical(self, "Imposible continuar", "No se pudo cargar la solución guardada", QMessageBox.Ok)
                    self.progressBar.setValue(0)
                    self.lblMensaje.setText("No se pudo cargar la solución guardada")
                    self.activabtnIniciar()
            else:
                self.activabtnIniciar()
                #QMessageBox.critical(self, "Imposible continuar", "No hay una solución para guardar", QMessageBox.Ok)
                self.progressBar.setValue(0)
                self.lblMensaje.setText("No se generó ninguna solución")
        else:
            self.activabtnIniciar()
            #QMessageBox.warning(self, "Imposible continuar", "La instancia del motor no se encuentra activa", QMessageBox.Ok)
            self.progressBar.setValue(0)
            self.lblMensaje.setText("La instancia del motor no se encuentra activa")

    def slot_TrabajadorMotor(self):
        try:
            # trabajador
            trabajadorMotor = Trabajador(fnEjecutaTrabajoMotor)
            # señales
            trabajadorMotor.signals.progreso.connect(self.trabjadorMotorCachaSenalProgreso)
            trabajadorMotor.signals.resultado.connect(self.trabjadorMotorCachaSenalResultado)
            trabajadorMotor.signals.finalizado.connect(self.trabjadorMotorCachaSenalFinalizado)
            trabajadorMotor.signals.error.connect(self.trabjadorMotorCachaSenalError)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorMotor.start(trabajadorMotor)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def slot_btnLogin(self):
        print('slot_btnLogin')
        response = authenticate()
        if response["status"]:
            print("Login correcto: "+response["data"]["nombre"])
            config.usuario = response["data"]
            #datos empresa
            folder_cedi = ""
            folder_origen = ""
            folder_resultados = ""
            print(config.usuario)

            # Datos
            self.datos = Datos(self)
            self.datos.cargaDatos()

            # Planes
            self.plan = Planes(self)
            self.plan.cargaPlanesCreados()
            

        else:
            print(response["message"])
            
       

    def slot_btnGenerarPlan(self):
        print('slot_btnGenerarPlan')
        try:
            # trabajador
            trabajadorGenerico = Trabajador(fnEjecutaTrabajoCrearPlan)
            # señales
            trabajadorGenerico.signals.progreso.connect(self.trabajadorGenericoCachaSenalProgreso)
            trabajadorGenerico.signals.resultado.connect(self.trabajadorGenericoCachaSenalResultado)
            trabajadorGenerico.signals.finalizado.connect(self.trabajadorGenericoCachaSenalFinalizado)
            trabajadorGenerico.signals.error.connect(self.trabajadorGenericoCachaSenalError)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorGenerico.start(trabajadorGenerico)
          # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print('Ocurrio un error al generar el plan', error)
            return False


    #
    # def slot_btnGenerarPlan(self):
    #     print('slot_btnGenerarPlan')
    #     nombreDelPlan = config.nombrePlan
    #     self.datos.crearPlan()
    #     # mostrar seleccionado el ss del plan generado
    #     index = self.listAbrePlan.findItems(nombreDelPlan, QtCore.Qt.MatchWrap)
    #     if len(index) > 0:
    #         self.listAbrePlan.setCurrentItem(index[0])

    def slot_btnAbrirPlan(self):
        print('slot_btnAbrirPlan')
        try:
            self.gridAsignar.setColumnCount(0)
            self.gridAsignar.setRowCount(0)
            if config.predespegados == {}:
                self.asignar.listBoxOperadores()
                self.asignar.listBoxOperadorespre()
                self.asignar.listBoxVehiculosPlan()
                self.asignar.listBoxVehiculosPredespegue()
            # trabajador
            # print("Antes check format", config.nombrePlan)
            # check_plan_format(config.nombrePlan)
            trabajadorGenerico = Trabajador(fnEjecutaTrabajoAbrirPlan)
            # señales
            trabajadorGenerico.signals.progreso.connect(self.trabajadorGenericoCachaSenalProgreso)
            trabajadorGenerico.signals.resultado.connect(self.trabajadorGenericoCachaSenalResultado)
            trabajadorGenerico.signals.error.connect(self.trabajadorGenericoCachaSenalError)
            trabajadorGenerico.signals.finalizado.connect(self.trabajadorGenericoCachaSenalFinalizado)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorGenerico.start(trabajadorGenerico)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print('Hubo un error al abrir el plan', error)
            return False

        #limpia

    def slot_listAbrePlanItemActivated(self, a):  # doble click
        print('slot_listAbrePlanItemActivated', a)
        try:
            self.gridAsignar.setColumnCount(0)
            self.gridAsignar.setRowCount(0)
            if config.predespegados == {}:
                self.asignar.listBoxOperadores()
                self.asignar.listBoxOperadorespre()
                self.asignar.listBoxVehiculosPlan()
                self.asignar.listBoxVehiculosPredespegue()
            # trabajador
            trabajadorGenerico = Trabajador(fnEjecutaTrabajoAbrirPlan)
            # señales
            trabajadorGenerico.signals.progreso.connect(self.trabajadorGenericoCachaSenalProgreso)
            trabajadorGenerico.signals.resultado.connect(self.trabajadorGenericoCachaSenalResultado)
            trabajadorGenerico.signals.finalizado.connect(self.trabajadorGenericoCachaSenalFinalizado)
            trabajadorGenerico.signals.error.connect(self.trabajadorGenericoCachaSenalError)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorGenerico.start(trabajadorGenerico)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    # def slot_btnAnadirPedidos(self):
    #     print('slot_btnAnadirPedidos')
    #     anade_pedidos(self.listAnadirPedidos.currentItem().text())
    #
    # def slot_btnBorrarPedidos(self):
    #     print('slot_btnBorrarPedidos')
    #     borra_pedidos(self.listAnadirPedidos.currentItem().text())

    def slot_btnAnadirPredespegados(self):
        print('slot_btnAnadirPredespegados')

    def slot_btnRecalculaManual(self):
        print('slot_btnRecalculaManual')
        dfPlanManual = fnGetGrid(self.gridPlanManual)
        callMotorOp = CallMotorOptimizado()
        dfPlanManual = callMotorOp.callMotor(dfPlanManual, 3)
        fnPutGrid(dfPlanManual, self.gridPlanManual, config.campos_ssOptimizadoManual)


    def slot_btnRecalculaPlan(self):
        print('slot_btnRecalculaPlan')
        config.dfPlan = fnGetGrid(self.gridElPlan)
        callMotorOp = CallMotorOptimizado()
        config.dfPlan = callMotorOp.callMotor(config.dfPlan, 3)
        fnPutGrid(config.dfPlan, self.gridElPlan, config.campos_ssPlan)

        config.TableroOptimizado = calculate_metrics(config.dfPlan)
        fnPutTablero(self.tableroPlan, solo_original=False)

    def slot_btnRecalculaV1(self):
        print('slot_btnRecalculaV1')
        dfGridV1Manual = fnGetGrid(self.gridV1Manual)
        callMotorOp = CallMotorOptimizado()
        dfGridV1Manual = callMotorOp.callMotor(dfGridV1Manual, 3)
        fnPutGrid(dfGridV1Manual, self.gridV1Manual, config.campos_ssPlan)

    def slot_btnRecalculaV2(self):
        print('slot_btnRecalculaV2')
        dfGridV2Manual = fnGetGrid(self.gridV2Manual)
        callMotorOp = CallMotorOptimizado()
        dfGridV2Manual = callMotorOp.callMotor(dfGridV2Manual, 3)
        fnPutGrid(dfGridV2Manual, self.gridV2Manual, config.campos_ssPlan)

    def slot_btnRefreshAsignar(self):
        self.asignar.refresh()
        print('slot_btnRefreshAsignar')

    def slot_btnRefreshAuto(self):
        print('slot_btnRefreshAuto')
        config.dfOptimizadoAuto = config.dfPlan
        # dataframes
        config.dfOptimizadoAuto = None
        # limpia
        fnLimpiaTableros('Opimizado')

    def slot_btnRefreshManual(self):
        print('slot_btnRefreshManual')
        config.dfOptimizadoManual = config.dfPlan
        fnPutGrid(config.dfPlan, self.gridPlanManual, config.campos_ssPlan)
        # copia la estructura de los campos a los grids de los V1 y V2
        # dimension de la tabla y borra antes
        # V1
        self.gridV1Manual.setRowCount(0)
        self.gridV1Manual.setColumnCount(config.dfPlan.shape[1])
        self.gridV1Manual.setHorizontalHeaderLabels(config.campos_ssPlan)
        self.gridV1Manual.resizeColumnsToContents()
        self.gridV1Manual.resizeRowsToContents()
        self.gridV1Manual.setSortingEnabled(True)
        # V2
        self.gridV2Manual.setRowCount(0)
        self.gridV2Manual.setColumnCount(config.dfPlan.shape[1])
        self.gridV2Manual.setHorizontalHeaderLabels(config.campos_ssPlan)
        self.gridV2Manual.resizeColumnsToContents()
        self.gridV2Manual.resizeRowsToContents()
        self.gridV2Manual.setSortingEnabled(True)

    def slot_btnRefreshPlan(self):
        print('slot_btnRefreshPlan')
        fnPutGrid(config.dfPlan, self.gridElPlan, config.campos_ssPlan)

    def slot_btnPropagaAsignar(self):
        print('slot_btnPropagaAsignar')
        fnPropaga(self, df=False, solo_df=False, grid=self.gridAsignar)

    def slot_btnPropagaAuto(self):
        print('slot_btnPropagaAuto')
        fnPropaga(self, df=config.dfOptimizadoAuto, solo_df=True, grid=False)
        # fnPutGrid(config.dfOptimizado, self.gridElPlan, config.campos_ssPlan)
        # fnPutTablero(self.tableroPlan, solo_original=False)
        # fnPutTablero(self.tableroOptimizaManual, solo_original=False)

    def slot_btnPropagaManual(self):
        print('slot_btnPropagaManual')
        fnPropaga(self, df=False, solo_df=False, grid=self.gridPlanManual)

    def slot_btnOptimizaManual(self):
        print('slot_btnOptimizaManual')
        dfPlanManual = fnGetGrid(self.gridPlanManual)
        callMotorOp = CallMotorOptimizado()
        dfPlanManual = callMotorOp.callMotor(dfPlanManual, 2)
        fnPutGrid(dfPlanManual, self.gridPlanManual, config.campos_ssOptimizadoManual)

        fnPutTablero(self.tableroOptimizaManual, solo_original=False)

    def slot_btnOptimizaPlan(self):
        print('slot_btnOptimizaPlan')
        config.dfPlan = fnGetGrid(self.gridElPlan)
        callMotorOp = CallMotorOptimizado()
        config.dfPlan = callMotorOp.callMotor(config.dfPlan, 2)
        fnPutGrid(config.dfPlan, self.gridElPlan, config.campos_ssPlan)

        config.TableroOptimizado = calculate_metrics(config.dfPlan)
        fnPutTablero(self.tableroPlan, solo_original=False)

    def slot_btnOptimizaV1(self):
        print('slot_btnOptimizaV1')
        dfGridV1Manual = fnGetGrid(self.gridV1Manual)
        callMotorOp = CallMotorOptimizado()
        dfGridV1Manual = callMotorOp.callMotor(dfGridV1Manual, 2)
        fnPutGrid(dfGridV1Manual, self.gridV1Manual, config.campos_ssPlan)

    def slot_btnOptimizaV2(self):
        print('slot_btnOptimizaV2')
        dfGridV2Manual = fnGetGrid(self.gridV2Manual)
        callMotorOp = CallMotorOptimizado()
        dfGridV2Manual = callMotorOp.callMotor(dfGridV2Manual, 2)
        fnPutGrid(dfGridV2Manual, self.gridV2Manual, config.campos_ssPlan)

    def slot_btnGuardaAsignar(self):
        # self.asignar.guardaSS()
        fnPropaga(self, df=False, solo_df=False, grid=self.gridAsignar)
        fnGuardaPlan()
        # config.dfPlan = self.res
        print('slot_btnGuardaAsignar')

    def slot_btnGuardaAuto(self):
        print('slot_btnGuardaAuto')
        fnPropaga(self, df=config.dfOptimizadoAuto, solo_df=True, grid=False)
        fnGuardaPlan()

    def slot_btnGuardaManual(self):
        print('slot_btnGuardaManual')
        fnPropaga(self, df=False, solo_df=False, grid=self.gridPlanManual)
        fnGuardaPlan()

    def slot_btnGuardaPlan(self):
        print('slot_btnGuardaPlan')
        fnPropaga(self, df=False, solo_df=False, grid=self.gridElPlan)
        fnGuardaPlan()

    def slot_btnGuardaConfiguracion(self):
        print('slot_btnGuardaConfiguracion')

    def slot_btnAbrirVehAsignar(self):
        self.asignar.leerVehiculos()
        print('slot_btnAbrirVehAsignar')

    def slot_btnAbrirOperadoresAsignar(self):
        self.asignar.leerOperadores(1)
        print('slot_btnAbrirOperadoresAsignar')


    def slot_btnAbrirVehPredespegueAsignar(self):
        self.asignar.leerVehiculosPre(1)
        print('slot_btnAbrirVehPredespegueAsignar')

    def slot_btnCargaPlanAutomatico(self):
        print('slot_btnCargaPlanAutomatico')
        config.dfOptimizadoAuto = fnGetGrid(self.gridElPlan)
        # tablero
        config.TableroOriginal = calculate_metrics(config.dfOptimizadoAuto)
        fnPutTablero(config.TableroOriginal, solo_original=False)
        self.lblMensaje.setText('Tablero Cargado de "config.dfOptimizadoAuto"')


    def slot_btnCargaPlanAbiertoManual(self):
        print('slot_btnCargaPlanAbiertoManual')
        config.dfOptimizadoManual = fnGetGrid(self.gridElPlan)
        fnPutGrid(config.dfOptimizadoManual, self.gridPlanManual, config.campos_ssOptimizadoManual)
        # tablero
        config.TableroOriginal = calculate_metrics(config.dfPlan)
        fnPutTablero(config.TableroOriginal, solo_original=False)

        # copia la estructura de los campos a los grids de los V1 y V2
        # dimension de la tabla y borra antes
        # V1
        self.gridV1Manual.setRowCount(0)
        self.gridV1Manual.setColumnCount(config.dfPlan.shape[1])
        self.gridV1Manual.setHorizontalHeaderLabels(config.campos_ssPlan)
        self.gridV1Manual.resizeColumnsToContents()
        self.gridV1Manual.resizeRowsToContents()
        self.gridV1Manual.setSortingEnabled(True)
        # V2
        self.gridV2Manual.setRowCount(0)
        self.gridV2Manual.setColumnCount(config.dfPlan.shape[1])
        self.gridV2Manual.setHorizontalHeaderLabels(config.campos_ssPlan)
        self.gridV2Manual.resizeColumnsToContents()
        self.gridV2Manual.resizeRowsToContents()
        self.gridV2Manual.setSortingEnabled(True)

    def slot_btnCargaPlanAbiertoAsignar(self):
        print('slot_btnCargaPlanAbiertoAsignar')
        # config.dfAsignar = fnGetGrid(self.gridElPlan)
        fnPutGrid(config.dfPlan, self.gridAsignar, config.campos_ssAsignar)
        self.asignar.leerResultados()

    def slot_btnPredespegaSeleccionados(self):
        print('slot_btnPredespegaSeleccionados')

    def slot_btnPredespegaPlan(self):
        print('slot_btnPredespegaPlan')
        self.predespegue.send2db()

    def slot_btnAbrirOperadoresPredespegueAsignar(self, ):
        self.asignar.leerOperadorespre(1)
        print('slot_btnAbrirOperadoresPredespegueAsignar')

    def slot_btnAsignarActualizaPredespegue(self):
        self.asignar.enviaViajes()
        print('slot_btnAsignarActualizaPredespegue')
        self.despegue.manejador()
        self.predespegue.cargar()

    def slot_btnCargaPlanPredespegue(self):
        print('slot_btnCargaPlanPredespegue')
        self.predespegue.cargar()

    def slot_btnOptimizaViajesAuto(self):
        print('slot_btnOptimizarViajes')
        config.dfPlan = fnGetGrid(self.gridElPlan)
        callMotorOp = CallMotorOptimizado()
        config.dfOptimizadoAuto = callMotorOp.callMotor(config.dfOptimizadoAuto, 3)
        config.TableroOptimizado = calculate_metrics(config.dfOptimizadoAuto)

    # listos con hilos
    def slot_btnOptimizar(self):
        try:
            # trabajador
            trabajadorGenerico = Trabajador(fnEjecutaTrabajoOptimizaAutomatico)
            # señales
            trabajadorGenerico.signals.progreso.connect(self.trabajadorGenericoCachaSenalProgreso)
            trabajadorGenerico.signals.resultado.connect(self.trabajadorGenericoCachaSenalResultado)
            trabajadorGenerico.signals.finalizado.connect(self.trabajadorGenericoCachaSenalFinalizado)
            trabajadorGenerico.signals.error.connect(self.trabajadorGenericoCachaSenalError)
            # Inicia el hilo del trabajador
            self.hiloTrabajadorGenerico.start(trabajadorGenerico)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    pass
    # endregion

    #region ==== REGION Slots
    def slot_editNumViajeManualV1(self, a):
        print('slot_editNumViajeManualV1', a)
        try:
            config.slot_editNumViajeManualV1 = int(a)
        except:
            config.slot_editNumViajeManualV1 = 0


    def slot_editNumViajeManualV2(self, a):
        print('slot_editNumViajeManualV2', a)
        config.slot_editNumViajeManualV2 = a
        try:
            config.slot_editNumViajeManualV2 = int(a)
        except:
            config.slot_editNumViajeManualV2 = 0

    def slot_editTipoVehManualV1(self, a):
        print('slot_editTipoVehManualV1', a)
        config.slot_editTipoVehManualV1 = a

    def slot_editTipoVehManualV2(self, a):
        print('slot_editTipoVehManualV2', a)
        config.slot_editTipoVehManualV2 = a

    def slot_btnCHV1(self):
        print('slot_btnCHV1')

    def slot_btnCHV2(self):
        print('slot_btnCHV2')

    def slot_btnColorDManual(self):
        print('slot_btnColorDManual')
        fnColoreaDestinos(self.gridPlanManual)

    def slot_btnColorDPlan(self):
        print('slot_btnColorDPlan')
        fnColoreaDestinos(self.gridElPlan)

    def slot_btnColorDV1(self):
        print('slot_btnColorDV1')
        fnColoreaDestinos(self.gridV1Manual)

    def slot_btnColorDV2(self):
        print('slot_btnColorDV2')
        fnColoreaDestinos(self.gridV2Manual)

    def slot_btnColorSinManual(self):
        print('slot_btnColorSinManual')
        fnColoreaSinColor(self.gridPlanManual)

    def slot_btnColorSinPlan(self):
        print('slot_btnColorSinPlan')
        fnColoreaSinColor(self.gridElPlan)

    def slot_btnColorSinV1(self):
        print('slot_btnColorSinV1')
        fnColoreaSinColor(self.gridV1Manual)

    def slot_btnColorSinV2(self):
        print('slot_btnColorSinV2')
        fnColoreaSinColor(self.gridV2Manual)

    def slot_btnColorVManual(self):
        print('slot_btnColorVManual')
        fnColoreaViajes(self.gridPlanManual)

    def slot_btnColorVPlan(self):
        print('slot_btnColorVPlan')
        fnColoreaViajes(self.gridElPlan)

    def slot_btnColorVV1(self):
        print('slot_btnColorVV1')
        fnColoreaViajes(self.gridV1Manual)

    def slot_btnColorVV2(self):
        print('slot_btnColorVV2')
        fnColoreaViajes(self.gridV2Manual)

    def slot_btnGV1(self):
        print('slot_GV1')

    def slot_btnGV2(self):
        print('slot_GV2')

    def slot_btnLimpiaV1(self):
        print('slot_btnLimpiaV1')

    def slot_btnLimpiaV2(self):
        print('slot_btnLimpiaV2')

    def slot_btnMV1(self):
        print('slot_MV1')

    def slot_btnMV2(self):
        print('slot_MV2')

    def slot_btnOtroV1(self):
        print('slot_btnOtroV1')
        dfGridV1Manual = fnGetGrid(self.gridV1Manual)
        if dfGridV1Manual.empty:
            print ("Vehículo vacío")
        else:
            verificaMedidas(dfGridV1Manual, 1, self)

    def slot_btnOtroV2(self):
        print('slot_btnOtroV2')
        dfGridV2Manual = fnGetGrid(self.gridV2Manual)
        if dfGridV2Manual.empty:
            print ("Vehículo vacío")
        else:
            verificaMedidas(dfGridV2Manual, 2, self)

    def slot_btnRegresaV1(self):
        print('slot_btnRegresaV1')

    def slot_btnRegresaV2(self):
        print('slot_btnRegresaV2')

    def slot_btnXGV1(self):
        print('slot_btnXGV1')

    def slot_btnXGV2(self):
        print('slot_btnXGV2')

    def slot_despegar_btnDespegar(self):
        print('slot_despegar_btnDespegar')
        desp = Despegue(self)
        desp.despegamelo()

    def slot_editMaxKgManualV1(self, a):
        print('slot_editMaxKgManualV1', a)
        try:
            config.slot_editMaxKgManualV1 = float(a)
        except:
            config.slot_editMaxKgManualV1 = 0

    def slot_editMaxKgManualV2(self, a):
        print('slot_editMaxKgManualV2', a)
        try:
            config.slot_editMaxKgManualV2 = float(a)
        except:
            config.slot_editMaxKgManualV2 = 0

    def slot_editMaxM3ManualV1(self, a):
        print('slot_editMaxM3ManualV1', a)
        try:
            config.slot_editMaxM3ManualV1 = float(a)
        except:
            config.slot_editMaxM3ManualV1 = 0

    def slot_editMaxM3ManualV2(self, a):
        print('slot_editMaxM3ManualV2', a)
        try:
            config.slot_editMaxM3ManualV2 = float(a)
        except:
            config.slot_editMaxM3ManualV2 = 0

    def slot_editMaxValorManualV1(self, a):
        print('slot_editMaxValorManualV1', a)
        try:
            config.slot_editMaxValorManualV1 = float(a)
        except:
            config.slot_editMaxValorManualV1 = 0

    def slot_editMaxValorManualV2(self, a):
        print('slot_editMaxValorManualV2', a)
        try:
            config.slot_editMaxValorManualV2 = float(a)
        except:
            config.slot_editMaxValorManualV2 = 0

    def slotGridtablaPlanClicked(self):
        print('slotGridtablaPlanClicked')

    def slotGridtablaV1Clicked(self):
        print('slotGridtablaV1Clicked')

    def slotGridtablaV2Clicked(self):
        print('slotGridtablaV2Clicked')

    def slot_checkIgnoraAcceso(self, a):
        print('slot_checkIgnoraAcceso', a)
        config.slot_checkIgnoraAcceso = a

    def slot_checkIgnoraComp(self, a):
        print('slot_checkIgnoraComp', a)
        config.slot_checkIgnoraComp = a

    def slot_comboOperadoresAsignar(self, a):
        print('slot_comboOperadoresAsignar', a)
        config.slot_comboOperadoresAsignar = a

    def slot_comboOperadoresAsignarPredespegue(self, a):
        print('slot_comboOperadoresAsignarPredespegue', a)
        config.slot_comboOperadoresAsignarPredespegue = a

    def slot_comboVehiculosAsignar(self, a):
        print('slot_comboVehiculosAsignar', a)
        config.slot_comboVehiculosAsignar = a

    def slot_comboVehiculosAsignarPredespegue(self, a):
        print('slot_comboVehiculosAsignarPredespegue', a)
        config.slot_comboVehiculosAsignarPredespegue = a

    def slot_comboNombrePlan(self, a):
        print('slot_comboNombrePlan', a)
        config.nombrePlan = a
        print('config.nombrePlan', a)

    def slot_editCEDI(self, a):
        print('slot_editCEDI', a)
        config.slot_editCEDI = a

    def slot_editUsuario(self, a):
        print('slot_editUsuario', a)
        config.slot_editUsuario = a

    def slot_editClave(self, a):
        print('slot_editClave', a)
        config.slot_editClave = a

    def slot_listPedidos(self, a):
        print('slot_listPedidos', a)
        config.slot_listPedidos = a
        # mostrar seleccionado el ss del plan generado
        b = a.replace('PEDIDOS', 'DESTINOS')
        index = self.listGenerarPlanDestinos.findItems(b, QtCore.Qt.MatchWrap)
        if len(index) > 0: self.listGenerarPlanDestinos.setCurrentItem(index[0])
        if config.datosPedidos.shape[0] != 0: config.idPedido = (config.datosPedidos[config.datosPedidos['name'] == a].id).iloc[0]


    def slot_listVeh(self, a):
        print('slot_listVeh', a)
        config.slot_listVeh = a
        if config.datosVehiculos.shape[0] != 0: config.idVehiculo = (config.datosVehiculos[config.datosVehiculos['name'] == a].id).iloc[0]

    def slot_listOperadores(self, a):
        print('slot_listOperadores', a)
        config.slot_listOperadores = a
        if config.datosOperadores.shape[0] != 0: config.idOperador = (config.datosOperadores[config.datosOperadores['name'] == a].id).iloc[0]

    def slot_listDestinos(self, a):
        print('slot_listDestinos', a)
        config.slot_listDestinos = a
        b = a.replace('DESTINOS', 'PEDIDOS')
        index = self.listGenerarPlanPedidos.findItems(b, QtCore.Qt.MatchWrap)
        if len(index) > 0: self.listGenerarPlanPedidos.setCurrentItem(index[0])
        if config.datosDestinos.shape[0] != 0: config.idDestino = (config.datosDestinos[config.datosDestinos['name'] == a].id).iloc[0]

    def slot_listPlanes(self, a):
        print('slot_listPlanes', a)
        config.nombrePlan = a
        config.slot_listPlanes = a
        if config.datosPlanes.shape[0] != 0 and a != '': config.idPlan = (config.datosPlanes[config.datosPlanes['name'] == a].id).iloc[0]

    def slot_listCompatibilidad(self, a):
        print('slot_listCompatibilidad', a)
        config.slot_listCompatibilidad = a
        if config.datosCompatibilidad.shape[0] != 0: config.idCompatibilidad = (config.datosCompatibilidad[config.datosCompatibilidad['name'] == a].id).iloc[0]


    def slot_listTarifario(self, a):
        print('slot_listTarifario', a)
        config.slot_listTarifario = a
        if config.datosTarifario.shape[0] != 0: config.idTarifario = (config.datosTarifario[config.datosTarifario['name'] == a].id).iloc[0]


    def slot_listPreferencias(self, a):
        print('slot_listPreferencias', a)
        config.slot_listPreferencias = a
        if config.datosPreferencias.shape[0] != 0: config.idPreferencia = (config.datosPreferencias[config.datosPreferencias['name'] == a].id).iloc[0]


    def slot_listPedidosAnadir(self, a):
        print('slot_listPedidosAnadir', a)
        config.slot_listPedidosAnadir = a

    def slot_listPredespegadosAnadir(self, a):
        print('slot_listPredespegadosAnadir', a)
        config.slot_listPredespegadosAnadir = a

    def slot_btnAnadirPedidos(self):
        print('slot_btnAnadirPedidos')
        anade_pedidos(self.listAnadirPedidos.currentItem().text())

    def slot_btnBorraPedidos(self):
        print('slot_btnBorrarPedidos')
        borra_pedidos(self.listAnadirPedidos.currentItem().text())

    # def slot_btnAnadirPredespegados(self):
    #     print('slot_btnAnadirPredespegados')

    # def slot_btnMotorIniciar(self):
    #     print('slot_btnMotorIniciar')
    #
    #     #verificar que todos arcos de los destinos esten calculados
    #     distanceService = DistanceService(window,'SILODISA','SILODISA')
    #     if distanceService.validate():
    #         if self.optaPlanner.instanciaEsActiva():
    #             if self.optaPlanner.generaJson():
    #                 self.optaPlanner.ejecutarPlan()
    #
    #                 # Inicia timer para obtener solución
    #                 timer = threading.Thread(target=self.optaPlanner.obtieneSolucion)
    #                 timer.start()
    #
    #             else:
    #                 print("No se pudo generar el plan para procesar")
    #                 QMessageBox.warning(self, "Imposible continuar", "No se pudo generar el plan para procesar",
    #                                     QMessageBox.Ok)
    #         else:
    #             print("La instancia del motor no se encuentra activa")
    #             QMessageBox.warning(self, "Imposible continuar", "La instancia del motor no se encuentra activa",
    #                                 QMessageBox.Ok)
    #     else:
    #         print("correr nuevamente")

    # def slot_btnMotorDetener(self):
    #     print('slot_btnMotorDetener')
    #
    #     if self.optaPlanner.instanciaEsActiva():
    #         self.optaPlanner.detenerEjecucion()
    #         if self.optaPlanner.guardaSolucion():
    #             print("Recuperando a DataFrame el JSON generado por el motor...")
    #             try:
    #                 config.dfPlan = json2df_motor('plan.json')
    #                 self.plan.cargar_grid_tablero()
    #                 fnPutGrid(config.dfPlan, self.gridElPlan, config.campos_ssPlan)
    #                 config.TableroOptimizado = calculate_metrics(config.dfPlan)
    #                 fnPutTablero(self.tableroPlan, solo_original=False)
    #                 fnPutTablero(self.tableroOptimizaAuto, solo_original=False)
    #                 fnPutTablero(self.tableroOptimizaManual, solo_original=False)
    #                 self.tabWidget1Principal.setCurrentIndex(3)
    #             except Exception as e:
    #                 exc_type, exc_obj, exc_tb = sys.exc_info()
    #                 fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    #                 error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
    #                 # config.b('**** ERROR **** {}'.format(error))
    #                 print("Error al recuperar JSON a DataFrame")
    #                 print(error)
    #                 QMessageBox.critical(self, "Imposible continuar", "No se pudo cargar la solución guardada",
    #                                     QMessageBox.Ok)
    #         else:
    #             print("No hay una solución qué guardar")
    #     else:
    #         QMessageBox.warning(self, "Imposible continuar", "La instancia del motor no se encuentra activa",
    #                             QMessageBox.Ok)

    # def slot_btnOptimizaViajesAuto(self):
    #     print('slot_btnOptimizarViajes')
    #     config.dfPlan = fnGetGrid(self.gridElPlan)
    #     callMotorOp = CallMotorOptimizado()
    #     config.dfOptimizado = callMotorOp.callMotor(config.dfOptimizado, 3)
    #     config.TableroOptimizado = calculate_metrics(config.dfOptimizado)

    def slot_editOcupacion(self, a):
        print('slot_editOcupacion', a)
        config.slot_editOcupacion = int(a)
        try:
            config.slot_editOcupacion = float(a)
        except:
            config.slot_editOcupacion = 0

    def slot_editCercania(self, a):
        print('slot_editCercania', a)
        try:
            config.slot_editCercania =  int(a)
        except:
            config.slot_editCercania = 0

    def slot_editAumentarVentana(self, a):
        print('slot_editAumentarVentana', a)
        try:
            config.slot_editAumentarVentana =  float(a)
        except:
            config.slot_editAumentarVentana = 0

    def slot_editAumentarValor(self, a):
        print('slot_editAumentarValor', a)
        try:
            config.slot_editAumentarValor =  float(a)
        except:
            config.slot_editAumentarValor = 0

    def slot_editAumentarVol(self, a):
        print('slot_editAumentarVol', a)
        try:
            config.slot_editAumentarVol =  1 + int(a)/100
        except:
            config.slot_editAumentarVol = 0

    def slot_editAumentarKg(self, a):
        print('slot_editAumentarKg', a)
        try:
            config.slot_editAumentarKg =  1 + int(a)/100
        except:
            config.slot_editAumentarKg = 0

    def slot_radioOptimizaVacios(self, a):
        print('slot_radioOptimizaVacios', a)
        config.radioOptimiza = 'Vacíos'

    def slot_radioOptimizaCercanos(self, a):
        print('slot_radioOptimizaCercanos', a)
        config.radioOptimiza = 'Cercanos'

    def slot_comboVehAsignar(self, a):
        print('slot_comboVehAsignar', a)
        config.slot_comboVehAsignar = a

    def slot_comboVehAsignarPredespegue(self, a):
        print('slot_comboVehAsignarPredespegue', a)
        config.slot_comboVehAsignarPredespegue = a

    def slot_btnDespegar(self):
        print('slot_btnDespegar')

        self.despegue.despegamelo()

    def slot_gridClickedConfiguracion(self, a):
        print('slot_gridClickedConfiguracion', a)

    def slot_gridDblClickedConfiguracion(self, a):
        print('slot_gridDblClickedConfiguracion', a)

    def slot_editNombrePlanGenera(self, a):
      pass

    def slot_gridConfiguracionCurrentCellChanged(self, a, b, c, d):
        print('slot_gridConfiguracionCurrentCellChanged', a, b, c, d)

    def slot_gridElPlanCurrentCellChanged(self, a, b, c, d):
        print('slot_gridElPlanCurrentCellChanged', a, b, c, d)

    def slot_gridCalibracionCurrentCellChanged(self, a, b, c, d):
        print('slot_gridCalibracionCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarPlanCurrentCellChanged(self, a, b, c, d):
        self.asignar.clickRes(a, b, c, d)
        print('slot_gridAsignarPlanCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarPredespegueCurrentCellChanged(self, a, b, c, d):
        self.asignar.selection_changed(a, b, c, d)
        print('slot_gridAsignarPredespegueCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarVehPlanCurrentCellChanged(self, a, b, c, d):
        self.asignar.clickNave(a,b,c,d)
        print('slot_gridAsignarVehPlanCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarOperadoresPlanCurrentCellChanged(self, a, b, c, d):
        self.asignar.clickOpe(a, b, c, d)
        print('slot_gridAsignarOperadoresPlanCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarVehPredespegueCurrentCellChanged(self, a, b, c, d):
        self.asignar.clickNavepre(a,b,c,d)
        print('slot_gridAsignarVehPredespegueCurrentCellChanged', a, b, c, d)

    def slot_gridAsignarOperadoresPredespegueCurrentCellChanged(self, a, b, c, d):
        self.asignar.clickOpepre(a,b,c,d)
        print('slot_gridAsignarOperadoresPredespegueCurrentCellChanged', a, b, c, d)

    def slot_gridPredespegarCurrentCellChanged(self, a, b, c, d):
        print('slot_gridPredespegarCurrentCellChanged', a, b, c, d)
        self.predespegue.selection_changed_resultados()

    def slot_gridDespegarCurrentCellChanged(self, a, b, c, d):
        print('slot_gridDespegarCurrentCellChanged', a, b, c, d)
        self.despegue.selection_changed_resultados()

    def slot_gridDespegadosCurrentCellChanged(self, a, b, c, d):
        print('slot_gridDespegdosCurrentCellChanged', a, b, c, d)

    def slot_tabWidgetPrincipalCurrentChange(self, a):
        print('slot_tabWidgetPrincipalCurrentChange', a)

        if a == self.tabWidget1Principal.indexOf(self.tab1Analizar):
             print("tab dashboards")
             addtabsdashboardsfun(self)
        # if a == 6:
        #     self.despegue.manejador()

    def slot_btnCargarPredespegados(self):
        print('slot_btnCargarPredespegados')
        self.despegue.cargarPredespegados()

    def slot_btnCargarDespegados(self):
        print('slot_btnCargarDespegados')
        self.despegue.cargarDespegados()

    def slot_btnAsignarCargarPredespegue(self):
        self.asignar.cargaTablaPredespegue()
        self.asignar.leeResultadosDb()
        print('slot_btnAsignarCargarPredespegue')

    def slot_btnGuardarConfiguracion(self):
        print('slot_btnGuardarConfiguracion')

    def slot_editNombrePlanGeneral(self, a):
        print('slot_editNombrePlanGeneral')
        config.nombrePlan = 'ARMS RESULTADOS ' + a
        config.nombreGenerarPlan = 'ARMS RESULTADOS ' + a
        print('config.nombrePlan', a)


    def slot_btnAutomaticaAsignar(self):
        print('slot_btnAutomaticaAsignar')
        self.asignar.AsignadorAutomatico()




if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    config.mainWindow = window
    window.show()
    sys.exit(app.exec_())
