import pandas as pd


# --------------------------------------------------------
mainWindow = None
progress_callback = None
# --------------------------------------------------------
aa = False
# inputs de ui
empresa = 1

folder_cedi = ""
folder_origen = ""
folder_resultados = ""

lista_ss = []
#dataframes de listas de los ss
datosPedidos = pd.DataFrame([])
datosVehiculos = pd.DataFrame([])
datosOperadores = pd.DataFrame([])
datosDestinos = pd.DataFrame([])
datosTarifario = pd.DataFrame([])
datosCompatibilidad = pd.DataFrame([])
datosPreferencias = pd.DataFrame([])
datosPlanes = pd.DataFrame([])

#ids de los ss seleccionados
idPedido=''
idDestino = ''
idVehiculo = ''
idOperador = ''
idTarifario = ''
idCompatibilidad = ''
idPreferencia = ''
idPlan = ''


tiempo_final = 0
tirada = 0

#combos
nombrePlan = ''
nombreGenerarPlan = ''
slot_comboOperadoresAsignar = None
slot_comboOperadoresAsignarPredespegue= None
slot_comboVehiculosAsignar= None
slot_comboVehiculosAsignarPredespegue= None
slot_comboVehAsignar= None
slot_comboVehAsignarPredespegue= None
#lists
slot_listPedidos= None
slot_listVeh= None
slot_listOperadores= None
slot_listDestinos= None
slot_listPlanes= None
slot_listAbrePlanItemActivated= None
slot_listCompatibilidad= None
slot_listTarifario= None
slot_listPreferencias= None
slot_listPedidosAnadir= None
slot_listPredespegadosAnadir= None
#edits
slot_editMaxKgManualV1= None
slot_editMaxKgManualV2= None
slot_editMaxM3ManualV1= None
slot_editMaxM3ManualV2= None
slot_editMaxValorManualV1= None
slot_editMaxValorManualV2= None

slot_editCEDI= "SILODISA"
slot_editUsuario= "avazquez@arete.ws"
slot_editPassword = "avazquez"

slot_editClave= None
slot_editOcupacion= 90
slot_editCercania= 50
slot_editAumentarVentana= 15
slot_editAumentarValor= 0
slot_editAumentarVol= 5
slot_editAumentarKg= 5

slot_editNumViajeManualV1 = None
slot_editNumViajeManualV2 = None
slot_editTipoVehManualV1 = None
slot_editTipoVehManualV2 = None
#radios
radioOptimiza = 'Vacíos'
#checkboxes
slot_checkIgnoraAcceso = False
slot_checkIgnoraComp = False
# --------------------------------------------------------
# Key pressed
Key_Shift = False
Key_Alt = False
Key_Control = False

# para el stausbar
progressBar = None
labelUser = None
labelEmpresa = None
labelCEDI = None
lblMensaje = None

# datos cachados textchanged()
numViajeV1 = 0
numViajeV2 = 0
tipoVehV1 = ''
tipoVehV2 = ''
maxKgV1 = 0
maxKgV2 = 0
maxM3V1 = 0
maxM3V2 = 0
maxValorV1 = 0
maxValorV2 = 0

xxx = 0
optimiza = ''
cont = 0
distancia_al_destino = 0
tiempo_google_al_destino = 0
contadorReubicados = 0
plan_abiierto_focus = 'planaer'
gcPlan = None
# SS
gc = None
sheet = None

# tablerito
valorMax_plan = 0.0
pesoMax_plan = 0.0
volumenMax_plan = 0.0

valorMax_v1 = 0.0
pesoMax_v1 = 0.0
volumenMax_v1 = 0.0

valorMax_v2 = 0.0
pesoMax_v2 = 0.0
volumenMax_v2 = 0.0



txtArcos = 0

TableroOriginal = 0
TableroOptimizado = 0
P = None
B = None # bitacora
bitacora = []
b = ''
bAbierto = False

#Login Data
session_started = False
usuario = dict()
predespegados = {}
#Plantilla de resultados
ss_resultados_template = "1NKK2xymqI1qg1-l_ciZNaih0vFyEPiLBbgCqkQR8EAg"

pedidos = {}
vehiculos = {}
operadores = {}
resultados = []
destinos = {}
preferencias = {}
tarifario = {}
compatibilidad = {}


dfPlan = 0
dfPredespegue = 0
dfDestinos = 0
dfVehiculos = 0
dfCedi = 0
dfConfiguracion = 0
dfCompatibilidad = 0
dfTarifas = 0
dfOperadores = 0
dfAnadirPedidos = 0
dfOptimizadoAuto = None
dfPredespegue = 0
dfAsignador = 0
dfOptimizadoManual = 0

# diccionarios
dictPlan = {}
dict = {}

#arcos
abiertoArcos = False

# dashboards
dashboards_cargados = 0
AcUrls = []

# listado de campos
campos_ssOriginal2 = [
 ['Pedido', 'Pedido','{}', 'AlignC', 'ColorDefault'],
 ['DestinoTR1','Destino','{}', 'AlignL', 'ColorDefault'],
 ['FechaEntregaPedido','Entrega','{}', 'AlignC', 'ColorDefault'],
 ['FechaSalidaPedido','Salida','{}', 'AlignC', 'ColorDefault'],
 ['TipoPedido','Tipo','{}', 'AlignL', 'ColorDefault'],
 ['TiemDescarga','Descarga','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['Volumen','Vol.','{:0,.3f}', 'AlignC', 'ColorDefault'],
 ['Peso','Peso','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['Piezas','Piezas','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['Valor','$','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['VentanaFechaInicioPedido','Ventana Ini.','{}', 'AlignC', 'ColorDefault'],
 ['VentanaHoraInicioPedido','Ventana Hr.Ini','{}', 'AlignC', 'ColorDefault'],
 ['VentanaFechaFinPedido','Ventana Fin','{}', 'AlignC', 'ColorDefault'],
 ['VentanaHoraFinPedido','Ventana Hr.Fin','{}', 'AlignC', 'ColorDefault'],
 ['TiemServicio','Tiempo Servicio','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['EntregaRecoleccion','Entrega/Recolecc','{}', 'AlignC', 'ColorDefault'],
 ['Productos','Productos','{}', 'AlignL', 'ColorDefault'],
 ['RecolectaCEDI2','Recolecta CEDI2','{}', 'AlignC', 'ColorDefault'],
 ['DestinoTR2','TR2','{}', 'AlignL', 'ColorDefault'],
 ['FechaEntregaTR2','Entrega TR2','{}', 'AlignC', 'ColorDefault'],
 ['RestriccionVolumen','Restricc.Vol.','{:0,.3f}', 'AlignC', 'ColorDefault'],
 ['Plan','Plan','{}', 'AlignL', 'ColorDefault'],
 ['Empresa','Empresa','{}', 'AlignL', 'ColorDefault'],
 ['PrioridadVehiculo','Prioridad Veh.','{}', 'AlignC', 'ColorDefault'],
 ['Viaje','Viaje','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['Tirada','Tirada','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['Latitud','Latitud','{:0,.9f}', 'AlignC', 'ColorDefault'],
 ['Longitud','Longitud','{:0,.9f}', 'AlignC', 'ColorDefault'],
 ['TipoVehiculo','Veh.','{}', 'AlignL', 'ColorDefault'],
 ['Refrigerado','Refrigerado','{}', 'AlignC', 'ColorDefault'],
 ['VolumenPermitido','Vol.Permitido','{:0,.3f}', 'AlignC', 'ColorDefault'],
 ['PesoPermitido','Peso Permitido','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['VolumenMax','Vol. Max','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['PesoMax','Peso Max','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['ValorMax','Valor Max','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['OcupacionVolumen','Ocup.Vol.','{:0,.1f}', 'AlignR', 'ColorDefault'],
 ['OcupacionPeso','Ocup.Peso','{:0,.1f}', 'AlignR', 'ColorDefault'],
 ['Dedicado','Dedicado','{}', 'AlignC', 'ColorDefault'],
 ['Cita','Cita','{}', 'AlignC', 'ColorDefault'],
 ['FechaSalida','Salida','{}', 'AlignC', 'ColorDefault'],
 ['FechaRetorno','Retorno','{}', 'AlignC', 'ColorDefault'],
 ['DuracionViaje','Duración Viaje','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['Km','Km','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['Co2','Co2','{:0,.2f}', 'AlignR', 'ColorDefault'],
 ['LitrosDiesel','Litros','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CEDI','CEDI','{}', 'AlignC', 'ColorDefault'],
 ['NumComidas','Num.Comidas','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['NumHoteles','Num.Hoteles','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['NumDescansos','Num.Descansos','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoProveedorFlete','$ Flete','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoCombustible','$ Combustible','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoHotel','$ Hotel','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoComidas','$ Comidas','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoThermo','$ Thermo','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoCasetas','$ Casetas','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoFerry','$ Ferry','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoOtros','$ Otros','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['CostoTotal','$ Total','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['NumComidasRetorno','Num.Comidas Retorno','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['NumHotelesRetorno','Num.Hotel Retorno','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['NumDescansosRetorno','Num.Descansos Retorno','{:0,.0f}', 'AlignR', 'ColorDefault'],
 ['Operador','Operador','{}', 'AlignL', 'ColorDefault'],
 ['Placas','Placas','{}', 'AlignC', 'ColorDefault'],
 ['ProveedorFlete','Proveedor','{}', 'AlignL', 'ColorDefault'],
 ['FechaEjecucionMotor','Ejecución Motor','{}', 'AlignC', 'ColorDefault'],
 ['UsuarioEjecucionMotor','Usuario Eje.Motor','{}', 'AlignL', 'ColorDefault'],
 ['FechaEjecucionOpti','Ejecución Optimiza','{}', 'AlignC', 'ColorDefault'],
 ['UsuarioEjecucionOpti','Usuario Eje.Optimiza','{}', 'AlignL', 'ColorDefault'],
 ['FechaEjecucionAsigna','Ejecución Asigna','{}', 'AlignC', 'ColorDefault'],
 ['UsuarioEjecucionAsigna','Usuario Eje.Asigna','{}', 'AlignL', 'ColorDefault'],
 ['ModificadoVacios','Modificado Vacíos','{:0,.0f}', 'AlignC', 'ColorDefault'],
 ['ModificadoCercanos','Modificado Cercanos','{:0,.0f}', 'AlignC', 'ColorDefault']
]

campos_ssOriginal = ['Pedido',
'DestinoTR1',
'FechaEntregaPedido',
'FechaSalidaPedido',
'TipoPedido',
'TiemDescarga',
'Volumen',
'Peso',
'Piezas',
'Valor',
'VentanaFechaInicioPedido',
'VentanaHoraInicioPedido',
'VentanaFechaFinPedido',
'VentanaHoraFinPedido',
'TiemServicio',
'EntregaRecoleccion',
'Productos',
'RecolectaCEDI2',
'DestinoTR2',
'FechaEntregaTR2',
'RestriccionVolumen',
'Plan',
'Empresa',
'PrioridadVehiculo',
'Viaje',
'Tirada',
'Latitud',
'Longitud',
'TipoVehiculo',
'Refrigerado',
'VolumenPermitido',
'PesoPermitido',
'VolumenMax',
'PesoMax',
'ValorMax',
'OcupacionVolumen',
'OcupacionPeso',
'Dedicado',
'Cita',
'FechaSalida',
'FechaRetorno',
'DuracionViaje',
'Km',
'Co2',
'LitrosDiesel',
'CEDI',
'NumComidas',
'NumHoteles',
'NumDescansos',
'CostoProveedorFlete',
'CostoCombustible',
'CostoHotel',
'CostoComidas',
'CostoThermo',
'CostoCasetas',
'CostoFerry',
'CostoOtros',
'CostoTotal',
'NumComidasRetorno',
'NumHotelesRetorno',
'NumDescansosRetorno',
'Operador',
'Placas',
'ProveedorFlete',
'FechaEjecucionMotor',
'UsuarioEjecucionMotor',
'FechaEjecucionOpti',
'UsuarioEjecucionOpti',
'FechaEjecucionAsigna',
'UsuarioEjecucionAsigna',
'ModificadoVacios',
'ModificadoCercanos']

campos_ssPlan = ['Pedido',
 'DestinoTR1',
 'Viaje',
 'Tirada',
 'OcupacionVolumen',
 'OcupacionPeso',
 'FechaEntregaPedido',
 'FechaSalidaPedido',
 'TipoPedido',
 'TiemDescarga',
 'Volumen',
 'Peso',
 'Piezas',
 'Valor',
 'VentanaFechaInicioPedido',
 'VentanaHoraInicioPedido',
 'VentanaFechaFinPedido',
 'VentanaHoraFinPedido',
 'TiemServicio',
 'EntregaRecoleccion',
 'Productos',
 'RecolectaCEDI2',
 'DestinoTR2',
 'FechaEntregaTR2',
 'RestriccionVolumen',
 'Plan',
 'Empresa',
 'PrioridadVehiculo',
 'Latitud',
 'Longitud',
 'TipoVehiculo',
 'Refrigerado',
 'VolumenPermitido',
 'PesoPermitido',
 'VolumenMax',
 'PesoMax',
 'ValorMax',
 'Dedicado',
 'Cita',
 'FechaSalida',
 'FechaRetorno',
 'DuracionViaje',
 'Km',
 'Co2',
 'LitrosDiesel',
 'CEDI',
 'NumComidas',
 'NumHoteles',
 'NumDescansos',
 'CostoProveedorFlete',
 'CostoCombustible',
 'CostoHotel',
 'CostoComidas',
 'CostoThermo',
 'CostoCasetas',
 'CostoFerry',
 'CostoOtros',
 'CostoTotal',
 'NumComidasRetorno',
 'NumHotelesRetorno',
 'NumDescansosRetorno',
 'Operador',
 'Placas',
 'ProveedorFlete',
 'FechaEjecucionMotor',
 'UsuarioEjecucionMotor',
 'FechaEjecucionOpti',
 'UsuarioEjecucionOpti',
 'FechaEjecucionAsigna',
 'UsuarioEjecucionAsigna',
 'ModificadoVacios',
 'ModificadoCercanos']

campos_ssOptimizadoAuto = ['Pedido',
 'DestinoTR1',
 'Viaje',
 'Tirada',
 'OcupacionVolumen',
 'OcupacionPeso',
 'FechaEntregaPedido',
 'FechaSalidaPedido',
 'TipoPedido',
 'TiemDescarga',
 'Volumen',
 'Peso',
 'Piezas',
 'Valor',
 'VentanaFechaInicioPedido',
 'VentanaHoraInicioPedido',
 'VentanaFechaFinPedido',
 'VentanaHoraFinPedido',
 'TiemServicio',
 'EntregaRecoleccion',
 'Productos',
 'RecolectaCEDI2',
 'DestinoTR2',
 'FechaEntregaTR2',
 'RestriccionVolumen',
 'Plan',
 'Empresa',
 'PrioridadVehiculo',
 'Latitud',
 'Longitud',
 'TipoVehiculo',
 'Refrigerado',
 'VolumenPermitido',
 'PesoPermitido',
 'VolumenMax',
 'PesoMax',
 'ValorMax',
 'Dedicado',
 'Cita',
 'FechaSalida',
 'FechaRetorno',
 'DuracionViaje',
 'Km',
 'Co2',
 'LitrosDiesel',
 'CEDI',
 'NumComidas',
 'NumHoteles',
 'NumDescansos',
 'CostoProveedorFlete',
 'CostoCombustible',
 'CostoHotel',
 'CostoComidas',
 'CostoThermo',
 'CostoCasetas',
 'CostoFerry',
 'CostoOtros',
 'CostoTotal',
 'NumComidasRetorno',
 'NumHotelesRetorno',
 'NumDescansosRetorno',
 'Operador',
 'Placas',
 'ProveedorFlete',
 'FechaEjecucionMotor',
 'UsuarioEjecucionMotor',
 'FechaEjecucionOpti',
 'UsuarioEjecucionOpti',
 'FechaEjecucionAsigna',
 'UsuarioEjecucionAsigna',
 'ModificadoVacios',
 'ModificadoCercanos']
campos_ssOptimizadoManual = ['Pedido',
 'DestinoTR1',
 'Viaje',
 'Tirada',
 'OcupacionVolumen',
 'OcupacionPeso',
 'FechaEntregaPedido',
 'FechaSalidaPedido',
 'TipoPedido',
 'TiemDescarga',
 'Volumen',
 'Peso',
 'Piezas',
 'Valor',
 'VentanaFechaInicioPedido',
 'VentanaHoraInicioPedido',
 'VentanaFechaFinPedido',
 'VentanaHoraFinPedido',
 'TiemServicio',
 'EntregaRecoleccion',
 'Productos',
 'RecolectaCEDI2',
 'DestinoTR2',
 'FechaEntregaTR2',
 'RestriccionVolumen',
 'Plan',
 'Empresa',
 'PrioridadVehiculo',
 'Latitud',
 'Longitud',
 'TipoVehiculo',
 'Refrigerado',
 'VolumenPermitido',
 'PesoPermitido',
 'VolumenMax',
 'PesoMax',
 'ValorMax',
 'Dedicado',
 'Cita',
 'FechaSalida',
 'FechaRetorno',
 'DuracionViaje',
 'Km',
 'Co2',
 'LitrosDiesel',
 'CEDI',
 'NumComidas',
 'NumHoteles',
 'NumDescansos',
 'CostoProveedorFlete',
 'CostoCombustible',
 'CostoHotel',
 'CostoComidas',
 'CostoThermo',
 'CostoCasetas',
 'CostoFerry',
 'CostoOtros',
 'CostoTotal',
 'NumComidasRetorno',
 'NumHotelesRetorno',
 'NumDescansosRetorno',
 'Operador',
 'Placas',
 'ProveedorFlete',
 'FechaEjecucionMotor',
 'UsuarioEjecucionMotor',
 'FechaEjecucionOpti',
 'UsuarioEjecucionOpti',
 'FechaEjecucionAsigna',
 'UsuarioEjecucionAsigna',
 'ModificadoVacios',
 'ModificadoCercanos']

campos_ssAsignarVehiculos = ['Placas',
                             'Economico',
                             'ProveedorFlete',
                             'PesoPermitido',
                             'VolumenPermitido',
                             'Rendimiento',
                             'HuellaCarbono',
                             'TipoVehiculo',
                             'Talla',
                             'Ejes',
                             'PesoMax',
                             'VolumenMax',
                             'Combustible',
                             'Refrigerado',
                             'PrioridadVehiculo',
                             'Licencia',
                             'Disponibilidad']
campos_ssAsignarOperadores = ['Operador', 'Correo', 'ProveedorFlete', 'Placas', 'Licencia']

campos_ssAsignar = ['Pedido',
 'DestinoTR1',
 'Operador',
 'Placas',
 'FechaEntregaPedido',
 'FechaSalidaPedido',
 'TipoPedido',
 'TiemDescarga',
 'Volumen',
 'Peso',
 'Piezas',
 'Valor',
 'VentanaFechaInicioPedido',
 'VentanaHoraInicioPedido',
 'VentanaFechaFinPedido',
 'VentanaHoraFinPedido',
 'TiemServicio',
 'EntregaRecoleccion',
 'Productos',
 'RecolectaCEDI2',
 'DestinoTR2',
 'FechaEntregaTR2',
 'RestriccionVolumen',
 'Plan',
 'Empresa',
 'PrioridadVehiculo',
 'Viaje',
 'Tirada',
 'Latitud',
 'Longitud',
 'TipoVehiculo',
 'Refrigerado',
 'VolumenPermitido',
 'PesoPermitido',
 'VolumenMax',
 'PesoMax',
 'ValorMax',
 'OcupacionVolumen',
 'OcupacionPeso',
 'Dedicado',
 'Cita',
 'FechaSalida',
 'FechaRetorno',
 'DuracionViaje',
 'Km',
 'Co2',
 'LitrosDiesel',
 'CEDI',
 'NumComidas',
 'NumHoteles',
 'NumDescansos',
 'CostoProveedorFlete',
 'CostoCombustible',
 'CostoHotel',
 'CostoComidas',
 'CostoThermo',
 'CostoCasetas',
 'CostoFerry',
 'CostoOtros',
 'CostoTotal',
 'NumComidasRetorno',
 'NumHotelesRetorno',
 'NumDescansosRetorno',
 'ProveedorFlete',
 'FechaEjecucionMotor',
 'UsuarioEjecucionMotor',
 'FechaEjecucionOpti',
 'UsuarioEjecucionOpti',
 'FechaEjecucionAsigna',
 'UsuarioEjecucionAsigna',
 'ModificadoVacios',
 'ModificadoCercanos']
