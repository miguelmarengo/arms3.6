/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget1Principal;
    QWidget *tab1Iniciar;
    QGridLayout *gridLayout_42;
    QTabWidget *tab2Iniciar;
    QWidget *tab2IniciarLogin;
    QGridLayout *gridLayout_41;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame_46;
    QGridLayout *gridLayout_16;
    QLineEdit *editClave;
    QLineEdit *editCEDI;
    QSpacerItem *verticalSpacer_2;
    QLineEdit *editUsuario;
    QPushButton *btnLogin;
    QLabel *label;
    QSpacerItem *horizontalSpacer_6;
    QWidget *tab2IniciarConfig;
    QGridLayout *gridLayout_12;
    QTableWidget *gridConfiguracion;
    QPushButton *btnConfiguracion;
    QWidget *tab2IniciarARMS;
    QGridLayout *gridLayout_48;
    QLabel *labelAcercaARMS;
    QSpacerItem *horizontalSpacer_14;
    QSpacerItem *horizontalSpacer_15;
    QLabel *logo_footer_2;
    QWidget *tab1ElPlan;
    QGridLayout *gridLayout_68;
    QTableWidget *tableroPlan;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_65;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_39;
    QLabel *lblTableroElPlanHeaderM3;
    QLabel *lblTableroElPlanHeaderKg;
    QLabel *lblTableroElPlanHeaderCostoViaje;
    QLabel *lblTableroElPlanHeaderPiezas;
    QLabel *lblTableroElPlanHeaderPedidos;
    QLabel *lblTableroElPlanHeaderCostoDestino;
    QLabel *lblTableroElPlanHeaderCostoPiezas;
    QLabel *lblTableroElPlanHeaderDestinos;
    QLabel *lblTableroElPlanHeaderViajes;
    QLabel *lblTableroElPlanHeaderCostoTotal;
    QLabel *lblTableroElPlanHeaderKm;
    QLabel *lblTableroElPlanHeaderCostoM3;
    QLabel *lblTableroElPlanHeaderCostoPedido;
    QLabel *lblTableroElPlanHeaderCostoKg;
    QTableWidget *gridElPlan;
    QFrame *frame;
    QGridLayout *gridLayout_3;
    QPushButton *btnRecalculaPlan;
    QPushButton *btnOptimizaPlan;
    QPushButton *btnGuardaPlan;
    QPushButton *btnColorDPlan;
    QPushButton *btnColorSinPlan;
    QComboBox *comboNombrePlan;
    QPushButton *btnRefrescaPlan;
    QPushButton *btnColorVPlan;
    QWidget *tab1Planear;
    QGridLayout *gridLayout_29;
    QTabWidget *tab2Planear;
    QWidget *tab2PlanearCrear;
    QGridLayout *gridLayout_50;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_23;
    QGroupBox *groupBoxCrearPlan_Pedidos;
    QGridLayout *gridLayout_9;
    QListWidget *listGenerarPlanPedidos;
    QGroupBox *groupBox_12;
    QGridLayout *gridLayout_25;
    QListWidget *listGenerarPlanVehiculos;
    QGroupBox *groupBox_13;
    QGridLayout *gridLayout_32;
    QListWidget *listGenerarPlanOperadores;
    QPushButton *btnGeneraPlan;
    QLineEdit *editNombrePlanGenera;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox_19;
    QGridLayout *gridLayout_63;
    QListWidget *listGenerarPlanPreferencias_2;
    QGroupBox *groupBox_15;
    QGridLayout *gridLayout_26;
    QListWidget *listGenerarPlanDestinos;
    QGroupBox *groupBox_17;
    QGridLayout *gridLayout_30;
    QListWidget *listGenerarPlanTarifario;
    QGroupBox *groupBox_18;
    QGridLayout *gridLayout_43;
    QListWidget *listGenerarPlanComp;
    QWidget *tab2PlanearAbrir;
    QGridLayout *gridLayout_7;
    QGroupBox *groupBox_25;
    QGridLayout *gridLayout_21;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnAbrePlan;
    QListWidget *listAbrePlan;
    QWidget *tab2PlanearAnadir;
    QGridLayout *gridLayout_52;
    QGroupBox *groupBox_20;
    QGridLayout *gridLayout_20;
    QPushButton *btnBorrarPedidos;
    QPushButton *btnAnadirPedidos;
    QSpacerItem *horizontalSpacer_8;
    QListWidget *listAnadirPedidos;
    QGroupBox *groupBox_21;
    QGridLayout *gridLayout_22;
    QPushButton *btnAnadirPredespegados;
    QSpacerItem *horizontalSpacer_7;
    QListWidget *listAnadirPredespegados;
    QWidget *tab1Rutear;
    QGridLayout *gridLayout_51;
    QTabWidget *tab2Rutear;
    QWidget *tab2RutearMotor;
    QGridLayout *gridLayout_18;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lblMotorPedidos;
    QLabel *lblMotorDestinos;
    QLabel *lblMotorKg;
    QLabel *lblMotorPiezas;
    QLabel *lblMotorM3;
    QGraphicsView *graphicsViewMotor;
    QWidget *widgetGrafica;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lblMotorCostoPorViaje;
    QLabel *lblMotorCostoPorDestino;
    QLabel *lblMotorCostoPorPedido;
    QLabel *lblMotorCostoPorPieza;
    QLabel *lblMotorCostoPorKg;
    QLabel *lblMotorCostoPorM3;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lblMotorViajes;
    QLabel *lblMotorTotalCosto;
    QLabel *lblMotorSegundos;
    QLabel *lblMotorKm;
    QPushButton *btnMotorIniciar;
    QPushButton *btnMotorDetener;
    QWidget *tab2RutearCalibracion;
    QGridLayout *gridLayout_10;
    QTableWidget *gridCalibrarMotor;
    QWidget *tab1Optimizar;
    QGridLayout *gridLayout_19;
    QTabWidget *tab2Optimizar;
    QWidget *tab2OptimizarAuto;
    QGridLayout *gridLayout_13;
    QSpacerItem *horizontalSpacer_13;
    QGroupBox *groupBox_36;
    QGridLayout *gridLayout_6;
    QRadioButton *radioOptimizaCercanos;
    QLineEdit *editAumentarKg;
    QRadioButton *radioOptimizaVacios;
    QLabel *label_16;
    QLineEdit *editAumentarVol;
    QLabel *label_15;
    QLineEdit *editAumentarVentana;
    QLabel *label_13;
    QLineEdit *editAumentarValor;
    QLabel *label_9;
    QLineEdit *editOcupacion;
    QLabel *label_11;
    QLineEdit *editCercania;
    QLabel *label_7;
    QPushButton *btnCargaPlanOptiAuto;
    QPushButton *btnOptimizarVaciosCercanos;
    QCheckBox *checkIgnorarComp;
    QSpacerItem *verticalSpacer;
    QCheckBox *checkIgnorarAcceso;
    QSpacerItem *horizontalSpacer_18;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_69;
    QLabel *lblTableroAutoHeaderM3;
    QLabel *lblTableroAutoHeaderKg;
    QLabel *lblTableroAutoHeaderCostoViaje;
    QLabel *lblTableroAutoHeaderPiezas;
    QLabel *lblTableroAutoHeaderPedidos;
    QLabel *lblTableroAutoHeaderCostoDestino;
    QLabel *lblTableroAutoHeaderCostoPieza;
    QLabel *lblTableroAutoHeaderDestinos;
    QLabel *lblTableroAutoHeaderViajes;
    QLabel *lblTableroAutoHeaderTotalCosto;
    QLabel *lblTableroAutoHeaderKm;
    QLabel *lblTableroAutoHeaderCostoM3;
    QLabel *lblTableroAutoHeaderCostoPedido;
    QLabel *lblTableroAutoHeaderCostoKg;
    QTableWidget *tableroAuto;
    QFrame *frame_3;
    QGridLayout *gridLayout_53;
    QPushButton *btnOptimizaPlan_2;
    QPushButton *btnPropagaAuto;
    QPushButton *btnRefrescaAuto;
    QPushButton *btnGuardaAuto;
    QWidget *tab2OptimizarManual;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_5;
    QGroupBox *groupBox_28;
    QGridLayout *gridLayout_73;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_72;
    QLabel *lblTableroManualHeaderCostoM3;
    QLabel *lblTableroManualHeaderCostoKg;
    QLabel *lblTableroManualHeaderTotalCosto;
    QLabel *lblTableroManualHeaderKm;
    QLabel *lblTableroManualHeaderCostoViaje;
    QLabel *lblTableroManualHeaderCostoPieza;
    QFrame *frame_2;
    QGridLayout *gridLayout_31;
    QPushButton *btnRefrescaManual;
    QPushButton *btnGuardaManual;
    QPushButton *btnColorVManual;
    QPushButton *btnColorDManual;
    QPushButton *btnColorSinManual;
    QPushButton *btnPropagaManual;
    QPushButton *btnOptimizaManual;
    QPushButton *btnRecalculaManual;
    QPushButton *pushButton_2;
    QGridLayout *layoutPlan;
    QGroupBox *groupBox_26;
    QGridLayout *gridLayout_70;
    QGridLayout *layoutV1;
    QFrame *frame_8;
    QGridLayout *gridLayout_14;
    QLabel *lblV1Acceso;
    QLabel *lblV1M3;
    QLabel *lblV1Kg;
    QLabel *lblV1Hrs;
    QLabel *lblV1Comp;
    QLabel *lblV1Valor;
    QFrame *frame_7;
    QGridLayout *gridLayout_2;
    QLabel *label_17;
    QLabel *label_14;
    QLabel *label_12;
    QLabel *label_10;
    QLabel *label_8;
    QLineEdit *editV1MaxKg;
    QLineEdit *editV1MaxM3;
    QLineEdit *editV1MaxValor;
    QLineEdit *editV1Tipo;
    QLineEdit *editV1NumViaje;
    QFrame *frame_5;
    QGridLayout *gridLayout_17;
    QPushButton *btnV1XG;
    QPushButton *btnV1ColorV;
    QPushButton *btnV1Optimiza;
    QPushButton *btnV1Recalcula;
    QPushButton *btnV1ColorD;
    QPushButton *btnV1CH;
    QPushButton *btnV1M;
    QPushButton *btnV1Otro;
    QPushButton *btnV1G;
    QPushButton *btnV1ColorSin;
    QPushButton *btnV1Regresa;
    QGroupBox *groupBox_27;
    QGridLayout *gridLayout_71;
    QGridLayout *layoutV2;
    QFrame *frame_10;
    QGridLayout *gridLayout_54;
    QLabel *lblV2M3;
    QLabel *lblV2Valor;
    QLabel *lblV2Kg;
    QLabel *lblV2Comp;
    QLabel *lblV2Acceso;
    QLabel *lblV2Hrs;
    QFrame *frame_9;
    QGridLayout *gridLayout_47;
    QLabel *label_19;
    QLabel *label_28;
    QLabel *label_27;
    QLabel *label_20;
    QLabel *label_21;
    QLineEdit *editV2MaxKg;
    QLineEdit *editV2MaxM3;
    QLineEdit *editV2MaxValor;
    QLineEdit *editV2Tipo;
    QLineEdit *editV2NumViaje;
    QFrame *frame_6;
    QGridLayout *gridLayout_15;
    QPushButton *btnV2XG;
    QPushButton *btnV2Otro;
    QPushButton *btnV2M;
    QPushButton *btnV2CH;
    QPushButton *btnV2ColorSin;
    QPushButton *btnV2ColorV;
    QPushButton *btnV2ColorD;
    QPushButton *btnV2Recalcula;
    QPushButton *btnV2Optimiza;
    QPushButton *btnV2Regresa;
    QPushButton *btnV2G;
    QWidget *tab2OptimizarBitacoras;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBoxCrearPlan_Pedidos_2;
    QGridLayout *gridLayout_59;
    QTableWidget *gridBitacoraVacios;
    QGroupBox *groupBoxCrearPlan_Pedidos_3;
    QGridLayout *gridLayout_56;
    QTableWidget *gridBitacoraCercanos;
    QWidget *tab1Asignar;
    QGridLayout *gridLayout_5;
    QTabWidget *tab2Asignar;
    QWidget *tab2AsignarPlan;
    QGridLayout *gridLayout_64;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QTableWidget *gridAsignar;
    QPushButton *pushButton;
    QPushButton *btnRefreshAsignar;
    QPushButton *btnGuardaAsignar;
    QPushButton *btnPropagaAsignar;
    QPushButton *btnAutomaticoAsignar;
    QGroupBox *groupBox_32;
    QGridLayout *gridLayout_8;
    QComboBox *comboOperadoresAsignarPlan;
    QPushButton *btnAbreOperadoresAsignarPlan;
    QTableWidget *gridAsignarOperadores;
    QGroupBox *groupBox_31;
    QGridLayout *gridLayout_24;
    QComboBox *comboVehAsignarPlan;
    QPushButton *btnAbreVehAsignarPlan;
    QTableWidget *gridAsignarVeh;
    QWidget *tab2AsignarPredespegue;
    QGridLayout *gridLayout_66;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_11;
    QSpacerItem *horizontalSpacer_21;
    QPushButton *btnAsignarCargarPredespegue;
    QTableWidget *gridAsignarPredespegue;
    QPushButton *pushButton_3;
    QGroupBox *groupBox_37;
    QGridLayout *gridLayout_35;
    QComboBox *comboOperadoresAsignarPredespegue;
    QPushButton *btnAbreOperadoresAsignarPredespegue;
    QTableWidget *gridAsignarOperadoresPredespegue;
    QGroupBox *groupBox_35;
    QGridLayout *gridLayout_36;
    QComboBox *comboVehAsignarPredespegue;
    QPushButton *btnAbreVehAsignarPredespegue;
    QTableWidget *gridAsignarVehPredespegue;
    QWidget *tab1Despegar;
    QGridLayout *gridLayout_33;
    QTabWidget *tab2Despegar;
    QWidget *tab2DespegarPredespegue;
    QGridLayout *gridLayout_27;
    QPushButton *btnPredespegarPlan;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btnPredespegarSelecc;
    QTableWidget *gridPredespegar;
    QPushButton *btnCargaPlanPredespegar;
    QSpacerItem *horizontalSpacer_12;
    QWidget *tab2DespegarDespegar;
    QGridLayout *gridLayout_37;
    QGroupBox *groupBox_33;
    QGridLayout *gridLayout_28;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *btnDespegarCargaPredespegados;
    QTableWidget *tableWidget_despegar_predespegue;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btnDespegar;
    QLabel *labelDespeguePIN;
    QGroupBox *groupBox_34;
    QGridLayout *gridLayout_34;
    QSpacerItem *horizontalSpacer_16;
    QPushButton *btnDespegarCargaDespegados;
    QTableWidget *tableWidget_despegar_despegados;
    QWidget *tab1Analizar;
    QGridLayout *gridLayout_46;
    QTabWidget *tab2Analizar;
    QWidget *tab2Analizar1;
    QGridLayout *gridLayout_45;
    QFrame *frame_analizar_1;
    QWidget *tab2Analizar2;
    QGridLayout *gridLayout_40;
    QFrame *frame_analizar_2;
    QWidget *tab2Analizar3;
    QGridLayout *gridLayout_44;
    QFrame *frame_analizar_3;
    QWidget *tab2Analizar4;
    QGridLayout *gridLayout_49;
    QFrame *frame_analizar_4;
    QWidget *tab2Analizar5;
    QGridLayout *gridLayout_57;
    QFrame *frame_analizar_5;
    QWidget *tab2Analizar6;
    QGridLayout *gridLayout_58;
    QFrame *frame_analizar_6;
    QWidget *tab2Analizar7;
    QGridLayout *gridLayout_67;
    QFrame *frame_analizar_7;
    QWidget *tab2Analizar8;
    QGridLayout *gridLayout_60;
    QFrame *frame_analizar_8;
    QWidget *tab2Analizar9;
    QGridLayout *gridLayout_61;
    QFrame *frame_analizar_9;
    QLabel *logo_footer;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1900, 847);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("QWidget#centralWidget {\n"
"border-image: url(:/IMG5/3fondo.png) 0 0 0 0 stretch stretch;}\n"
"\n"
"QProgressBar{\n"
"border: 0px;\n"
"border-radius: 5px;\n"
"text-align: center;}\n"
"\n"
"QProgressBar::chunk {\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border:none;\n"
"width: 5px;\n"
"height: 5px;}\n"
"\n"
"QGroupBox{\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border: none;\n"
"border-radius: 12px;\n"
"padding:6px;}\n"
"\n"
"QLabel{\n"
"color: rgba(190, 219, 189, 255);\n"
"background:transparent;\n"
"border: none;\n"
"font: 12pt \"Arial Narrow\";\n"
"border-radius: 12px;\n"
"padding:2px;}\n"
"\n"
"QTableView {\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border: none;\n"
"border-radius: 12px;\n"
"font: 10pt \"Arial Narrow\";\n"
"gridline-color: rgba(190, 219, 189,33);}\n"
"\n"
"QTableView::item:focus {\n"
"color: rgb(135, 171, 141);\n"
"background: white;\n"
"border: none;\n"
"width: 25px;\n"
"height: 25px;}\n"
"\n"
"QTableView::item:selected, QTableView:"
                        ":item:selected:!active {\n"
"color: rgb(173, 197, 113);\n"
"background: white;\n"
"border: none;\n"
"width: 25px;\n"
"height: 25px;}\n"
"\n"
"QTableView QTableCornerButton::section {\n"
"background:transparent;\n"
"border: none;\n"
"}\n"
"\n"
"QTableView QTableCornerButton {\n"
"background:transparent;\n"
"border: none;\n"
"}\n"
"\n"
"QHeaderView::section{\n"
"color: rgba(190, 219, 189,255);;\n"
"background:transparent;\n"
"border: 1px dotted rgba(190, 219, 189, 255);\n"
"text-align: center;\n"
"font: 9pt \"Arial Narrow\";\n"
"height: 22px;\n"
"width: 40px;}\n"
"\n"
"\n"
"QTabBar::tab {\n"
"background-color: transparent;\n"
"color: rgb(135, 171, 141);\n"
"padding: 2px;\n"
"font: 13pt \"Roboto\";\n"
"width:150px;\n"
"height:17;\n"
"}\n"
"\n"
"QTabWidget::pane { /* The tab widget frame */\n"
"background-color: transparent;}\n"
"\n"
"QTabBar::tab::selected\n"
"{\n"
"color: white;\n"
"}\n"
"\n"
"QPushButton{\n"
"color:  rgb(190, 219, 189);\n"
"background:rgba(190, 219, 189, 75);\n"
"border: 1px solid rgba(190, 219"
                        ", 189, 33);\n"
"border-radius: 12px;\n"
"width: 200px;\n"
"height: 40px;}\n"
"\n"
"\n"
"QPushButton:pressed{\n"
"color: white;\n"
"background:rgba(190, 219, 189, 88);\n"
"border: 1px solid rgba(190, 219, 189, 33);\n"
"border-radius: 12px;\n"
"width: 200px;\n"
"height: 40px;}\n"
"\n"
"\n"
"QLineEdit{\n"
"color:  rgb(190, 219, 189);\n"
"background:rgba(190, 219, 189, 33);\n"
"border: none;\n"
"border-radius: 12px;\n"
"width: 200px;\n"
"height: 30px;}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"image: url(:/IMG5/checkbox_check.png);}\n"
"QCheckBox::indicator:unchecked {\n"
"image: url(:/IMG5/checkbox_uncheck.png);}\n"
"\n"
"\n"
"QRadioButton::indicator::checked {\n"
"image: url(:/IMG5/radiobutton_check.png);}\n"
"QRadioButton::indicator::unchecked {\n"
"image: url(:/IMG5/radiobutton_uncheck.png);}\n"
"\n"
"QListWidget{\n"
"color: white;\n"
"background:transparent;\n"
"border: none;\n"
"border-radius: 12px;}\n"
"\n"
"\n"
"QListWidget::item:selected {\n"
"color: rgb(190, 219, 189);\n"
"background:transparent;\n"
"b"
                        "order: none;\n"
"}\n"
"\n"
"QComboBox{\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border: none;\n"
"border-radius: 12px;\n"
"width: 200px;\n"
"height: 30px;}\n"
"\n"
"QComboBox:on\n"
"{\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border:none;\n"
"border-radius: 12px;\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"border: none;\n"
"border-radius: 12px;}\n"
"\n"
"QComboBox::down-arrow {\n"
"image: url(:/IMG5/3dropdown.png);\n"
"}\n"
"QComboBox::down-arrow:on {\n"
"top: 1px;\n"
"left: 1px;\n"
"}\n"
"QComboBox::item:selected, QTableView::item:selected:!active {\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"}\n"
"QComboBox::item {\n"
"color: white;\n"
"background:rgba(190, 219, 189, 33);\n"
"}\n"
"\n"
"\n"
"\n"
"QScrollBar:horizontal {\n"
"border: 2px rgba(190, 219, 189, 100);\n"
"background:rgba(190, 219, 189, 33);\n"
"height: 15px;\n"
"}\n"
"QScrollBar::handle:horizontal {\n"
"background:rgba(190, 219, 189, 1"
                        "11);\n"
"min-width: 20px;\n"
"}\n"
"QScrollBar:vertical {\n"
"border: 0px solid rgb(68, 92, 53);\n"
"background:rgba(190, 219, 189, 33);\n"
"width: 15px;\n"
"}\n"
"QScrollBar::handle:vertical {\n"
"background:rgba(190, 219, 189, 111);\n"
"min-height: 20px;\n"
"}\n"
"\n"
"\n"
"QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {\n"
"width: 3px;\n"
"height: 3px;\n"
"background:rgba(190, 219, 189, 33);\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {\n"
"background: none;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {\n"
"border: 0px solid rgb(68, 92, 53);\n"
"width: 3px;\n"
"height: 3px;\n"
"background:rgba(190, 219, 189, 33);\n"
"}\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {\n"
"background: none;\n"
"}\n"
""));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget1Principal = new QTabWidget(centralWidget);
        tabWidget1Principal->setObjectName(QStringLiteral("tabWidget1Principal"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget1Principal->sizePolicy().hasHeightForWidth());
        tabWidget1Principal->setSizePolicy(sizePolicy);
        tabWidget1Principal->setMaximumSize(QSize(1920, 1200));
        tabWidget1Principal->setLayoutDirection(Qt::LeftToRight);
        tabWidget1Principal->setStyleSheet(QStringLiteral("background:transparent;"));
        tabWidget1Principal->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        tabWidget1Principal->setTabPosition(QTabWidget::North);
        tabWidget1Principal->setIconSize(QSize(64, 64));
        tabWidget1Principal->setElideMode(Qt::ElideRight);
        tabWidget1Principal->setUsesScrollButtons(false);
        tab1Iniciar = new QWidget();
        tab1Iniciar->setObjectName(QStringLiteral("tab1Iniciar"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tab1Iniciar->sizePolicy().hasHeightForWidth());
        tab1Iniciar->setSizePolicy(sizePolicy1);
        tab1Iniciar->setMaximumSize(QSize(12345, 12345));
        gridLayout_42 = new QGridLayout(tab1Iniciar);
        gridLayout_42->setSpacing(6);
        gridLayout_42->setContentsMargins(11, 11, 11, 11);
        gridLayout_42->setObjectName(QStringLiteral("gridLayout_42"));
        tab2Iniciar = new QTabWidget(tab1Iniciar);
        tab2Iniciar->setObjectName(QStringLiteral("tab2Iniciar"));
        sizePolicy1.setHeightForWidth(tab2Iniciar->sizePolicy().hasHeightForWidth());
        tab2Iniciar->setSizePolicy(sizePolicy1);
        tab2Iniciar->setTabPosition(QTabWidget::North);
        tab2Iniciar->setIconSize(QSize(22, 22));
        tab2IniciarLogin = new QWidget();
        tab2IniciarLogin->setObjectName(QStringLiteral("tab2IniciarLogin"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tab2IniciarLogin->sizePolicy().hasHeightForWidth());
        tab2IniciarLogin->setSizePolicy(sizePolicy2);
        gridLayout_41 = new QGridLayout(tab2IniciarLogin);
        gridLayout_41->setSpacing(6);
        gridLayout_41->setContentsMargins(11, 11, 11, 11);
        gridLayout_41->setObjectName(QStringLiteral("gridLayout_41"));
        horizontalSpacer_4 = new QSpacerItem(727, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_41->addItem(horizontalSpacer_4, 0, 0, 1, 1);

        frame_46 = new QFrame(tab2IniciarLogin);
        frame_46->setObjectName(QStringLiteral("frame_46"));
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(frame_46->sizePolicy().hasHeightForWidth());
        frame_46->setSizePolicy(sizePolicy3);
        frame_46->setMinimumSize(QSize(0, 0));
        frame_46->setMaximumSize(QSize(520, 600));
        frame_46->setFrameShape(QFrame::NoFrame);
        frame_46->setFrameShadow(QFrame::Raised);
        gridLayout_16 = new QGridLayout(frame_46);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        editClave = new QLineEdit(frame_46);
        editClave->setObjectName(QStringLiteral("editClave"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(editClave->sizePolicy().hasHeightForWidth());
        editClave->setSizePolicy(sizePolicy4);
        editClave->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editClave->setEchoMode(QLineEdit::Password);

        gridLayout_16->addWidget(editClave, 5, 0, 1, 1);

        editCEDI = new QLineEdit(frame_46);
        editCEDI->setObjectName(QStringLiteral("editCEDI"));
        sizePolicy4.setHeightForWidth(editCEDI->sizePolicy().hasHeightForWidth());
        editCEDI->setSizePolicy(sizePolicy4);
        editCEDI->setMinimumSize(QSize(0, 0));
        editCEDI->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));

        gridLayout_16->addWidget(editCEDI, 2, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_16->addItem(verticalSpacer_2, 1, 0, 1, 1);

        editUsuario = new QLineEdit(frame_46);
        editUsuario->setObjectName(QStringLiteral("editUsuario"));
        sizePolicy4.setHeightForWidth(editUsuario->sizePolicy().hasHeightForWidth());
        editUsuario->setSizePolicy(sizePolicy4);
        editUsuario->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));

        gridLayout_16->addWidget(editUsuario, 4, 0, 1, 1);

        btnLogin = new QPushButton(frame_46);
        btnLogin->setObjectName(QStringLiteral("btnLogin"));
        sizePolicy1.setHeightForWidth(btnLogin->sizePolicy().hasHeightForWidth());
        btnLogin->setSizePolicy(sizePolicy1);
        btnLogin->setMaximumSize(QSize(12345, 40));
        QPalette palette;
        QBrush brush(QColor(190, 219, 189, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 0, 0, 0));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnLogin->setPalette(palette);
        btnLogin->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));
        btnLogin->setCheckable(true);

        gridLayout_16->addWidget(btnLogin, 6, 0, 1, 1);

        label = new QLabel(frame_46);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setStyleSheet(QStringLiteral("background:transparent;"));
        label->setPixmap(QPixmap(QString::fromUtf8(":/IMG5/3logo_iniciosesion.png")));

        gridLayout_16->addWidget(label, 0, 0, 1, 1);


        gridLayout_41->addWidget(frame_46, 0, 1, 2, 1);

        horizontalSpacer_6 = new QSpacerItem(726, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_41->addItem(horizontalSpacer_6, 1, 2, 1, 1);

        tab2Iniciar->addTab(tab2IniciarLogin, QString());
        tab2IniciarConfig = new QWidget();
        tab2IniciarConfig->setObjectName(QStringLiteral("tab2IniciarConfig"));
        sizePolicy1.setHeightForWidth(tab2IniciarConfig->sizePolicy().hasHeightForWidth());
        tab2IniciarConfig->setSizePolicy(sizePolicy1);
        gridLayout_12 = new QGridLayout(tab2IniciarConfig);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        gridConfiguracion = new QTableWidget(tab2IniciarConfig);
        if (gridConfiguracion->columnCount() < 25)
            gridConfiguracion->setColumnCount(25);
        if (gridConfiguracion->rowCount() < 50)
            gridConfiguracion->setRowCount(50);
        gridConfiguracion->setObjectName(QStringLiteral("gridConfiguracion"));
        sizePolicy1.setHeightForWidth(gridConfiguracion->sizePolicy().hasHeightForWidth());
        gridConfiguracion->setSizePolicy(sizePolicy1);
        QFont font;
        font.setFamily(QStringLiteral("Arial Narrow"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        gridConfiguracion->setFont(font);
        gridConfiguracion->setMouseTracking(true);
        gridConfiguracion->setAcceptDrops(true);
        gridConfiguracion->setToolTipDuration(2);
        gridConfiguracion->setFrameShape(QFrame::NoFrame);
        gridConfiguracion->setLineWidth(0);
        gridConfiguracion->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridConfiguracion->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridConfiguracion->setDragEnabled(true);
        gridConfiguracion->setDragDropMode(QAbstractItemView::DragDrop);
        gridConfiguracion->setDefaultDropAction(Qt::MoveAction);
        gridConfiguracion->setAlternatingRowColors(false);
        gridConfiguracion->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridConfiguracion->setGridStyle(Qt::DotLine);
        gridConfiguracion->setSortingEnabled(true);
        gridConfiguracion->setRowCount(50);
        gridConfiguracion->setColumnCount(25);
        gridConfiguracion->verticalHeader()->setDefaultSectionSize(22);
        gridConfiguracion->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_12->addWidget(gridConfiguracion, 0, 0, 1, 2);

        btnConfiguracion = new QPushButton(tab2IniciarConfig);
        btnConfiguracion->setObjectName(QStringLiteral("btnConfiguracion"));
        sizePolicy3.setHeightForWidth(btnConfiguracion->sizePolicy().hasHeightForWidth());
        btnConfiguracion->setSizePolicy(sizePolicy3);
        btnConfiguracion->setMaximumSize(QSize(12345, 40));
        btnConfiguracion->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_12->addWidget(btnConfiguracion, 1, 1, 1, 1);

        tab2Iniciar->addTab(tab2IniciarConfig, QString());
        tab2IniciarARMS = new QWidget();
        tab2IniciarARMS->setObjectName(QStringLiteral("tab2IniciarARMS"));
        sizePolicy1.setHeightForWidth(tab2IniciarARMS->sizePolicy().hasHeightForWidth());
        tab2IniciarARMS->setSizePolicy(sizePolicy1);
        gridLayout_48 = new QGridLayout(tab2IniciarARMS);
        gridLayout_48->setSpacing(6);
        gridLayout_48->setContentsMargins(11, 11, 11, 11);
        gridLayout_48->setObjectName(QStringLiteral("gridLayout_48"));
        labelAcercaARMS = new QLabel(tab2IniciarARMS);
        labelAcercaARMS->setObjectName(QStringLiteral("labelAcercaARMS"));
        sizePolicy3.setHeightForWidth(labelAcercaARMS->sizePolicy().hasHeightForWidth());
        labelAcercaARMS->setSizePolicy(sizePolicy3);
        QFont font1;
        font1.setFamily(QStringLiteral("Arial Narrow"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        labelAcercaARMS->setFont(font1);
        labelAcercaARMS->setAlignment(Qt::AlignCenter);

        gridLayout_48->addWidget(labelAcercaARMS, 0, 0, 1, 1);

        horizontalSpacer_14 = new QSpacerItem(957, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        gridLayout_48->addItem(horizontalSpacer_14, 0, 1, 2, 1);

        horizontalSpacer_15 = new QSpacerItem(956, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        gridLayout_48->addItem(horizontalSpacer_15, 0, 3, 2, 1);

        logo_footer_2 = new QLabel(tab2IniciarARMS);
        logo_footer_2->setObjectName(QStringLiteral("logo_footer_2"));
        sizePolicy2.setHeightForWidth(logo_footer_2->sizePolicy().hasHeightForWidth());
        logo_footer_2->setSizePolicy(sizePolicy2);
        logo_footer_2->setMinimumSize(QSize(420, 420));
        logo_footer_2->setMaximumSize(QSize(420, 420));
        logo_footer_2->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        logo_footer_2->setPixmap(QPixmap(QString::fromUtf8(":/IMG5/3logo_iniciosesion.png")));
        logo_footer_2->setScaledContents(true);
        logo_footer_2->setAlignment(Qt::AlignCenter);

        gridLayout_48->addWidget(logo_footer_2, 1, 2, 1, 1);

        tab2Iniciar->addTab(tab2IniciarARMS, QString());

        gridLayout_42->addWidget(tab2Iniciar, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Iniciar, QString());
        tab1ElPlan = new QWidget();
        tab1ElPlan->setObjectName(QStringLiteral("tab1ElPlan"));
        sizePolicy1.setHeightForWidth(tab1ElPlan->sizePolicy().hasHeightForWidth());
        tab1ElPlan->setSizePolicy(sizePolicy1);
        gridLayout_68 = new QGridLayout(tab1ElPlan);
        gridLayout_68->setSpacing(6);
        gridLayout_68->setContentsMargins(11, 11, 11, 11);
        gridLayout_68->setObjectName(QStringLiteral("gridLayout_68"));
        tableroPlan = new QTableWidget(tab1ElPlan);
        if (tableroPlan->columnCount() < 31)
            tableroPlan->setColumnCount(31);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        __qtablewidgetitem3->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        __qtablewidgetitem4->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        __qtablewidgetitem5->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        __qtablewidgetitem6->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        __qtablewidgetitem7->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        __qtablewidgetitem8->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        __qtablewidgetitem9->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        __qtablewidgetitem10->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        __qtablewidgetitem11->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(11, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        __qtablewidgetitem12->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(12, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        __qtablewidgetitem13->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(13, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        __qtablewidgetitem14->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(14, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        __qtablewidgetitem15->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(15, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        __qtablewidgetitem16->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(16, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        __qtablewidgetitem17->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(17, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        __qtablewidgetitem18->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(18, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        __qtablewidgetitem19->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(19, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        __qtablewidgetitem20->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(20, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        __qtablewidgetitem21->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(21, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        __qtablewidgetitem22->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(22, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        __qtablewidgetitem23->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(23, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        __qtablewidgetitem24->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(24, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        __qtablewidgetitem25->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(25, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        __qtablewidgetitem26->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(26, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        __qtablewidgetitem27->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(27, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        __qtablewidgetitem28->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(28, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        __qtablewidgetitem29->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(29, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        __qtablewidgetitem30->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setHorizontalHeaderItem(30, __qtablewidgetitem30);
        if (tableroPlan->rowCount() < 3)
            tableroPlan->setRowCount(3);
        QTableWidgetItem *__qtablewidgetitem31 = new QTableWidgetItem();
        __qtablewidgetitem31->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroPlan->setVerticalHeaderItem(0, __qtablewidgetitem31);
        QTableWidgetItem *__qtablewidgetitem32 = new QTableWidgetItem();
        __qtablewidgetitem32->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroPlan->setVerticalHeaderItem(1, __qtablewidgetitem32);
        QTableWidgetItem *__qtablewidgetitem33 = new QTableWidgetItem();
        __qtablewidgetitem33->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroPlan->setVerticalHeaderItem(2, __qtablewidgetitem33);
        QTableWidgetItem *__qtablewidgetitem34 = new QTableWidgetItem();
        __qtablewidgetitem34->setFlags(Qt::ItemIsDragEnabled|Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
        tableroPlan->setItem(0, 0, __qtablewidgetitem34);
        QTableWidgetItem *__qtablewidgetitem35 = new QTableWidgetItem();
        __qtablewidgetitem35->setTextAlignment(Qt::AlignCenter);
        tableroPlan->setItem(0, 1, __qtablewidgetitem35);
        QTableWidgetItem *__qtablewidgetitem36 = new QTableWidgetItem();
        tableroPlan->setItem(1, 0, __qtablewidgetitem36);
        QTableWidgetItem *__qtablewidgetitem37 = new QTableWidgetItem();
        tableroPlan->setItem(1, 1, __qtablewidgetitem37);
        QTableWidgetItem *__qtablewidgetitem38 = new QTableWidgetItem();
        tableroPlan->setItem(2, 0, __qtablewidgetitem38);
        QTableWidgetItem *__qtablewidgetitem39 = new QTableWidgetItem();
        tableroPlan->setItem(2, 1, __qtablewidgetitem39);
        tableroPlan->setObjectName(QStringLiteral("tableroPlan"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(tableroPlan->sizePolicy().hasHeightForWidth());
        tableroPlan->setSizePolicy(sizePolicy5);
        tableroPlan->setMinimumSize(QSize(0, 0));
        tableroPlan->setMaximumSize(QSize(12345, 95));
        tableroPlan->setFont(font);
        tableroPlan->setMouseTracking(false);
        tableroPlan->setAcceptDrops(false);
        tableroPlan->setToolTipDuration(2);
        tableroPlan->setStyleSheet(QLatin1String("background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
""));
        tableroPlan->setFrameShape(QFrame::Panel);
        tableroPlan->setFrameShadow(QFrame::Raised);
        tableroPlan->setLineWidth(0);
        tableroPlan->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        tableroPlan->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        tableroPlan->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        tableroPlan->setAutoScroll(false);
        tableroPlan->setEditTriggers(QAbstractItemView::AllEditTriggers);
        tableroPlan->setTabKeyNavigation(false);
        tableroPlan->setProperty("showDropIndicator", QVariant(false));
        tableroPlan->setDragEnabled(false);
        tableroPlan->setDragDropOverwriteMode(false);
        tableroPlan->setDragDropMode(QAbstractItemView::NoDragDrop);
        tableroPlan->setDefaultDropAction(Qt::IgnoreAction);
        tableroPlan->setAlternatingRowColors(false);
        tableroPlan->setSelectionMode(QAbstractItemView::NoSelection);
        tableroPlan->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableroPlan->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
        tableroPlan->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        tableroPlan->setShowGrid(true);
        tableroPlan->setGridStyle(Qt::DotLine);
        tableroPlan->setSortingEnabled(false);
        tableroPlan->setWordWrap(false);
        tableroPlan->setCornerButtonEnabled(false);
        tableroPlan->setRowCount(3);
        tableroPlan->setColumnCount(31);
        tableroPlan->horizontalHeader()->setDefaultSectionSize(80);
        tableroPlan->horizontalHeader()->setMinimumSectionSize(19);
        tableroPlan->verticalHeader()->setVisible(false);
        tableroPlan->verticalHeader()->setDefaultSectionSize(15);
        tableroPlan->verticalHeader()->setMinimumSectionSize(15);
        tableroPlan->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_68->addWidget(tableroPlan, 1, 0, 2, 1);

        groupBox = new QGroupBox(tab1ElPlan);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        gridLayout_65 = new QGridLayout(groupBox);
        gridLayout_65->setSpacing(6);
        gridLayout_65->setContentsMargins(11, 11, 11, 11);
        gridLayout_65->setObjectName(QStringLiteral("gridLayout_65"));
        groupBox_6 = new QGroupBox(groupBox);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setStyleSheet(QLatin1String("background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
""));
        gridLayout_39 = new QGridLayout(groupBox_6);
        gridLayout_39->setSpacing(6);
        gridLayout_39->setContentsMargins(11, 11, 11, 11);
        gridLayout_39->setObjectName(QStringLiteral("gridLayout_39"));
        lblTableroElPlanHeaderM3 = new QLabel(groupBox_6);
        lblTableroElPlanHeaderM3->setObjectName(QStringLiteral("lblTableroElPlanHeaderM3"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderM3->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderM3->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderM3->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderM3->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderM3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderM3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderM3->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderM3, 0, 4, 1, 1);

        lblTableroElPlanHeaderKg = new QLabel(groupBox_6);
        lblTableroElPlanHeaderKg->setObjectName(QStringLiteral("lblTableroElPlanHeaderKg"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderKg->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderKg->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderKg->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderKg->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderKg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderKg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderKg->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderKg, 0, 2, 1, 1);

        lblTableroElPlanHeaderCostoViaje = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoViaje->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoViaje"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoViaje->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoViaje->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoViaje->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoViaje->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoViaje->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoViaje->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoViaje->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoViaje, 0, 8, 1, 1);

        lblTableroElPlanHeaderPiezas = new QLabel(groupBox_6);
        lblTableroElPlanHeaderPiezas->setObjectName(QStringLiteral("lblTableroElPlanHeaderPiezas"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderPiezas->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderPiezas->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderPiezas->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderPiezas->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderPiezas->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderPiezas->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderPiezas->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderPiezas, 0, 3, 1, 1);

        lblTableroElPlanHeaderPedidos = new QLabel(groupBox_6);
        lblTableroElPlanHeaderPedidos->setObjectName(QStringLiteral("lblTableroElPlanHeaderPedidos"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderPedidos->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderPedidos->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderPedidos->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderPedidos->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderPedidos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderPedidos->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderPedidos->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderPedidos, 0, 0, 1, 1);

        lblTableroElPlanHeaderCostoDestino = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoDestino->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoDestino"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoDestino->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoDestino->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoDestino->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoDestino->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoDestino->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoDestino->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoDestino->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoDestino, 0, 9, 1, 1);

        lblTableroElPlanHeaderCostoPiezas = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoPiezas->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoPiezas"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoPiezas->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoPiezas->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoPiezas->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoPiezas->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoPiezas->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoPiezas->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoPiezas->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoPiezas, 0, 11, 1, 1);

        lblTableroElPlanHeaderDestinos = new QLabel(groupBox_6);
        lblTableroElPlanHeaderDestinos->setObjectName(QStringLiteral("lblTableroElPlanHeaderDestinos"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderDestinos->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderDestinos->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderDestinos->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderDestinos->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderDestinos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderDestinos->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderDestinos->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderDestinos, 0, 1, 1, 1);

        lblTableroElPlanHeaderViajes = new QLabel(groupBox_6);
        lblTableroElPlanHeaderViajes->setObjectName(QStringLiteral("lblTableroElPlanHeaderViajes"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderViajes->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderViajes->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderViajes->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderViajes->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderViajes->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderViajes->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderViajes, 0, 5, 1, 1);

        lblTableroElPlanHeaderCostoTotal = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoTotal->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoTotal"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoTotal->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoTotal->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoTotal->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoTotal->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoTotal->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoTotal->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoTotal, 0, 6, 1, 1);

        lblTableroElPlanHeaderKm = new QLabel(groupBox_6);
        lblTableroElPlanHeaderKm->setObjectName(QStringLiteral("lblTableroElPlanHeaderKm"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderKm->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderKm->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderKm->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderKm->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderKm->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderKm->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderKm, 0, 7, 1, 1);

        lblTableroElPlanHeaderCostoM3 = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoM3->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoM3"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoM3->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoM3->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoM3->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoM3->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoM3->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoM3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoM3->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoM3, 0, 13, 1, 1);

        lblTableroElPlanHeaderCostoPedido = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoPedido->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoPedido"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoPedido->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoPedido->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoPedido->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoPedido->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoPedido->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoPedido->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoPedido->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoPedido, 0, 10, 1, 1);

        lblTableroElPlanHeaderCostoKg = new QLabel(groupBox_6);
        lblTableroElPlanHeaderCostoKg->setObjectName(QStringLiteral("lblTableroElPlanHeaderCostoKg"));
        sizePolicy4.setHeightForWidth(lblTableroElPlanHeaderCostoKg->sizePolicy().hasHeightForWidth());
        lblTableroElPlanHeaderCostoKg->setSizePolicy(sizePolicy4);
        lblTableroElPlanHeaderCostoKg->setMinimumSize(QSize(0, 50));
        lblTableroElPlanHeaderCostoKg->setMaximumSize(QSize(16777215, 50));
        lblTableroElPlanHeaderCostoKg->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroElPlanHeaderCostoKg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroElPlanHeaderCostoKg->setIndent(0);

        gridLayout_39->addWidget(lblTableroElPlanHeaderCostoKg, 0, 12, 1, 1);


        gridLayout_65->addWidget(groupBox_6, 0, 0, 1, 1);

        gridElPlan = new QTableWidget(groupBox);
        if (gridElPlan->columnCount() < 25)
            gridElPlan->setColumnCount(25);
        if (gridElPlan->rowCount() < 50)
            gridElPlan->setRowCount(50);
        gridElPlan->setObjectName(QStringLiteral("gridElPlan"));
        sizePolicy1.setHeightForWidth(gridElPlan->sizePolicy().hasHeightForWidth());
        gridElPlan->setSizePolicy(sizePolicy1);
        gridElPlan->setMinimumSize(QSize(0, 0));
        gridElPlan->setMaximumSize(QSize(12345, 12345));
        gridElPlan->setFont(font);
        gridElPlan->setMouseTracking(true);
        gridElPlan->setAcceptDrops(true);
        gridElPlan->setToolTipDuration(2);
        gridElPlan->setStyleSheet(QStringLiteral("background: transparent;"));
        gridElPlan->setFrameShape(QFrame::NoFrame);
        gridElPlan->setLineWidth(0);
        gridElPlan->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        gridElPlan->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        gridElPlan->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        gridElPlan->setAutoScroll(true);
        gridElPlan->setAutoScrollMargin(20);
        gridElPlan->setEditTriggers(QAbstractItemView::AnyKeyPressed);
        gridElPlan->setTabKeyNavigation(true);
        gridElPlan->setProperty("showDropIndicator", QVariant(true));
        gridElPlan->setDragEnabled(true);
        gridElPlan->setDragDropOverwriteMode(true);
        gridElPlan->setDragDropMode(QAbstractItemView::DragDrop);
        gridElPlan->setDefaultDropAction(Qt::IgnoreAction);
        gridElPlan->setAlternatingRowColors(false);
        gridElPlan->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridElPlan->setTextElideMode(Qt::ElideNone);
        gridElPlan->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
        gridElPlan->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        gridElPlan->setGridStyle(Qt::DotLine);
        gridElPlan->setSortingEnabled(true);
        gridElPlan->setWordWrap(false);
        gridElPlan->setRowCount(50);
        gridElPlan->setColumnCount(25);
        gridElPlan->horizontalHeader()->setDefaultSectionSize(75);
        gridElPlan->verticalHeader()->setDefaultSectionSize(15);
        gridElPlan->verticalHeader()->setMinimumSectionSize(15);
        gridElPlan->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_65->addWidget(gridElPlan, 1, 0, 1, 1);


        gridLayout_68->addWidget(groupBox, 0, 0, 1, 2);

        frame = new QFrame(tab1ElPlan);
        frame->setObjectName(QStringLiteral("frame"));
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(0, 0));
        frame->setMaximumSize(QSize(12345, 12345));
        frame->setStyleSheet(QLatin1String("padding: 0px;\n"
"margin: 0px;\n"
""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        btnRecalculaPlan = new QPushButton(frame);
        btnRecalculaPlan->setObjectName(QStringLiteral("btnRecalculaPlan"));
        sizePolicy3.setHeightForWidth(btnRecalculaPlan->sizePolicy().hasHeightForWidth());
        btnRecalculaPlan->setSizePolicy(sizePolicy3);
        btnRecalculaPlan->setMinimumSize(QSize(0, 0));
        btnRecalculaPlan->setMaximumSize(QSize(48, 48));
        btnRecalculaPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/IMG5/4recalcular.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnRecalculaPlan->setIcon(icon);
        btnRecalculaPlan->setIconSize(QSize(32, 32));
        btnRecalculaPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnRecalculaPlan, 0, 0, 1, 1);

        btnOptimizaPlan = new QPushButton(frame);
        btnOptimizaPlan->setObjectName(QStringLiteral("btnOptimizaPlan"));
        sizePolicy3.setHeightForWidth(btnOptimizaPlan->sizePolicy().hasHeightForWidth());
        btnOptimizaPlan->setSizePolicy(sizePolicy3);
        btnOptimizaPlan->setMinimumSize(QSize(0, 0));
        btnOptimizaPlan->setMaximumSize(QSize(48, 48));
        btnOptimizaPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/IMG5/4optimizar.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnOptimizaPlan->setIcon(icon1);
        btnOptimizaPlan->setIconSize(QSize(32, 32));
        btnOptimizaPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnOptimizaPlan, 0, 1, 1, 1);

        btnGuardaPlan = new QPushButton(frame);
        btnGuardaPlan->setObjectName(QStringLiteral("btnGuardaPlan"));
        sizePolicy3.setHeightForWidth(btnGuardaPlan->sizePolicy().hasHeightForWidth());
        btnGuardaPlan->setSizePolicy(sizePolicy3);
        btnGuardaPlan->setMinimumSize(QSize(0, 0));
        btnGuardaPlan->setMaximumSize(QSize(48, 48));
        btnGuardaPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/IMG5/4guardar_1.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnGuardaPlan->setIcon(icon2);
        btnGuardaPlan->setIconSize(QSize(32, 32));
        btnGuardaPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnGuardaPlan, 0, 7, 1, 1);

        btnColorDPlan = new QPushButton(frame);
        btnColorDPlan->setObjectName(QStringLiteral("btnColorDPlan"));
        sizePolicy3.setHeightForWidth(btnColorDPlan->sizePolicy().hasHeightForWidth());
        btnColorDPlan->setSizePolicy(sizePolicy3);
        btnColorDPlan->setMinimumSize(QSize(0, 0));
        btnColorDPlan->setMaximumSize(QSize(48, 48));
        btnColorDPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/IMG5/4pintarDestinos.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnColorDPlan->setIcon(icon3);
        btnColorDPlan->setIconSize(QSize(32, 32));
        btnColorDPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnColorDPlan, 0, 3, 1, 1);

        btnColorSinPlan = new QPushButton(frame);
        btnColorSinPlan->setObjectName(QStringLiteral("btnColorSinPlan"));
        sizePolicy3.setHeightForWidth(btnColorSinPlan->sizePolicy().hasHeightForWidth());
        btnColorSinPlan->setSizePolicy(sizePolicy3);
        btnColorSinPlan->setMinimumSize(QSize(0, 0));
        btnColorSinPlan->setMaximumSize(QSize(48, 48));
        btnColorSinPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/IMG5/4NoColor.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnColorSinPlan->setIcon(icon4);
        btnColorSinPlan->setIconSize(QSize(32, 32));
        btnColorSinPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnColorSinPlan, 0, 4, 1, 1);

        comboNombrePlan = new QComboBox(frame);
        comboNombrePlan->addItem(QString());
        comboNombrePlan->addItem(QString());
        comboNombrePlan->setObjectName(QStringLiteral("comboNombrePlan"));
        QSizePolicy sizePolicy6(QSizePolicy::Maximum, QSizePolicy::Minimum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(comboNombrePlan->sizePolicy().hasHeightForWidth());
        comboNombrePlan->setSizePolicy(sizePolicy6);
        comboNombrePlan->setMinimumSize(QSize(0, 0));
        comboNombrePlan->setMaximumSize(QSize(12345, 16777215));
        comboNombrePlan->setToolTipDuration(2);
        comboNombrePlan->setEditable(true);

        gridLayout_3->addWidget(comboNombrePlan, 0, 6, 1, 1);

        btnRefrescaPlan = new QPushButton(frame);
        btnRefrescaPlan->setObjectName(QStringLiteral("btnRefrescaPlan"));
        sizePolicy3.setHeightForWidth(btnRefrescaPlan->sizePolicy().hasHeightForWidth());
        btnRefrescaPlan->setSizePolicy(sizePolicy3);
        btnRefrescaPlan->setMinimumSize(QSize(0, 0));
        btnRefrescaPlan->setMaximumSize(QSize(48, 48));
        btnRefrescaPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/IMG5/4refresh.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnRefrescaPlan->setIcon(icon5);
        btnRefrescaPlan->setIconSize(QSize(32, 32));
        btnRefrescaPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnRefrescaPlan, 0, 5, 1, 1);

        btnColorVPlan = new QPushButton(frame);
        btnColorVPlan->setObjectName(QStringLiteral("btnColorVPlan"));
        sizePolicy3.setHeightForWidth(btnColorVPlan->sizePolicy().hasHeightForWidth());
        btnColorVPlan->setSizePolicy(sizePolicy3);
        btnColorVPlan->setMinimumSize(QSize(0, 0));
        btnColorVPlan->setMaximumSize(QSize(48, 48));
        btnColorVPlan->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/IMG5/4pintarViajes.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnColorVPlan->setIcon(icon6);
        btnColorVPlan->setIconSize(QSize(32, 32));
        btnColorVPlan->setAutoDefault(true);

        gridLayout_3->addWidget(btnColorVPlan, 0, 2, 1, 1);


        gridLayout_68->addWidget(frame, 2, 1, 1, 1);

        tabWidget1Principal->addTab(tab1ElPlan, QString());
        tab1Planear = new QWidget();
        tab1Planear->setObjectName(QStringLiteral("tab1Planear"));
        sizePolicy1.setHeightForWidth(tab1Planear->sizePolicy().hasHeightForWidth());
        tab1Planear->setSizePolicy(sizePolicy1);
        tab1Planear->setMaximumSize(QSize(12345, 12345));
        gridLayout_29 = new QGridLayout(tab1Planear);
        gridLayout_29->setSpacing(6);
        gridLayout_29->setContentsMargins(11, 11, 11, 11);
        gridLayout_29->setObjectName(QStringLiteral("gridLayout_29"));
        tab2Planear = new QTabWidget(tab1Planear);
        tab2Planear->setObjectName(QStringLiteral("tab2Planear"));
        sizePolicy1.setHeightForWidth(tab2Planear->sizePolicy().hasHeightForWidth());
        tab2Planear->setSizePolicy(sizePolicy1);
        tab2Planear->setTabPosition(QTabWidget::North);
        tab2Planear->setIconSize(QSize(22, 22));
        tab2PlanearCrear = new QWidget();
        tab2PlanearCrear->setObjectName(QStringLiteral("tab2PlanearCrear"));
        sizePolicy1.setHeightForWidth(tab2PlanearCrear->sizePolicy().hasHeightForWidth());
        tab2PlanearCrear->setSizePolicy(sizePolicy1);
        gridLayout_50 = new QGridLayout(tab2PlanearCrear);
        gridLayout_50->setSpacing(0);
        gridLayout_50->setContentsMargins(11, 11, 11, 11);
        gridLayout_50->setObjectName(QStringLiteral("gridLayout_50"));
        gridLayout_50->setContentsMargins(0, 0, 0, 0);
        groupBox_4 = new QGroupBox(tab2PlanearCrear);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        sizePolicy1.setHeightForWidth(groupBox_4->sizePolicy().hasHeightForWidth());
        groupBox_4->setSizePolicy(sizePolicy1);
        gridLayout_23 = new QGridLayout(groupBox_4);
        gridLayout_23->setSpacing(0);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        gridLayout_23->setContentsMargins(20, 25, 0, 0);
        groupBoxCrearPlan_Pedidos = new QGroupBox(groupBox_4);
        groupBoxCrearPlan_Pedidos->setObjectName(QStringLiteral("groupBoxCrearPlan_Pedidos"));
        sizePolicy1.setHeightForWidth(groupBoxCrearPlan_Pedidos->sizePolicy().hasHeightForWidth());
        groupBoxCrearPlan_Pedidos->setSizePolicy(sizePolicy1);
        groupBoxCrearPlan_Pedidos->setMinimumSize(QSize(0, 0));
        groupBoxCrearPlan_Pedidos->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_9 = new QGridLayout(groupBoxCrearPlan_Pedidos);
        gridLayout_9->setSpacing(0);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        gridLayout_9->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanPedidos = new QListWidget(groupBoxCrearPlan_Pedidos);
        new QListWidgetItem(listGenerarPlanPedidos);
        new QListWidgetItem(listGenerarPlanPedidos);
        new QListWidgetItem(listGenerarPlanPedidos);
        listGenerarPlanPedidos->setObjectName(QStringLiteral("listGenerarPlanPedidos"));
        sizePolicy1.setHeightForWidth(listGenerarPlanPedidos->sizePolicy().hasHeightForWidth());
        listGenerarPlanPedidos->setSizePolicy(sizePolicy1);
        listGenerarPlanPedidos->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanPedidos->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanPedidos->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_9->addWidget(listGenerarPlanPedidos, 0, 0, 1, 1);


        gridLayout_23->addWidget(groupBoxCrearPlan_Pedidos, 0, 0, 1, 1);

        groupBox_12 = new QGroupBox(groupBox_4);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        sizePolicy1.setHeightForWidth(groupBox_12->sizePolicy().hasHeightForWidth());
        groupBox_12->setSizePolicy(sizePolicy1);
        groupBox_12->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_25 = new QGridLayout(groupBox_12);
        gridLayout_25->setSpacing(0);
        gridLayout_25->setContentsMargins(11, 11, 11, 11);
        gridLayout_25->setObjectName(QStringLiteral("gridLayout_25"));
        gridLayout_25->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanVehiculos = new QListWidget(groupBox_12);
        new QListWidgetItem(listGenerarPlanVehiculos);
        new QListWidgetItem(listGenerarPlanVehiculos);
        new QListWidgetItem(listGenerarPlanVehiculos);
        listGenerarPlanVehiculos->setObjectName(QStringLiteral("listGenerarPlanVehiculos"));
        sizePolicy1.setHeightForWidth(listGenerarPlanVehiculos->sizePolicy().hasHeightForWidth());
        listGenerarPlanVehiculos->setSizePolicy(sizePolicy1);
        listGenerarPlanVehiculos->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanVehiculos->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanVehiculos->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_25->addWidget(listGenerarPlanVehiculos, 0, 0, 1, 1);


        gridLayout_23->addWidget(groupBox_12, 0, 1, 1, 1);

        groupBox_13 = new QGroupBox(groupBox_4);
        groupBox_13->setObjectName(QStringLiteral("groupBox_13"));
        sizePolicy1.setHeightForWidth(groupBox_13->sizePolicy().hasHeightForWidth());
        groupBox_13->setSizePolicy(sizePolicy1);
        groupBox_13->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_32 = new QGridLayout(groupBox_13);
        gridLayout_32->setSpacing(0);
        gridLayout_32->setContentsMargins(11, 11, 11, 11);
        gridLayout_32->setObjectName(QStringLiteral("gridLayout_32"));
        gridLayout_32->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanOperadores = new QListWidget(groupBox_13);
        new QListWidgetItem(listGenerarPlanOperadores);
        new QListWidgetItem(listGenerarPlanOperadores);
        new QListWidgetItem(listGenerarPlanOperadores);
        listGenerarPlanOperadores->setObjectName(QStringLiteral("listGenerarPlanOperadores"));
        sizePolicy1.setHeightForWidth(listGenerarPlanOperadores->sizePolicy().hasHeightForWidth());
        listGenerarPlanOperadores->setSizePolicy(sizePolicy1);
        listGenerarPlanOperadores->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanOperadores->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanOperadores->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_32->addWidget(listGenerarPlanOperadores, 0, 0, 1, 1);


        gridLayout_23->addWidget(groupBox_13, 0, 2, 1, 1);

        btnGeneraPlan = new QPushButton(groupBox_4);
        btnGeneraPlan->setObjectName(QStringLiteral("btnGeneraPlan"));
        sizePolicy3.setHeightForWidth(btnGeneraPlan->sizePolicy().hasHeightForWidth());
        btnGeneraPlan->setSizePolicy(sizePolicy3);
        btnGeneraPlan->setMaximumSize(QSize(12345, 40));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnGeneraPlan->setPalette(palette1);
        btnGeneraPlan->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_23->addWidget(btnGeneraPlan, 1, 2, 1, 1);

        editNombrePlanGenera = new QLineEdit(groupBox_4);
        editNombrePlanGenera->setObjectName(QStringLiteral("editNombrePlanGenera"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(editNombrePlanGenera->sizePolicy().hasHeightForWidth());
        editNombrePlanGenera->setSizePolicy(sizePolicy7);

        gridLayout_23->addWidget(editNombrePlanGenera, 1, 0, 1, 2);


        gridLayout_50->addWidget(groupBox_4, 0, 0, 1, 2);

        groupBox_5 = new QGroupBox(tab2PlanearCrear);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        sizePolicy1.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy1);
        gridLayout_4 = new QGridLayout(groupBox_5);
        gridLayout_4->setSpacing(0);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 15, 0, 0);
        groupBox_19 = new QGroupBox(groupBox_5);
        groupBox_19->setObjectName(QStringLiteral("groupBox_19"));
        sizePolicy1.setHeightForWidth(groupBox_19->sizePolicy().hasHeightForWidth());
        groupBox_19->setSizePolicy(sizePolicy1);
        groupBox_19->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_63 = new QGridLayout(groupBox_19);
        gridLayout_63->setSpacing(0);
        gridLayout_63->setContentsMargins(11, 11, 11, 11);
        gridLayout_63->setObjectName(QStringLiteral("gridLayout_63"));
        gridLayout_63->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanPreferencias_2 = new QListWidget(groupBox_19);
        new QListWidgetItem(listGenerarPlanPreferencias_2);
        new QListWidgetItem(listGenerarPlanPreferencias_2);
        new QListWidgetItem(listGenerarPlanPreferencias_2);
        listGenerarPlanPreferencias_2->setObjectName(QStringLiteral("listGenerarPlanPreferencias_2"));
        sizePolicy1.setHeightForWidth(listGenerarPlanPreferencias_2->sizePolicy().hasHeightForWidth());
        listGenerarPlanPreferencias_2->setSizePolicy(sizePolicy1);
        listGenerarPlanPreferencias_2->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanPreferencias_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanPreferencias_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_63->addWidget(listGenerarPlanPreferencias_2, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_19, 1, 2, 1, 1);

        groupBox_15 = new QGroupBox(groupBox_5);
        groupBox_15->setObjectName(QStringLiteral("groupBox_15"));
        sizePolicy1.setHeightForWidth(groupBox_15->sizePolicy().hasHeightForWidth());
        groupBox_15->setSizePolicy(sizePolicy1);
        groupBox_15->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_26 = new QGridLayout(groupBox_15);
        gridLayout_26->setSpacing(0);
        gridLayout_26->setContentsMargins(11, 11, 11, 11);
        gridLayout_26->setObjectName(QStringLiteral("gridLayout_26"));
        gridLayout_26->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanDestinos = new QListWidget(groupBox_15);
        new QListWidgetItem(listGenerarPlanDestinos);
        new QListWidgetItem(listGenerarPlanDestinos);
        new QListWidgetItem(listGenerarPlanDestinos);
        listGenerarPlanDestinos->setObjectName(QStringLiteral("listGenerarPlanDestinos"));
        sizePolicy1.setHeightForWidth(listGenerarPlanDestinos->sizePolicy().hasHeightForWidth());
        listGenerarPlanDestinos->setSizePolicy(sizePolicy1);
        listGenerarPlanDestinos->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanDestinos->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanDestinos->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_26->addWidget(listGenerarPlanDestinos, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_15, 0, 0, 1, 1);

        groupBox_17 = new QGroupBox(groupBox_5);
        groupBox_17->setObjectName(QStringLiteral("groupBox_17"));
        sizePolicy1.setHeightForWidth(groupBox_17->sizePolicy().hasHeightForWidth());
        groupBox_17->setSizePolicy(sizePolicy1);
        groupBox_17->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_30 = new QGridLayout(groupBox_17);
        gridLayout_30->setSpacing(0);
        gridLayout_30->setContentsMargins(11, 11, 11, 11);
        gridLayout_30->setObjectName(QStringLiteral("gridLayout_30"));
        gridLayout_30->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanTarifario = new QListWidget(groupBox_17);
        new QListWidgetItem(listGenerarPlanTarifario);
        new QListWidgetItem(listGenerarPlanTarifario);
        new QListWidgetItem(listGenerarPlanTarifario);
        listGenerarPlanTarifario->setObjectName(QStringLiteral("listGenerarPlanTarifario"));
        sizePolicy1.setHeightForWidth(listGenerarPlanTarifario->sizePolicy().hasHeightForWidth());
        listGenerarPlanTarifario->setSizePolicy(sizePolicy1);
        listGenerarPlanTarifario->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanTarifario->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanTarifario->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_30->addWidget(listGenerarPlanTarifario, 0, 1, 1, 1);


        gridLayout_4->addWidget(groupBox_17, 0, 1, 1, 2);

        groupBox_18 = new QGroupBox(groupBox_5);
        groupBox_18->setObjectName(QStringLiteral("groupBox_18"));
        sizePolicy1.setHeightForWidth(groupBox_18->sizePolicy().hasHeightForWidth());
        groupBox_18->setSizePolicy(sizePolicy1);
        groupBox_18->setStyleSheet(QLatin1String("background:transparent;\n"
""));
        gridLayout_43 = new QGridLayout(groupBox_18);
        gridLayout_43->setSpacing(0);
        gridLayout_43->setContentsMargins(11, 11, 11, 11);
        gridLayout_43->setObjectName(QStringLiteral("gridLayout_43"));
        gridLayout_43->setContentsMargins(20, 25, 0, 0);
        listGenerarPlanComp = new QListWidget(groupBox_18);
        new QListWidgetItem(listGenerarPlanComp);
        new QListWidgetItem(listGenerarPlanComp);
        new QListWidgetItem(listGenerarPlanComp);
        listGenerarPlanComp->setObjectName(QStringLiteral("listGenerarPlanComp"));
        sizePolicy1.setHeightForWidth(listGenerarPlanComp->sizePolicy().hasHeightForWidth());
        listGenerarPlanComp->setSizePolicy(sizePolicy1);
        listGenerarPlanComp->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listGenerarPlanComp->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listGenerarPlanComp->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_43->addWidget(listGenerarPlanComp, 0, 0, 1, 1);


        gridLayout_4->addWidget(groupBox_18, 1, 0, 1, 2);


        gridLayout_50->addWidget(groupBox_5, 0, 2, 2, 1);

        tab2Planear->addTab(tab2PlanearCrear, QString());
        tab2PlanearAbrir = new QWidget();
        tab2PlanearAbrir->setObjectName(QStringLiteral("tab2PlanearAbrir"));
        gridLayout_7 = new QGridLayout(tab2PlanearAbrir);
        gridLayout_7->setSpacing(0);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setContentsMargins(0, 0, 0, 0);
        groupBox_25 = new QGroupBox(tab2PlanearAbrir);
        groupBox_25->setObjectName(QStringLiteral("groupBox_25"));
        sizePolicy1.setHeightForWidth(groupBox_25->sizePolicy().hasHeightForWidth());
        groupBox_25->setSizePolicy(sizePolicy1);
        groupBox_25->setMaximumSize(QSize(800, 16777215));
        gridLayout_21 = new QGridLayout(groupBox_25);
        gridLayout_21->setSpacing(0);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        gridLayout_21->setContentsMargins(20, 25, 0, 0);
        horizontalSpacer = new QSpacerItem(1497, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_21->addItem(horizontalSpacer, 1, 0, 1, 1);

        btnAbrePlan = new QPushButton(groupBox_25);
        btnAbrePlan->setObjectName(QStringLiteral("btnAbrePlan"));
        sizePolicy3.setHeightForWidth(btnAbrePlan->sizePolicy().hasHeightForWidth());
        btnAbrePlan->setSizePolicy(sizePolicy3);
        btnAbrePlan->setMinimumSize(QSize(0, 0));
        btnAbrePlan->setMaximumSize(QSize(12345, 40));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAbrePlan->setPalette(palette2);
        btnAbrePlan->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_21->addWidget(btnAbrePlan, 1, 1, 1, 1);

        listAbrePlan = new QListWidget(groupBox_25);
        new QListWidgetItem(listAbrePlan);
        new QListWidgetItem(listAbrePlan);
        new QListWidgetItem(listAbrePlan);
        listAbrePlan->setObjectName(QStringLiteral("listAbrePlan"));
        sizePolicy1.setHeightForWidth(listAbrePlan->sizePolicy().hasHeightForWidth());
        listAbrePlan->setSizePolicy(sizePolicy1);
        listAbrePlan->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listAbrePlan->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listAbrePlan->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_21->addWidget(listAbrePlan, 0, 0, 1, 2);


        gridLayout_7->addWidget(groupBox_25, 0, 0, 2, 1);

        tab2Planear->addTab(tab2PlanearAbrir, QString());
        tab2PlanearAnadir = new QWidget();
        tab2PlanearAnadir->setObjectName(QStringLiteral("tab2PlanearAnadir"));
        sizePolicy1.setHeightForWidth(tab2PlanearAnadir->sizePolicy().hasHeightForWidth());
        tab2PlanearAnadir->setSizePolicy(sizePolicy1);
        gridLayout_52 = new QGridLayout(tab2PlanearAnadir);
        gridLayout_52->setSpacing(0);
        gridLayout_52->setContentsMargins(11, 11, 11, 11);
        gridLayout_52->setObjectName(QStringLiteral("gridLayout_52"));
        gridLayout_52->setContentsMargins(0, 0, 0, 0);
        groupBox_20 = new QGroupBox(tab2PlanearAnadir);
        groupBox_20->setObjectName(QStringLiteral("groupBox_20"));
        sizePolicy1.setHeightForWidth(groupBox_20->sizePolicy().hasHeightForWidth());
        groupBox_20->setSizePolicy(sizePolicy1);
        groupBox_20->setMinimumSize(QSize(0, 0));
        groupBox_20->setMaximumSize(QSize(12345, 12345));
        gridLayout_20 = new QGridLayout(groupBox_20);
        gridLayout_20->setSpacing(0);
        gridLayout_20->setContentsMargins(11, 11, 11, 11);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        gridLayout_20->setContentsMargins(20, 25, 0, 0);
        btnBorrarPedidos = new QPushButton(groupBox_20);
        btnBorrarPedidos->setObjectName(QStringLiteral("btnBorrarPedidos"));
        sizePolicy3.setHeightForWidth(btnBorrarPedidos->sizePolicy().hasHeightForWidth());
        btnBorrarPedidos->setSizePolicy(sizePolicy3);
        btnBorrarPedidos->setMaximumSize(QSize(12345, 40));
        btnBorrarPedidos->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_20->addWidget(btnBorrarPedidos, 1, 2, 1, 1);

        btnAnadirPedidos = new QPushButton(groupBox_20);
        btnAnadirPedidos->setObjectName(QStringLiteral("btnAnadirPedidos"));
        sizePolicy3.setHeightForWidth(btnAnadirPedidos->sizePolicy().hasHeightForWidth());
        btnAnadirPedidos->setSizePolicy(sizePolicy3);
        btnAnadirPedidos->setMaximumSize(QSize(12345, 40));
        btnAnadirPedidos->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_20->addWidget(btnAnadirPedidos, 1, 1, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_20->addItem(horizontalSpacer_8, 1, 0, 1, 1);

        listAnadirPedidos = new QListWidget(groupBox_20);
        new QListWidgetItem(listAnadirPedidos);
        new QListWidgetItem(listAnadirPedidos);
        new QListWidgetItem(listAnadirPedidos);
        listAnadirPedidos->setObjectName(QStringLiteral("listAnadirPedidos"));
        sizePolicy1.setHeightForWidth(listAnadirPedidos->sizePolicy().hasHeightForWidth());
        listAnadirPedidos->setSizePolicy(sizePolicy1);
        listAnadirPedidos->setMinimumSize(QSize(500, 0));
        listAnadirPedidos->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listAnadirPedidos->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listAnadirPedidos->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_20->addWidget(listAnadirPedidos, 0, 0, 1, 3);


        gridLayout_52->addWidget(groupBox_20, 0, 0, 1, 1);

        groupBox_21 = new QGroupBox(tab2PlanearAnadir);
        groupBox_21->setObjectName(QStringLiteral("groupBox_21"));
        sizePolicy1.setHeightForWidth(groupBox_21->sizePolicy().hasHeightForWidth());
        groupBox_21->setSizePolicy(sizePolicy1);
        groupBox_21->setMinimumSize(QSize(0, 0));
        groupBox_21->setMaximumSize(QSize(12345, 16777215));
        gridLayout_22 = new QGridLayout(groupBox_21);
        gridLayout_22->setSpacing(0);
        gridLayout_22->setContentsMargins(11, 11, 11, 11);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        gridLayout_22->setContentsMargins(20, 25, 0, 0);
        btnAnadirPredespegados = new QPushButton(groupBox_21);
        btnAnadirPredespegados->setObjectName(QStringLiteral("btnAnadirPredespegados"));
        sizePolicy3.setHeightForWidth(btnAnadirPredespegados->sizePolicy().hasHeightForWidth());
        btnAnadirPredespegados->setSizePolicy(sizePolicy3);
        btnAnadirPredespegados->setMaximumSize(QSize(12345, 40));
        btnAnadirPredespegados->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_22->addWidget(btnAnadirPredespegados, 1, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_22->addItem(horizontalSpacer_7, 1, 0, 1, 1);

        listAnadirPredespegados = new QListWidget(groupBox_21);
        new QListWidgetItem(listAnadirPredespegados);
        new QListWidgetItem(listAnadirPredespegados);
        new QListWidgetItem(listAnadirPredespegados);
        listAnadirPredespegados->setObjectName(QStringLiteral("listAnadirPredespegados"));
        sizePolicy1.setHeightForWidth(listAnadirPredespegados->sizePolicy().hasHeightForWidth());
        listAnadirPredespegados->setSizePolicy(sizePolicy1);
        listAnadirPredespegados->setStyleSheet(QLatin1String("background:transparent;\n"
"border: 1px  solid rgba(190, 219, 189, 55);\n"
""));
        listAnadirPredespegados->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listAnadirPredespegados->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        gridLayout_22->addWidget(listAnadirPredespegados, 0, 0, 1, 2);


        gridLayout_52->addWidget(groupBox_21, 0, 1, 1, 1);

        tab2Planear->addTab(tab2PlanearAnadir, QString());

        gridLayout_29->addWidget(tab2Planear, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Planear, QString());
        tab1Rutear = new QWidget();
        tab1Rutear->setObjectName(QStringLiteral("tab1Rutear"));
        sizePolicy1.setHeightForWidth(tab1Rutear->sizePolicy().hasHeightForWidth());
        tab1Rutear->setSizePolicy(sizePolicy1);
        tab1Rutear->setMaximumSize(QSize(12345, 12345));
        gridLayout_51 = new QGridLayout(tab1Rutear);
        gridLayout_51->setSpacing(6);
        gridLayout_51->setContentsMargins(11, 11, 11, 11);
        gridLayout_51->setObjectName(QStringLiteral("gridLayout_51"));
        tab2Rutear = new QTabWidget(tab1Rutear);
        tab2Rutear->setObjectName(QStringLiteral("tab2Rutear"));
        sizePolicy1.setHeightForWidth(tab2Rutear->sizePolicy().hasHeightForWidth());
        tab2Rutear->setSizePolicy(sizePolicy1);
        tab2Rutear->setStyleSheet(QStringLiteral(""));
        tab2Rutear->setTabPosition(QTabWidget::North);
        tab2Rutear->setIconSize(QSize(22, 22));
        tab2RutearMotor = new QWidget();
        tab2RutearMotor->setObjectName(QStringLiteral("tab2RutearMotor"));
        sizePolicy1.setHeightForWidth(tab2RutearMotor->sizePolicy().hasHeightForWidth());
        tab2RutearMotor->setSizePolicy(sizePolicy1);
        gridLayout_18 = new QGridLayout(tab2RutearMotor);
        gridLayout_18->setSpacing(6);
        gridLayout_18->setContentsMargins(11, 11, 11, 11);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lblMotorPedidos = new QLabel(tab2RutearMotor);
        lblMotorPedidos->setObjectName(QStringLiteral("lblMotorPedidos"));
        sizePolicy7.setHeightForWidth(lblMotorPedidos->sizePolicy().hasHeightForWidth());
        lblMotorPedidos->setSizePolicy(sizePolicy7);
        lblMotorPedidos->setMaximumSize(QSize(16777215, 50));
        lblMotorPedidos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorPedidos->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblMotorPedidos);

        lblMotorDestinos = new QLabel(tab2RutearMotor);
        lblMotorDestinos->setObjectName(QStringLiteral("lblMotorDestinos"));
        sizePolicy7.setHeightForWidth(lblMotorDestinos->sizePolicy().hasHeightForWidth());
        lblMotorDestinos->setSizePolicy(sizePolicy7);
        lblMotorDestinos->setMaximumSize(QSize(16777215, 50));
        lblMotorDestinos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorDestinos->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblMotorDestinos);

        lblMotorKg = new QLabel(tab2RutearMotor);
        lblMotorKg->setObjectName(QStringLiteral("lblMotorKg"));
        sizePolicy7.setHeightForWidth(lblMotorKg->sizePolicy().hasHeightForWidth());
        lblMotorKg->setSizePolicy(sizePolicy7);
        lblMotorKg->setMaximumSize(QSize(16777215, 50));
        lblMotorKg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorKg->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblMotorKg);

        lblMotorPiezas = new QLabel(tab2RutearMotor);
        lblMotorPiezas->setObjectName(QStringLiteral("lblMotorPiezas"));
        sizePolicy7.setHeightForWidth(lblMotorPiezas->sizePolicy().hasHeightForWidth());
        lblMotorPiezas->setSizePolicy(sizePolicy7);
        lblMotorPiezas->setMaximumSize(QSize(16777215, 50));
        lblMotorPiezas->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorPiezas->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblMotorPiezas);

        lblMotorM3 = new QLabel(tab2RutearMotor);
        lblMotorM3->setObjectName(QStringLiteral("lblMotorM3"));
        sizePolicy7.setHeightForWidth(lblMotorM3->sizePolicy().hasHeightForWidth());
        lblMotorM3->setSizePolicy(sizePolicy7);
        lblMotorM3->setMaximumSize(QSize(16777215, 50));
        lblMotorM3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorM3->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(lblMotorM3);


        gridLayout_18->addLayout(horizontalLayout_2, 0, 0, 1, 3);

        graphicsViewMotor = new QGraphicsView(tab2RutearMotor);
        graphicsViewMotor->setObjectName(QStringLiteral("graphicsViewMotor"));
        QSizePolicy sizePolicy8(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(graphicsViewMotor->sizePolicy().hasHeightForWidth());
        graphicsViewMotor->setSizePolicy(sizePolicy8);
        graphicsViewMotor->setMinimumSize(QSize(600, 400));
        graphicsViewMotor->setMaximumSize(QSize(12345, 16777215));
        graphicsViewMotor->setStyleSheet(QLatin1String("color: rgba(173, 197, 113, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));

        gridLayout_18->addWidget(graphicsViewMotor, 1, 0, 1, 1);

        widgetGrafica = new QWidget(tab2RutearMotor);
        widgetGrafica->setObjectName(QStringLiteral("widgetGrafica"));
        sizePolicy8.setHeightForWidth(widgetGrafica->sizePolicy().hasHeightForWidth());
        widgetGrafica->setSizePolicy(sizePolicy8);
        widgetGrafica->setMinimumSize(QSize(600, 400));
        widgetGrafica->setMaximumSize(QSize(12345, 16777215));
        widgetGrafica->setStyleSheet(QLatin1String("color: rgba(173, 197, 113, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));

        gridLayout_18->addWidget(widgetGrafica, 1, 1, 1, 2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        lblMotorCostoPorViaje = new QLabel(tab2RutearMotor);
        lblMotorCostoPorViaje->setObjectName(QStringLiteral("lblMotorCostoPorViaje"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorViaje->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorViaje->setSizePolicy(sizePolicy4);
        lblMotorCostoPorViaje->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorViaje->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorViaje->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorViaje);

        lblMotorCostoPorDestino = new QLabel(tab2RutearMotor);
        lblMotorCostoPorDestino->setObjectName(QStringLiteral("lblMotorCostoPorDestino"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorDestino->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorDestino->setSizePolicy(sizePolicy4);
        lblMotorCostoPorDestino->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorDestino->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorDestino->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorDestino);

        lblMotorCostoPorPedido = new QLabel(tab2RutearMotor);
        lblMotorCostoPorPedido->setObjectName(QStringLiteral("lblMotorCostoPorPedido"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorPedido->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorPedido->setSizePolicy(sizePolicy4);
        lblMotorCostoPorPedido->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorPedido->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorPedido->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorPedido);

        lblMotorCostoPorPieza = new QLabel(tab2RutearMotor);
        lblMotorCostoPorPieza->setObjectName(QStringLiteral("lblMotorCostoPorPieza"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorPieza->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorPieza->setSizePolicy(sizePolicy4);
        lblMotorCostoPorPieza->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorPieza->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorPieza->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorPieza);

        lblMotorCostoPorKg = new QLabel(tab2RutearMotor);
        lblMotorCostoPorKg->setObjectName(QStringLiteral("lblMotorCostoPorKg"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorKg->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorKg->setSizePolicy(sizePolicy4);
        lblMotorCostoPorKg->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorKg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorKg->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorKg);

        lblMotorCostoPorM3 = new QLabel(tab2RutearMotor);
        lblMotorCostoPorM3->setObjectName(QStringLiteral("lblMotorCostoPorM3"));
        sizePolicy4.setHeightForWidth(lblMotorCostoPorM3->sizePolicy().hasHeightForWidth());
        lblMotorCostoPorM3->setSizePolicy(sizePolicy4);
        lblMotorCostoPorM3->setMaximumSize(QSize(16777215, 50));
        lblMotorCostoPorM3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorCostoPorM3->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lblMotorCostoPorM3);


        gridLayout_18->addLayout(horizontalLayout_3, 2, 0, 2, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        lblMotorViajes = new QLabel(tab2RutearMotor);
        lblMotorViajes->setObjectName(QStringLiteral("lblMotorViajes"));
        sizePolicy1.setHeightForWidth(lblMotorViajes->sizePolicy().hasHeightForWidth());
        lblMotorViajes->setSizePolicy(sizePolicy1);
        lblMotorViajes->setMinimumSize(QSize(0, 0));
        lblMotorViajes->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background:rgba(135, 171, 141,88);\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorViajes->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lblMotorViajes);

        lblMotorTotalCosto = new QLabel(tab2RutearMotor);
        lblMotorTotalCosto->setObjectName(QStringLiteral("lblMotorTotalCosto"));
        sizePolicy1.setHeightForWidth(lblMotorTotalCosto->sizePolicy().hasHeightForWidth());
        lblMotorTotalCosto->setSizePolicy(sizePolicy1);
        lblMotorTotalCosto->setMinimumSize(QSize(0, 0));
        lblMotorTotalCosto->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background:rgba(135, 171, 141,88);\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorTotalCosto->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lblMotorTotalCosto);

        lblMotorSegundos = new QLabel(tab2RutearMotor);
        lblMotorSegundos->setObjectName(QStringLiteral("lblMotorSegundos"));
        sizePolicy1.setHeightForWidth(lblMotorSegundos->sizePolicy().hasHeightForWidth());
        lblMotorSegundos->setSizePolicy(sizePolicy1);
        lblMotorSegundos->setMinimumSize(QSize(0, 0));
        lblMotorSegundos->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background:rgba(135, 171, 141,88);\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorSegundos->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lblMotorSegundos);

        lblMotorKm = new QLabel(tab2RutearMotor);
        lblMotorKm->setObjectName(QStringLiteral("lblMotorKm"));
        sizePolicy1.setHeightForWidth(lblMotorKm->sizePolicy().hasHeightForWidth());
        lblMotorKm->setSizePolicy(sizePolicy1);
        lblMotorKm->setMinimumSize(QSize(0, 0));
        lblMotorKm->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background:rgba(135, 171, 141,88);\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblMotorKm->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(lblMotorKm);


        gridLayout_18->addLayout(horizontalLayout_4, 2, 1, 2, 1);

        btnMotorIniciar = new QPushButton(tab2RutearMotor);
        btnMotorIniciar->setObjectName(QStringLiteral("btnMotorIniciar"));
        sizePolicy4.setHeightForWidth(btnMotorIniciar->sizePolicy().hasHeightForWidth());
        btnMotorIniciar->setSizePolicy(sizePolicy4);
        btnMotorIniciar->setMaximumSize(QSize(12345, 40));
        btnMotorIniciar->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_18->addWidget(btnMotorIniciar, 2, 2, 1, 1);

        btnMotorDetener = new QPushButton(tab2RutearMotor);
        btnMotorDetener->setObjectName(QStringLiteral("btnMotorDetener"));
        sizePolicy4.setHeightForWidth(btnMotorDetener->sizePolicy().hasHeightForWidth());
        btnMotorDetener->setSizePolicy(sizePolicy4);
        btnMotorDetener->setMaximumSize(QSize(12345, 40));
        btnMotorDetener->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_18->addWidget(btnMotorDetener, 3, 2, 1, 1);

        tab2Rutear->addTab(tab2RutearMotor, QString());
        tab2RutearCalibracion = new QWidget();
        tab2RutearCalibracion->setObjectName(QStringLiteral("tab2RutearCalibracion"));
        sizePolicy1.setHeightForWidth(tab2RutearCalibracion->sizePolicy().hasHeightForWidth());
        tab2RutearCalibracion->setSizePolicy(sizePolicy1);
        gridLayout_10 = new QGridLayout(tab2RutearCalibracion);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        gridCalibrarMotor = new QTableWidget(tab2RutearCalibracion);
        if (gridCalibrarMotor->columnCount() < 25)
            gridCalibrarMotor->setColumnCount(25);
        if (gridCalibrarMotor->rowCount() < 50)
            gridCalibrarMotor->setRowCount(50);
        gridCalibrarMotor->setObjectName(QStringLiteral("gridCalibrarMotor"));
        sizePolicy1.setHeightForWidth(gridCalibrarMotor->sizePolicy().hasHeightForWidth());
        gridCalibrarMotor->setSizePolicy(sizePolicy1);
        gridCalibrarMotor->setFont(font);
        gridCalibrarMotor->setMouseTracking(true);
        gridCalibrarMotor->setAcceptDrops(true);
        gridCalibrarMotor->setToolTipDuration(2);
        gridCalibrarMotor->setFrameShape(QFrame::NoFrame);
        gridCalibrarMotor->setLineWidth(0);
        gridCalibrarMotor->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridCalibrarMotor->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridCalibrarMotor->setDragEnabled(true);
        gridCalibrarMotor->setDragDropMode(QAbstractItemView::DragDrop);
        gridCalibrarMotor->setDefaultDropAction(Qt::MoveAction);
        gridCalibrarMotor->setAlternatingRowColors(false);
        gridCalibrarMotor->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridCalibrarMotor->setGridStyle(Qt::DotLine);
        gridCalibrarMotor->setSortingEnabled(true);
        gridCalibrarMotor->setRowCount(50);
        gridCalibrarMotor->setColumnCount(25);
        gridCalibrarMotor->verticalHeader()->setDefaultSectionSize(22);
        gridCalibrarMotor->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_10->addWidget(gridCalibrarMotor, 0, 0, 1, 1);

        tab2Rutear->addTab(tab2RutearCalibracion, QString());

        gridLayout_51->addWidget(tab2Rutear, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Rutear, QString());
        tab1Optimizar = new QWidget();
        tab1Optimizar->setObjectName(QStringLiteral("tab1Optimizar"));
        sizePolicy1.setHeightForWidth(tab1Optimizar->sizePolicy().hasHeightForWidth());
        tab1Optimizar->setSizePolicy(sizePolicy1);
        gridLayout_19 = new QGridLayout(tab1Optimizar);
        gridLayout_19->setSpacing(6);
        gridLayout_19->setContentsMargins(11, 11, 11, 11);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        tab2Optimizar = new QTabWidget(tab1Optimizar);
        tab2Optimizar->setObjectName(QStringLiteral("tab2Optimizar"));
        sizePolicy1.setHeightForWidth(tab2Optimizar->sizePolicy().hasHeightForWidth());
        tab2Optimizar->setSizePolicy(sizePolicy1);
        tab2Optimizar->setTabPosition(QTabWidget::North);
        tab2Optimizar->setIconSize(QSize(22, 22));
        tab2OptimizarAuto = new QWidget();
        tab2OptimizarAuto->setObjectName(QStringLiteral("tab2OptimizarAuto"));
        sizePolicy1.setHeightForWidth(tab2OptimizarAuto->sizePolicy().hasHeightForWidth());
        tab2OptimizarAuto->setSizePolicy(sizePolicy1);
        gridLayout_13 = new QGridLayout(tab2OptimizarAuto);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        horizontalSpacer_13 = new QSpacerItem(675, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_13->addItem(horizontalSpacer_13, 0, 0, 1, 1);

        groupBox_36 = new QGroupBox(tab2OptimizarAuto);
        groupBox_36->setObjectName(QStringLiteral("groupBox_36"));
        sizePolicy3.setHeightForWidth(groupBox_36->sizePolicy().hasHeightForWidth());
        groupBox_36->setSizePolicy(sizePolicy3);
        groupBox_36->setMinimumSize(QSize(0, 0));
        groupBox_36->setMaximumSize(QSize(12345, 12345));
        gridLayout_6 = new QGridLayout(groupBox_36);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        radioOptimizaCercanos = new QRadioButton(groupBox_36);
        radioOptimizaCercanos->setObjectName(QStringLiteral("radioOptimizaCercanos"));
        sizePolicy3.setHeightForWidth(radioOptimizaCercanos->sizePolicy().hasHeightForWidth());
        radioOptimizaCercanos->setSizePolicy(sizePolicy3);
        radioOptimizaCercanos->setMinimumSize(QSize(0, 0));
        radioOptimizaCercanos->setMaximumSize(QSize(12345, 16777215));
        radioOptimizaCercanos->setStyleSheet(QStringLiteral("color:  rgb(190, 219, 189);"));

        gridLayout_6->addWidget(radioOptimizaCercanos, 10, 1, 1, 1);

        editAumentarKg = new QLineEdit(groupBox_36);
        editAumentarKg->setObjectName(QStringLiteral("editAumentarKg"));
        sizePolicy3.setHeightForWidth(editAumentarKg->sizePolicy().hasHeightForWidth());
        editAumentarKg->setSizePolicy(sizePolicy3);
        editAumentarKg->setMaximumSize(QSize(12345, 16777215));
        editAumentarKg->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editAumentarKg->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editAumentarKg, 6, 1, 1, 1);

        radioOptimizaVacios = new QRadioButton(groupBox_36);
        radioOptimizaVacios->setObjectName(QStringLiteral("radioOptimizaVacios"));
        sizePolicy3.setHeightForWidth(radioOptimizaVacios->sizePolicy().hasHeightForWidth());
        radioOptimizaVacios->setSizePolicy(sizePolicy3);
        radioOptimizaVacios->setMinimumSize(QSize(0, 0));
        radioOptimizaVacios->setMaximumSize(QSize(12345, 16777215));
        radioOptimizaVacios->setLayoutDirection(Qt::RightToLeft);
        radioOptimizaVacios->setStyleSheet(QStringLiteral("color:  rgb(190, 219, 189);"));
        radioOptimizaVacios->setChecked(true);

        gridLayout_6->addWidget(radioOptimizaVacios, 10, 0, 1, 1);

        label_16 = new QLabel(groupBox_36);
        label_16->setObjectName(QStringLiteral("label_16"));
        sizePolicy3.setHeightForWidth(label_16->sizePolicy().hasHeightForWidth());
        label_16->setSizePolicy(sizePolicy3);
        label_16->setMaximumSize(QSize(1234, 16777215));
        label_16->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_16->setWordWrap(false);

        gridLayout_6->addWidget(label_16, 6, 0, 1, 1);

        editAumentarVol = new QLineEdit(groupBox_36);
        editAumentarVol->setObjectName(QStringLiteral("editAumentarVol"));
        sizePolicy3.setHeightForWidth(editAumentarVol->sizePolicy().hasHeightForWidth());
        editAumentarVol->setSizePolicy(sizePolicy3);
        editAumentarVol->setMaximumSize(QSize(12345, 16777215));
        editAumentarVol->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editAumentarVol->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editAumentarVol, 5, 1, 1, 1);

        label_15 = new QLabel(groupBox_36);
        label_15->setObjectName(QStringLiteral("label_15"));
        sizePolicy3.setHeightForWidth(label_15->sizePolicy().hasHeightForWidth());
        label_15->setSizePolicy(sizePolicy3);
        label_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_15->setWordWrap(false);

        gridLayout_6->addWidget(label_15, 5, 0, 1, 1);

        editAumentarVentana = new QLineEdit(groupBox_36);
        editAumentarVentana->setObjectName(QStringLiteral("editAumentarVentana"));
        sizePolicy3.setHeightForWidth(editAumentarVentana->sizePolicy().hasHeightForWidth());
        editAumentarVentana->setSizePolicy(sizePolicy3);
        editAumentarVentana->setMaximumSize(QSize(12345, 16777215));
        editAumentarVentana->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editAumentarVentana->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editAumentarVentana, 4, 1, 1, 1);

        label_13 = new QLabel(groupBox_36);
        label_13->setObjectName(QStringLiteral("label_13"));
        sizePolicy3.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy3);
        label_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_13->setWordWrap(false);

        gridLayout_6->addWidget(label_13, 4, 0, 1, 1);

        editAumentarValor = new QLineEdit(groupBox_36);
        editAumentarValor->setObjectName(QStringLiteral("editAumentarValor"));
        sizePolicy3.setHeightForWidth(editAumentarValor->sizePolicy().hasHeightForWidth());
        editAumentarValor->setSizePolicy(sizePolicy3);
        editAumentarValor->setMaximumSize(QSize(12345, 16777215));
        editAumentarValor->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editAumentarValor->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editAumentarValor, 3, 1, 1, 1);

        label_9 = new QLabel(groupBox_36);
        label_9->setObjectName(QStringLiteral("label_9"));
        sizePolicy3.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy3);
        label_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_9->setWordWrap(false);

        gridLayout_6->addWidget(label_9, 2, 0, 1, 1);

        editOcupacion = new QLineEdit(groupBox_36);
        editOcupacion->setObjectName(QStringLiteral("editOcupacion"));
        sizePolicy3.setHeightForWidth(editOcupacion->sizePolicy().hasHeightForWidth());
        editOcupacion->setSizePolicy(sizePolicy3);
        editOcupacion->setMaximumSize(QSize(12345, 16777215));
        editOcupacion->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editOcupacion->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editOcupacion, 1, 1, 1, 1);

        label_11 = new QLabel(groupBox_36);
        label_11->setObjectName(QStringLiteral("label_11"));
        sizePolicy3.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy3);
        label_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_11->setWordWrap(false);

        gridLayout_6->addWidget(label_11, 3, 0, 1, 1);

        editCercania = new QLineEdit(groupBox_36);
        editCercania->setObjectName(QStringLiteral("editCercania"));
        sizePolicy3.setHeightForWidth(editCercania->sizePolicy().hasHeightForWidth());
        editCercania->setSizePolicy(sizePolicy3);
        editCercania->setMaximumSize(QSize(12345, 16777215));
        editCercania->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
""));
        editCercania->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_6->addWidget(editCercania, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox_36);
        label_7->setObjectName(QStringLiteral("label_7"));
        sizePolicy3.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy3);
        label_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_7->setWordWrap(false);

        gridLayout_6->addWidget(label_7, 1, 0, 1, 1);

        btnCargaPlanOptiAuto = new QPushButton(groupBox_36);
        btnCargaPlanOptiAuto->setObjectName(QStringLiteral("btnCargaPlanOptiAuto"));
        sizePolicy3.setHeightForWidth(btnCargaPlanOptiAuto->sizePolicy().hasHeightForWidth());
        btnCargaPlanOptiAuto->setSizePolicy(sizePolicy3);
        btnCargaPlanOptiAuto->setMinimumSize(QSize(0, 0));
        btnCargaPlanOptiAuto->setMaximumSize(QSize(12345, 40));
        btnCargaPlanOptiAuto->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_6->addWidget(btnCargaPlanOptiAuto, 0, 0, 1, 2);

        btnOptimizarVaciosCercanos = new QPushButton(groupBox_36);
        btnOptimizarVaciosCercanos->setObjectName(QStringLiteral("btnOptimizarVaciosCercanos"));
        sizePolicy3.setHeightForWidth(btnOptimizarVaciosCercanos->sizePolicy().hasHeightForWidth());
        btnOptimizarVaciosCercanos->setSizePolicy(sizePolicy3);
        btnOptimizarVaciosCercanos->setMinimumSize(QSize(0, 0));
        btnOptimizarVaciosCercanos->setMaximumSize(QSize(12345, 40));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnOptimizarVaciosCercanos->setPalette(palette3);
        btnOptimizarVaciosCercanos->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_6->addWidget(btnOptimizarVaciosCercanos, 12, 0, 1, 2);

        checkIgnorarComp = new QCheckBox(groupBox_36);
        checkIgnorarComp->setObjectName(QStringLiteral("checkIgnorarComp"));
        sizePolicy3.setHeightForWidth(checkIgnorarComp->sizePolicy().hasHeightForWidth());
        checkIgnorarComp->setSizePolicy(sizePolicy3);
        checkIgnorarComp->setLayoutDirection(Qt::LeftToRight);
        checkIgnorarComp->setStyleSheet(QStringLiteral("color:  rgb(190, 219, 189);"));

        gridLayout_6->addWidget(checkIgnorarComp, 7, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer, 9, 0, 1, 1);

        checkIgnorarAcceso = new QCheckBox(groupBox_36);
        checkIgnorarAcceso->setObjectName(QStringLiteral("checkIgnorarAcceso"));
        sizePolicy3.setHeightForWidth(checkIgnorarAcceso->sizePolicy().hasHeightForWidth());
        checkIgnorarAcceso->setSizePolicy(sizePolicy3);
        checkIgnorarAcceso->setLayoutDirection(Qt::LeftToRight);
        checkIgnorarAcceso->setStyleSheet(QStringLiteral("color:  rgb(190, 219, 189);"));

        gridLayout_6->addWidget(checkIgnorarAcceso, 7, 1, 1, 1);


        gridLayout_13->addWidget(groupBox_36, 0, 1, 2, 1);

        horizontalSpacer_18 = new QSpacerItem(675, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_13->addItem(horizontalSpacer_18, 1, 2, 1, 2);

        groupBox_7 = new QGroupBox(tab2OptimizarAuto);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setStyleSheet(QLatin1String("color: white;\n"
"background:rgba(135, 171, 141,88);\n"
"border: none;\n"
"border-radius: 12px;\n"
"text-align: Left;\n"
""));
        gridLayout_69 = new QGridLayout(groupBox_7);
        gridLayout_69->setSpacing(6);
        gridLayout_69->setContentsMargins(11, 11, 11, 11);
        gridLayout_69->setObjectName(QStringLiteral("gridLayout_69"));
        lblTableroAutoHeaderM3 = new QLabel(groupBox_7);
        lblTableroAutoHeaderM3->setObjectName(QStringLiteral("lblTableroAutoHeaderM3"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderM3->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderM3->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderM3->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderM3->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderM3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderM3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderM3->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderM3, 0, 4, 1, 1);

        lblTableroAutoHeaderKg = new QLabel(groupBox_7);
        lblTableroAutoHeaderKg->setObjectName(QStringLiteral("lblTableroAutoHeaderKg"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderKg->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderKg->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderKg->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderKg->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderKg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderKg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderKg->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderKg, 0, 2, 1, 1);

        lblTableroAutoHeaderCostoViaje = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoViaje->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoViaje"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoViaje->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoViaje->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoViaje->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoViaje->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoViaje->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoViaje->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoViaje->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoViaje, 0, 8, 1, 1);

        lblTableroAutoHeaderPiezas = new QLabel(groupBox_7);
        lblTableroAutoHeaderPiezas->setObjectName(QStringLiteral("lblTableroAutoHeaderPiezas"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderPiezas->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderPiezas->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderPiezas->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderPiezas->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderPiezas->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderPiezas->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderPiezas->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderPiezas, 0, 3, 1, 1);

        lblTableroAutoHeaderPedidos = new QLabel(groupBox_7);
        lblTableroAutoHeaderPedidos->setObjectName(QStringLiteral("lblTableroAutoHeaderPedidos"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderPedidos->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderPedidos->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderPedidos->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderPedidos->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderPedidos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderPedidos->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderPedidos->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderPedidos, 0, 0, 1, 1);

        lblTableroAutoHeaderCostoDestino = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoDestino->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoDestino"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoDestino->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoDestino->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoDestino->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoDestino->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoDestino->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoDestino->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoDestino->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoDestino, 0, 9, 1, 1);

        lblTableroAutoHeaderCostoPieza = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoPieza->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoPieza"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoPieza->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoPieza->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoPieza->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoPieza->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoPieza->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoPieza->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoPieza->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoPieza, 0, 11, 1, 1);

        lblTableroAutoHeaderDestinos = new QLabel(groupBox_7);
        lblTableroAutoHeaderDestinos->setObjectName(QStringLiteral("lblTableroAutoHeaderDestinos"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderDestinos->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderDestinos->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderDestinos->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderDestinos->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderDestinos->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 155);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderDestinos->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderDestinos->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderDestinos, 0, 1, 1, 1);

        lblTableroAutoHeaderViajes = new QLabel(groupBox_7);
        lblTableroAutoHeaderViajes->setObjectName(QStringLiteral("lblTableroAutoHeaderViajes"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderViajes->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderViajes->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderViajes->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderViajes->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderViajes->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderViajes->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderViajes, 0, 5, 1, 1);

        lblTableroAutoHeaderTotalCosto = new QLabel(groupBox_7);
        lblTableroAutoHeaderTotalCosto->setObjectName(QStringLiteral("lblTableroAutoHeaderTotalCosto"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderTotalCosto->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderTotalCosto->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderTotalCosto->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderTotalCosto->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderTotalCosto->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderTotalCosto->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderTotalCosto, 0, 6, 1, 1);

        lblTableroAutoHeaderKm = new QLabel(groupBox_7);
        lblTableroAutoHeaderKm->setObjectName(QStringLiteral("lblTableroAutoHeaderKm"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderKm->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderKm->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderKm->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderKm->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderKm->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderKm->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderKm, 0, 7, 1, 1);

        lblTableroAutoHeaderCostoM3 = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoM3->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoM3"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoM3->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoM3->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoM3->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoM3->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoM3->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoM3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoM3->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoM3, 0, 13, 1, 1);

        lblTableroAutoHeaderCostoPedido = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoPedido->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoPedido"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoPedido->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoPedido->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoPedido->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoPedido->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoPedido->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoPedido->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoPedido->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoPedido, 0, 10, 1, 1);

        lblTableroAutoHeaderCostoKg = new QLabel(groupBox_7);
        lblTableroAutoHeaderCostoKg->setObjectName(QStringLiteral("lblTableroAutoHeaderCostoKg"));
        sizePolicy4.setHeightForWidth(lblTableroAutoHeaderCostoKg->sizePolicy().hasHeightForWidth());
        lblTableroAutoHeaderCostoKg->setSizePolicy(sizePolicy4);
        lblTableroAutoHeaderCostoKg->setMinimumSize(QSize(0, 50));
        lblTableroAutoHeaderCostoKg->setMaximumSize(QSize(16777215, 50));
        lblTableroAutoHeaderCostoKg->setStyleSheet(QLatin1String("color: rgba(200, 206, 152, 204);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 15pt \"Arial Narrow\";"));
        lblTableroAutoHeaderCostoKg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroAutoHeaderCostoKg->setIndent(0);

        gridLayout_69->addWidget(lblTableroAutoHeaderCostoKg, 0, 12, 1, 1);


        gridLayout_13->addWidget(groupBox_7, 2, 0, 1, 4);

        tableroAuto = new QTableWidget(tab2OptimizarAuto);
        if (tableroAuto->columnCount() < 31)
            tableroAuto->setColumnCount(31);
        QTableWidgetItem *__qtablewidgetitem40 = new QTableWidgetItem();
        __qtablewidgetitem40->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(0, __qtablewidgetitem40);
        QTableWidgetItem *__qtablewidgetitem41 = new QTableWidgetItem();
        __qtablewidgetitem41->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(1, __qtablewidgetitem41);
        QTableWidgetItem *__qtablewidgetitem42 = new QTableWidgetItem();
        __qtablewidgetitem42->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(2, __qtablewidgetitem42);
        QTableWidgetItem *__qtablewidgetitem43 = new QTableWidgetItem();
        __qtablewidgetitem43->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(3, __qtablewidgetitem43);
        QTableWidgetItem *__qtablewidgetitem44 = new QTableWidgetItem();
        __qtablewidgetitem44->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(4, __qtablewidgetitem44);
        QTableWidgetItem *__qtablewidgetitem45 = new QTableWidgetItem();
        __qtablewidgetitem45->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(5, __qtablewidgetitem45);
        QTableWidgetItem *__qtablewidgetitem46 = new QTableWidgetItem();
        __qtablewidgetitem46->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(6, __qtablewidgetitem46);
        QTableWidgetItem *__qtablewidgetitem47 = new QTableWidgetItem();
        __qtablewidgetitem47->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(7, __qtablewidgetitem47);
        QTableWidgetItem *__qtablewidgetitem48 = new QTableWidgetItem();
        __qtablewidgetitem48->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(8, __qtablewidgetitem48);
        QTableWidgetItem *__qtablewidgetitem49 = new QTableWidgetItem();
        __qtablewidgetitem49->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(9, __qtablewidgetitem49);
        QTableWidgetItem *__qtablewidgetitem50 = new QTableWidgetItem();
        __qtablewidgetitem50->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(10, __qtablewidgetitem50);
        QTableWidgetItem *__qtablewidgetitem51 = new QTableWidgetItem();
        __qtablewidgetitem51->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(11, __qtablewidgetitem51);
        QTableWidgetItem *__qtablewidgetitem52 = new QTableWidgetItem();
        __qtablewidgetitem52->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(12, __qtablewidgetitem52);
        QTableWidgetItem *__qtablewidgetitem53 = new QTableWidgetItem();
        __qtablewidgetitem53->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(13, __qtablewidgetitem53);
        QTableWidgetItem *__qtablewidgetitem54 = new QTableWidgetItem();
        __qtablewidgetitem54->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(14, __qtablewidgetitem54);
        QTableWidgetItem *__qtablewidgetitem55 = new QTableWidgetItem();
        __qtablewidgetitem55->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(15, __qtablewidgetitem55);
        QTableWidgetItem *__qtablewidgetitem56 = new QTableWidgetItem();
        __qtablewidgetitem56->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(16, __qtablewidgetitem56);
        QTableWidgetItem *__qtablewidgetitem57 = new QTableWidgetItem();
        __qtablewidgetitem57->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(17, __qtablewidgetitem57);
        QTableWidgetItem *__qtablewidgetitem58 = new QTableWidgetItem();
        __qtablewidgetitem58->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(18, __qtablewidgetitem58);
        QTableWidgetItem *__qtablewidgetitem59 = new QTableWidgetItem();
        __qtablewidgetitem59->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(19, __qtablewidgetitem59);
        QTableWidgetItem *__qtablewidgetitem60 = new QTableWidgetItem();
        __qtablewidgetitem60->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(20, __qtablewidgetitem60);
        QTableWidgetItem *__qtablewidgetitem61 = new QTableWidgetItem();
        __qtablewidgetitem61->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(21, __qtablewidgetitem61);
        QTableWidgetItem *__qtablewidgetitem62 = new QTableWidgetItem();
        __qtablewidgetitem62->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(22, __qtablewidgetitem62);
        QTableWidgetItem *__qtablewidgetitem63 = new QTableWidgetItem();
        __qtablewidgetitem63->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(23, __qtablewidgetitem63);
        QTableWidgetItem *__qtablewidgetitem64 = new QTableWidgetItem();
        __qtablewidgetitem64->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(24, __qtablewidgetitem64);
        QTableWidgetItem *__qtablewidgetitem65 = new QTableWidgetItem();
        __qtablewidgetitem65->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(25, __qtablewidgetitem65);
        QTableWidgetItem *__qtablewidgetitem66 = new QTableWidgetItem();
        __qtablewidgetitem66->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(26, __qtablewidgetitem66);
        QTableWidgetItem *__qtablewidgetitem67 = new QTableWidgetItem();
        __qtablewidgetitem67->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(27, __qtablewidgetitem67);
        QTableWidgetItem *__qtablewidgetitem68 = new QTableWidgetItem();
        __qtablewidgetitem68->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(28, __qtablewidgetitem68);
        QTableWidgetItem *__qtablewidgetitem69 = new QTableWidgetItem();
        __qtablewidgetitem69->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(29, __qtablewidgetitem69);
        QTableWidgetItem *__qtablewidgetitem70 = new QTableWidgetItem();
        __qtablewidgetitem70->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setHorizontalHeaderItem(30, __qtablewidgetitem70);
        if (tableroAuto->rowCount() < 3)
            tableroAuto->setRowCount(3);
        QTableWidgetItem *__qtablewidgetitem71 = new QTableWidgetItem();
        __qtablewidgetitem71->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroAuto->setVerticalHeaderItem(0, __qtablewidgetitem71);
        QTableWidgetItem *__qtablewidgetitem72 = new QTableWidgetItem();
        __qtablewidgetitem72->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroAuto->setVerticalHeaderItem(1, __qtablewidgetitem72);
        QTableWidgetItem *__qtablewidgetitem73 = new QTableWidgetItem();
        __qtablewidgetitem73->setTextAlignment(Qt::AlignTrailing|Qt::AlignVCenter);
        tableroAuto->setVerticalHeaderItem(2, __qtablewidgetitem73);
        QTableWidgetItem *__qtablewidgetitem74 = new QTableWidgetItem();
        __qtablewidgetitem74->setFlags(Qt::ItemIsDragEnabled|Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
        tableroAuto->setItem(0, 0, __qtablewidgetitem74);
        QTableWidgetItem *__qtablewidgetitem75 = new QTableWidgetItem();
        __qtablewidgetitem75->setTextAlignment(Qt::AlignCenter);
        tableroAuto->setItem(0, 1, __qtablewidgetitem75);
        QTableWidgetItem *__qtablewidgetitem76 = new QTableWidgetItem();
        tableroAuto->setItem(1, 0, __qtablewidgetitem76);
        QTableWidgetItem *__qtablewidgetitem77 = new QTableWidgetItem();
        tableroAuto->setItem(1, 1, __qtablewidgetitem77);
        QTableWidgetItem *__qtablewidgetitem78 = new QTableWidgetItem();
        tableroAuto->setItem(2, 0, __qtablewidgetitem78);
        QTableWidgetItem *__qtablewidgetitem79 = new QTableWidgetItem();
        tableroAuto->setItem(2, 1, __qtablewidgetitem79);
        tableroAuto->setObjectName(QStringLiteral("tableroAuto"));
        sizePolicy5.setHeightForWidth(tableroAuto->sizePolicy().hasHeightForWidth());
        tableroAuto->setSizePolicy(sizePolicy5);
        tableroAuto->setMinimumSize(QSize(0, 0));
        tableroAuto->setMaximumSize(QSize(12345, 95));
        tableroAuto->setFont(font);
        tableroAuto->setMouseTracking(false);
        tableroAuto->setAcceptDrops(false);
        tableroAuto->setToolTipDuration(2);
        tableroAuto->setStyleSheet(QLatin1String("background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
""));
        tableroAuto->setFrameShape(QFrame::Panel);
        tableroAuto->setFrameShadow(QFrame::Raised);
        tableroAuto->setLineWidth(0);
        tableroAuto->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        tableroAuto->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        tableroAuto->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        tableroAuto->setAutoScroll(false);
        tableroAuto->setEditTriggers(QAbstractItemView::AllEditTriggers);
        tableroAuto->setTabKeyNavigation(false);
        tableroAuto->setProperty("showDropIndicator", QVariant(false));
        tableroAuto->setDragEnabled(false);
        tableroAuto->setDragDropOverwriteMode(false);
        tableroAuto->setDragDropMode(QAbstractItemView::NoDragDrop);
        tableroAuto->setDefaultDropAction(Qt::IgnoreAction);
        tableroAuto->setAlternatingRowColors(false);
        tableroAuto->setSelectionMode(QAbstractItemView::NoSelection);
        tableroAuto->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableroAuto->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
        tableroAuto->setHorizontalScrollMode(QAbstractItemView::ScrollPerItem);
        tableroAuto->setShowGrid(true);
        tableroAuto->setGridStyle(Qt::DotLine);
        tableroAuto->setSortingEnabled(false);
        tableroAuto->setWordWrap(false);
        tableroAuto->setCornerButtonEnabled(false);
        tableroAuto->setRowCount(3);
        tableroAuto->setColumnCount(31);
        tableroAuto->horizontalHeader()->setDefaultSectionSize(80);
        tableroAuto->horizontalHeader()->setMinimumSectionSize(19);
        tableroAuto->verticalHeader()->setVisible(false);
        tableroAuto->verticalHeader()->setDefaultSectionSize(15);
        tableroAuto->verticalHeader()->setMinimumSectionSize(15);
        tableroAuto->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_13->addWidget(tableroAuto, 3, 0, 1, 3);

        frame_3 = new QFrame(tab2OptimizarAuto);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        QSizePolicy sizePolicy9(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(frame_3->sizePolicy().hasHeightForWidth());
        frame_3->setSizePolicy(sizePolicy9);
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_53 = new QGridLayout(frame_3);
        gridLayout_53->setSpacing(6);
        gridLayout_53->setContentsMargins(11, 11, 11, 11);
        gridLayout_53->setObjectName(QStringLiteral("gridLayout_53"));
        btnOptimizaPlan_2 = new QPushButton(frame_3);
        btnOptimizaPlan_2->setObjectName(QStringLiteral("btnOptimizaPlan_2"));
        sizePolicy3.setHeightForWidth(btnOptimizaPlan_2->sizePolicy().hasHeightForWidth());
        btnOptimizaPlan_2->setSizePolicy(sizePolicy3);
        btnOptimizaPlan_2->setMinimumSize(QSize(48, 48));
        btnOptimizaPlan_2->setMaximumSize(QSize(48, 48));
        btnOptimizaPlan_2->setStyleSheet(QStringLiteral("background:transparent;"));
        btnOptimizaPlan_2->setIcon(icon1);
        btnOptimizaPlan_2->setIconSize(QSize(32, 32));
        btnOptimizaPlan_2->setAutoDefault(true);

        gridLayout_53->addWidget(btnOptimizaPlan_2, 0, 0, 1, 1);

        btnPropagaAuto = new QPushButton(frame_3);
        btnPropagaAuto->setObjectName(QStringLiteral("btnPropagaAuto"));
        sizePolicy2.setHeightForWidth(btnPropagaAuto->sizePolicy().hasHeightForWidth());
        btnPropagaAuto->setSizePolicy(sizePolicy2);
        btnPropagaAuto->setMinimumSize(QSize(48, 48));
        btnPropagaAuto->setMaximumSize(QSize(48, 48));
        btnPropagaAuto->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/IMG5/4actualizar.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnPropagaAuto->setIcon(icon7);
        btnPropagaAuto->setIconSize(QSize(32, 32));
        btnPropagaAuto->setAutoDefault(true);

        gridLayout_53->addWidget(btnPropagaAuto, 0, 1, 1, 1);

        btnRefrescaAuto = new QPushButton(frame_3);
        btnRefrescaAuto->setObjectName(QStringLiteral("btnRefrescaAuto"));
        sizePolicy3.setHeightForWidth(btnRefrescaAuto->sizePolicy().hasHeightForWidth());
        btnRefrescaAuto->setSizePolicy(sizePolicy3);
        btnRefrescaAuto->setMinimumSize(QSize(48, 48));
        btnRefrescaAuto->setMaximumSize(QSize(48, 48));
        btnRefrescaAuto->setStyleSheet(QStringLiteral("background:transparent;"));
        btnRefrescaAuto->setIcon(icon5);
        btnRefrescaAuto->setIconSize(QSize(32, 32));
        btnRefrescaAuto->setAutoDefault(true);

        gridLayout_53->addWidget(btnRefrescaAuto, 0, 2, 1, 1);

        btnGuardaAuto = new QPushButton(frame_3);
        btnGuardaAuto->setObjectName(QStringLiteral("btnGuardaAuto"));
        sizePolicy3.setHeightForWidth(btnGuardaAuto->sizePolicy().hasHeightForWidth());
        btnGuardaAuto->setSizePolicy(sizePolicy3);
        btnGuardaAuto->setMinimumSize(QSize(48, 48));
        btnGuardaAuto->setMaximumSize(QSize(48, 48));
        btnGuardaAuto->setStyleSheet(QStringLiteral("background:transparent;"));
        btnGuardaAuto->setIcon(icon2);
        btnGuardaAuto->setIconSize(QSize(32, 32));
        btnGuardaAuto->setAutoDefault(true);

        gridLayout_53->addWidget(btnGuardaAuto, 0, 3, 1, 1);


        gridLayout_13->addWidget(frame_3, 3, 3, 1, 1);

        tab2Optimizar->addTab(tab2OptimizarAuto, QString());
        tab2OptimizarManual = new QWidget();
        tab2OptimizarManual->setObjectName(QStringLiteral("tab2OptimizarManual"));
        sizePolicy1.setHeightForWidth(tab2OptimizarManual->sizePolicy().hasHeightForWidth());
        tab2OptimizarManual->setSizePolicy(sizePolicy1);
        horizontalLayout_6 = new QHBoxLayout(tab2OptimizarManual);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        groupBox_28 = new QGroupBox(tab2OptimizarManual);
        groupBox_28->setObjectName(QStringLiteral("groupBox_28"));
        QSizePolicy sizePolicy10(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy10.setHorizontalStretch(0);
        sizePolicy10.setVerticalStretch(0);
        sizePolicy10.setHeightForWidth(groupBox_28->sizePolicy().hasHeightForWidth());
        groupBox_28->setSizePolicy(sizePolicy10);
        groupBox_28->setMinimumSize(QSize(0, 0));
        groupBox_28->setMaximumSize(QSize(12345, 12345));
        gridLayout_73 = new QGridLayout(groupBox_28);
        gridLayout_73->setSpacing(6);
        gridLayout_73->setContentsMargins(11, 11, 11, 11);
        gridLayout_73->setObjectName(QStringLiteral("gridLayout_73"));
        groupBox_8 = new QGroupBox(groupBox_28);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        sizePolicy4.setHeightForWidth(groupBox_8->sizePolicy().hasHeightForWidth());
        groupBox_8->setSizePolicy(sizePolicy4);
        groupBox_8->setStyleSheet(QLatin1String("background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
""));
        gridLayout_72 = new QGridLayout(groupBox_8);
        gridLayout_72->setSpacing(6);
        gridLayout_72->setContentsMargins(11, 11, 11, 11);
        gridLayout_72->setObjectName(QStringLiteral("gridLayout_72"));
        lblTableroManualHeaderCostoM3 = new QLabel(groupBox_8);
        lblTableroManualHeaderCostoM3->setObjectName(QStringLiteral("lblTableroManualHeaderCostoM3"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderCostoM3->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderCostoM3->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderCostoM3->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderCostoM3->setMaximumSize(QSize(16777215, 50));
        lblTableroManualHeaderCostoM3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderCostoM3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderCostoM3->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderCostoM3, 0, 5, 1, 1);

        lblTableroManualHeaderCostoKg = new QLabel(groupBox_8);
        lblTableroManualHeaderCostoKg->setObjectName(QStringLiteral("lblTableroManualHeaderCostoKg"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderCostoKg->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderCostoKg->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderCostoKg->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderCostoKg->setMaximumSize(QSize(16777215, 50));
        lblTableroManualHeaderCostoKg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderCostoKg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderCostoKg->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderCostoKg, 0, 4, 1, 1);

        lblTableroManualHeaderTotalCosto = new QLabel(groupBox_8);
        lblTableroManualHeaderTotalCosto->setObjectName(QStringLiteral("lblTableroManualHeaderTotalCosto"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderTotalCosto->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderTotalCosto->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderTotalCosto->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderTotalCosto->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderTotalCosto->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderTotalCosto->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderTotalCosto, 0, 0, 1, 1);

        lblTableroManualHeaderKm = new QLabel(groupBox_8);
        lblTableroManualHeaderKm->setObjectName(QStringLiteral("lblTableroManualHeaderKm"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderKm->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderKm->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderKm->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderKm->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderKm->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderKm->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderKm, 0, 1, 1, 1);

        lblTableroManualHeaderCostoViaje = new QLabel(groupBox_8);
        lblTableroManualHeaderCostoViaje->setObjectName(QStringLiteral("lblTableroManualHeaderCostoViaje"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderCostoViaje->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderCostoViaje->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderCostoViaje->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderCostoViaje->setMaximumSize(QSize(16777215, 50));
        lblTableroManualHeaderCostoViaje->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderCostoViaje->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderCostoViaje->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderCostoViaje, 0, 2, 1, 1);

        lblTableroManualHeaderCostoPieza = new QLabel(groupBox_8);
        lblTableroManualHeaderCostoPieza->setObjectName(QStringLiteral("lblTableroManualHeaderCostoPieza"));
        sizePolicy4.setHeightForWidth(lblTableroManualHeaderCostoPieza->sizePolicy().hasHeightForWidth());
        lblTableroManualHeaderCostoPieza->setSizePolicy(sizePolicy4);
        lblTableroManualHeaderCostoPieza->setMinimumSize(QSize(0, 50));
        lblTableroManualHeaderCostoPieza->setMaximumSize(QSize(16777215, 50));
        lblTableroManualHeaderCostoPieza->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblTableroManualHeaderCostoPieza->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        lblTableroManualHeaderCostoPieza->setIndent(0);

        gridLayout_72->addWidget(lblTableroManualHeaderCostoPieza, 0, 3, 1, 1);


        gridLayout_73->addWidget(groupBox_8, 2, 0, 1, 1);

        frame_2 = new QFrame(groupBox_28);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        sizePolicy5.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy5);
        frame_2->setMinimumSize(QSize(0, 0));
        frame_2->setMaximumSize(QSize(12345, 66));
        frame_2->setStyleSheet(QLatin1String("padding: 0px;\n"
"margin: 0px;\n"
""));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_31 = new QGridLayout(frame_2);
        gridLayout_31->setSpacing(0);
        gridLayout_31->setContentsMargins(11, 11, 11, 11);
        gridLayout_31->setObjectName(QStringLiteral("gridLayout_31"));
        gridLayout_31->setContentsMargins(0, 0, 0, 0);
        btnRefrescaManual = new QPushButton(frame_2);
        btnRefrescaManual->setObjectName(QStringLiteral("btnRefrescaManual"));
        sizePolicy6.setHeightForWidth(btnRefrescaManual->sizePolicy().hasHeightForWidth());
        btnRefrescaManual->setSizePolicy(sizePolicy6);
        btnRefrescaManual->setMinimumSize(QSize(0, 0));
        btnRefrescaManual->setMaximumSize(QSize(30, 30));
        btnRefrescaManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnRefrescaManual->setIcon(icon5);
        btnRefrescaManual->setIconSize(QSize(20, 20));
        btnRefrescaManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnRefrescaManual, 0, 5, 1, 1);

        btnGuardaManual = new QPushButton(frame_2);
        btnGuardaManual->setObjectName(QStringLiteral("btnGuardaManual"));
        sizePolicy6.setHeightForWidth(btnGuardaManual->sizePolicy().hasHeightForWidth());
        btnGuardaManual->setSizePolicy(sizePolicy6);
        btnGuardaManual->setMinimumSize(QSize(0, 0));
        btnGuardaManual->setMaximumSize(QSize(30, 30));
        btnGuardaManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnGuardaManual->setIcon(icon2);
        btnGuardaManual->setIconSize(QSize(20, 20));
        btnGuardaManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnGuardaManual, 0, 7, 1, 1);

        btnColorVManual = new QPushButton(frame_2);
        btnColorVManual->setObjectName(QStringLiteral("btnColorVManual"));
        sizePolicy6.setHeightForWidth(btnColorVManual->sizePolicy().hasHeightForWidth());
        btnColorVManual->setSizePolicy(sizePolicy6);
        btnColorVManual->setMinimumSize(QSize(0, 0));
        btnColorVManual->setMaximumSize(QSize(30, 30));
        btnColorVManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnColorVManual->setIcon(icon6);
        btnColorVManual->setIconSize(QSize(20, 20));
        btnColorVManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnColorVManual, 0, 2, 1, 1);

        btnColorDManual = new QPushButton(frame_2);
        btnColorDManual->setObjectName(QStringLiteral("btnColorDManual"));
        sizePolicy6.setHeightForWidth(btnColorDManual->sizePolicy().hasHeightForWidth());
        btnColorDManual->setSizePolicy(sizePolicy6);
        btnColorDManual->setMinimumSize(QSize(0, 0));
        btnColorDManual->setMaximumSize(QSize(30, 30));
        btnColorDManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnColorDManual->setIcon(icon3);
        btnColorDManual->setIconSize(QSize(20, 20));
        btnColorDManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnColorDManual, 0, 3, 1, 1);

        btnColorSinManual = new QPushButton(frame_2);
        btnColorSinManual->setObjectName(QStringLiteral("btnColorSinManual"));
        sizePolicy6.setHeightForWidth(btnColorSinManual->sizePolicy().hasHeightForWidth());
        btnColorSinManual->setSizePolicy(sizePolicy6);
        btnColorSinManual->setMinimumSize(QSize(0, 0));
        btnColorSinManual->setMaximumSize(QSize(30, 30));
        btnColorSinManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnColorSinManual->setIcon(icon4);
        btnColorSinManual->setIconSize(QSize(20, 20));
        btnColorSinManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnColorSinManual, 0, 4, 1, 1);

        btnPropagaManual = new QPushButton(frame_2);
        btnPropagaManual->setObjectName(QStringLiteral("btnPropagaManual"));
        sizePolicy6.setHeightForWidth(btnPropagaManual->sizePolicy().hasHeightForWidth());
        btnPropagaManual->setSizePolicy(sizePolicy6);
        btnPropagaManual->setMinimumSize(QSize(0, 0));
        btnPropagaManual->setMaximumSize(QSize(30, 30));
        btnPropagaManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnPropagaManual->setIcon(icon7);
        btnPropagaManual->setIconSize(QSize(20, 20));
        btnPropagaManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnPropagaManual, 0, 6, 1, 1);

        btnOptimizaManual = new QPushButton(frame_2);
        btnOptimizaManual->setObjectName(QStringLiteral("btnOptimizaManual"));
        sizePolicy6.setHeightForWidth(btnOptimizaManual->sizePolicy().hasHeightForWidth());
        btnOptimizaManual->setSizePolicy(sizePolicy6);
        btnOptimizaManual->setMinimumSize(QSize(0, 0));
        btnOptimizaManual->setMaximumSize(QSize(30, 30));
        btnOptimizaManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnOptimizaManual->setIcon(icon1);
        btnOptimizaManual->setIconSize(QSize(20, 20));
        btnOptimizaManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnOptimizaManual, 0, 1, 1, 1);

        btnRecalculaManual = new QPushButton(frame_2);
        btnRecalculaManual->setObjectName(QStringLiteral("btnRecalculaManual"));
        sizePolicy6.setHeightForWidth(btnRecalculaManual->sizePolicy().hasHeightForWidth());
        btnRecalculaManual->setSizePolicy(sizePolicy6);
        btnRecalculaManual->setMinimumSize(QSize(0, 0));
        btnRecalculaManual->setMaximumSize(QSize(30, 30));
        btnRecalculaManual->setStyleSheet(QStringLiteral("background:transparent;"));
        btnRecalculaManual->setIcon(icon);
        btnRecalculaManual->setIconSize(QSize(20, 20));
        btnRecalculaManual->setAutoDefault(true);

        gridLayout_31->addWidget(btnRecalculaManual, 0, 0, 1, 1);


        gridLayout_73->addWidget(frame_2, 3, 0, 1, 1);

        pushButton_2 = new QPushButton(groupBox_28);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        sizePolicy3.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy3);
        pushButton_2->setMaximumSize(QSize(12345, 40));
        pushButton_2->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_73->addWidget(pushButton_2, 0, 0, 1, 1);

        layoutPlan = new QGridLayout();
        layoutPlan->setSpacing(0);
        layoutPlan->setObjectName(QStringLiteral("layoutPlan"));
        layoutPlan->setSizeConstraint(QLayout::SetDefaultConstraint);

        gridLayout_73->addLayout(layoutPlan, 1, 0, 1, 1);


        horizontalLayout_5->addWidget(groupBox_28);

        groupBox_26 = new QGroupBox(tab2OptimizarManual);
        groupBox_26->setObjectName(QStringLiteral("groupBox_26"));
        sizePolicy9.setHeightForWidth(groupBox_26->sizePolicy().hasHeightForWidth());
        groupBox_26->setSizePolicy(sizePolicy9);
        groupBox_26->setMinimumSize(QSize(0, 0));
        groupBox_26->setMaximumSize(QSize(600, 12345));
        gridLayout_70 = new QGridLayout(groupBox_26);
        gridLayout_70->setSpacing(6);
        gridLayout_70->setContentsMargins(11, 11, 11, 11);
        gridLayout_70->setObjectName(QStringLiteral("gridLayout_70"));
        layoutV1 = new QGridLayout();
        layoutV1->setSpacing(0);
        layoutV1->setObjectName(QStringLiteral("layoutV1"));

        gridLayout_70->addLayout(layoutV1, 0, 0, 1, 2);

        frame_8 = new QFrame(groupBox_26);
        frame_8->setObjectName(QStringLiteral("frame_8"));
        sizePolicy5.setHeightForWidth(frame_8->sizePolicy().hasHeightForWidth());
        frame_8->setSizePolicy(sizePolicy5);
        frame_8->setMinimumSize(QSize(0, 0));
        frame_8->setMaximumSize(QSize(16777215, 66));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        gridLayout_14 = new QGridLayout(frame_8);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        gridLayout_14->setContentsMargins(0, 0, 0, 0);
        lblV1Acceso = new QLabel(frame_8);
        lblV1Acceso->setObjectName(QStringLiteral("lblV1Acceso"));
        sizePolicy4.setHeightForWidth(lblV1Acceso->sizePolicy().hasHeightForWidth());
        lblV1Acceso->setSizePolicy(sizePolicy4);
        lblV1Acceso->setMinimumSize(QSize(0, 0));
        lblV1Acceso->setMaximumSize(QSize(12345, 20));
        lblV1Acceso->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1Acceso->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1Acceso, 2, 1, 1, 1);

        lblV1M3 = new QLabel(frame_8);
        lblV1M3->setObjectName(QStringLiteral("lblV1M3"));
        sizePolicy4.setHeightForWidth(lblV1M3->sizePolicy().hasHeightForWidth());
        lblV1M3->setSizePolicy(sizePolicy4);
        lblV1M3->setMinimumSize(QSize(0, 0));
        lblV1M3->setMaximumSize(QSize(12345, 24));
        lblV1M3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1M3->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1M3, 1, 1, 1, 1);

        lblV1Kg = new QLabel(frame_8);
        lblV1Kg->setObjectName(QStringLiteral("lblV1Kg"));
        sizePolicy4.setHeightForWidth(lblV1Kg->sizePolicy().hasHeightForWidth());
        lblV1Kg->setSizePolicy(sizePolicy4);
        lblV1Kg->setMinimumSize(QSize(0, 0));
        lblV1Kg->setMaximumSize(QSize(12345, 24));
        lblV1Kg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1Kg->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1Kg, 1, 0, 1, 1);

        lblV1Hrs = new QLabel(frame_8);
        lblV1Hrs->setObjectName(QStringLiteral("lblV1Hrs"));
        sizePolicy4.setHeightForWidth(lblV1Hrs->sizePolicy().hasHeightForWidth());
        lblV1Hrs->setSizePolicy(sizePolicy4);
        lblV1Hrs->setMinimumSize(QSize(0, 0));
        lblV1Hrs->setMaximumSize(QSize(12345, 24));
        lblV1Hrs->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1Hrs->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1Hrs, 2, 0, 1, 1);

        lblV1Comp = new QLabel(frame_8);
        lblV1Comp->setObjectName(QStringLiteral("lblV1Comp"));
        sizePolicy4.setHeightForWidth(lblV1Comp->sizePolicy().hasHeightForWidth());
        lblV1Comp->setSizePolicy(sizePolicy4);
        lblV1Comp->setMinimumSize(QSize(0, 0));
        lblV1Comp->setMaximumSize(QSize(12345, 20));
        lblV1Comp->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1Comp->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1Comp, 2, 2, 1, 1);

        lblV1Valor = new QLabel(frame_8);
        lblV1Valor->setObjectName(QStringLiteral("lblV1Valor"));
        sizePolicy4.setHeightForWidth(lblV1Valor->sizePolicy().hasHeightForWidth());
        lblV1Valor->setSizePolicy(sizePolicy4);
        lblV1Valor->setMinimumSize(QSize(0, 0));
        lblV1Valor->setMaximumSize(QSize(12345, 24));
        lblV1Valor->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV1Valor->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(lblV1Valor, 1, 2, 1, 1);


        gridLayout_70->addWidget(frame_8, 1, 0, 1, 1);

        frame_7 = new QFrame(groupBox_26);
        frame_7->setObjectName(QStringLiteral("frame_7"));
        sizePolicy5.setHeightForWidth(frame_7->sizePolicy().hasHeightForWidth());
        frame_7->setSizePolicy(sizePolicy5);
        frame_7->setMinimumSize(QSize(0, 0));
        frame_7->setMaximumSize(QSize(16777215, 66));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_7);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_17 = new QLabel(frame_7);
        label_17->setObjectName(QStringLiteral("label_17"));
        sizePolicy4.setHeightForWidth(label_17->sizePolicy().hasHeightForWidth());
        label_17->setSizePolicy(sizePolicy4);
        label_17->setMinimumSize(QSize(0, 0));
        label_17->setMaximumSize(QSize(1234, 30));
        label_17->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_17->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_17, 0, 8, 1, 1);

        label_14 = new QLabel(frame_7);
        label_14->setObjectName(QStringLiteral("label_14"));
        sizePolicy4.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy4);
        label_14->setMinimumSize(QSize(0, 0));
        label_14->setMaximumSize(QSize(1234, 30));
        label_14->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_14->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_14, 0, 6, 1, 1);

        label_12 = new QLabel(frame_7);
        label_12->setObjectName(QStringLiteral("label_12"));
        sizePolicy4.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy4);
        label_12->setMinimumSize(QSize(0, 0));
        label_12->setMaximumSize(QSize(1234, 30));
        label_12->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_12->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_12, 0, 4, 1, 1);

        label_10 = new QLabel(frame_7);
        label_10->setObjectName(QStringLiteral("label_10"));
        sizePolicy4.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy4);
        label_10->setMinimumSize(QSize(0, 0));
        label_10->setMaximumSize(QSize(1234, 30));
        label_10->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_10, 0, 2, 1, 1);

        label_8 = new QLabel(frame_7);
        label_8->setObjectName(QStringLiteral("label_8"));
        sizePolicy4.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy4);
        label_8->setMinimumSize(QSize(0, 0));
        label_8->setMaximumSize(QSize(1234, 30));
        label_8->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_8, 0, 0, 1, 1);

        editV1MaxKg = new QLineEdit(frame_7);
        editV1MaxKg->setObjectName(QStringLiteral("editV1MaxKg"));
        sizePolicy4.setHeightForWidth(editV1MaxKg->sizePolicy().hasHeightForWidth());
        editV1MaxKg->setSizePolicy(sizePolicy4);
        editV1MaxKg->setMinimumSize(QSize(0, 0));
        editV1MaxKg->setMaximumSize(QSize(53, 55));
        editV1MaxKg->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV1MaxKg->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(editV1MaxKg, 1, 0, 1, 1);

        editV1MaxM3 = new QLineEdit(frame_7);
        editV1MaxM3->setObjectName(QStringLiteral("editV1MaxM3"));
        sizePolicy4.setHeightForWidth(editV1MaxM3->sizePolicy().hasHeightForWidth());
        editV1MaxM3->setSizePolicy(sizePolicy4);
        editV1MaxM3->setMinimumSize(QSize(0, 0));
        editV1MaxM3->setMaximumSize(QSize(53, 22));
        editV1MaxM3->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV1MaxM3->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(editV1MaxM3, 1, 2, 1, 1);

        editV1MaxValor = new QLineEdit(frame_7);
        editV1MaxValor->setObjectName(QStringLiteral("editV1MaxValor"));
        sizePolicy4.setHeightForWidth(editV1MaxValor->sizePolicy().hasHeightForWidth());
        editV1MaxValor->setSizePolicy(sizePolicy4);
        editV1MaxValor->setMinimumSize(QSize(0, 0));
        editV1MaxValor->setMaximumSize(QSize(53, 22));
        editV1MaxValor->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV1MaxValor->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(editV1MaxValor, 1, 4, 1, 1);

        editV1Tipo = new QLineEdit(frame_7);
        editV1Tipo->setObjectName(QStringLiteral("editV1Tipo"));
        sizePolicy4.setHeightForWidth(editV1Tipo->sizePolicy().hasHeightForWidth());
        editV1Tipo->setSizePolicy(sizePolicy4);
        editV1Tipo->setMinimumSize(QSize(0, 0));
        editV1Tipo->setMaximumSize(QSize(53, 55));
        editV1Tipo->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV1Tipo->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(editV1Tipo, 1, 6, 1, 1);

        editV1NumViaje = new QLineEdit(frame_7);
        editV1NumViaje->setObjectName(QStringLiteral("editV1NumViaje"));
        sizePolicy4.setHeightForWidth(editV1NumViaje->sizePolicy().hasHeightForWidth());
        editV1NumViaje->setSizePolicy(sizePolicy4);
        editV1NumViaje->setMinimumSize(QSize(0, 0));
        editV1NumViaje->setMaximumSize(QSize(53, 22));
        editV1NumViaje->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV1NumViaje->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(editV1NumViaje, 1, 8, 1, 1);


        gridLayout_70->addWidget(frame_7, 1, 1, 1, 1);

        frame_5 = new QFrame(groupBox_26);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        sizePolicy5.setHeightForWidth(frame_5->sizePolicy().hasHeightForWidth());
        frame_5->setSizePolicy(sizePolicy5);
        frame_5->setMinimumSize(QSize(0, 0));
        frame_5->setMaximumSize(QSize(16777215, 66));
        frame_5->setStyleSheet(QLatin1String("QFrame\n"
"{\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
"}\n"
""));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        gridLayout_17 = new QGridLayout(frame_5);
        gridLayout_17->setSpacing(0);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        gridLayout_17->setContentsMargins(0, 0, 0, 0);
        btnV1XG = new QPushButton(frame_5);
        btnV1XG->setObjectName(QStringLiteral("btnV1XG"));
        sizePolicy3.setHeightForWidth(btnV1XG->sizePolicy().hasHeightForWidth());
        btnV1XG->setSizePolicy(sizePolicy3);
        btnV1XG->setMinimumSize(QSize(0, 0));
        btnV1XG->setMaximumSize(QSize(30, 30));
        btnV1XG->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/IMG5/4xg.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1XG->setIcon(icon8);
        btnV1XG->setIconSize(QSize(20, 20));
        btnV1XG->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1XG, 0, 7, 1, 1);

        btnV1ColorV = new QPushButton(frame_5);
        btnV1ColorV->setObjectName(QStringLiteral("btnV1ColorV"));
        sizePolicy3.setHeightForWidth(btnV1ColorV->sizePolicy().hasHeightForWidth());
        btnV1ColorV->setSizePolicy(sizePolicy3);
        btnV1ColorV->setMinimumSize(QSize(0, 0));
        btnV1ColorV->setMaximumSize(QSize(30, 30));
        btnV1ColorV->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV1ColorV->setIcon(icon6);
        btnV1ColorV->setIconSize(QSize(20, 20));
        btnV1ColorV->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1ColorV, 0, 2, 1, 1);

        btnV1Optimiza = new QPushButton(frame_5);
        btnV1Optimiza->setObjectName(QStringLiteral("btnV1Optimiza"));
        sizePolicy3.setHeightForWidth(btnV1Optimiza->sizePolicy().hasHeightForWidth());
        btnV1Optimiza->setSizePolicy(sizePolicy3);
        btnV1Optimiza->setMinimumSize(QSize(0, 0));
        btnV1Optimiza->setMaximumSize(QSize(30, 30));
        btnV1Optimiza->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV1Optimiza->setIcon(icon1);
        btnV1Optimiza->setIconSize(QSize(20, 20));
        btnV1Optimiza->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1Optimiza, 0, 1, 1, 1);

        btnV1Recalcula = new QPushButton(frame_5);
        btnV1Recalcula->setObjectName(QStringLiteral("btnV1Recalcula"));
        sizePolicy3.setHeightForWidth(btnV1Recalcula->sizePolicy().hasHeightForWidth());
        btnV1Recalcula->setSizePolicy(sizePolicy3);
        btnV1Recalcula->setMinimumSize(QSize(0, 0));
        btnV1Recalcula->setMaximumSize(QSize(30, 30));
        btnV1Recalcula->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV1Recalcula->setIcon(icon);
        btnV1Recalcula->setIconSize(QSize(20, 20));
        btnV1Recalcula->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1Recalcula, 0, 0, 1, 1);

        btnV1ColorD = new QPushButton(frame_5);
        btnV1ColorD->setObjectName(QStringLiteral("btnV1ColorD"));
        sizePolicy3.setHeightForWidth(btnV1ColorD->sizePolicy().hasHeightForWidth());
        btnV1ColorD->setSizePolicy(sizePolicy3);
        btnV1ColorD->setMinimumSize(QSize(0, 0));
        btnV1ColorD->setMaximumSize(QSize(30, 30));
        btnV1ColorD->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV1ColorD->setIcon(icon3);
        btnV1ColorD->setIconSize(QSize(20, 20));
        btnV1ColorD->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1ColorD, 0, 3, 1, 1);

        btnV1CH = new QPushButton(frame_5);
        btnV1CH->setObjectName(QStringLiteral("btnV1CH"));
        sizePolicy3.setHeightForWidth(btnV1CH->sizePolicy().hasHeightForWidth());
        btnV1CH->setSizePolicy(sizePolicy3);
        btnV1CH->setMinimumSize(QSize(0, 0));
        btnV1CH->setMaximumSize(QSize(30, 30));
        btnV1CH->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/IMG5/4s.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1CH->setIcon(icon9);
        btnV1CH->setIconSize(QSize(20, 20));
        btnV1CH->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1CH, 0, 10, 1, 1);

        btnV1M = new QPushButton(frame_5);
        btnV1M->setObjectName(QStringLiteral("btnV1M"));
        sizePolicy3.setHeightForWidth(btnV1M->sizePolicy().hasHeightForWidth());
        btnV1M->setSizePolicy(sizePolicy3);
        btnV1M->setMinimumSize(QSize(0, 0));
        btnV1M->setMaximumSize(QSize(30, 30));
        btnV1M->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/IMG5/4m.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1M->setIcon(icon10);
        btnV1M->setIconSize(QSize(20, 20));
        btnV1M->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1M, 0, 9, 1, 1);

        btnV1Otro = new QPushButton(frame_5);
        btnV1Otro->setObjectName(QStringLiteral("btnV1Otro"));
        sizePolicy3.setHeightForWidth(btnV1Otro->sizePolicy().hasHeightForWidth());
        btnV1Otro->setSizePolicy(sizePolicy3);
        btnV1Otro->setMinimumSize(QSize(0, 0));
        btnV1Otro->setMaximumSize(QSize(30, 30));
        btnV1Otro->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/IMG5/radiobutton_check.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1Otro->setIcon(icon11);
        btnV1Otro->setIconSize(QSize(20, 20));
        btnV1Otro->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1Otro, 0, 11, 1, 1);

        btnV1G = new QPushButton(frame_5);
        btnV1G->setObjectName(QStringLiteral("btnV1G"));
        sizePolicy3.setHeightForWidth(btnV1G->sizePolicy().hasHeightForWidth());
        btnV1G->setSizePolicy(sizePolicy3);
        btnV1G->setMinimumSize(QSize(0, 0));
        btnV1G->setMaximumSize(QSize(30, 30));
        btnV1G->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/IMG5/4l.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1G->setIcon(icon12);
        btnV1G->setIconSize(QSize(20, 20));
        btnV1G->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1G, 0, 8, 1, 1);

        btnV1ColorSin = new QPushButton(frame_5);
        btnV1ColorSin->setObjectName(QStringLiteral("btnV1ColorSin"));
        sizePolicy3.setHeightForWidth(btnV1ColorSin->sizePolicy().hasHeightForWidth());
        btnV1ColorSin->setSizePolicy(sizePolicy3);
        btnV1ColorSin->setMinimumSize(QSize(0, 0));
        btnV1ColorSin->setMaximumSize(QSize(30, 30));
        btnV1ColorSin->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV1ColorSin->setIcon(icon4);
        btnV1ColorSin->setIconSize(QSize(20, 20));
        btnV1ColorSin->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1ColorSin, 0, 4, 1, 1);

        btnV1Regresa = new QPushButton(frame_5);
        btnV1Regresa->setObjectName(QStringLiteral("btnV1Regresa"));
        sizePolicy3.setHeightForWidth(btnV1Regresa->sizePolicy().hasHeightForWidth());
        btnV1Regresa->setSizePolicy(sizePolicy3);
        btnV1Regresa->setMinimumSize(QSize(0, 0));
        btnV1Regresa->setMaximumSize(QSize(30, 30));
        btnV1Regresa->setStyleSheet(QStringLiteral("background:transparent;"));
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/IMG5/4regresar.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnV1Regresa->setIcon(icon13);
        btnV1Regresa->setIconSize(QSize(20, 20));
        btnV1Regresa->setAutoDefault(true);

        gridLayout_17->addWidget(btnV1Regresa, 0, 6, 1, 1);


        gridLayout_70->addWidget(frame_5, 2, 0, 1, 2);


        horizontalLayout_5->addWidget(groupBox_26);

        groupBox_27 = new QGroupBox(tab2OptimizarManual);
        groupBox_27->setObjectName(QStringLiteral("groupBox_27"));
        sizePolicy9.setHeightForWidth(groupBox_27->sizePolicy().hasHeightForWidth());
        groupBox_27->setSizePolicy(sizePolicy9);
        groupBox_27->setMinimumSize(QSize(0, 0));
        groupBox_27->setMaximumSize(QSize(600, 12345));
        gridLayout_71 = new QGridLayout(groupBox_27);
        gridLayout_71->setSpacing(6);
        gridLayout_71->setContentsMargins(11, 11, 11, 11);
        gridLayout_71->setObjectName(QStringLiteral("gridLayout_71"));
        layoutV2 = new QGridLayout();
        layoutV2->setSpacing(0);
        layoutV2->setObjectName(QStringLiteral("layoutV2"));
        layoutV2->setSizeConstraint(QLayout::SetDefaultConstraint);

        gridLayout_71->addLayout(layoutV2, 0, 0, 1, 2);

        frame_10 = new QFrame(groupBox_27);
        frame_10->setObjectName(QStringLiteral("frame_10"));
        sizePolicy5.setHeightForWidth(frame_10->sizePolicy().hasHeightForWidth());
        frame_10->setSizePolicy(sizePolicy5);
        frame_10->setMaximumSize(QSize(16777215, 66));
        frame_10->setFrameShape(QFrame::StyledPanel);
        frame_10->setFrameShadow(QFrame::Raised);
        gridLayout_54 = new QGridLayout(frame_10);
        gridLayout_54->setSpacing(6);
        gridLayout_54->setContentsMargins(11, 11, 11, 11);
        gridLayout_54->setObjectName(QStringLiteral("gridLayout_54"));
        gridLayout_54->setContentsMargins(0, 0, 0, 0);
        lblV2M3 = new QLabel(frame_10);
        lblV2M3->setObjectName(QStringLiteral("lblV2M3"));
        sizePolicy4.setHeightForWidth(lblV2M3->sizePolicy().hasHeightForWidth());
        lblV2M3->setSizePolicy(sizePolicy4);
        lblV2M3->setMinimumSize(QSize(0, 0));
        lblV2M3->setMaximumSize(QSize(12345, 24));
        lblV2M3->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2M3->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2M3, 0, 1, 1, 1);

        lblV2Valor = new QLabel(frame_10);
        lblV2Valor->setObjectName(QStringLiteral("lblV2Valor"));
        sizePolicy4.setHeightForWidth(lblV2Valor->sizePolicy().hasHeightForWidth());
        lblV2Valor->setSizePolicy(sizePolicy4);
        lblV2Valor->setMinimumSize(QSize(0, 0));
        lblV2Valor->setMaximumSize(QSize(12345, 24));
        lblV2Valor->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2Valor->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2Valor, 0, 2, 1, 2);

        lblV2Kg = new QLabel(frame_10);
        lblV2Kg->setObjectName(QStringLiteral("lblV2Kg"));
        sizePolicy4.setHeightForWidth(lblV2Kg->sizePolicy().hasHeightForWidth());
        lblV2Kg->setSizePolicy(sizePolicy4);
        lblV2Kg->setMinimumSize(QSize(0, 0));
        lblV2Kg->setMaximumSize(QSize(12345, 24));
        lblV2Kg->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2Kg->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2Kg, 0, 0, 1, 1);

        lblV2Comp = new QLabel(frame_10);
        lblV2Comp->setObjectName(QStringLiteral("lblV2Comp"));
        sizePolicy4.setHeightForWidth(lblV2Comp->sizePolicy().hasHeightForWidth());
        lblV2Comp->setSizePolicy(sizePolicy4);
        lblV2Comp->setMinimumSize(QSize(0, 0));
        lblV2Comp->setMaximumSize(QSize(12345, 24));
        lblV2Comp->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2Comp->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2Comp, 1, 2, 1, 1);

        lblV2Acceso = new QLabel(frame_10);
        lblV2Acceso->setObjectName(QStringLiteral("lblV2Acceso"));
        sizePolicy4.setHeightForWidth(lblV2Acceso->sizePolicy().hasHeightForWidth());
        lblV2Acceso->setSizePolicy(sizePolicy4);
        lblV2Acceso->setMinimumSize(QSize(0, 0));
        lblV2Acceso->setMaximumSize(QSize(12345, 24));
        lblV2Acceso->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2Acceso->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2Acceso, 1, 1, 1, 1);

        lblV2Hrs = new QLabel(frame_10);
        lblV2Hrs->setObjectName(QStringLiteral("lblV2Hrs"));
        sizePolicy4.setHeightForWidth(lblV2Hrs->sizePolicy().hasHeightForWidth());
        lblV2Hrs->setSizePolicy(sizePolicy4);
        lblV2Hrs->setMinimumSize(QSize(0, 0));
        lblV2Hrs->setMaximumSize(QSize(12345, 24));
        lblV2Hrs->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        lblV2Hrs->setAlignment(Qt::AlignCenter);

        gridLayout_54->addWidget(lblV2Hrs, 1, 0, 1, 1);


        gridLayout_71->addWidget(frame_10, 1, 0, 1, 1);

        frame_9 = new QFrame(groupBox_27);
        frame_9->setObjectName(QStringLiteral("frame_9"));
        sizePolicy5.setHeightForWidth(frame_9->sizePolicy().hasHeightForWidth());
        frame_9->setSizePolicy(sizePolicy5);
        frame_9->setMinimumSize(QSize(0, 0));
        frame_9->setMaximumSize(QSize(16777215, 66));
        frame_9->setFrameShape(QFrame::StyledPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        gridLayout_47 = new QGridLayout(frame_9);
        gridLayout_47->setSpacing(6);
        gridLayout_47->setContentsMargins(11, 11, 11, 11);
        gridLayout_47->setObjectName(QStringLiteral("gridLayout_47"));
        gridLayout_47->setContentsMargins(0, 0, 0, 0);
        label_19 = new QLabel(frame_9);
        label_19->setObjectName(QStringLiteral("label_19"));
        sizePolicy4.setHeightForWidth(label_19->sizePolicy().hasHeightForWidth());
        label_19->setSizePolicy(sizePolicy4);
        label_19->setMinimumSize(QSize(0, 0));
        label_19->setMaximumSize(QSize(12345, 30));
        label_19->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_19->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(label_19, 0, 2, 1, 1);

        label_28 = new QLabel(frame_9);
        label_28->setObjectName(QStringLiteral("label_28"));
        sizePolicy4.setHeightForWidth(label_28->sizePolicy().hasHeightForWidth());
        label_28->setSizePolicy(sizePolicy4);
        label_28->setMinimumSize(QSize(0, 0));
        label_28->setMaximumSize(QSize(12345, 30));
        label_28->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_28->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(label_28, 0, 8, 1, 1);

        label_27 = new QLabel(frame_9);
        label_27->setObjectName(QStringLiteral("label_27"));
        sizePolicy4.setHeightForWidth(label_27->sizePolicy().hasHeightForWidth());
        label_27->setSizePolicy(sizePolicy4);
        label_27->setMinimumSize(QSize(0, 0));
        label_27->setMaximumSize(QSize(12345, 30));
        label_27->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_27->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(label_27, 0, 4, 1, 1);

        label_20 = new QLabel(frame_9);
        label_20->setObjectName(QStringLiteral("label_20"));
        sizePolicy4.setHeightForWidth(label_20->sizePolicy().hasHeightForWidth());
        label_20->setSizePolicy(sizePolicy4);
        label_20->setMinimumSize(QSize(0, 0));
        label_20->setMaximumSize(QSize(12345, 30));
        label_20->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_20->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(label_20, 0, 6, 1, 1);

        label_21 = new QLabel(frame_9);
        label_21->setObjectName(QStringLiteral("label_21"));
        sizePolicy4.setHeightForWidth(label_21->sizePolicy().hasHeightForWidth());
        label_21->setSizePolicy(sizePolicy4);
        label_21->setMinimumSize(QSize(0, 0));
        label_21->setMaximumSize(QSize(12345, 30));
        label_21->setStyleSheet(QLatin1String("color: rgba(190, 219, 189, 255);\n"
"background: transparent;\n"
"border: 1px solid rgba(190, 219, 189,55);\n"
"font: 10pt \"Arial Narrow\";"));
        label_21->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(label_21, 0, 0, 1, 1);

        editV2MaxKg = new QLineEdit(frame_9);
        editV2MaxKg->setObjectName(QStringLiteral("editV2MaxKg"));
        sizePolicy4.setHeightForWidth(editV2MaxKg->sizePolicy().hasHeightForWidth());
        editV2MaxKg->setSizePolicy(sizePolicy4);
        editV2MaxKg->setMinimumSize(QSize(0, 0));
        editV2MaxKg->setMaximumSize(QSize(12345, 22));
        editV2MaxKg->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV2MaxKg->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(editV2MaxKg, 1, 0, 1, 1);

        editV2MaxM3 = new QLineEdit(frame_9);
        editV2MaxM3->setObjectName(QStringLiteral("editV2MaxM3"));
        sizePolicy4.setHeightForWidth(editV2MaxM3->sizePolicy().hasHeightForWidth());
        editV2MaxM3->setSizePolicy(sizePolicy4);
        editV2MaxM3->setMinimumSize(QSize(0, 0));
        editV2MaxM3->setMaximumSize(QSize(12345, 22));
        editV2MaxM3->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV2MaxM3->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(editV2MaxM3, 1, 2, 1, 1);

        editV2MaxValor = new QLineEdit(frame_9);
        editV2MaxValor->setObjectName(QStringLiteral("editV2MaxValor"));
        sizePolicy4.setHeightForWidth(editV2MaxValor->sizePolicy().hasHeightForWidth());
        editV2MaxValor->setSizePolicy(sizePolicy4);
        editV2MaxValor->setMinimumSize(QSize(0, 0));
        editV2MaxValor->setMaximumSize(QSize(12345, 22));
        editV2MaxValor->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV2MaxValor->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(editV2MaxValor, 1, 4, 1, 1);

        editV2Tipo = new QLineEdit(frame_9);
        editV2Tipo->setObjectName(QStringLiteral("editV2Tipo"));
        sizePolicy4.setHeightForWidth(editV2Tipo->sizePolicy().hasHeightForWidth());
        editV2Tipo->setSizePolicy(sizePolicy4);
        editV2Tipo->setMinimumSize(QSize(0, 0));
        editV2Tipo->setMaximumSize(QSize(12345, 22));
        editV2Tipo->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV2Tipo->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(editV2Tipo, 1, 6, 1, 1);

        editV2NumViaje = new QLineEdit(frame_9);
        editV2NumViaje->setObjectName(QStringLiteral("editV2NumViaje"));
        sizePolicy4.setHeightForWidth(editV2NumViaje->sizePolicy().hasHeightForWidth());
        editV2NumViaje->setSizePolicy(sizePolicy4);
        editV2NumViaje->setMinimumSize(QSize(0, 0));
        editV2NumViaje->setMaximumSize(QSize(12345, 22));
        editV2NumViaje->setStyleSheet(QLatin1String("border: 1px solid rgba(190, 219, 189, 122);\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
""));
        editV2NumViaje->setAlignment(Qt::AlignCenter);

        gridLayout_47->addWidget(editV2NumViaje, 1, 8, 1, 1);


        gridLayout_71->addWidget(frame_9, 1, 1, 1, 1);

        frame_6 = new QFrame(groupBox_27);
        frame_6->setObjectName(QStringLiteral("frame_6"));
        sizePolicy5.setHeightForWidth(frame_6->sizePolicy().hasHeightForWidth());
        frame_6->setSizePolicy(sizePolicy5);
        frame_6->setMinimumSize(QSize(0, 0));
        frame_6->setMaximumSize(QSize(16777215, 66));
        frame_6->setStyleSheet(QLatin1String("QFrame\n"
"{\n"
"font: 9pt \"Arial Narrow\";\n"
"padding: 0px;\n"
"margin: 0px;\n"
"}\n"
""));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        gridLayout_15 = new QGridLayout(frame_6);
        gridLayout_15->setSpacing(0);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        gridLayout_15->setContentsMargins(0, 0, 0, 0);
        btnV2XG = new QPushButton(frame_6);
        btnV2XG->setObjectName(QStringLiteral("btnV2XG"));
        sizePolicy3.setHeightForWidth(btnV2XG->sizePolicy().hasHeightForWidth());
        btnV2XG->setSizePolicy(sizePolicy3);
        btnV2XG->setMinimumSize(QSize(0, 0));
        btnV2XG->setMaximumSize(QSize(30, 30));
        btnV2XG->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2XG->setIcon(icon8);
        btnV2XG->setIconSize(QSize(20, 20));
        btnV2XG->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2XG, 0, 6, 1, 1);

        btnV2Otro = new QPushButton(frame_6);
        btnV2Otro->setObjectName(QStringLiteral("btnV2Otro"));
        sizePolicy3.setHeightForWidth(btnV2Otro->sizePolicy().hasHeightForWidth());
        btnV2Otro->setSizePolicy(sizePolicy3);
        btnV2Otro->setMinimumSize(QSize(0, 0));
        btnV2Otro->setMaximumSize(QSize(30, 30));
        btnV2Otro->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2Otro->setIcon(icon11);
        btnV2Otro->setIconSize(QSize(20, 20));
        btnV2Otro->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2Otro, 0, 10, 1, 1);

        btnV2M = new QPushButton(frame_6);
        btnV2M->setObjectName(QStringLiteral("btnV2M"));
        sizePolicy3.setHeightForWidth(btnV2M->sizePolicy().hasHeightForWidth());
        btnV2M->setSizePolicy(sizePolicy3);
        btnV2M->setMinimumSize(QSize(0, 0));
        btnV2M->setMaximumSize(QSize(30, 30));
        btnV2M->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2M->setIcon(icon10);
        btnV2M->setIconSize(QSize(20, 20));
        btnV2M->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2M, 0, 8, 1, 1);

        btnV2CH = new QPushButton(frame_6);
        btnV2CH->setObjectName(QStringLiteral("btnV2CH"));
        sizePolicy3.setHeightForWidth(btnV2CH->sizePolicy().hasHeightForWidth());
        btnV2CH->setSizePolicy(sizePolicy3);
        btnV2CH->setMinimumSize(QSize(0, 0));
        btnV2CH->setMaximumSize(QSize(30, 30));
        btnV2CH->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2CH->setIcon(icon9);
        btnV2CH->setIconSize(QSize(20, 20));
        btnV2CH->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2CH, 0, 9, 1, 1);

        btnV2ColorSin = new QPushButton(frame_6);
        btnV2ColorSin->setObjectName(QStringLiteral("btnV2ColorSin"));
        sizePolicy3.setHeightForWidth(btnV2ColorSin->sizePolicy().hasHeightForWidth());
        btnV2ColorSin->setSizePolicy(sizePolicy3);
        btnV2ColorSin->setMinimumSize(QSize(0, 0));
        btnV2ColorSin->setMaximumSize(QSize(30, 30));
        btnV2ColorSin->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2ColorSin->setIcon(icon4);
        btnV2ColorSin->setIconSize(QSize(20, 20));
        btnV2ColorSin->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2ColorSin, 0, 4, 1, 1);

        btnV2ColorV = new QPushButton(frame_6);
        btnV2ColorV->setObjectName(QStringLiteral("btnV2ColorV"));
        sizePolicy3.setHeightForWidth(btnV2ColorV->sizePolicy().hasHeightForWidth());
        btnV2ColorV->setSizePolicy(sizePolicy3);
        btnV2ColorV->setMinimumSize(QSize(0, 0));
        btnV2ColorV->setMaximumSize(QSize(30, 30));
        btnV2ColorV->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2ColorV->setIcon(icon6);
        btnV2ColorV->setIconSize(QSize(20, 20));
        btnV2ColorV->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2ColorV, 0, 2, 1, 1);

        btnV2ColorD = new QPushButton(frame_6);
        btnV2ColorD->setObjectName(QStringLiteral("btnV2ColorD"));
        sizePolicy3.setHeightForWidth(btnV2ColorD->sizePolicy().hasHeightForWidth());
        btnV2ColorD->setSizePolicy(sizePolicy3);
        btnV2ColorD->setMinimumSize(QSize(0, 0));
        btnV2ColorD->setMaximumSize(QSize(30, 30));
        btnV2ColorD->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2ColorD->setIcon(icon3);
        btnV2ColorD->setIconSize(QSize(20, 20));
        btnV2ColorD->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2ColorD, 0, 3, 1, 1);

        btnV2Recalcula = new QPushButton(frame_6);
        btnV2Recalcula->setObjectName(QStringLiteral("btnV2Recalcula"));
        sizePolicy3.setHeightForWidth(btnV2Recalcula->sizePolicy().hasHeightForWidth());
        btnV2Recalcula->setSizePolicy(sizePolicy3);
        btnV2Recalcula->setMinimumSize(QSize(0, 0));
        btnV2Recalcula->setMaximumSize(QSize(30, 30));
        btnV2Recalcula->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2Recalcula->setIcon(icon);
        btnV2Recalcula->setIconSize(QSize(20, 20));
        btnV2Recalcula->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2Recalcula, 0, 0, 1, 1);

        btnV2Optimiza = new QPushButton(frame_6);
        btnV2Optimiza->setObjectName(QStringLiteral("btnV2Optimiza"));
        sizePolicy3.setHeightForWidth(btnV2Optimiza->sizePolicy().hasHeightForWidth());
        btnV2Optimiza->setSizePolicy(sizePolicy3);
        btnV2Optimiza->setMinimumSize(QSize(0, 0));
        btnV2Optimiza->setMaximumSize(QSize(30, 30));
        btnV2Optimiza->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2Optimiza->setIcon(icon1);
        btnV2Optimiza->setIconSize(QSize(20, 20));
        btnV2Optimiza->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2Optimiza, 0, 1, 1, 1);

        btnV2Regresa = new QPushButton(frame_6);
        btnV2Regresa->setObjectName(QStringLiteral("btnV2Regresa"));
        sizePolicy3.setHeightForWidth(btnV2Regresa->sizePolicy().hasHeightForWidth());
        btnV2Regresa->setSizePolicy(sizePolicy3);
        btnV2Regresa->setMinimumSize(QSize(0, 0));
        btnV2Regresa->setMaximumSize(QSize(30, 30));
        btnV2Regresa->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2Regresa->setIcon(icon13);
        btnV2Regresa->setIconSize(QSize(20, 20));
        btnV2Regresa->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2Regresa, 0, 5, 1, 1);

        btnV2G = new QPushButton(frame_6);
        btnV2G->setObjectName(QStringLiteral("btnV2G"));
        sizePolicy3.setHeightForWidth(btnV2G->sizePolicy().hasHeightForWidth());
        btnV2G->setSizePolicy(sizePolicy3);
        btnV2G->setMinimumSize(QSize(0, 0));
        btnV2G->setMaximumSize(QSize(30, 30));
        btnV2G->setStyleSheet(QStringLiteral("background:transparent;"));
        btnV2G->setIcon(icon12);
        btnV2G->setIconSize(QSize(20, 20));
        btnV2G->setAutoDefault(true);

        gridLayout_15->addWidget(btnV2G, 0, 7, 1, 1);


        gridLayout_71->addWidget(frame_6, 2, 0, 1, 2);


        horizontalLayout_5->addWidget(groupBox_27);

        horizontalLayout_5->setStretch(0, 2);
        horizontalLayout_5->setStretch(1, 1);
        horizontalLayout_5->setStretch(2, 1);

        horizontalLayout_6->addLayout(horizontalLayout_5);

        tab2Optimizar->addTab(tab2OptimizarManual, QString());
        tab2OptimizarBitacoras = new QWidget();
        tab2OptimizarBitacoras->setObjectName(QStringLiteral("tab2OptimizarBitacoras"));
        sizePolicy1.setHeightForWidth(tab2OptimizarBitacoras->sizePolicy().hasHeightForWidth());
        tab2OptimizarBitacoras->setSizePolicy(sizePolicy1);
        horizontalLayout = new QHBoxLayout(tab2OptimizarBitacoras);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        groupBoxCrearPlan_Pedidos_2 = new QGroupBox(tab2OptimizarBitacoras);
        groupBoxCrearPlan_Pedidos_2->setObjectName(QStringLiteral("groupBoxCrearPlan_Pedidos_2"));
        sizePolicy1.setHeightForWidth(groupBoxCrearPlan_Pedidos_2->sizePolicy().hasHeightForWidth());
        groupBoxCrearPlan_Pedidos_2->setSizePolicy(sizePolicy1);
        groupBoxCrearPlan_Pedidos_2->setMinimumSize(QSize(0, 0));
        gridLayout_59 = new QGridLayout(groupBoxCrearPlan_Pedidos_2);
        gridLayout_59->setSpacing(6);
        gridLayout_59->setContentsMargins(11, 11, 11, 11);
        gridLayout_59->setObjectName(QStringLiteral("gridLayout_59"));
        gridLayout_59->setContentsMargins(0, 0, 0, 0);
        gridBitacoraVacios = new QTableWidget(groupBoxCrearPlan_Pedidos_2);
        if (gridBitacoraVacios->columnCount() < 25)
            gridBitacoraVacios->setColumnCount(25);
        if (gridBitacoraVacios->rowCount() < 50)
            gridBitacoraVacios->setRowCount(50);
        gridBitacoraVacios->setObjectName(QStringLiteral("gridBitacoraVacios"));
        sizePolicy1.setHeightForWidth(gridBitacoraVacios->sizePolicy().hasHeightForWidth());
        gridBitacoraVacios->setSizePolicy(sizePolicy1);
        gridBitacoraVacios->setMinimumSize(QSize(720, 0));
        gridBitacoraVacios->setFont(font);
        gridBitacoraVacios->setMouseTracking(true);
        gridBitacoraVacios->setFocusPolicy(Qt::ClickFocus);
        gridBitacoraVacios->setAcceptDrops(true);
        gridBitacoraVacios->setToolTipDuration(2);
        gridBitacoraVacios->setStyleSheet(QStringLiteral("background:transparent;"));
        gridBitacoraVacios->setFrameShape(QFrame::NoFrame);
        gridBitacoraVacios->setLineWidth(0);
        gridBitacoraVacios->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        gridBitacoraVacios->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridBitacoraVacios->setDragEnabled(true);
        gridBitacoraVacios->setDragDropMode(QAbstractItemView::DragDrop);
        gridBitacoraVacios->setDefaultDropAction(Qt::MoveAction);
        gridBitacoraVacios->setAlternatingRowColors(false);
        gridBitacoraVacios->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridBitacoraVacios->setGridStyle(Qt::DotLine);
        gridBitacoraVacios->setSortingEnabled(true);
        gridBitacoraVacios->setRowCount(50);
        gridBitacoraVacios->setColumnCount(25);
        gridBitacoraVacios->verticalHeader()->setDefaultSectionSize(15);
        gridBitacoraVacios->verticalHeader()->setMinimumSectionSize(15);
        gridBitacoraVacios->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_59->addWidget(gridBitacoraVacios, 0, 0, 1, 1);


        horizontalLayout->addWidget(groupBoxCrearPlan_Pedidos_2);

        groupBoxCrearPlan_Pedidos_3 = new QGroupBox(tab2OptimizarBitacoras);
        groupBoxCrearPlan_Pedidos_3->setObjectName(QStringLiteral("groupBoxCrearPlan_Pedidos_3"));
        sizePolicy1.setHeightForWidth(groupBoxCrearPlan_Pedidos_3->sizePolicy().hasHeightForWidth());
        groupBoxCrearPlan_Pedidos_3->setSizePolicy(sizePolicy1);
        groupBoxCrearPlan_Pedidos_3->setMinimumSize(QSize(0, 0));
        gridLayout_56 = new QGridLayout(groupBoxCrearPlan_Pedidos_3);
        gridLayout_56->setSpacing(6);
        gridLayout_56->setContentsMargins(11, 11, 11, 11);
        gridLayout_56->setObjectName(QStringLiteral("gridLayout_56"));
        gridLayout_56->setContentsMargins(0, 0, 0, 0);
        gridBitacoraCercanos = new QTableWidget(groupBoxCrearPlan_Pedidos_3);
        if (gridBitacoraCercanos->columnCount() < 25)
            gridBitacoraCercanos->setColumnCount(25);
        if (gridBitacoraCercanos->rowCount() < 50)
            gridBitacoraCercanos->setRowCount(50);
        gridBitacoraCercanos->setObjectName(QStringLiteral("gridBitacoraCercanos"));
        sizePolicy1.setHeightForWidth(gridBitacoraCercanos->sizePolicy().hasHeightForWidth());
        gridBitacoraCercanos->setSizePolicy(sizePolicy1);
        gridBitacoraCercanos->setMinimumSize(QSize(0, 0));
        gridBitacoraCercanos->setFont(font);
        gridBitacoraCercanos->setMouseTracking(true);
        gridBitacoraCercanos->setAcceptDrops(true);
        gridBitacoraCercanos->setToolTipDuration(2);
        gridBitacoraCercanos->setStyleSheet(QStringLiteral("background:transparent;"));
        gridBitacoraCercanos->setFrameShape(QFrame::NoFrame);
        gridBitacoraCercanos->setLineWidth(0);
        gridBitacoraCercanos->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        gridBitacoraCercanos->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridBitacoraCercanos->setDragEnabled(true);
        gridBitacoraCercanos->setDragDropMode(QAbstractItemView::DragDrop);
        gridBitacoraCercanos->setDefaultDropAction(Qt::MoveAction);
        gridBitacoraCercanos->setAlternatingRowColors(false);
        gridBitacoraCercanos->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridBitacoraCercanos->setGridStyle(Qt::DotLine);
        gridBitacoraCercanos->setSortingEnabled(true);
        gridBitacoraCercanos->setRowCount(50);
        gridBitacoraCercanos->setColumnCount(25);
        gridBitacoraCercanos->verticalHeader()->setDefaultSectionSize(15);
        gridBitacoraCercanos->verticalHeader()->setMinimumSectionSize(15);
        gridBitacoraCercanos->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_56->addWidget(gridBitacoraCercanos, 0, 0, 1, 1);


        horizontalLayout->addWidget(groupBoxCrearPlan_Pedidos_3);

        tab2Optimizar->addTab(tab2OptimizarBitacoras, QString());

        gridLayout_19->addWidget(tab2Optimizar, 1, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Optimizar, QString());
        tab1Asignar = new QWidget();
        tab1Asignar->setObjectName(QStringLiteral("tab1Asignar"));
        sizePolicy1.setHeightForWidth(tab1Asignar->sizePolicy().hasHeightForWidth());
        tab1Asignar->setSizePolicy(sizePolicy1);
        gridLayout_5 = new QGridLayout(tab1Asignar);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        tab2Asignar = new QTabWidget(tab1Asignar);
        tab2Asignar->setObjectName(QStringLiteral("tab2Asignar"));
        sizePolicy1.setHeightForWidth(tab2Asignar->sizePolicy().hasHeightForWidth());
        tab2Asignar->setSizePolicy(sizePolicy1);
        tab2Asignar->setTabPosition(QTabWidget::North);
        tab2Asignar->setIconSize(QSize(22, 22));
        tab2AsignarPlan = new QWidget();
        tab2AsignarPlan->setObjectName(QStringLiteral("tab2AsignarPlan"));
        sizePolicy1.setHeightForWidth(tab2AsignarPlan->sizePolicy().hasHeightForWidth());
        tab2AsignarPlan->setSizePolicy(sizePolicy1);
        gridLayout_64 = new QGridLayout(tab2AsignarPlan);
        gridLayout_64->setSpacing(6);
        gridLayout_64->setContentsMargins(11, 11, 11, 11);
        gridLayout_64->setObjectName(QStringLiteral("gridLayout_64"));
        groupBox_2 = new QGroupBox(tab2AsignarPlan);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        groupBox_2->setMaximumSize(QSize(12344, 16777215));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 15, 0, 0);
        gridAsignar = new QTableWidget(groupBox_2);
        if (gridAsignar->columnCount() < 25)
            gridAsignar->setColumnCount(25);
        if (gridAsignar->rowCount() < 50)
            gridAsignar->setRowCount(50);
        gridAsignar->setObjectName(QStringLiteral("gridAsignar"));
        sizePolicy1.setHeightForWidth(gridAsignar->sizePolicy().hasHeightForWidth());
        gridAsignar->setSizePolicy(sizePolicy1);
        gridAsignar->setMinimumSize(QSize(0, 0));
        gridAsignar->setMaximumSize(QSize(12345, 12345));
        gridAsignar->setFont(font);
        gridAsignar->setMouseTracking(true);
        gridAsignar->setAcceptDrops(true);
        gridAsignar->setToolTipDuration(2);
        gridAsignar->setStyleSheet(QLatin1String("background-color: transparent;\n"
"margin: 0px;"));
        gridAsignar->setFrameShape(QFrame::NoFrame);
        gridAsignar->setLineWidth(0);
        gridAsignar->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignar->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignar->setDragEnabled(true);
        gridAsignar->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignar->setDefaultDropAction(Qt::MoveAction);
        gridAsignar->setAlternatingRowColors(false);
        gridAsignar->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignar->setGridStyle(Qt::DotLine);
        gridAsignar->setSortingEnabled(true);
        gridAsignar->setRowCount(50);
        gridAsignar->setColumnCount(25);
        gridAsignar->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignar->verticalHeader()->setDefaultSectionSize(15);
        gridAsignar->verticalHeader()->setMinimumSectionSize(15);
        gridAsignar->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout->addWidget(gridAsignar, 1, 0, 1, 6);

        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        sizePolicy3.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy3);
        pushButton->setMaximumSize(QSize(16777215, 40));
        pushButton->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        btnRefreshAsignar = new QPushButton(groupBox_2);
        btnRefreshAsignar->setObjectName(QStringLiteral("btnRefreshAsignar"));
        sizePolicy3.setHeightForWidth(btnRefreshAsignar->sizePolicy().hasHeightForWidth());
        btnRefreshAsignar->setSizePolicy(sizePolicy3);
        btnRefreshAsignar->setMinimumSize(QSize(48, 48));
        btnRefreshAsignar->setMaximumSize(QSize(48, 48));
        btnRefreshAsignar->setStyleSheet(QStringLiteral("background:transparent;"));
        btnRefreshAsignar->setIcon(icon5);
        btnRefreshAsignar->setIconSize(QSize(32, 32));
        btnRefreshAsignar->setAutoDefault(true);

        gridLayout->addWidget(btnRefreshAsignar, 2, 3, 1, 1);

        btnGuardaAsignar = new QPushButton(groupBox_2);
        btnGuardaAsignar->setObjectName(QStringLiteral("btnGuardaAsignar"));
        sizePolicy3.setHeightForWidth(btnGuardaAsignar->sizePolicy().hasHeightForWidth());
        btnGuardaAsignar->setSizePolicy(sizePolicy3);
        btnGuardaAsignar->setMinimumSize(QSize(0, 0));
        btnGuardaAsignar->setMaximumSize(QSize(48, 48));
        btnGuardaAsignar->setStyleSheet(QStringLiteral("background:transparent;"));
        btnGuardaAsignar->setIcon(icon2);
        btnGuardaAsignar->setIconSize(QSize(32, 32));
        btnGuardaAsignar->setAutoDefault(true);

        gridLayout->addWidget(btnGuardaAsignar, 2, 5, 1, 1);

        btnPropagaAsignar = new QPushButton(groupBox_2);
        btnPropagaAsignar->setObjectName(QStringLiteral("btnPropagaAsignar"));
        QSizePolicy sizePolicy11(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy11.setHorizontalStretch(16);
        sizePolicy11.setVerticalStretch(16);
        sizePolicy11.setHeightForWidth(btnPropagaAsignar->sizePolicy().hasHeightForWidth());
        btnPropagaAsignar->setSizePolicy(sizePolicy11);
        btnPropagaAsignar->setMinimumSize(QSize(0, 0));
        btnPropagaAsignar->setMaximumSize(QSize(48, 48));
        btnPropagaAsignar->setStyleSheet(QStringLiteral("background:transparent;"));
        btnPropagaAsignar->setIcon(icon7);
        btnPropagaAsignar->setIconSize(QSize(30, 30));
        btnPropagaAsignar->setAutoDefault(true);

        gridLayout->addWidget(btnPropagaAsignar, 2, 4, 1, 1);

        btnAutomaticoAsignar = new QPushButton(groupBox_2);
        btnAutomaticoAsignar->setObjectName(QStringLiteral("btnAutomaticoAsignar"));
        sizePolicy3.setHeightForWidth(btnAutomaticoAsignar->sizePolicy().hasHeightForWidth());
        btnAutomaticoAsignar->setSizePolicy(sizePolicy3);
        btnAutomaticoAsignar->setMinimumSize(QSize(0, 0));
        btnAutomaticoAsignar->setMaximumSize(QSize(12345, 40));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAutomaticoAsignar->setPalette(palette4);
        btnAutomaticoAsignar->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout->addWidget(btnAutomaticoAsignar, 0, 5, 1, 1);


        gridLayout_64->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox_32 = new QGroupBox(tab2AsignarPlan);
        groupBox_32->setObjectName(QStringLiteral("groupBox_32"));
        sizePolicy1.setHeightForWidth(groupBox_32->sizePolicy().hasHeightForWidth());
        groupBox_32->setSizePolicy(sizePolicy1);
        groupBox_32->setMaximumSize(QSize(12345, 16777215));
        gridLayout_8 = new QGridLayout(groupBox_32);
        gridLayout_8->setSpacing(0);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 15, 0, 0);
        comboOperadoresAsignarPlan = new QComboBox(groupBox_32);
        comboOperadoresAsignarPlan->addItem(QString());
        comboOperadoresAsignarPlan->addItem(QString());
        comboOperadoresAsignarPlan->addItem(QString());
        comboOperadoresAsignarPlan->setObjectName(QStringLiteral("comboOperadoresAsignarPlan"));
        sizePolicy7.setHeightForWidth(comboOperadoresAsignarPlan->sizePolicy().hasHeightForWidth());
        comboOperadoresAsignarPlan->setSizePolicy(sizePolicy7);

        gridLayout_8->addWidget(comboOperadoresAsignarPlan, 0, 0, 1, 1);

        btnAbreOperadoresAsignarPlan = new QPushButton(groupBox_32);
        btnAbreOperadoresAsignarPlan->setObjectName(QStringLiteral("btnAbreOperadoresAsignarPlan"));
        sizePolicy3.setHeightForWidth(btnAbreOperadoresAsignarPlan->sizePolicy().hasHeightForWidth());
        btnAbreOperadoresAsignarPlan->setSizePolicy(sizePolicy3);
        btnAbreOperadoresAsignarPlan->setMaximumSize(QSize(16777215, 40));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAbreOperadoresAsignarPlan->setPalette(palette5);
        btnAbreOperadoresAsignarPlan->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_8->addWidget(btnAbreOperadoresAsignarPlan, 0, 1, 1, 1);

        gridAsignarOperadores = new QTableWidget(groupBox_32);
        if (gridAsignarOperadores->columnCount() < 25)
            gridAsignarOperadores->setColumnCount(25);
        if (gridAsignarOperadores->rowCount() < 50)
            gridAsignarOperadores->setRowCount(50);
        gridAsignarOperadores->setObjectName(QStringLiteral("gridAsignarOperadores"));
        sizePolicy1.setHeightForWidth(gridAsignarOperadores->sizePolicy().hasHeightForWidth());
        gridAsignarOperadores->setSizePolicy(sizePolicy1);
        gridAsignarOperadores->setMaximumSize(QSize(12345, 12345));
        gridAsignarOperadores->setFont(font);
        gridAsignarOperadores->setMouseTracking(true);
        gridAsignarOperadores->setAcceptDrops(true);
        gridAsignarOperadores->setToolTipDuration(2);
        gridAsignarOperadores->setStyleSheet(QStringLiteral("background-color: transparent;"));
        gridAsignarOperadores->setFrameShape(QFrame::NoFrame);
        gridAsignarOperadores->setLineWidth(0);
        gridAsignarOperadores->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignarOperadores->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignarOperadores->setDragEnabled(true);
        gridAsignarOperadores->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignarOperadores->setDefaultDropAction(Qt::MoveAction);
        gridAsignarOperadores->setAlternatingRowColors(false);
        gridAsignarOperadores->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignarOperadores->setGridStyle(Qt::DotLine);
        gridAsignarOperadores->setSortingEnabled(true);
        gridAsignarOperadores->setRowCount(50);
        gridAsignarOperadores->setColumnCount(25);
        gridAsignarOperadores->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignarOperadores->verticalHeader()->setDefaultSectionSize(15);
        gridAsignarOperadores->verticalHeader()->setMinimumSectionSize(15);
        gridAsignarOperadores->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_8->addWidget(gridAsignarOperadores, 1, 0, 1, 2);


        gridLayout_64->addWidget(groupBox_32, 0, 1, 1, 1);

        groupBox_31 = new QGroupBox(tab2AsignarPlan);
        groupBox_31->setObjectName(QStringLiteral("groupBox_31"));
        sizePolicy1.setHeightForWidth(groupBox_31->sizePolicy().hasHeightForWidth());
        groupBox_31->setSizePolicy(sizePolicy1);
        groupBox_31->setMinimumSize(QSize(0, 0));
        groupBox_31->setMaximumSize(QSize(12345, 16777215));
        gridLayout_24 = new QGridLayout(groupBox_31);
        gridLayout_24->setSpacing(0);
        gridLayout_24->setContentsMargins(11, 11, 11, 11);
        gridLayout_24->setObjectName(QStringLiteral("gridLayout_24"));
        gridLayout_24->setContentsMargins(0, 15, 0, 0);
        comboVehAsignarPlan = new QComboBox(groupBox_31);
        comboVehAsignarPlan->addItem(QString());
        comboVehAsignarPlan->addItem(QString());
        comboVehAsignarPlan->addItem(QString());
        comboVehAsignarPlan->setObjectName(QStringLiteral("comboVehAsignarPlan"));
        sizePolicy7.setHeightForWidth(comboVehAsignarPlan->sizePolicy().hasHeightForWidth());
        comboVehAsignarPlan->setSizePolicy(sizePolicy7);

        gridLayout_24->addWidget(comboVehAsignarPlan, 0, 0, 1, 1);

        btnAbreVehAsignarPlan = new QPushButton(groupBox_31);
        btnAbreVehAsignarPlan->setObjectName(QStringLiteral("btnAbreVehAsignarPlan"));
        sizePolicy3.setHeightForWidth(btnAbreVehAsignarPlan->sizePolicy().hasHeightForWidth());
        btnAbreVehAsignarPlan->setSizePolicy(sizePolicy3);
        btnAbreVehAsignarPlan->setMaximumSize(QSize(16777215, 40));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAbreVehAsignarPlan->setPalette(palette6);
        btnAbreVehAsignarPlan->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_24->addWidget(btnAbreVehAsignarPlan, 0, 1, 1, 1);

        gridAsignarVeh = new QTableWidget(groupBox_31);
        if (gridAsignarVeh->columnCount() < 25)
            gridAsignarVeh->setColumnCount(25);
        if (gridAsignarVeh->rowCount() < 50)
            gridAsignarVeh->setRowCount(50);
        gridAsignarVeh->setObjectName(QStringLiteral("gridAsignarVeh"));
        sizePolicy1.setHeightForWidth(gridAsignarVeh->sizePolicy().hasHeightForWidth());
        gridAsignarVeh->setSizePolicy(sizePolicy1);
        gridAsignarVeh->setMaximumSize(QSize(12345, 12345));
        gridAsignarVeh->setFont(font);
        gridAsignarVeh->setMouseTracking(true);
        gridAsignarVeh->setAcceptDrops(true);
        gridAsignarVeh->setToolTipDuration(2);
        gridAsignarVeh->setStyleSheet(QStringLiteral("background-color: transparent;"));
        gridAsignarVeh->setFrameShape(QFrame::NoFrame);
        gridAsignarVeh->setLineWidth(0);
        gridAsignarVeh->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignarVeh->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignarVeh->setDragEnabled(true);
        gridAsignarVeh->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignarVeh->setDefaultDropAction(Qt::MoveAction);
        gridAsignarVeh->setAlternatingRowColors(false);
        gridAsignarVeh->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignarVeh->setGridStyle(Qt::DotLine);
        gridAsignarVeh->setSortingEnabled(true);
        gridAsignarVeh->setRowCount(50);
        gridAsignarVeh->setColumnCount(25);
        gridAsignarVeh->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignarVeh->verticalHeader()->setDefaultSectionSize(22);
        gridAsignarVeh->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_24->addWidget(gridAsignarVeh, 1, 0, 1, 2);


        gridLayout_64->addWidget(groupBox_31, 0, 2, 1, 1);

        tab2Asignar->addTab(tab2AsignarPlan, QString());
        tab2AsignarPredespegue = new QWidget();
        tab2AsignarPredespegue->setObjectName(QStringLiteral("tab2AsignarPredespegue"));
        sizePolicy1.setHeightForWidth(tab2AsignarPredespegue->sizePolicy().hasHeightForWidth());
        tab2AsignarPredespegue->setSizePolicy(sizePolicy1);
        gridLayout_66 = new QGridLayout(tab2AsignarPredespegue);
        gridLayout_66->setSpacing(6);
        gridLayout_66->setContentsMargins(11, 11, 11, 11);
        gridLayout_66->setObjectName(QStringLiteral("gridLayout_66"));
        groupBox_3 = new QGroupBox(tab2AsignarPredespegue);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        sizePolicy1.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy1);
        groupBox_3->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_11 = new QGridLayout(groupBox_3);
        gridLayout_11->setSpacing(0);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        gridLayout_11->setContentsMargins(0, 15, 0, 0);
        horizontalSpacer_21 = new QSpacerItem(63, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_11->addItem(horizontalSpacer_21, 0, 1, 1, 1);

        btnAsignarCargarPredespegue = new QPushButton(groupBox_3);
        btnAsignarCargarPredespegue->setObjectName(QStringLiteral("btnAsignarCargarPredespegue"));
        sizePolicy3.setHeightForWidth(btnAsignarCargarPredespegue->sizePolicy().hasHeightForWidth());
        btnAsignarCargarPredespegue->setSizePolicy(sizePolicy3);
        btnAsignarCargarPredespegue->setMaximumSize(QSize(16777215, 40));
        btnAsignarCargarPredespegue->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_11->addWidget(btnAsignarCargarPredespegue, 0, 2, 1, 1);

        gridAsignarPredespegue = new QTableWidget(groupBox_3);
        if (gridAsignarPredespegue->columnCount() < 25)
            gridAsignarPredespegue->setColumnCount(25);
        if (gridAsignarPredespegue->rowCount() < 50)
            gridAsignarPredespegue->setRowCount(50);
        gridAsignarPredespegue->setObjectName(QStringLiteral("gridAsignarPredespegue"));
        sizePolicy1.setHeightForWidth(gridAsignarPredespegue->sizePolicy().hasHeightForWidth());
        gridAsignarPredespegue->setSizePolicy(sizePolicy1);
        gridAsignarPredespegue->setMaximumSize(QSize(16777215, 16777215));
        gridAsignarPredespegue->setFont(font);
        gridAsignarPredespegue->setMouseTracking(true);
        gridAsignarPredespegue->setAcceptDrops(true);
        gridAsignarPredespegue->setToolTipDuration(2);
        gridAsignarPredespegue->setStyleSheet(QStringLiteral("background-color: transparent;"));
        gridAsignarPredespegue->setFrameShape(QFrame::NoFrame);
        gridAsignarPredespegue->setLineWidth(0);
        gridAsignarPredespegue->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignarPredespegue->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignarPredespegue->setDragEnabled(true);
        gridAsignarPredespegue->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignarPredespegue->setDefaultDropAction(Qt::MoveAction);
        gridAsignarPredespegue->setAlternatingRowColors(false);
        gridAsignarPredespegue->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignarPredespegue->setGridStyle(Qt::DotLine);
        gridAsignarPredespegue->setSortingEnabled(true);
        gridAsignarPredespegue->setRowCount(50);
        gridAsignarPredespegue->setColumnCount(25);
        gridAsignarPredespegue->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignarPredespegue->verticalHeader()->setDefaultSectionSize(15);
        gridAsignarPredespegue->verticalHeader()->setMinimumSectionSize(15);
        gridAsignarPredespegue->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_11->addWidget(gridAsignarPredespegue, 1, 0, 1, 3);

        pushButton_3 = new QPushButton(groupBox_3);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        sizePolicy3.setHeightForWidth(pushButton_3->sizePolicy().hasHeightForWidth());
        pushButton_3->setSizePolicy(sizePolicy3);
        pushButton_3->setMaximumSize(QSize(16777215, 40));
        pushButton_3->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_11->addWidget(pushButton_3, 2, 2, 1, 1);


        gridLayout_66->addWidget(groupBox_3, 0, 0, 1, 1);

        groupBox_37 = new QGroupBox(tab2AsignarPredespegue);
        groupBox_37->setObjectName(QStringLiteral("groupBox_37"));
        sizePolicy1.setHeightForWidth(groupBox_37->sizePolicy().hasHeightForWidth());
        groupBox_37->setSizePolicy(sizePolicy1);
        groupBox_37->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_35 = new QGridLayout(groupBox_37);
        gridLayout_35->setSpacing(0);
        gridLayout_35->setContentsMargins(11, 11, 11, 11);
        gridLayout_35->setObjectName(QStringLiteral("gridLayout_35"));
        gridLayout_35->setContentsMargins(0, 15, 0, 0);
        comboOperadoresAsignarPredespegue = new QComboBox(groupBox_37);
        comboOperadoresAsignarPredespegue->addItem(QString());
        comboOperadoresAsignarPredespegue->addItem(QString());
        comboOperadoresAsignarPredespegue->addItem(QString());
        comboOperadoresAsignarPredespegue->setObjectName(QStringLiteral("comboOperadoresAsignarPredespegue"));
        sizePolicy7.setHeightForWidth(comboOperadoresAsignarPredespegue->sizePolicy().hasHeightForWidth());
        comboOperadoresAsignarPredespegue->setSizePolicy(sizePolicy7);

        gridLayout_35->addWidget(comboOperadoresAsignarPredespegue, 0, 0, 1, 1);

        btnAbreOperadoresAsignarPredespegue = new QPushButton(groupBox_37);
        btnAbreOperadoresAsignarPredespegue->setObjectName(QStringLiteral("btnAbreOperadoresAsignarPredespegue"));
        sizePolicy3.setHeightForWidth(btnAbreOperadoresAsignarPredespegue->sizePolicy().hasHeightForWidth());
        btnAbreOperadoresAsignarPredespegue->setSizePolicy(sizePolicy3);
        btnAbreOperadoresAsignarPredespegue->setMaximumSize(QSize(16777215, 40));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Text, brush);
        palette7.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette7.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette7.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAbreOperadoresAsignarPredespegue->setPalette(palette7);
        btnAbreOperadoresAsignarPredespegue->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_35->addWidget(btnAbreOperadoresAsignarPredespegue, 0, 1, 1, 1);

        gridAsignarOperadoresPredespegue = new QTableWidget(groupBox_37);
        if (gridAsignarOperadoresPredespegue->columnCount() < 25)
            gridAsignarOperadoresPredespegue->setColumnCount(25);
        if (gridAsignarOperadoresPredespegue->rowCount() < 50)
            gridAsignarOperadoresPredespegue->setRowCount(50);
        gridAsignarOperadoresPredespegue->setObjectName(QStringLiteral("gridAsignarOperadoresPredespegue"));
        sizePolicy3.setHeightForWidth(gridAsignarOperadoresPredespegue->sizePolicy().hasHeightForWidth());
        gridAsignarOperadoresPredespegue->setSizePolicy(sizePolicy3);
        gridAsignarOperadoresPredespegue->setFont(font);
        gridAsignarOperadoresPredespegue->setMouseTracking(true);
        gridAsignarOperadoresPredespegue->setAcceptDrops(true);
        gridAsignarOperadoresPredespegue->setToolTipDuration(2);
        gridAsignarOperadoresPredespegue->setStyleSheet(QStringLiteral("background-color: transparent;"));
        gridAsignarOperadoresPredespegue->setFrameShape(QFrame::NoFrame);
        gridAsignarOperadoresPredespegue->setLineWidth(0);
        gridAsignarOperadoresPredespegue->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignarOperadoresPredespegue->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignarOperadoresPredespegue->setDragEnabled(true);
        gridAsignarOperadoresPredespegue->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignarOperadoresPredespegue->setDefaultDropAction(Qt::MoveAction);
        gridAsignarOperadoresPredespegue->setAlternatingRowColors(false);
        gridAsignarOperadoresPredespegue->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignarOperadoresPredespegue->setGridStyle(Qt::DotLine);
        gridAsignarOperadoresPredespegue->setSortingEnabled(true);
        gridAsignarOperadoresPredespegue->setRowCount(50);
        gridAsignarOperadoresPredespegue->setColumnCount(25);
        gridAsignarOperadoresPredespegue->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignarOperadoresPredespegue->verticalHeader()->setDefaultSectionSize(15);
        gridAsignarOperadoresPredespegue->verticalHeader()->setMinimumSectionSize(15);
        gridAsignarOperadoresPredespegue->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_35->addWidget(gridAsignarOperadoresPredespegue, 1, 0, 1, 2);


        gridLayout_66->addWidget(groupBox_37, 0, 1, 1, 1);

        groupBox_35 = new QGroupBox(tab2AsignarPredespegue);
        groupBox_35->setObjectName(QStringLiteral("groupBox_35"));
        sizePolicy1.setHeightForWidth(groupBox_35->sizePolicy().hasHeightForWidth());
        groupBox_35->setSizePolicy(sizePolicy1);
        groupBox_35->setMinimumSize(QSize(0, 0));
        groupBox_35->setMaximumSize(QSize(16777215, 16777215));
        gridLayout_36 = new QGridLayout(groupBox_35);
        gridLayout_36->setSpacing(0);
        gridLayout_36->setContentsMargins(11, 11, 11, 11);
        gridLayout_36->setObjectName(QStringLiteral("gridLayout_36"));
        gridLayout_36->setContentsMargins(0, 15, 0, 0);
        comboVehAsignarPredespegue = new QComboBox(groupBox_35);
        comboVehAsignarPredespegue->addItem(QString());
        comboVehAsignarPredespegue->addItem(QString());
        comboVehAsignarPredespegue->addItem(QString());
        comboVehAsignarPredespegue->setObjectName(QStringLiteral("comboVehAsignarPredespegue"));
        sizePolicy7.setHeightForWidth(comboVehAsignarPredespegue->sizePolicy().hasHeightForWidth());
        comboVehAsignarPredespegue->setSizePolicy(sizePolicy7);

        gridLayout_36->addWidget(comboVehAsignarPredespegue, 0, 0, 1, 1);

        btnAbreVehAsignarPredespegue = new QPushButton(groupBox_35);
        btnAbreVehAsignarPredespegue->setObjectName(QStringLiteral("btnAbreVehAsignarPredespegue"));
        sizePolicy3.setHeightForWidth(btnAbreVehAsignarPredespegue->sizePolicy().hasHeightForWidth());
        btnAbreVehAsignarPredespegue->setSizePolicy(sizePolicy3);
        btnAbreVehAsignarPredespegue->setMaximumSize(QSize(16777215, 40));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Text, brush);
        palette8.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette8.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette8.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette8.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette8.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnAbreVehAsignarPredespegue->setPalette(palette8);
        btnAbreVehAsignarPredespegue->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_36->addWidget(btnAbreVehAsignarPredespegue, 0, 1, 1, 1);

        gridAsignarVehPredespegue = new QTableWidget(groupBox_35);
        if (gridAsignarVehPredespegue->columnCount() < 25)
            gridAsignarVehPredespegue->setColumnCount(25);
        if (gridAsignarVehPredespegue->rowCount() < 50)
            gridAsignarVehPredespegue->setRowCount(50);
        gridAsignarVehPredespegue->setObjectName(QStringLiteral("gridAsignarVehPredespegue"));
        sizePolicy3.setHeightForWidth(gridAsignarVehPredespegue->sizePolicy().hasHeightForWidth());
        gridAsignarVehPredespegue->setSizePolicy(sizePolicy3);
        gridAsignarVehPredespegue->setFont(font);
        gridAsignarVehPredespegue->setMouseTracking(true);
        gridAsignarVehPredespegue->setAcceptDrops(true);
        gridAsignarVehPredespegue->setToolTipDuration(2);
        gridAsignarVehPredespegue->setStyleSheet(QStringLiteral("background-color: transparent;"));
        gridAsignarVehPredespegue->setFrameShape(QFrame::NoFrame);
        gridAsignarVehPredespegue->setLineWidth(0);
        gridAsignarVehPredespegue->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridAsignarVehPredespegue->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridAsignarVehPredespegue->setDragEnabled(true);
        gridAsignarVehPredespegue->setDragDropMode(QAbstractItemView::DragDrop);
        gridAsignarVehPredespegue->setDefaultDropAction(Qt::MoveAction);
        gridAsignarVehPredespegue->setAlternatingRowColors(false);
        gridAsignarVehPredespegue->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridAsignarVehPredespegue->setGridStyle(Qt::DotLine);
        gridAsignarVehPredespegue->setSortingEnabled(true);
        gridAsignarVehPredespegue->setRowCount(50);
        gridAsignarVehPredespegue->setColumnCount(25);
        gridAsignarVehPredespegue->horizontalHeader()->setDefaultSectionSize(80);
        gridAsignarVehPredespegue->verticalHeader()->setDefaultSectionSize(22);
        gridAsignarVehPredespegue->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_36->addWidget(gridAsignarVehPredespegue, 1, 0, 1, 2);


        gridLayout_66->addWidget(groupBox_35, 0, 2, 1, 1);

        tab2Asignar->addTab(tab2AsignarPredespegue, QString());

        gridLayout_5->addWidget(tab2Asignar, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Asignar, QString());
        tab1Despegar = new QWidget();
        tab1Despegar->setObjectName(QStringLiteral("tab1Despegar"));
        sizePolicy1.setHeightForWidth(tab1Despegar->sizePolicy().hasHeightForWidth());
        tab1Despegar->setSizePolicy(sizePolicy1);
        gridLayout_33 = new QGridLayout(tab1Despegar);
        gridLayout_33->setSpacing(6);
        gridLayout_33->setContentsMargins(11, 11, 11, 11);
        gridLayout_33->setObjectName(QStringLiteral("gridLayout_33"));
        tab2Despegar = new QTabWidget(tab1Despegar);
        tab2Despegar->setObjectName(QStringLiteral("tab2Despegar"));
        sizePolicy1.setHeightForWidth(tab2Despegar->sizePolicy().hasHeightForWidth());
        tab2Despegar->setSizePolicy(sizePolicy1);
        tab2Despegar->setTabPosition(QTabWidget::North);
        tab2Despegar->setIconSize(QSize(22, 22));
        tab2DespegarPredespegue = new QWidget();
        tab2DespegarPredespegue->setObjectName(QStringLiteral("tab2DespegarPredespegue"));
        sizePolicy1.setHeightForWidth(tab2DespegarPredespegue->sizePolicy().hasHeightForWidth());
        tab2DespegarPredespegue->setSizePolicy(sizePolicy1);
        gridLayout_27 = new QGridLayout(tab2DespegarPredespegue);
        gridLayout_27->setSpacing(6);
        gridLayout_27->setContentsMargins(11, 11, 11, 11);
        gridLayout_27->setObjectName(QStringLiteral("gridLayout_27"));
        btnPredespegarPlan = new QPushButton(tab2DespegarPredespegue);
        btnPredespegarPlan->setObjectName(QStringLiteral("btnPredespegarPlan"));
        sizePolicy3.setHeightForWidth(btnPredespegarPlan->sizePolicy().hasHeightForWidth());
        btnPredespegarPlan->setSizePolicy(sizePolicy3);
        btnPredespegarPlan->setMinimumSize(QSize(0, 0));
        btnPredespegarPlan->setMaximumSize(QSize(12345, 40));
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette9.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Text, brush);
        palette9.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette9.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette9.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette9.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette9.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnPredespegarPlan->setPalette(palette9);

        gridLayout_27->addWidget(btnPredespegarPlan, 2, 3, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_27->addItem(horizontalSpacer_3, 2, 0, 1, 1);

        btnPredespegarSelecc = new QPushButton(tab2DespegarPredespegue);
        btnPredespegarSelecc->setObjectName(QStringLiteral("btnPredespegarSelecc"));
        sizePolicy3.setHeightForWidth(btnPredespegarSelecc->sizePolicy().hasHeightForWidth());
        btnPredespegarSelecc->setSizePolicy(sizePolicy3);
        btnPredespegarSelecc->setMinimumSize(QSize(0, 0));
        btnPredespegarSelecc->setMaximumSize(QSize(12345, 40));
        QPalette palette10;
        palette10.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette10.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette10.setBrush(QPalette::Active, QPalette::Text, brush);
        palette10.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette10.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette10.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette10.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette10.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette10.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette10.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette10.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette10.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette10.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette10.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnPredespegarSelecc->setPalette(palette10);

        gridLayout_27->addWidget(btnPredespegarSelecc, 2, 2, 1, 1);

        gridPredespegar = new QTableWidget(tab2DespegarPredespegue);
        if (gridPredespegar->columnCount() < 25)
            gridPredespegar->setColumnCount(25);
        if (gridPredespegar->rowCount() < 50)
            gridPredespegar->setRowCount(50);
        gridPredespegar->setObjectName(QStringLiteral("gridPredespegar"));
        sizePolicy1.setHeightForWidth(gridPredespegar->sizePolicy().hasHeightForWidth());
        gridPredespegar->setSizePolicy(sizePolicy1);
        gridPredespegar->setMaximumSize(QSize(12345, 16777215));
        gridPredespegar->setFont(font);
        gridPredespegar->setMouseTracking(true);
        gridPredespegar->setAcceptDrops(true);
        gridPredespegar->setToolTipDuration(2);
        gridPredespegar->setFrameShape(QFrame::NoFrame);
        gridPredespegar->setLineWidth(0);
        gridPredespegar->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        gridPredespegar->setEditTriggers(QAbstractItemView::AllEditTriggers);
        gridPredespegar->setDragEnabled(true);
        gridPredespegar->setDragDropMode(QAbstractItemView::DragDrop);
        gridPredespegar->setDefaultDropAction(Qt::MoveAction);
        gridPredespegar->setAlternatingRowColors(false);
        gridPredespegar->setSelectionBehavior(QAbstractItemView::SelectRows);
        gridPredespegar->setGridStyle(Qt::DotLine);
        gridPredespegar->setSortingEnabled(true);
        gridPredespegar->setRowCount(50);
        gridPredespegar->setColumnCount(25);
        gridPredespegar->horizontalHeader()->setDefaultSectionSize(80);
        gridPredespegar->verticalHeader()->setDefaultSectionSize(15);
        gridPredespegar->verticalHeader()->setMinimumSectionSize(15);
        gridPredespegar->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_27->addWidget(gridPredespegar, 1, 0, 1, 4);

        btnCargaPlanPredespegar = new QPushButton(tab2DespegarPredespegue);
        btnCargaPlanPredespegar->setObjectName(QStringLiteral("btnCargaPlanPredespegar"));
        sizePolicy3.setHeightForWidth(btnCargaPlanPredespegar->sizePolicy().hasHeightForWidth());
        btnCargaPlanPredespegar->setSizePolicy(sizePolicy3);
        btnCargaPlanPredespegar->setMinimumSize(QSize(0, 0));
        btnCargaPlanPredespegar->setMaximumSize(QSize(12345, 40));

        gridLayout_27->addWidget(btnCargaPlanPredespegar, 0, 3, 1, 1);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_27->addItem(horizontalSpacer_12, 0, 0, 1, 3);

        tab2Despegar->addTab(tab2DespegarPredespegue, QString());
        tab2DespegarDespegar = new QWidget();
        tab2DespegarDespegar->setObjectName(QStringLiteral("tab2DespegarDespegar"));
        sizePolicy1.setHeightForWidth(tab2DespegarDespegar->sizePolicy().hasHeightForWidth());
        tab2DespegarDespegar->setSizePolicy(sizePolicy1);
        gridLayout_37 = new QGridLayout(tab2DespegarDespegar);
        gridLayout_37->setSpacing(6);
        gridLayout_37->setContentsMargins(11, 11, 11, 11);
        gridLayout_37->setObjectName(QStringLiteral("gridLayout_37"));
        groupBox_33 = new QGroupBox(tab2DespegarDespegar);
        groupBox_33->setObjectName(QStringLiteral("groupBox_33"));
        sizePolicy1.setHeightForWidth(groupBox_33->sizePolicy().hasHeightForWidth());
        groupBox_33->setSizePolicy(sizePolicy1);
        gridLayout_28 = new QGridLayout(groupBox_33);
        gridLayout_28->setSpacing(0);
        gridLayout_28->setContentsMargins(11, 11, 11, 11);
        gridLayout_28->setObjectName(QStringLiteral("gridLayout_28"));
        gridLayout_28->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_9 = new QSpacerItem(294, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_28->addItem(horizontalSpacer_9, 0, 1, 1, 1);

        btnDespegarCargaPredespegados = new QPushButton(groupBox_33);
        btnDespegarCargaPredespegados->setObjectName(QStringLiteral("btnDespegarCargaPredespegados"));
        sizePolicy3.setHeightForWidth(btnDespegarCargaPredespegados->sizePolicy().hasHeightForWidth());
        btnDespegarCargaPredespegados->setSizePolicy(sizePolicy3);
        btnDespegarCargaPredespegados->setMaximumSize(QSize(16777215, 40));
        QPalette palette11;
        palette11.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette11.setBrush(QPalette::Active, QPalette::Text, brush);
        palette11.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette11.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette11.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette11.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette11.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette11.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette11.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette11.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette11.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette11.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette11.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnDespegarCargaPredespegados->setPalette(palette11);
        btnDespegarCargaPredespegados->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_28->addWidget(btnDespegarCargaPredespegados, 0, 2, 1, 1);

        tableWidget_despegar_predespegue = new QTableWidget(groupBox_33);
        if (tableWidget_despegar_predespegue->columnCount() < 25)
            tableWidget_despegar_predespegue->setColumnCount(25);
        if (tableWidget_despegar_predespegue->rowCount() < 50)
            tableWidget_despegar_predespegue->setRowCount(50);
        tableWidget_despegar_predespegue->setObjectName(QStringLiteral("tableWidget_despegar_predespegue"));
        sizePolicy1.setHeightForWidth(tableWidget_despegar_predespegue->sizePolicy().hasHeightForWidth());
        tableWidget_despegar_predespegue->setSizePolicy(sizePolicy1);
        tableWidget_despegar_predespegue->setFont(font);
        tableWidget_despegar_predespegue->setMouseTracking(true);
        tableWidget_despegar_predespegue->setAcceptDrops(true);
        tableWidget_despegar_predespegue->setToolTipDuration(2);
        tableWidget_despegar_predespegue->setStyleSheet(QStringLiteral("background-color: transparent;"));
        tableWidget_despegar_predespegue->setFrameShape(QFrame::NoFrame);
        tableWidget_despegar_predespegue->setLineWidth(0);
        tableWidget_despegar_predespegue->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        tableWidget_despegar_predespegue->setEditTriggers(QAbstractItemView::AllEditTriggers);
        tableWidget_despegar_predespegue->setDragEnabled(true);
        tableWidget_despegar_predespegue->setDragDropMode(QAbstractItemView::DragDrop);
        tableWidget_despegar_predespegue->setDefaultDropAction(Qt::MoveAction);
        tableWidget_despegar_predespegue->setAlternatingRowColors(false);
        tableWidget_despegar_predespegue->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget_despegar_predespegue->setGridStyle(Qt::DotLine);
        tableWidget_despegar_predespegue->setSortingEnabled(true);
        tableWidget_despegar_predespegue->setRowCount(50);
        tableWidget_despegar_predespegue->setColumnCount(25);
        tableWidget_despegar_predespegue->horizontalHeader()->setDefaultSectionSize(80);
        tableWidget_despegar_predespegue->verticalHeader()->setDefaultSectionSize(15);
        tableWidget_despegar_predespegue->verticalHeader()->setMinimumSectionSize(15);
        tableWidget_despegar_predespegue->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_28->addWidget(tableWidget_despegar_predespegue, 1, 0, 1, 3);

        horizontalSpacer_2 = new QSpacerItem(258, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_28->addItem(horizontalSpacer_2, 2, 0, 1, 1);

        btnDespegar = new QPushButton(groupBox_33);
        btnDespegar->setObjectName(QStringLiteral("btnDespegar"));
        sizePolicy3.setHeightForWidth(btnDespegar->sizePolicy().hasHeightForWidth());
        btnDespegar->setSizePolicy(sizePolicy3);
        btnDespegar->setMaximumSize(QSize(16777215, 40));
        QPalette palette12;
        palette12.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette12.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette12.setBrush(QPalette::Active, QPalette::Text, brush);
        palette12.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette12.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette12.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette12.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette12.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette12.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette12.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette12.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette12.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette12.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette12.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette12.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette12.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette12.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette12.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnDespegar->setPalette(palette12);
        btnDespegar->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_28->addWidget(btnDespegar, 2, 2, 1, 1);

        labelDespeguePIN = new QLabel(groupBox_33);
        labelDespeguePIN->setObjectName(QStringLiteral("labelDespeguePIN"));
        sizePolicy3.setHeightForWidth(labelDespeguePIN->sizePolicy().hasHeightForWidth());
        labelDespeguePIN->setSizePolicy(sizePolicy3);
        QFont font2;
        font2.setFamily(QStringLiteral("Helvetica"));
        font2.setPointSize(36);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setWeight(50);
        labelDespeguePIN->setFont(font2);
        labelDespeguePIN->setStyleSheet(QLatin1String("color: rgba(106, 193, 140, 220); /* verde */\n"
"font: 36pt \"Helvetica\";\n"
""));
        labelDespeguePIN->setAlignment(Qt::AlignCenter);

        gridLayout_28->addWidget(labelDespeguePIN, 2, 1, 1, 1);


        gridLayout_37->addWidget(groupBox_33, 0, 0, 1, 1);

        groupBox_34 = new QGroupBox(tab2DespegarDespegar);
        groupBox_34->setObjectName(QStringLiteral("groupBox_34"));
        sizePolicy1.setHeightForWidth(groupBox_34->sizePolicy().hasHeightForWidth());
        groupBox_34->setSizePolicy(sizePolicy1);
        gridLayout_34 = new QGridLayout(groupBox_34);
        gridLayout_34->setSpacing(0);
        gridLayout_34->setContentsMargins(11, 11, 11, 11);
        gridLayout_34->setObjectName(QStringLiteral("gridLayout_34"));
        gridLayout_34->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_16 = new QSpacerItem(601, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_16, 0, 0, 1, 1);

        btnDespegarCargaDespegados = new QPushButton(groupBox_34);
        btnDespegarCargaDespegados->setObjectName(QStringLiteral("btnDespegarCargaDespegados"));
        sizePolicy3.setHeightForWidth(btnDespegarCargaDespegados->sizePolicy().hasHeightForWidth());
        btnDespegarCargaDespegados->setSizePolicy(sizePolicy3);
        btnDespegarCargaDespegados->setMaximumSize(QSize(16777215, 40));
        QPalette palette13;
        palette13.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette13.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette13.setBrush(QPalette::Active, QPalette::Text, brush);
        palette13.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette13.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette13.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette13.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette13.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette13.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette13.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette13.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette13.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette13.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette13.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette13.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette13.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        btnDespegarCargaDespegados->setPalette(palette13);
        btnDespegarCargaDespegados->setStyleSheet(QLatin1String("border: 2px solid rgba(190, 219, 189, 55);\n"
""));

        gridLayout_34->addWidget(btnDespegarCargaDespegados, 0, 1, 1, 1);

        tableWidget_despegar_despegados = new QTableWidget(groupBox_34);
        if (tableWidget_despegar_despegados->columnCount() < 25)
            tableWidget_despegar_despegados->setColumnCount(25);
        if (tableWidget_despegar_despegados->rowCount() < 50)
            tableWidget_despegar_despegados->setRowCount(50);
        tableWidget_despegar_despegados->setObjectName(QStringLiteral("tableWidget_despegar_despegados"));
        sizePolicy1.setHeightForWidth(tableWidget_despegar_despegados->sizePolicy().hasHeightForWidth());
        tableWidget_despegar_despegados->setSizePolicy(sizePolicy1);
        tableWidget_despegar_despegados->setFont(font);
        tableWidget_despegar_despegados->setMouseTracking(true);
        tableWidget_despegar_despegados->setAcceptDrops(true);
        tableWidget_despegar_despegados->setToolTipDuration(2);
        tableWidget_despegar_despegados->setStyleSheet(QStringLiteral("background-color: transparent;"));
        tableWidget_despegar_despegados->setFrameShape(QFrame::NoFrame);
        tableWidget_despegar_despegados->setLineWidth(0);
        tableWidget_despegar_despegados->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        tableWidget_despegar_despegados->setEditTriggers(QAbstractItemView::AllEditTriggers);
        tableWidget_despegar_despegados->setDragEnabled(true);
        tableWidget_despegar_despegados->setDragDropMode(QAbstractItemView::DragDrop);
        tableWidget_despegar_despegados->setDefaultDropAction(Qt::MoveAction);
        tableWidget_despegar_despegados->setAlternatingRowColors(false);
        tableWidget_despegar_despegados->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget_despegar_despegados->setGridStyle(Qt::DotLine);
        tableWidget_despegar_despegados->setSortingEnabled(true);
        tableWidget_despegar_despegados->setRowCount(50);
        tableWidget_despegar_despegados->setColumnCount(25);
        tableWidget_despegar_despegados->horizontalHeader()->setDefaultSectionSize(80);
        tableWidget_despegar_despegados->verticalHeader()->setDefaultSectionSize(15);
        tableWidget_despegar_despegados->verticalHeader()->setMinimumSectionSize(15);
        tableWidget_despegar_despegados->verticalHeader()->setProperty("showSortIndicator", QVariant(false));

        gridLayout_34->addWidget(tableWidget_despegar_despegados, 1, 0, 1, 2);


        gridLayout_37->addWidget(groupBox_34, 0, 1, 1, 1);

        tab2Despegar->addTab(tab2DespegarDespegar, QString());

        gridLayout_33->addWidget(tab2Despegar, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Despegar, QString());
        tab1Analizar = new QWidget();
        tab1Analizar->setObjectName(QStringLiteral("tab1Analizar"));
        sizePolicy1.setHeightForWidth(tab1Analizar->sizePolicy().hasHeightForWidth());
        tab1Analizar->setSizePolicy(sizePolicy1);
        gridLayout_46 = new QGridLayout(tab1Analizar);
        gridLayout_46->setSpacing(6);
        gridLayout_46->setContentsMargins(11, 11, 11, 11);
        gridLayout_46->setObjectName(QStringLiteral("gridLayout_46"));
        tab2Analizar = new QTabWidget(tab1Analizar);
        tab2Analizar->setObjectName(QStringLiteral("tab2Analizar"));
        sizePolicy1.setHeightForWidth(tab2Analizar->sizePolicy().hasHeightForWidth());
        tab2Analizar->setSizePolicy(sizePolicy1);
        tab2Analizar->setTabPosition(QTabWidget::North);
        tab2Analizar->setIconSize(QSize(22, 22));
        tab2Analizar1 = new QWidget();
        tab2Analizar1->setObjectName(QStringLiteral("tab2Analizar1"));
        sizePolicy1.setHeightForWidth(tab2Analizar1->sizePolicy().hasHeightForWidth());
        tab2Analizar1->setSizePolicy(sizePolicy1);
        gridLayout_45 = new QGridLayout(tab2Analizar1);
        gridLayout_45->setSpacing(6);
        gridLayout_45->setContentsMargins(11, 11, 11, 11);
        gridLayout_45->setObjectName(QStringLiteral("gridLayout_45"));
        frame_analizar_1 = new QFrame(tab2Analizar1);
        frame_analizar_1->setObjectName(QStringLiteral("frame_analizar_1"));
        sizePolicy3.setHeightForWidth(frame_analizar_1->sizePolicy().hasHeightForWidth());
        frame_analizar_1->setSizePolicy(sizePolicy3);
        frame_analizar_1->setFrameShape(QFrame::StyledPanel);
        frame_analizar_1->setFrameShadow(QFrame::Raised);

        gridLayout_45->addWidget(frame_analizar_1, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar1, QString());
        tab2Analizar2 = new QWidget();
        tab2Analizar2->setObjectName(QStringLiteral("tab2Analizar2"));
        sizePolicy1.setHeightForWidth(tab2Analizar2->sizePolicy().hasHeightForWidth());
        tab2Analizar2->setSizePolicy(sizePolicy1);
        gridLayout_40 = new QGridLayout(tab2Analizar2);
        gridLayout_40->setSpacing(6);
        gridLayout_40->setContentsMargins(11, 11, 11, 11);
        gridLayout_40->setObjectName(QStringLiteral("gridLayout_40"));
        frame_analizar_2 = new QFrame(tab2Analizar2);
        frame_analizar_2->setObjectName(QStringLiteral("frame_analizar_2"));
        sizePolicy3.setHeightForWidth(frame_analizar_2->sizePolicy().hasHeightForWidth());
        frame_analizar_2->setSizePolicy(sizePolicy3);
        frame_analizar_2->setFrameShape(QFrame::StyledPanel);
        frame_analizar_2->setFrameShadow(QFrame::Raised);

        gridLayout_40->addWidget(frame_analizar_2, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar2, QString());
        tab2Analizar3 = new QWidget();
        tab2Analizar3->setObjectName(QStringLiteral("tab2Analizar3"));
        sizePolicy1.setHeightForWidth(tab2Analizar3->sizePolicy().hasHeightForWidth());
        tab2Analizar3->setSizePolicy(sizePolicy1);
        gridLayout_44 = new QGridLayout(tab2Analizar3);
        gridLayout_44->setSpacing(6);
        gridLayout_44->setContentsMargins(11, 11, 11, 11);
        gridLayout_44->setObjectName(QStringLiteral("gridLayout_44"));
        frame_analizar_3 = new QFrame(tab2Analizar3);
        frame_analizar_3->setObjectName(QStringLiteral("frame_analizar_3"));
        sizePolicy3.setHeightForWidth(frame_analizar_3->sizePolicy().hasHeightForWidth());
        frame_analizar_3->setSizePolicy(sizePolicy3);
        frame_analizar_3->setFrameShape(QFrame::StyledPanel);
        frame_analizar_3->setFrameShadow(QFrame::Raised);

        gridLayout_44->addWidget(frame_analizar_3, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar3, QString());
        tab2Analizar4 = new QWidget();
        tab2Analizar4->setObjectName(QStringLiteral("tab2Analizar4"));
        sizePolicy1.setHeightForWidth(tab2Analizar4->sizePolicy().hasHeightForWidth());
        tab2Analizar4->setSizePolicy(sizePolicy1);
        gridLayout_49 = new QGridLayout(tab2Analizar4);
        gridLayout_49->setSpacing(6);
        gridLayout_49->setContentsMargins(11, 11, 11, 11);
        gridLayout_49->setObjectName(QStringLiteral("gridLayout_49"));
        frame_analizar_4 = new QFrame(tab2Analizar4);
        frame_analizar_4->setObjectName(QStringLiteral("frame_analizar_4"));
        sizePolicy3.setHeightForWidth(frame_analizar_4->sizePolicy().hasHeightForWidth());
        frame_analizar_4->setSizePolicy(sizePolicy3);
        frame_analizar_4->setFrameShape(QFrame::StyledPanel);
        frame_analizar_4->setFrameShadow(QFrame::Raised);

        gridLayout_49->addWidget(frame_analizar_4, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar4, QString());
        tab2Analizar5 = new QWidget();
        tab2Analizar5->setObjectName(QStringLiteral("tab2Analizar5"));
        sizePolicy1.setHeightForWidth(tab2Analizar5->sizePolicy().hasHeightForWidth());
        tab2Analizar5->setSizePolicy(sizePolicy1);
        gridLayout_57 = new QGridLayout(tab2Analizar5);
        gridLayout_57->setSpacing(6);
        gridLayout_57->setContentsMargins(11, 11, 11, 11);
        gridLayout_57->setObjectName(QStringLiteral("gridLayout_57"));
        frame_analizar_5 = new QFrame(tab2Analizar5);
        frame_analizar_5->setObjectName(QStringLiteral("frame_analizar_5"));
        sizePolicy3.setHeightForWidth(frame_analizar_5->sizePolicy().hasHeightForWidth());
        frame_analizar_5->setSizePolicy(sizePolicy3);
        frame_analizar_5->setFrameShape(QFrame::StyledPanel);
        frame_analizar_5->setFrameShadow(QFrame::Raised);

        gridLayout_57->addWidget(frame_analizar_5, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar5, QString());
        tab2Analizar6 = new QWidget();
        tab2Analizar6->setObjectName(QStringLiteral("tab2Analizar6"));
        sizePolicy1.setHeightForWidth(tab2Analizar6->sizePolicy().hasHeightForWidth());
        tab2Analizar6->setSizePolicy(sizePolicy1);
        gridLayout_58 = new QGridLayout(tab2Analizar6);
        gridLayout_58->setSpacing(6);
        gridLayout_58->setContentsMargins(11, 11, 11, 11);
        gridLayout_58->setObjectName(QStringLiteral("gridLayout_58"));
        frame_analizar_6 = new QFrame(tab2Analizar6);
        frame_analizar_6->setObjectName(QStringLiteral("frame_analizar_6"));
        sizePolicy3.setHeightForWidth(frame_analizar_6->sizePolicy().hasHeightForWidth());
        frame_analizar_6->setSizePolicy(sizePolicy3);
        frame_analizar_6->setFrameShape(QFrame::StyledPanel);
        frame_analizar_6->setFrameShadow(QFrame::Raised);

        gridLayout_58->addWidget(frame_analizar_6, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar6, QString());
        tab2Analizar7 = new QWidget();
        tab2Analizar7->setObjectName(QStringLiteral("tab2Analizar7"));
        sizePolicy1.setHeightForWidth(tab2Analizar7->sizePolicy().hasHeightForWidth());
        tab2Analizar7->setSizePolicy(sizePolicy1);
        gridLayout_67 = new QGridLayout(tab2Analizar7);
        gridLayout_67->setSpacing(6);
        gridLayout_67->setContentsMargins(11, 11, 11, 11);
        gridLayout_67->setObjectName(QStringLiteral("gridLayout_67"));
        frame_analizar_7 = new QFrame(tab2Analizar7);
        frame_analizar_7->setObjectName(QStringLiteral("frame_analizar_7"));
        sizePolicy3.setHeightForWidth(frame_analizar_7->sizePolicy().hasHeightForWidth());
        frame_analizar_7->setSizePolicy(sizePolicy3);
        frame_analizar_7->setFrameShape(QFrame::StyledPanel);
        frame_analizar_7->setFrameShadow(QFrame::Raised);

        gridLayout_67->addWidget(frame_analizar_7, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar7, QString());
        tab2Analizar8 = new QWidget();
        tab2Analizar8->setObjectName(QStringLiteral("tab2Analizar8"));
        sizePolicy1.setHeightForWidth(tab2Analizar8->sizePolicy().hasHeightForWidth());
        tab2Analizar8->setSizePolicy(sizePolicy1);
        gridLayout_60 = new QGridLayout(tab2Analizar8);
        gridLayout_60->setSpacing(6);
        gridLayout_60->setContentsMargins(11, 11, 11, 11);
        gridLayout_60->setObjectName(QStringLiteral("gridLayout_60"));
        frame_analizar_8 = new QFrame(tab2Analizar8);
        frame_analizar_8->setObjectName(QStringLiteral("frame_analizar_8"));
        sizePolicy3.setHeightForWidth(frame_analizar_8->sizePolicy().hasHeightForWidth());
        frame_analizar_8->setSizePolicy(sizePolicy3);
        frame_analizar_8->setFrameShape(QFrame::StyledPanel);
        frame_analizar_8->setFrameShadow(QFrame::Raised);

        gridLayout_60->addWidget(frame_analizar_8, 0, 0, 1, 1);

        tab2Analizar->addTab(tab2Analizar8, QString());
        tab2Analizar9 = new QWidget();
        tab2Analizar9->setObjectName(QStringLiteral("tab2Analizar9"));
        sizePolicy1.setHeightForWidth(tab2Analizar9->sizePolicy().hasHeightForWidth());
        tab2Analizar9->setSizePolicy(sizePolicy1);
        gridLayout_61 = new QGridLayout(tab2Analizar9);
        gridLayout_61->setSpacing(6);
        gridLayout_61->setContentsMargins(11, 11, 11, 11);
        gridLayout_61->setObjectName(QStringLiteral("gridLayout_61"));
        frame_analizar_9 = new QFrame(tab2Analizar9);
        frame_analizar_9->setObjectName(QStringLiteral("frame_analizar_9"));
        sizePolicy3.setHeightForWidth(frame_analizar_9->sizePolicy().hasHeightForWidth());
        frame_analizar_9->setSizePolicy(sizePolicy3);
        frame_analizar_9->setStyleSheet(QLatin1String("background-color: rgb(37,37,37);\n"
""));
        frame_analizar_9->setFrameShape(QFrame::StyledPanel);
        frame_analizar_9->setFrameShadow(QFrame::Raised);

        gridLayout_61->addWidget(frame_analizar_9, 0, 0, 1, 1);

        QIcon icon14;
        icon14.addFile(QStringLiteral(":/Img2/iconfinder-32 (4).png"), QSize(), QIcon::Normal, QIcon::Off);
        tab2Analizar->addTab(tab2Analizar9, icon14, QString());

        gridLayout_46->addWidget(tab2Analizar, 0, 0, 1, 1);

        tabWidget1Principal->addTab(tab1Analizar, QString());

        verticalLayout->addWidget(tabWidget1Principal);

        logo_footer = new QLabel(centralWidget);
        logo_footer->setObjectName(QStringLiteral("logo_footer"));
        sizePolicy5.setHeightForWidth(logo_footer->sizePolicy().hasHeightForWidth());
        logo_footer->setSizePolicy(sizePolicy5);
        logo_footer->setMinimumSize(QSize(0, 0));
        logo_footer->setMaximumSize(QSize(12345, 40));
        logo_footer->setStyleSheet(QLatin1String("font: 17PT \"Roboto\";\n"
"color:rgb(200, 206, 152, 204);\n"
"background:transparent;\n"
"\n"
""));
        logo_footer->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(logo_footer);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1900, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setStyleSheet(QStringLiteral(""));
        MainWindow->setStatusBar(statusBar);
#ifndef QT_NO_SHORTCUT
        label_17->setBuddy(editV1NumViaje);
        label_14->setBuddy(editV1Tipo);
        label_12->setBuddy(editV1MaxValor);
        label_10->setBuddy(editV1MaxM3);
        label_8->setBuddy(editV1MaxKg);
        label_19->setBuddy(editV2MaxM3);
        label_28->setBuddy(editV2NumViaje);
        label_27->setBuddy(editV2MaxValor);
        label_20->setBuddy(editV2Tipo);
        label_21->setBuddy(editV2MaxKg);
#endif // QT_NO_SHORTCUT

        retranslateUi(MainWindow);

        tabWidget1Principal->setCurrentIndex(0);
        tab2Iniciar->setCurrentIndex(0);
        tab2Planear->setCurrentIndex(0);
        listGenerarPlanPedidos->setCurrentRow(0);
        listGenerarPlanVehiculos->setCurrentRow(0);
        listGenerarPlanOperadores->setCurrentRow(0);
        listGenerarPlanPreferencias_2->setCurrentRow(0);
        listGenerarPlanDestinos->setCurrentRow(0);
        listGenerarPlanTarifario->setCurrentRow(0);
        listGenerarPlanComp->setCurrentRow(0);
        listAbrePlan->setCurrentRow(0);
        listAnadirPedidos->setCurrentRow(0);
        listAnadirPredespegados->setCurrentRow(0);
        tab2Rutear->setCurrentIndex(1);
        tab2Optimizar->setCurrentIndex(0);
        tab2Asignar->setCurrentIndex(0);
        tab2Despegar->setCurrentIndex(0);
        tab2Analizar->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        editClave->setPlaceholderText(QApplication::translate("MainWindow", "Clave", nullptr));
        editCEDI->setText(QApplication::translate("MainWindow", "CENADI", nullptr));
        editCEDI->setPlaceholderText(QApplication::translate("MainWindow", "CEDI", nullptr));
        editUsuario->setPlaceholderText(QApplication::translate("MainWindow", "Usuario", nullptr));
        btnLogin->setText(QApplication::translate("MainWindow", "INICIAR", nullptr));
        label->setText(QString());
        tab2Iniciar->setTabText(tab2Iniciar->indexOf(tab2IniciarLogin), QApplication::translate("MainWindow", "INGRESAR", nullptr));
        btnConfiguracion->setText(QApplication::translate("MainWindow", "GUARDAR", nullptr));
        tab2Iniciar->setTabText(tab2Iniciar->indexOf(tab2IniciarConfig), QApplication::translate("MainWindow", "CONFIGURACION", nullptr));
        tab2Iniciar->setTabText(tab2Iniciar->indexOf(tab2IniciarARMS), QApplication::translate("MainWindow", "ARETE", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Iniciar), QApplication::translate("MainWindow", "INICIAR", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableroPlan->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "$/Pza", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableroPlan->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "$/Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableroPlan->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "$/M3", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableroPlan->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "$/Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableroPlan->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "$/Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableroPlan->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "$ Total", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableroPlan->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "Km", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableroPlan->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "Co2", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableroPlan->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "% Ocupa. m3", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableroPlan->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "% Ocupa. Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableroPlan->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "Pedidos", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableroPlan->horizontalHeaderItem(11);
        ___qtablewidgetitem11->setText(QApplication::translate("MainWindow", "Destinos", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableroPlan->horizontalHeaderItem(12);
        ___qtablewidgetitem12->setText(QApplication::translate("MainWindow", "Viajes", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableroPlan->horizontalHeaderItem(13);
        ___qtablewidgetitem13->setText(QApplication::translate("MainWindow", "m3", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableroPlan->horizontalHeaderItem(14);
        ___qtablewidgetitem14->setText(QApplication::translate("MainWindow", "Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableroPlan->horizontalHeaderItem(15);
        ___qtablewidgetitem15->setText(QApplication::translate("MainWindow", "Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableroPlan->horizontalHeaderItem(16);
        ___qtablewidgetitem16->setText(QApplication::translate("MainWindow", "Tiempo", nullptr));
        QTableWidgetItem *___qtablewidgetitem17 = tableroPlan->horizontalHeaderItem(17);
        ___qtablewidgetitem17->setText(QApplication::translate("MainWindow", "$ / Km", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = tableroPlan->horizontalHeaderItem(18);
        ___qtablewidgetitem18->setText(QApplication::translate("MainWindow", "$ / Co2", nullptr));
        QTableWidgetItem *___qtablewidgetitem19 = tableroPlan->horizontalHeaderItem(19);
        ___qtablewidgetitem19->setText(QApplication::translate("MainWindow", "$ / Hora", nullptr));
        QTableWidgetItem *___qtablewidgetitem20 = tableroPlan->horizontalHeaderItem(20);
        ___qtablewidgetitem20->setText(QApplication::translate("MainWindow", "Piezas / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem21 = tableroPlan->horizontalHeaderItem(21);
        ___qtablewidgetitem21->setText(QApplication::translate("MainWindow", "m3 / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem22 = tableroPlan->horizontalHeaderItem(22);
        ___qtablewidgetitem22->setText(QApplication::translate("MainWindow", "kg / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem23 = tableroPlan->horizontalHeaderItem(23);
        ___qtablewidgetitem23->setText(QApplication::translate("MainWindow", "valor / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem24 = tableroPlan->horizontalHeaderItem(24);
        ___qtablewidgetitem24->setText(QApplication::translate("MainWindow", "km / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem25 = tableroPlan->horizontalHeaderItem(25);
        ___qtablewidgetitem25->setText(QApplication::translate("MainWindow", "Co2 / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem26 = tableroPlan->horizontalHeaderItem(26);
        ___qtablewidgetitem26->setText(QApplication::translate("MainWindow", "Hrs / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem27 = tableroPlan->horizontalHeaderItem(27);
        ___qtablewidgetitem27->setText(QApplication::translate("MainWindow", "VIOLO Ventana", nullptr));
        QTableWidgetItem *___qtablewidgetitem28 = tableroPlan->horizontalHeaderItem(28);
        ___qtablewidgetitem28->setText(QApplication::translate("MainWindow", "VIOLO Vol", nullptr));
        QTableWidgetItem *___qtablewidgetitem29 = tableroPlan->horizontalHeaderItem(29);
        ___qtablewidgetitem29->setText(QApplication::translate("MainWindow", "VIOLO Peso", nullptr));
        QTableWidgetItem *___qtablewidgetitem30 = tableroPlan->horizontalHeaderItem(30);
        ___qtablewidgetitem30->setText(QApplication::translate("MainWindow", "VIOLO Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem31 = tableroPlan->verticalHeaderItem(0);
        ___qtablewidgetitem31->setText(QApplication::translate("MainWindow", "Resumen Plan", nullptr));
        QTableWidgetItem *___qtablewidgetitem32 = tableroPlan->verticalHeaderItem(1);
        ___qtablewidgetitem32->setText(QApplication::translate("MainWindow", "Optimizado", nullptr));
        QTableWidgetItem *___qtablewidgetitem33 = tableroPlan->verticalHeaderItem(2);
        ___qtablewidgetitem33->setText(QApplication::translate("MainWindow", "Diferencia", nullptr));

        const bool __sortingEnabled = tableroPlan->isSortingEnabled();
        tableroPlan->setSortingEnabled(false);
        tableroPlan->setSortingEnabled(__sortingEnabled);

        groupBox->setTitle(QString());
        groupBox_6->setTitle(QString());
        lblTableroElPlanHeaderM3->setText(QApplication::translate("MainWindow", "m3\n"
"123", nullptr));
        lblTableroElPlanHeaderKg->setText(QApplication::translate("MainWindow", "Kg\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoViaje->setText(QApplication::translate("MainWindow", "$Viaje\n"
"123", nullptr));
        lblTableroElPlanHeaderPiezas->setText(QApplication::translate("MainWindow", "Pzas\n"
"123", nullptr));
        lblTableroElPlanHeaderPedidos->setText(QApplication::translate("MainWindow", "Pedidos\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoDestino->setText(QApplication::translate("MainWindow", "$Destino\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoPiezas->setText(QApplication::translate("MainWindow", "$Pzas\n"
"123", nullptr));
        lblTableroElPlanHeaderDestinos->setText(QApplication::translate("MainWindow", "Destinos\n"
"123", nullptr));
        lblTableroElPlanHeaderViajes->setText(QApplication::translate("MainWindow", "Viajes\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoTotal->setText(QApplication::translate("MainWindow", "$Total\n"
"123", nullptr));
        lblTableroElPlanHeaderKm->setText(QApplication::translate("MainWindow", "Km\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoM3->setText(QApplication::translate("MainWindow", "$m3\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoPedido->setText(QApplication::translate("MainWindow", "$Pedido\n"
"123", nullptr));
        lblTableroElPlanHeaderCostoKg->setText(QApplication::translate("MainWindow", "$Kg\n"
"123", nullptr));
#ifndef QT_NO_TOOLTIP
        gridElPlan->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        btnRecalculaPlan->setText(QString());
        btnOptimizaPlan->setText(QString());
        btnGuardaPlan->setText(QString());
        btnColorDPlan->setText(QString());
        btnColorSinPlan->setText(QString());
        comboNombrePlan->setItemText(0, QApplication::translate("MainWindow", "Plan 22 octubre 1994", nullptr));
        comboNombrePlan->setItemText(1, QApplication::translate("MainWindow", "Plan 31 septiembre 1995", nullptr));

#ifndef QT_NO_TOOLTIP
        comboNombrePlan->setToolTip(QApplication::translate("MainWindow", "Nombre del Plan ", nullptr));
#endif // QT_NO_TOOLTIP
        btnRefrescaPlan->setText(QString());
        btnColorVPlan->setText(QString());
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1ElPlan), QApplication::translate("MainWindow", "EL PLAN", nullptr));
        groupBox_4->setTitle(QString());
        groupBoxCrearPlan_Pedidos->setTitle(QApplication::translate("MainWindow", "PEDIDOS", nullptr));

        const bool __sortingEnabled1 = listGenerarPlanPedidos->isSortingEnabled();
        listGenerarPlanPedidos->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listGenerarPlanPedidos->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem1 = listGenerarPlanPedidos->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem2 = listGenerarPlanPedidos->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanPedidos->setSortingEnabled(__sortingEnabled1);

        groupBox_12->setTitle(QApplication::translate("MainWindow", "VEHICULOS", nullptr));

        const bool __sortingEnabled2 = listGenerarPlanVehiculos->isSortingEnabled();
        listGenerarPlanVehiculos->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem3 = listGenerarPlanVehiculos->item(0);
        ___qlistwidgetitem3->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem4 = listGenerarPlanVehiculos->item(1);
        ___qlistwidgetitem4->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem5 = listGenerarPlanVehiculos->item(2);
        ___qlistwidgetitem5->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanVehiculos->setSortingEnabled(__sortingEnabled2);

        groupBox_13->setTitle(QApplication::translate("MainWindow", "OPERADORES", nullptr));

        const bool __sortingEnabled3 = listGenerarPlanOperadores->isSortingEnabled();
        listGenerarPlanOperadores->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem6 = listGenerarPlanOperadores->item(0);
        ___qlistwidgetitem6->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem7 = listGenerarPlanOperadores->item(1);
        ___qlistwidgetitem7->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem8 = listGenerarPlanOperadores->item(2);
        ___qlistwidgetitem8->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanOperadores->setSortingEnabled(__sortingEnabled3);

        btnGeneraPlan->setText(QApplication::translate("MainWindow", "CREAR", nullptr));
        editNombrePlanGenera->setPlaceholderText(QApplication::translate("MainWindow", "nombre del plan a guardar en Google Drive", nullptr));
        groupBox_5->setTitle(QString());
        groupBox_19->setTitle(QApplication::translate("MainWindow", "PREFERENCIAS", nullptr));

        const bool __sortingEnabled4 = listGenerarPlanPreferencias_2->isSortingEnabled();
        listGenerarPlanPreferencias_2->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem9 = listGenerarPlanPreferencias_2->item(0);
        ___qlistwidgetitem9->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem10 = listGenerarPlanPreferencias_2->item(1);
        ___qlistwidgetitem10->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem11 = listGenerarPlanPreferencias_2->item(2);
        ___qlistwidgetitem11->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanPreferencias_2->setSortingEnabled(__sortingEnabled4);

        groupBox_15->setTitle(QApplication::translate("MainWindow", "DESTINOS", nullptr));

        const bool __sortingEnabled5 = listGenerarPlanDestinos->isSortingEnabled();
        listGenerarPlanDestinos->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem12 = listGenerarPlanDestinos->item(0);
        ___qlistwidgetitem12->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem13 = listGenerarPlanDestinos->item(1);
        ___qlistwidgetitem13->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem14 = listGenerarPlanDestinos->item(2);
        ___qlistwidgetitem14->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanDestinos->setSortingEnabled(__sortingEnabled5);

        groupBox_17->setTitle(QApplication::translate("MainWindow", "TARIFARIO", nullptr));

        const bool __sortingEnabled6 = listGenerarPlanTarifario->isSortingEnabled();
        listGenerarPlanTarifario->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem15 = listGenerarPlanTarifario->item(0);
        ___qlistwidgetitem15->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem16 = listGenerarPlanTarifario->item(1);
        ___qlistwidgetitem16->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem17 = listGenerarPlanTarifario->item(2);
        ___qlistwidgetitem17->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanTarifario->setSortingEnabled(__sortingEnabled6);

        groupBox_18->setTitle(QApplication::translate("MainWindow", "MATRIZ COMPATIBILIDAD", nullptr));

        const bool __sortingEnabled7 = listGenerarPlanComp->isSortingEnabled();
        listGenerarPlanComp->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem18 = listGenerarPlanComp->item(0);
        ___qlistwidgetitem18->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem19 = listGenerarPlanComp->item(1);
        ___qlistwidgetitem19->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem20 = listGenerarPlanComp->item(2);
        ___qlistwidgetitem20->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listGenerarPlanComp->setSortingEnabled(__sortingEnabled7);

        tab2Planear->setTabText(tab2Planear->indexOf(tab2PlanearCrear), QApplication::translate("MainWindow", "CREAR", nullptr));
        groupBox_25->setTitle(QString());
        btnAbrePlan->setText(QApplication::translate("MainWindow", "ABRIR", nullptr));

        const bool __sortingEnabled8 = listAbrePlan->isSortingEnabled();
        listAbrePlan->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem21 = listAbrePlan->item(0);
        ___qlistwidgetitem21->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem22 = listAbrePlan->item(1);
        ___qlistwidgetitem22->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem23 = listAbrePlan->item(2);
        ___qlistwidgetitem23->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listAbrePlan->setSortingEnabled(__sortingEnabled8);

        tab2Planear->setTabText(tab2Planear->indexOf(tab2PlanearAbrir), QApplication::translate("MainWindow", "ABRIR", nullptr));
        groupBox_20->setTitle(QApplication::translate("MainWindow", "AGREGA / BORRA PEDIDOS", nullptr));
        btnBorrarPedidos->setText(QApplication::translate("MainWindow", "BORRAR", nullptr));
        btnAnadirPedidos->setText(QApplication::translate("MainWindow", "AGREGA", nullptr));

        const bool __sortingEnabled9 = listAnadirPedidos->isSortingEnabled();
        listAnadirPedidos->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem24 = listAnadirPedidos->item(0);
        ___qlistwidgetitem24->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem25 = listAnadirPedidos->item(1);
        ___qlistwidgetitem25->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem26 = listAnadirPedidos->item(2);
        ___qlistwidgetitem26->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listAnadirPedidos->setSortingEnabled(__sortingEnabled9);

        groupBox_21->setTitle(QApplication::translate("MainWindow", "AGREGA PREDESPAGADOS", nullptr));
        btnAnadirPredespegados->setText(QApplication::translate("MainWindow", "AGREGA", nullptr));

        const bool __sortingEnabled10 = listAnadirPredespegados->isSortingEnabled();
        listAnadirPredespegados->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem27 = listAnadirPredespegados->item(0);
        ___qlistwidgetitem27->setText(QApplication::translate("MainWindow", "Archivo AA", nullptr));
        QListWidgetItem *___qlistwidgetitem28 = listAnadirPredespegados->item(1);
        ___qlistwidgetitem28->setText(QApplication::translate("MainWindow", "Archivo BB", nullptr));
        QListWidgetItem *___qlistwidgetitem29 = listAnadirPredespegados->item(2);
        ___qlistwidgetitem29->setText(QApplication::translate("MainWindow", "Archivo CC", nullptr));
        listAnadirPredespegados->setSortingEnabled(__sortingEnabled10);

        tab2Planear->setTabText(tab2Planear->indexOf(tab2PlanearAnadir), QApplication::translate("MainWindow", "AGREGA", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Planear), QApplication::translate("MainWindow", "PLANEAR", nullptr));
        lblMotorPedidos->setText(QApplication::translate("MainWindow", "Pedidos", nullptr));
        lblMotorDestinos->setText(QApplication::translate("MainWindow", "Destinos", nullptr));
        lblMotorKg->setText(QApplication::translate("MainWindow", "Kg", nullptr));
        lblMotorPiezas->setText(QApplication::translate("MainWindow", "Pzas", nullptr));
        lblMotorM3->setText(QApplication::translate("MainWindow", "m3", nullptr));
        lblMotorCostoPorViaje->setText(QApplication::translate("MainWindow", "$Viaje", nullptr));
        lblMotorCostoPorDestino->setText(QApplication::translate("MainWindow", "$Destino", nullptr));
        lblMotorCostoPorPedido->setText(QApplication::translate("MainWindow", "$Pedido", nullptr));
        lblMotorCostoPorPieza->setText(QApplication::translate("MainWindow", "$Pzas", nullptr));
        lblMotorCostoPorKg->setText(QApplication::translate("MainWindow", "$Kg", nullptr));
        lblMotorCostoPorM3->setText(QApplication::translate("MainWindow", "$m3", nullptr));
        lblMotorViajes->setText(QApplication::translate("MainWindow", "Viajes", nullptr));
        lblMotorTotalCosto->setText(QApplication::translate("MainWindow", "$Total", nullptr));
        lblMotorSegundos->setText(QApplication::translate("MainWindow", "Segundos", nullptr));
        lblMotorKm->setText(QApplication::translate("MainWindow", "Km", nullptr));
        btnMotorIniciar->setText(QApplication::translate("MainWindow", "INICIAR", nullptr));
        btnMotorDetener->setText(QApplication::translate("MainWindow", "DETENER", nullptr));
        tab2Rutear->setTabText(tab2Rutear->indexOf(tab2RutearMotor), QApplication::translate("MainWindow", "MOTOR", nullptr));
        tab2Rutear->setTabText(tab2Rutear->indexOf(tab2RutearCalibracion), QApplication::translate("MainWindow", "CALIBRACION", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Rutear), QApplication::translate("MainWindow", "RUTEAR", nullptr));
        groupBox_36->setTitle(QString());
        radioOptimizaCercanos->setText(QApplication::translate("MainWindow", "OPTIMIZA CERCANOS", nullptr));
        editAumentarKg->setText(QApplication::translate("MainWindow", "5", nullptr));
        radioOptimizaVacios->setText(QApplication::translate("MainWindow", "OPTIMIZA VACIOS", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "AUMENTAR CAPACIDAD PESO (%)", nullptr));
        editAumentarVol->setText(QApplication::translate("MainWindow", "5", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "AUMENTAR CAPACIDAD VOLUMEN (%)", nullptr));
        editAumentarVentana->setText(QApplication::translate("MainWindow", "15", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "AUMENTAR VENTANA ENTREGA (MINS)", nullptr));
        editAumentarValor->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "CERCANIA MAX. (km)", nullptr));
        editOcupacion->setText(QApplication::translate("MainWindow", "90", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "AUMENTAR VALOR (000 PESOS)", nullptr));
        editCercania->setText(QApplication::translate("MainWindow", "50", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "OCUPACION MENOR AL (%)", nullptr));
        btnCargaPlanOptiAuto->setText(QApplication::translate("MainWindow", "CARGAR PLAN", nullptr));
        btnOptimizarVaciosCercanos->setText(QApplication::translate("MainWindow", "OPTIMIZAR", nullptr));
        checkIgnorarComp->setText(QApplication::translate("MainWindow", "IGNORA M. COMPATIBILIDAD", nullptr));
        checkIgnorarAcceso->setText(QApplication::translate("MainWindow", "IGNORA RESTRICCION ACCESO", nullptr));
        groupBox_7->setTitle(QString());
        lblTableroAutoHeaderM3->setText(QApplication::translate("MainWindow", "m3\n"
"123", nullptr));
        lblTableroAutoHeaderKg->setText(QApplication::translate("MainWindow", "Kg\n"
"123", nullptr));
        lblTableroAutoHeaderCostoViaje->setText(QApplication::translate("MainWindow", "$Viaje\n"
"123", nullptr));
        lblTableroAutoHeaderPiezas->setText(QApplication::translate("MainWindow", "Pzas\n"
"123", nullptr));
        lblTableroAutoHeaderPedidos->setText(QApplication::translate("MainWindow", "Pedidos\n"
"123", nullptr));
        lblTableroAutoHeaderCostoDestino->setText(QApplication::translate("MainWindow", "$Destino\n"
"123", nullptr));
        lblTableroAutoHeaderCostoPieza->setText(QApplication::translate("MainWindow", "$Pzas\n"
"123", nullptr));
        lblTableroAutoHeaderDestinos->setText(QApplication::translate("MainWindow", "Destinos\n"
"123", nullptr));
        lblTableroAutoHeaderViajes->setText(QApplication::translate("MainWindow", "Viajes\n"
"123", nullptr));
        lblTableroAutoHeaderTotalCosto->setText(QApplication::translate("MainWindow", "$Total\n"
"123", nullptr));
        lblTableroAutoHeaderKm->setText(QApplication::translate("MainWindow", "Km\n"
"123", nullptr));
        lblTableroAutoHeaderCostoM3->setText(QApplication::translate("MainWindow", "$m3\n"
"123", nullptr));
        lblTableroAutoHeaderCostoPedido->setText(QApplication::translate("MainWindow", "$Pedido\n"
"123", nullptr));
        lblTableroAutoHeaderCostoKg->setText(QApplication::translate("MainWindow", "$Kg\n"
"123", nullptr));
        QTableWidgetItem *___qtablewidgetitem34 = tableroAuto->horizontalHeaderItem(0);
        ___qtablewidgetitem34->setText(QApplication::translate("MainWindow", "$/Pza", nullptr));
        QTableWidgetItem *___qtablewidgetitem35 = tableroAuto->horizontalHeaderItem(1);
        ___qtablewidgetitem35->setText(QApplication::translate("MainWindow", "$/Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem36 = tableroAuto->horizontalHeaderItem(2);
        ___qtablewidgetitem36->setText(QApplication::translate("MainWindow", "$/M3", nullptr));
        QTableWidgetItem *___qtablewidgetitem37 = tableroAuto->horizontalHeaderItem(3);
        ___qtablewidgetitem37->setText(QApplication::translate("MainWindow", "$/Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem38 = tableroAuto->horizontalHeaderItem(4);
        ___qtablewidgetitem38->setText(QApplication::translate("MainWindow", "$/Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem39 = tableroAuto->horizontalHeaderItem(5);
        ___qtablewidgetitem39->setText(QApplication::translate("MainWindow", "$ Total", nullptr));
        QTableWidgetItem *___qtablewidgetitem40 = tableroAuto->horizontalHeaderItem(6);
        ___qtablewidgetitem40->setText(QApplication::translate("MainWindow", "Km", nullptr));
        QTableWidgetItem *___qtablewidgetitem41 = tableroAuto->horizontalHeaderItem(7);
        ___qtablewidgetitem41->setText(QApplication::translate("MainWindow", "Co2", nullptr));
        QTableWidgetItem *___qtablewidgetitem42 = tableroAuto->horizontalHeaderItem(8);
        ___qtablewidgetitem42->setText(QApplication::translate("MainWindow", "% Ocupa. m3", nullptr));
        QTableWidgetItem *___qtablewidgetitem43 = tableroAuto->horizontalHeaderItem(9);
        ___qtablewidgetitem43->setText(QApplication::translate("MainWindow", "% Ocupa. Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem44 = tableroAuto->horizontalHeaderItem(10);
        ___qtablewidgetitem44->setText(QApplication::translate("MainWindow", "Pedidos", nullptr));
        QTableWidgetItem *___qtablewidgetitem45 = tableroAuto->horizontalHeaderItem(11);
        ___qtablewidgetitem45->setText(QApplication::translate("MainWindow", "Destinos", nullptr));
        QTableWidgetItem *___qtablewidgetitem46 = tableroAuto->horizontalHeaderItem(12);
        ___qtablewidgetitem46->setText(QApplication::translate("MainWindow", "Viajes", nullptr));
        QTableWidgetItem *___qtablewidgetitem47 = tableroAuto->horizontalHeaderItem(13);
        ___qtablewidgetitem47->setText(QApplication::translate("MainWindow", "m3", nullptr));
        QTableWidgetItem *___qtablewidgetitem48 = tableroAuto->horizontalHeaderItem(14);
        ___qtablewidgetitem48->setText(QApplication::translate("MainWindow", "Kg", nullptr));
        QTableWidgetItem *___qtablewidgetitem49 = tableroAuto->horizontalHeaderItem(15);
        ___qtablewidgetitem49->setText(QApplication::translate("MainWindow", "Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem50 = tableroAuto->horizontalHeaderItem(16);
        ___qtablewidgetitem50->setText(QApplication::translate("MainWindow", "Tiempo", nullptr));
        QTableWidgetItem *___qtablewidgetitem51 = tableroAuto->horizontalHeaderItem(17);
        ___qtablewidgetitem51->setText(QApplication::translate("MainWindow", "$ / Km", nullptr));
        QTableWidgetItem *___qtablewidgetitem52 = tableroAuto->horizontalHeaderItem(18);
        ___qtablewidgetitem52->setText(QApplication::translate("MainWindow", "$ / Co2", nullptr));
        QTableWidgetItem *___qtablewidgetitem53 = tableroAuto->horizontalHeaderItem(19);
        ___qtablewidgetitem53->setText(QApplication::translate("MainWindow", "$ / Hora", nullptr));
        QTableWidgetItem *___qtablewidgetitem54 = tableroAuto->horizontalHeaderItem(20);
        ___qtablewidgetitem54->setText(QApplication::translate("MainWindow", "Piezas / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem55 = tableroAuto->horizontalHeaderItem(21);
        ___qtablewidgetitem55->setText(QApplication::translate("MainWindow", "m3 / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem56 = tableroAuto->horizontalHeaderItem(22);
        ___qtablewidgetitem56->setText(QApplication::translate("MainWindow", "kg / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem57 = tableroAuto->horizontalHeaderItem(23);
        ___qtablewidgetitem57->setText(QApplication::translate("MainWindow", "valor / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem58 = tableroAuto->horizontalHeaderItem(24);
        ___qtablewidgetitem58->setText(QApplication::translate("MainWindow", "km / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem59 = tableroAuto->horizontalHeaderItem(25);
        ___qtablewidgetitem59->setText(QApplication::translate("MainWindow", "Co2 / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem60 = tableroAuto->horizontalHeaderItem(26);
        ___qtablewidgetitem60->setText(QApplication::translate("MainWindow", "Hrs / Viaje", nullptr));
        QTableWidgetItem *___qtablewidgetitem61 = tableroAuto->horizontalHeaderItem(27);
        ___qtablewidgetitem61->setText(QApplication::translate("MainWindow", "Viol\303\263 Ventana", nullptr));
        QTableWidgetItem *___qtablewidgetitem62 = tableroAuto->horizontalHeaderItem(28);
        ___qtablewidgetitem62->setText(QApplication::translate("MainWindow", "Viol\303\263 Vol", nullptr));
        QTableWidgetItem *___qtablewidgetitem63 = tableroAuto->horizontalHeaderItem(29);
        ___qtablewidgetitem63->setText(QApplication::translate("MainWindow", "Viol\303\263 Peso", nullptr));
        QTableWidgetItem *___qtablewidgetitem64 = tableroAuto->horizontalHeaderItem(30);
        ___qtablewidgetitem64->setText(QApplication::translate("MainWindow", "Viol\303\263 Valor", nullptr));
        QTableWidgetItem *___qtablewidgetitem65 = tableroAuto->verticalHeaderItem(0);
        ___qtablewidgetitem65->setText(QApplication::translate("MainWindow", "Resumen Plan", nullptr));
        QTableWidgetItem *___qtablewidgetitem66 = tableroAuto->verticalHeaderItem(1);
        ___qtablewidgetitem66->setText(QApplication::translate("MainWindow", "Optimizado", nullptr));
        QTableWidgetItem *___qtablewidgetitem67 = tableroAuto->verticalHeaderItem(2);
        ___qtablewidgetitem67->setText(QApplication::translate("MainWindow", "Diferencia", nullptr));

        const bool __sortingEnabled11 = tableroAuto->isSortingEnabled();
        tableroAuto->setSortingEnabled(false);
        tableroAuto->setSortingEnabled(__sortingEnabled11);

        btnOptimizaPlan_2->setText(QString());
        btnPropagaAuto->setText(QString());
        btnRefrescaAuto->setText(QString());
        btnGuardaAuto->setText(QString());
        tab2Optimizar->setTabText(tab2Optimizar->indexOf(tab2OptimizarAuto), QApplication::translate("MainWindow", "AUTOMATICO", nullptr));
        groupBox_28->setTitle(QApplication::translate("MainWindow", "PLAN", nullptr));
        groupBox_8->setTitle(QString());
        lblTableroManualHeaderCostoM3->setText(QApplication::translate("MainWindow", "$m3\n"
"123", nullptr));
        lblTableroManualHeaderCostoKg->setText(QApplication::translate("MainWindow", "$Kg\n"
"123", nullptr));
        lblTableroManualHeaderTotalCosto->setText(QApplication::translate("MainWindow", "$Total\n"
"123", nullptr));
        lblTableroManualHeaderKm->setText(QApplication::translate("MainWindow", "Km\n"
"123", nullptr));
        lblTableroManualHeaderCostoViaje->setText(QApplication::translate("MainWindow", "$Viaje\n"
"123", nullptr));
        lblTableroManualHeaderCostoPieza->setText(QApplication::translate("MainWindow", "$Pzas\n"
"123", nullptr));
        btnRefrescaManual->setText(QString());
        btnGuardaManual->setText(QString());
        btnColorVManual->setText(QString());
        btnColorDManual->setText(QString());
        btnColorSinManual->setText(QString());
        btnPropagaManual->setText(QString());
        btnOptimizaManual->setText(QString());
        btnRecalculaManual->setText(QString());
        pushButton_2->setText(QApplication::translate("MainWindow", "CARGAR PLAN", nullptr));
        groupBox_26->setTitle(QApplication::translate("MainWindow", "VEHICULO A", nullptr));
        lblV1Acceso->setText(QApplication::translate("MainWindow", "acceso", nullptr));
        lblV1M3->setText(QApplication::translate("MainWindow", "m3", nullptr));
        lblV1Kg->setText(QApplication::translate("MainWindow", "kg", nullptr));
        lblV1Hrs->setText(QApplication::translate("MainWindow", "hrs", nullptr));
        lblV1Comp->setText(QApplication::translate("MainWindow", "comp", nullptr));
        lblV1Valor->setText(QApplication::translate("MainWindow", "$", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "No.Viaje", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "Tipo Veh", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Max $", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "Max m3", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Max Kg", nullptr));
        editV1MaxKg->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV1MaxM3->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV1MaxValor->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV1Tipo->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV1NumViaje->setText(QApplication::translate("MainWindow", "1234", nullptr));
        btnV1XG->setText(QString());
        btnV1ColorV->setText(QString());
        btnV1Optimiza->setText(QString());
        btnV1Recalcula->setText(QString());
        btnV1ColorD->setText(QString());
        btnV1CH->setText(QString());
        btnV1M->setText(QString());
        btnV1Otro->setText(QString());
        btnV1G->setText(QString());
        btnV1ColorSin->setText(QString());
        btnV1Regresa->setText(QString());
        groupBox_27->setTitle(QApplication::translate("MainWindow", "VEHICULO B", nullptr));
        lblV2M3->setText(QApplication::translate("MainWindow", "m3", nullptr));
        lblV2Valor->setText(QApplication::translate("MainWindow", "$", nullptr));
        lblV2Kg->setText(QApplication::translate("MainWindow", "kg", nullptr));
        lblV2Comp->setText(QApplication::translate("MainWindow", "comp", nullptr));
        lblV2Acceso->setText(QApplication::translate("MainWindow", "acceso", nullptr));
        lblV2Hrs->setText(QApplication::translate("MainWindow", "hrs", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "Max m3", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "No.Viaje", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "Max $", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "Tipo Veh", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "Max Kg", nullptr));
        editV2MaxKg->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV2MaxM3->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV2MaxValor->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV2Tipo->setText(QApplication::translate("MainWindow", "1234", nullptr));
        editV2NumViaje->setText(QApplication::translate("MainWindow", "1234", nullptr));
        btnV2XG->setText(QString());
        btnV2Otro->setText(QString());
        btnV2M->setText(QString());
        btnV2CH->setText(QString());
        btnV2ColorSin->setText(QString());
        btnV2ColorV->setText(QString());
        btnV2ColorD->setText(QString());
        btnV2Recalcula->setText(QString());
        btnV2Optimiza->setText(QString());
        btnV2Regresa->setText(QString());
        btnV2G->setText(QString());
        tab2Optimizar->setTabText(tab2Optimizar->indexOf(tab2OptimizarManual), QApplication::translate("MainWindow", "MANUAL", nullptr));
        groupBoxCrearPlan_Pedidos_2->setTitle(QApplication::translate("MainWindow", "BITACORA VACIOS", nullptr));
        groupBoxCrearPlan_Pedidos_3->setTitle(QApplication::translate("MainWindow", "BITACORA CERCANOS", nullptr));
        tab2Optimizar->setTabText(tab2Optimizar->indexOf(tab2OptimizarBitacoras), QApplication::translate("MainWindow", "BITACORAS", nullptr));
        tab2Optimizar->setTabToolTip(tab2Optimizar->indexOf(tab2OptimizarBitacoras), QApplication::translate("MainWindow", "Ver Bit\303\203\302\241cora", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Optimizar), QApplication::translate("MainWindow", "OPTIMIZAR", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "PLAN", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "CARGAR", nullptr));
        btnRefreshAsignar->setText(QString());
        btnGuardaAsignar->setText(QString());
        btnPropagaAsignar->setText(QString());
        btnAutomaticoAsignar->setText(QApplication::translate("MainWindow", "AUTOMATICO", nullptr));
        groupBox_32->setTitle(QApplication::translate("MainWindow", "OPERADORES", nullptr));
        comboOperadoresAsignarPlan->setItemText(0, QApplication::translate("MainWindow", "Juan", nullptr));
        comboOperadoresAsignarPlan->setItemText(1, QApplication::translate("MainWindow", "Pedro", nullptr));
        comboOperadoresAsignarPlan->setItemText(2, QApplication::translate("MainWindow", "Mar\303\203\302\255a", nullptr));

        btnAbreOperadoresAsignarPlan->setText(QApplication::translate("MainWindow", "ABRIR", nullptr));
        groupBox_31->setTitle(QApplication::translate("MainWindow", "VEHICULOS", nullptr));
        comboVehAsignarPlan->setItemText(0, QApplication::translate("MainWindow", "Veh AA", nullptr));
        comboVehAsignarPlan->setItemText(1, QApplication::translate("MainWindow", "Veh BB", nullptr));
        comboVehAsignarPlan->setItemText(2, QApplication::translate("MainWindow", "Veh CC", nullptr));

        btnAbreVehAsignarPlan->setText(QApplication::translate("MainWindow", "ABRIR", nullptr));
        tab2Asignar->setTabText(tab2Asignar->indexOf(tab2AsignarPlan), QApplication::translate("MainWindow", "PLAN", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "PREDESPEGUE", nullptr));
        btnAsignarCargarPredespegue->setText(QApplication::translate("MainWindow", "CARGAR", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "ACTUALIZAR PREDESPEGUE", nullptr));
        groupBox_37->setTitle(QApplication::translate("MainWindow", "OPERADORES", nullptr));
        comboOperadoresAsignarPredespegue->setItemText(0, QApplication::translate("MainWindow", "Juan", nullptr));
        comboOperadoresAsignarPredespegue->setItemText(1, QApplication::translate("MainWindow", "Pedro", nullptr));
        comboOperadoresAsignarPredespegue->setItemText(2, QApplication::translate("MainWindow", "Mar\303\203\302\255a", nullptr));

        btnAbreOperadoresAsignarPredespegue->setText(QApplication::translate("MainWindow", "ABRIR", nullptr));
        groupBox_35->setTitle(QApplication::translate("MainWindow", "VEHICULOS", nullptr));
        comboVehAsignarPredespegue->setItemText(0, QApplication::translate("MainWindow", "Veh AA", nullptr));
        comboVehAsignarPredespegue->setItemText(1, QApplication::translate("MainWindow", "Veh BB", nullptr));
        comboVehAsignarPredespegue->setItemText(2, QApplication::translate("MainWindow", "Veh CC", nullptr));

        btnAbreVehAsignarPredespegue->setText(QApplication::translate("MainWindow", "ABRIR", nullptr));
        tab2Asignar->setTabText(tab2Asignar->indexOf(tab2AsignarPredespegue), QApplication::translate("MainWindow", "PREDESPEGUE", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Asignar), QApplication::translate("MainWindow", "ASIGNAR", nullptr));
        btnPredespegarPlan->setText(QApplication::translate("MainWindow", "PREDESPEGAR PLAN", nullptr));
        btnPredespegarSelecc->setText(QApplication::translate("MainWindow", "PREDESPEGAR VIAJES SELECCIONADOS", nullptr));
        btnCargaPlanPredespegar->setText(QApplication::translate("MainWindow", "CARGAR PLAN", nullptr));
        tab2Despegar->setTabText(tab2Despegar->indexOf(tab2DespegarPredespegue), QApplication::translate("MainWindow", "PREDESPEGAR", nullptr));
        groupBox_33->setTitle(QApplication::translate("MainWindow", "PREDESPEGUE", nullptr));
        btnDespegarCargaPredespegados->setText(QApplication::translate("MainWindow", "CARGAR PREDESPEGADOS", nullptr));
        btnDespegar->setText(QApplication::translate("MainWindow", "DESPEGAR", nullptr));
        labelDespeguePIN->setText(QApplication::translate("MainWindow", "PIN: 123456", nullptr));
        groupBox_34->setTitle(QApplication::translate("MainWindow", "DESPEGADOS", nullptr));
        btnDespegarCargaDespegados->setText(QApplication::translate("MainWindow", "CARGAR DESPEGADOS", nullptr));
        tab2Despegar->setTabText(tab2Despegar->indexOf(tab2DespegarDespegar), QApplication::translate("MainWindow", "DESPEGAR", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Despegar), QApplication::translate("MainWindow", "DESPEGAR", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar1), QApplication::translate("MainWindow", "BSC", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar2), QApplication::translate("MainWindow", "GASTOS", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar3), QApplication::translate("MainWindow", "SATISFACCION", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar4), QApplication::translate("MainWindow", "VENTAS", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar5), QApplication::translate("MainWindow", "PRODUCCION", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar6), QApplication::translate("MainWindow", "OPERACION", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar7), QApplication::translate("MainWindow", "DISTANCIAS", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar8), QApplication::translate("MainWindow", "ULTIMO", nullptr));
        tab2Analizar->setTabText(tab2Analizar->indexOf(tab2Analizar9), QApplication::translate("MainWindow", "DEEP LEARNING", nullptr));
        tabWidget1Principal->setTabText(tabWidget1Principal->indexOf(tab1Analizar), QApplication::translate("MainWindow", "ANALIZAR", nullptr));
        logo_footer->setText(QApplication::translate("MainWindow", "A R M S", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
