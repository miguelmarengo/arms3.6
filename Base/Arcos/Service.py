
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    CLASE SERVICE

    @autor Oscar Diaz (hyper)
    @Description
    Esta clase sirve de base para consultar todo lo relacionado
    a los arcos
    
    metodos:
        1) synchronize
        @description 
           verificar la existencia del archivo local si no existe
           traerlo de s3 y guardarlo localmente
           @return
           boolean

        2) @description
           obtener las distancias y tiempo entre los destinos pedidos
           @return 
           json distancias

        3) @description
           estimar el tiempo que tardara en calcular cierto número 
           de arcos, que no existan en la matriz.
           @return
           

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import googlemaps
import boto3
import botocore
from time import gmtime, strftime
import json
import os
import sys
#import pyrebase
import pandas as pd
from io import StringIO
from .ConfigH import Config
from pathlib import Path
from IO.Apis.destinos_unicos import *




class DistanceService:
    def __init__(self,ui,empresa, cedi):
        print("SERVICIO DE ARCOS ACTIVO")
        #almacenando datos
        self.ui         = ui
        self.empresa    = empresa
        self.cedi       = cedi
        self.config     = Config()
        self.home       = str(Path.home())
        self.pathFolder = self.home+"/apps/arms/"+empresa+"/"+cedi+"/arcos"
        self.pathFile   = self.pathFolder+"/arcos.txt"

    def validate(self,statusBar):
        destinos = None
        self.statusBar = statusBar
        try:
            destinos = fnObtenerDestinos()   
        except Exception as ex:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"fallo al leer destinos unicos API")
            return False

        
        if destinos is not None:
            try:  
                synchronizeService = False             
                self.statusBar.emit({'progressBar': 0, 'lblMensaje': 'Verificando distancias...'})                
                response = self.calculateTime(json.dumps(destinos))               
                self.statusBar.emit({'progressBar': 50, 'lblMensaje': 'Revisando distancias faltantes...'})   
                print(response)    
                #caso especial si no existe el archivo de arcos trata de sincronizar y al terminar intenta nuevamente obtener el tiempo faltante
                if response["message"] == "no existe el archivo de arcos localmente":
                    self.synchronizeFile()
                    synchronizeService = True
                    print("se creo el archivo")
                
                if synchronizeService:
                    print("intenta validar una vez creado el archivo localmente")
                    self.statusBar.emit({'progressBar': 0, 'lblMensaje': 'Verificando distancias...'})
                    self.statusBar.emit({'progressBar': 50, 'lblMensaje': 'Revisando distancias faltantes...'})
                    response = self.calculateTime(json.dumps(destinos))  
                    if response["message"] == "no existe el archivo de arcos localmente":
                        return False
                    
                if response["data"][1]["faltantes"] == 0:
                    print("ya puede correr")
                    self.statusBar.emit({'progressBar': 100, 'lblMensaje': 'Existen todas las distancias...'})
                    return True
                else: 
                    self.calculateMissing(json.dumps(destinos))
                    response = self.calculateTime(json.dumps(destinos))
                    if response["data"][1]["faltantes"] == 0:
                        return True
                    else:
                        return False
                return False

            except Exception as ex:
                return False
        return False

    # regresa el dataframe del archivo 
    def synchronizeFile(self):

        '''
            CASOS

            1) no existe el archivo de s3 y tampoco el local
                Solucion:
                    - crear archivo arcos local vacio
            2) existe el archivo de s3 y local no existe
                Solucion:
                    - crear el archivo de arcos local con el contenido de s3
            3) existe el archivo de s3 y tambien el local 
                #TODO validar no estoy muy seguro de la funcionalidad en multi usuario
                Solucion:
                    - verificar las fechas de creacion
                        - si el de s3 es mas reciente reemplazar el local 
                        - si el local es mas reciente reemplazar s3
            4) no existe el archivo de s3 pero si el local
                Solucion:
                    - subir el archivo local a s3
        '''

        #buscar en s3 el archivo de arcos
        
        fileS3,success = self._searchFileS3()        
        fileLocal,success = self._searchLocalFile()
        
        #fileLocal = None
        #fileS3  = None



        if fileS3 is  None and fileLocal is None:
            print("caso 1")
            self._createLocalFile(pd.DataFrame())
        elif fileS3 is not None and fileLocal is None:
            print("caso 2")
            self._createLocalFile(fileS3)
        elif fileS3 is not None and fileLocal is not None:
            print("caso 3")            
        elif fileS3 is None and fileLocal is not None:
            print("caso 4")
            saved  = self._createS3File(fileLocal)

    

    def calculateTime(self,request):

        #convertir datos a json
        reqJson = None
        messageResponse = ""
        try:
            reqJson = json.loads(request)    
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"la peticion no viene en formato JSON valido")
            
        
        if reqJson is not None:
            #obtener el archivo local
            fileLocal,success = self._searchLocalFile()

            if fileLocal is not None:
                            
                reqJson = reqJson["data"]
                run,missing,json_results,dataFrameFile =self._verifyDifferencesDataFrame(reqJson,fileLocal)

                m = len(missing)
                print("arcos faltantes: "+str(m))
                if m>0:
                    return self._timeProcess(m)
                else:
                    messageResponse="ya existen todos los arcos"

            else:
                print("falta sincronizar...")
                #print("sincronizando...")
                #self.synchronizeFile()
                
                messageResponse="no existe el archivo de arcos localmente"
        else:
            messageResponse="la petición no tiene un formato JSON válido"

        return {
                "data":[
                    {"tiempo" : 0},
                    {"faltantes":0},
                    {"ticket":0}
                ],
                "message":messageResponse
                }

    def calculateMissing(self,request):

        print("calculando arcos faltantes")

        try:
            reqJson = json.loads(request)    
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"la peticion no viene en formato JSON valido")

        if reqJson is not None:
            #obtener el archivo local
            fileLocal,success = self._searchLocalFile()

            if fileLocal is not None:
                reqJson = reqJson["data"]
                run,missing,json_results,dataFrameFile =self._verifyDifferencesDataFrame(reqJson,fileLocal)

                m = len(missing)
                print("arcos faltantes en calcular: "+str(m))
                print(m)
                if m>0:
                    #return self._timeProcess(m)

                    gmaps = googlemaps.Client(client_id="gme-aselogasesoriaylogistica", client_secret="dOMPldxavHvLFQPV1_dmTlRAEME=")
                    time = strftime("%Y-%m-%d %H:%M:%S", gmtime())

                    newContent = []
                    total = len(missing)
                    count=0
                    percent = 0

                    

                    for idx, missing in enumerate(missing):
                        #pd.read_json('[{"A": 1, "B": 2}, {"A": 3, "B": 4}]')
                        self.statusBar.emit(
                            {'progressBar': percent, 'lblMensaje': str(count)+"/"+ str(total)+" calculando distancias entre destinos...", 'TotalCosto': 0, 'Viajes': 0,
                    'KmTotales': 0, 'CostoXViaje': 0,  'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,  'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [], 'datosGraficaY': []}                            
                        )
                        c = self._processNewDistance(missing,gmaps,time)
                        if c!=None:
                            newContent.append(c)                        
                        count +=1                  
                        if count==total:                            
                            percent = 100
                        else:
                            percent = int(round(count*100/total)) 
                        
                        

                    dfResults = None
                    try:
                        dfResults = pd.read_json(json.dumps(newContent))                    
                    except Exception as ex:
                        print(ex)

                    if dfResults is not None:
                        print("tranformacion correcta")
                        content = None
                        if dataFrameFile.shape[0]==0:
                            #solo guardar el dataFrame de resultados      
                            print("no hay nada en el archivo de arcos")                      
                            content = dfResults[['destino_a','destino_b','distancia','tiempo','latitud_a','longitud_b','latitud_b','longitud_b','fecha']]   
                            #content = dfResults.to_csv(None,sep="\t",header=False,index = False).encode()                    
                        else:
                            #crear merge de los dataframes
                            print("merge resultados y arcos")
                            dfToSave = pd.concat([dataFrameFile,dfResults],ignore_index=True)
                            content = dfToSave[['destino_a','destino_b','distancia','tiempo','latitud_a','longitud_b','latitud_b','longitud_b','fecha']]                    
                            #content = dfToSave.to_csv(None,sep="\t",header=False,index = False).encode()
                        
                        if content is not None:

                            #guardar localmente
                            self._createLocalFile(content)

                            '''
                            statusSaved = S3Service.saveS3(company,name,content)
                            if statusSaved:
                                print("--ARCOS GUARDADOS--")
                            if ticket is not None:
                                S3Service.ticketFireBaseConsume(company,name,ticket)
                            '''

                    else:
                        print("no hay nada que guardar por el fallo de la transformacion")
                else:
                    messageResponse="ya existen todos los arcos"
                    return json_results

            else:
                print("falta sincronizar...")
                self.synchronizeFile()
                print("sincronizando...")
                messageResponse="no existe el archivo de arcos localmente"
        else:
            messageResponse="la petición no tiene un formato JSON válido"

        return None

    def _processNewDistance(self,missing,gmaps,time):

        destino_01 = missing["destino_01"]
        destino_02 = missing["destino_02"]

        #print("no ha sido creada esta combinacion")
        c = None;
        
        try:
            directions_result = gmaps.distance_matrix(
                {
                "lat":destino_01["latitud"]
                ,"lng":destino_01["longitud"]
                }
                ,{
                "lat":destino_02["latitud"]
                ,"lng":destino_02["longitud"]
                }
                )
            
            res = directions_result["rows"][0]["elements"][0]
            if directions_result["status"] == "OK" and res["status"]!= "ZERO_RESULTS":

                #fileContent ="destino_a\tdestino_b\tdistancia\ttiempo\tlatitud_a\tlongitud_a\tlatitud_b\tlongitud_b\tfecha\n
                c = {
                    "destino_a":destino_01["iddestino"],
                    "destino_b":destino_02["iddestino"],
                    "distancia":str(res["distance"]["value"]),
                    "tiempo":str(res["duration"]["value"]),
                    "latitud_a":str(destino_01["latitud"]),
                    "longitud_a":str(destino_01["longitud"]),
                    "latitud_b":str(destino_02["latitud"]),
                    "longitud_b":str(destino_02["longitud"]),
                    "fecha":time
                }
                
                #c = destino_01["iddestino"] +"\t" +destino_02["iddestino"] +"\t"+ str(res["distance"]["value"]) +"\t" + str(res["duration"]["value"]) +"\t"  +str(destino_01["latitud"]) +"\t" +str(destino_01["longitud"]) +"\t" +str(destino_02["latitud"]) +"\t"  +str(destino_02["longitud"]) + "\t"+time+"\n"

            else:
                print("arco no calculado")
        except Exception as e:
            print("arco no calculado")
            print(e)
        return c
        

    

    
    def _timeProcess(self,missing):
        print("3) calculando tiempo restante")
        obj = self.ticketFireBase(missing,self.empresa,self.cedi)

        if obj is not None:

            token = "sin servicio"
            if obj is not None:
                token = obj["name"]

            time_default = 0.019
            if missing < 50:
                time_default = 0.012
            elif missing>50 and missing <100:
                time_default = 0.013
            time = ((missing)*time_default)/60
            return {
                    "data":[
                        {"tiempo" : str(round(time, 3)) + " min"},
                        {"faltantes":str(missing)},
                        {"ticket":token}
                    ],"message":""
                    }
        else:
            return {
                    "data":[
                        {"tiempo" : "-1"},
                        {"faltantes":str(missing)},
                        {"ticket":""}
                    ],"message":"sin servicio de arcos"
                    }


    def ticketFireBase(self,missing,company,cedi):
        print("===> Creando ticket")
        #TODO extraer a un archivo de configuracion
        config = {
          "apiKey": "AIzaSyAaaeFDBGen5zUwClGvwXygyg4r1nGXY4s",
          "authDomain": "arcostickets.firebaseapp.com",
          "databaseURL": "https://arcostickets.firebaseio.com/",
          "storageBucket": "arcostickets.appspot.com"
        }
        obj = None
        try:
            firebase = pyrebase.initialize_app(config)
            auth = firebase.auth()
            user = auth.sign_in_with_email_and_password("arms@arete.ws", "123456")
            db = firebase.database()

            #generar codigo
            ticket = {"status":"pendiente","arcos":missing}
            
            #asegura que no truene firebase por tener el cedi un punto
            cedi = cedi.replace(".", "_")     
            cedi = cedi.replace("#", "_")
            cedi = cedi.replace("[", "_")
            cedi = cedi.replace("]", "_")
            cedi = cedi.replace("$", "_")       
            cedi = cedi.replace("/", "_")
            obj = db.child("prod/"+company+"/"+cedi).push(ticket, user['idToken'])
        except Exception as e:
            print("token no generado")
            print(e)           
            return None

        return obj

        
    def _verifyDifferencesDataFrame(self,unique_places_orders,file):
        
        run = False
        missing = list()
        json_results = {"data":[]}

        '''
        casos encontrados

        1) el usuario desea forzar el reacalcular todos los arcos en su peticion
            SOLUCION:
                - si todos los arcos son forzados encontrar la diferencia entre estas combinaciones
                  y el archivo de ARCOS 
                - convertir los arcos forzados en FALTANTES
                - regresar la diferencia del archivo ( es lo que se debe de guardar al final para evitar duplicados)
                - regresar FALTANTES y ARCHIVO
        2) el usuario no desea forzar al recalculo y solo verificar si ya tiene todos los arcos
            SOLUCION:
                - si todos los arcos son de verificacion, encontrar la diferencia entre estas combinaciones
                  y el archivo de ARCOS estos seran los arcos FALTANTES
                - si los FALTANTES son 0 indica que ya tiene todos los arcos por lo que
                    - regresa un JSON_RESULT con las distancias entre todos
                    - regresa FALTANTES vacio ( ya no hay nada que calcular )
                - si hay FALTANTES
                    - convertirlos en el json FALTANTES
                    - regresar FALTANTES
                    - regresar JSON_RESULT vacio ( no existen todos los arcos )
        3) peticion del usuario mixta forzados y no forzados
            SOLUCION
                - FORZADOS
                    - quitar del archivo de ARCOS los forzados
                        - convertirlos en FALTANTES
                        - generar un DF de lo restante del archivo de ARCOS
                - NO FORZADOS
                    - comparar el generado de restantes de arcos contra los NO FORZADOS
                      y generar un DF de FALTANTES 
                    - si no hay FALTANTES en los no forzados
                        - regresar DF restantes ARCOS 
                    - agrupar los FALTANTES forzados con los FALTANTES convertilos en DF faltantes
                - regresar DF restantes ARCOS y DF faltantes 
        '''



         #identificar el tipo de arcos forzado y no forzado
        recalculate = False  
        content = ""
        contentRecalculate = ""    
        content,contentRecalculate = self._verifyCombinations(unique_places_orders,content,contentRecalculate) 
        dataFrameFile =pd.DataFrame()

        try:            
            #convertir archivo de arcos a dataFrame
            #header_df_file ="destino_a\tdestino_b\tdistancia\ttiempo\tlatitud_a\tlongitud_a\tlatitud_b\tlongitud_b\tfecha\n"
            header_df_request ="destino_a\tdestino_b\tlatitud_a\tlongitud_a\tlatitud_b\tlongitud_b\n"

            dataFrameArcos = file

            if contentRecalculate != "" and content=="":

                print("existen solo arcos forzados: ")                

                dataFrameForced,dataFrameFile = self._verifyMergeForced(header_df_request+contentRecalculate,dataFrameArcos);

                #print("arcos forzados: " + str(dataFrameForced.shape[0]) + " restante: " + str(dataFrameFile.shape[0]) )

                #convertir forzados en faltantes
                missing = self._verifyDataFrameMissing(missing,dataFrameForced)
               
                run = True
                return run,missing,json_results,dataFrameFile



            elif contentRecalculate =="" and content!="":

               
                dataFrameRequest= pd.read_csv(StringIO(header_df_request+content),sep='\t')
                dataFrameMissing = pd.merge(dataFrameRequest,dataFrameArcos,how='left',on=['destino_a', 'destino_b'],indicator=True)
              
                #obtener faltantes
                dataFrameMissingRes = dataFrameMissing[dataFrameMissing._merge=='left_only'][['destino_a', 'destino_b','distancia','tiempo','latitud_a_x','longitud_a_x','latitud_b_x','longitud_b_x','fecha']]
                  #cambiando nombres de columnas
                dataFrameMissingRes.columns = ['destino_a', 'destino_b','distancia','tiempo','latitud_a','longitud_a','latitud_b','longitud_b','fecha']            
              
                if dataFrameMissingRes.shape[0]==0:
                    
                    dfResp = pd.merge(dataFrameRequest, dataFrameArcos[['destino_a', 'destino_b','distancia','tiempo']], how='inner', on=['destino_a', 'destino_b'])          
                                        
                    for index, row in dfResp.iterrows():
                        json_results["data"].append(
                            {
                                "idorigen":row['destino_a'],"iddestino":row['destino_b'],"distancia":row["distancia"],"tiempo":row["tiempo"]
                            }
                        )
                    
                    run = True
                    return run,missing,json_results,dataFrameArcos
                else:
                    print("faltan varios arcos de la peticion")
                    #convertir forzados en faltantes
                    missing = self._verifyDataFrameMissing(missing,dataFrameMissingRes)
                    run = True
                    return run,missing,json_results,dataFrameArcos
            elif contentRecalculate !="" and content!="":

                print("mixto")     

                #obtiene dos dataframes , los que se van a forzar y el resultado de quitar en el archivo de arcos los forzados                
                dataFrameForced,dataFrameFile = self._verifyMergeForced(header_df_request+contentRecalculate,dataFrameArcos);


                dataFrameRequest= pd.read_csv(StringIO(header_df_request+content),sep='\t')
                dataFrameMissing = pd.merge(dataFrameRequest,dataFrameFile,how='left',on=['destino_a', 'destino_b'],indicator=True)

                #obtener faltantes
                dataFrameMissingRes = dataFrameMissing[dataFrameMissing._merge=='left_only'][['destino_a', 'destino_b','distancia','tiempo','latitud_a_x','longitud_a_x','latitud_b_x','longitud_b_x','fecha']]
                  #cambiando nombres de columnas
                dataFrameMissingRes.columns = ['destino_a', 'destino_b','distancia','tiempo','latitud_a','longitud_a','latitud_b','longitud_b','fecha']            

                print("faltan peticion por calcular:")
                

                if dataFrameMissingRes.shape[0]==0:
                    print("faltan solo los forzados")
                    missing = S3Service.verifyDifferencesDataFrameMissing(missing,dataFrameForced)
                    return run,missing,json_results,dataFrameFile

                else:
                    print("faltan forzados y no calculados")

                    missing = self._verifyDataFrameMissing(missing,dataFrameForced)
                    missing = self._verifyDataFrameMissing(missing,dataFrameMissingRes)
                   
                    return run,missing,json_results,dataFrameFile


        except Exception as e:
            print("Fallo al buscar diferencias: " +str(e))
            return run,missing,json_results,dataFrameFile

    def _verifyMergeForced(self,dataFrameBase,dataFrameArcos):

        dataFrameRecalculate = pd.read_csv(StringIO(dataFrameBase),sep='\t')
        dataFrameForced = pd.merge(dataFrameArcos,dataFrameRecalculate,how='left',on=['destino_a', 'destino_b'],indicator=True)

        #restante del archivo
        dataFrameFile = dataFrameForced[dataFrameForced._merge=='left_only'][['destino_a', 'destino_b','distancia','tiempo','latitud_a_x','longitud_a_x','latitud_b_x','longitud_b_x','fecha']]
        #cambiando nombres de columnas
        dataFrameFile.columns = ['destino_a', 'destino_b','distancia','tiempo','latitud_a','longitud_a','latitud_b','longitud_b','fecha']            

        '''
        #arcos forzados a recalcular
        dataFrameForced = dataFrameForced[dataFrameForced._merge=='both'][['destino_a', 'destino_b','distancia','tiempo','latitud_a_x','longitud_a_x','latitud_b_x','longitud_b_x','fecha']]
        #cambiando nombres de columnas
        dataFrameForced.columns = ['destino_a', 'destino_b','distancia','tiempo','latitud_a','longitud_a','latitud_b','longitud_b','fecha']            
        '''

        print("arcos forzados: " + str(dataFrameRecalculate.shape[0]) + " restante: " + str(dataFrameFile.shape[0]) )

        return dataFrameRecalculate,dataFrameFile


    def _verifyCombinations(self,unique_places_orders,content,contentRecalculate):
        recalculateA = False 
        reacalcularB = False
        for idx_01,row_unique_01 in enumerate(unique_places_orders):
            
            # si status = 1 debe de recalcular todos los arcos afectados por este destino
            # por lo tanto se guardara una lista de destinos forzados a actualizar
            if row_unique_01["status"]=="1":
                recalculateA = True
            for idx_02,row_unique_02 in enumerate(unique_places_orders):   
                if row_unique_02["status"]=="1":
                    reacalcularB = True           
                if recalculateA or reacalcularB:
                    contentRecalculate += row_unique_01["iddestino"]+"\t"+row_unique_02["iddestino"]+"\t"+str(row_unique_01["latitud"])+"\t"+str(row_unique_01["longitud"])+"\t"+str(row_unique_02["latitud"])+"\t"+str(row_unique_02["longitud"])+"\n"
                else:
                    content += row_unique_01["iddestino"]+"\t"+row_unique_02["iddestino"]+"\t"+str(row_unique_01["latitud"])+"\t"+str(row_unique_01["longitud"])+"\t"+str(row_unique_02["latitud"])+"\t"+str(row_unique_02["longitud"])+"\n"
                reacalcularB =False
            recalculateA = False
            
        
        return content,contentRecalculate


    def _verifyDataFrameMissing(self,missing,dataFrameAppend):        
        for index, row in dataFrameAppend.iterrows():
            missing.append(
                {
                    "destino_01":{"iddestino":row['destino_a'],"latitud":row['latitud_a'],"longitud":row['longitud_a']},
                    "destino_02":{"iddestino":row['destino_b'],"latitud":row['latitud_b'],"longitud":row['longitud_b']}
                }
            ) 
        return missing
        

            
    '''
    /*================================================*\
        @description
        Crea un archivo local en la direccion
        especificada por self.pathFolder 
        creando el directorio si no existe
        el contenido es un dataframe
    \*=================================================*/
    '''
    def _createLocalFile(self,dataFrameContent):
        #content dataframe
        #crear directorio
       
        try:
            directory = Path(self.pathFolder)
            directory.mkdir(exist_ok=True, parents=True)           
            dataFrameContent.to_csv(Path(self.pathFile),header=False,sep="\t",index = False)#.encode()
            print("guardando contenido")
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"no es posible guardar el archivo en local")
        
    '''
    /*================================================*\
        @description
        Guarda un archivo 
        guarda el contenido de un DataFrame en s3

    \*=================================================*/
    '''
    def _createS3File(self,dataFrameContent):


        try:
            bucket,key,access = self.config.enviroment()
            s3 = boto3.resource('s3',
                aws_access_key_id=key,
                aws_secret_access_key=access
                )
            prefix = self.empresa+'/'+self.cedi+'/arcos.txt'
            content = dataFrameContent.to_csv(None,header=False,sep="\t",index = False).encode()
            o = s3.Object(bucket,prefix)
            o.put(Body=content)
            return True
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"no es posible guardar en s3")
            return  False


    '''
    /*================================================*\
        @description
        Busca el contenido del archivo de arcos en s3 
        usa la empresa y el cedi y lo trata de
        convertir a DataFrame
        @return
        dataFrame (pd.DataFrame) => contenido del archivo s3
        success  (boolean) => true o false 
    \*=================================================*/
    '''
    def _searchFileS3(self):
        bucket,key,access = self.config.enviroment()
        s3 = boto3.resource('s3',
            aws_access_key_id=key,
            aws_secret_access_key=access
            )
       
        prefix = self.empresa+'/'+self.cedi+'/arcos.txt'        
                
        success = False        
        dataFrameArcosS3 = None 
        try:
            o = s3.Object(bucket,prefix)
            o.load()
            fileS3 = o.get()['Body'].read().decode('utf-8')
            #agregar header
            header_df_file ="destino_a\tdestino_b\tdistancia\ttiempo\tlatitud_a\tlongitud_a\tlatitud_b\tlongitud_b\tfecha\n"
            dataFrameArcosS3 = pd.read_csv(StringIO(header_df_file+fileS3),sep='\t')  
            success = True
            #print("existe el archivo en s3")   
            return dataFrameArcosS3,success       
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"no es posible obtener el archivo de s3")
            return None,success

    '''
    /*================================================*\
        @description
        Busca el contenido del archvio de arcos en local
        usando empresa y cedi y lo trata de convertir a 
        DataFrame
        @return
        dataFrame (pd.DataFrame) => contenido del archivo local
        success  (boolean) => true o false 
    \*=================================================*/
    '''
    def _searchLocalFile(self):
        
        path_file = self.pathFile

        success = False
        dataFrameArcosLocal = None
       
        try:
            if Path(path_file).exists(): 
                #agregar header
                header_df_file ="destino_a\tdestino_b\tdistancia\ttiempo\tlatitud_a\tlongitud_a\tlatitud_b\tlongitud_b\tfecha\n"                     
                dataFrameArcosLocal = pd.read_csv(StringIO(header_df_file + (open(path_file, "r").read()) ),sep='\t')               
                success = True
                #print("existe el archivo en local")   
            else:
                print("no existe el archivo local")
            return dataFrameArcosLocal,success
        except Exception as e:
            this_function_name = sys._getframe().f_code.co_name
            self._errorLog(e,this_function_name,"no es posible buscar el archivo local")
            return None,success


    def _errorLog(self,e,function,message):
        file_name = os.path.basename(sys.argv[0])        
        error = '{} - {} - {} - {}'.format(file_name, function, str(e),message)
        print(error)