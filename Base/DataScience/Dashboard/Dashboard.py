from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
import config

def showdashboardsfun(ui):

        empresa = "POCHTECA"

        DashUrls = {
            "SILODISA": {
                "BSC": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRem1vbjJOOVotSEE/page/RftE",
                "Gastos": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRMUh1WE1aRDRRMWs/page/RftE",
                "Ventas": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRVnZpaXN2Qnd2N2c/page/GXyF",
                "Satisfaccion": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRRmluVGczOG0wb2M/page/RftE",
                "Eficiencia": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRS0dISnN4elJ2eUE/page/RftE",
                "Huella": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRRS1ZaXJ0R3ZJb1k/page/RftE",
                "Distancia": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRQVVDZ3NBR0gxSkU/page/RftE",
                "Tiempos": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRQzVvS0c5R0l1RzQ/page/RftE",
                # "Comparacion": "https://datastudio.google.com/u/0/reporting/0BzxncdOmLHRKSENOX1RHMXBZUWc/page/rh9I"
                },
            "POCHTECA": {
                "BSC": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRWFozWDJaUGM5ZVk/page/RftE",
                "Gastos": "https://datastudio.google.com/reporting/0B-RXriF9NUXRd3VOclppNHQ1Vkk/page/RftE/edit",
                #"Gastos": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRbzlJalduV09Manc/page/RftE",
                "Ventas": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRNGo2XzJCWFgyN2c/page/GXyF",
                "Satisfaccion": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRWGxVbzZRVGxWcHM/page/RftE",
                "Eficiencia": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRT3gyWlEwRzQzMVU/page/RftE",
                "Huella": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRSS1Ybm03cjBNajQ/page/RftE",
                "Distancia": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRNnB3eTlIc1VCejg/page/RftE",
                "Tiempos": "https://datastudio.google.com/u/0/reporting/0B-RXriF9NUXRY212YUpDZnItNEU/page/RftE",
                #"Comparacion": "https://datastudio.google.com/u/0/reporting/0BzxncdOmLHRKSENOX1RHMXBZUWc/page/rh9I"
                }
            }
        config.AcUrls = DashUrls[empresa]


def addtabsdashboardsfun(ui):

    if config.dashboards_cargados == 0:

        config.dashboards_cargados = 1

        heightframe = ui.tab2Analizar.geometry().height()
        widthframe = ui.tab2Analizar.geometry().width()
        widthdash = (heightframe-32) * 16/9
        xdash = (widthframe-widthdash)/2

        ui.dashboard_1.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_2.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_3.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_4.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_5.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_6.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_7.setGeometry(xdash, 0, widthdash, heightframe)
        ui.dashboard_8.setGeometry(xdash, 0, widthdash, heightframe)

        ui.dashboard_1.setUrl(QtCore.QUrl(config.AcUrls["BSC"]))
        ui.dashboard_2.setUrl(QtCore.QUrl(config.AcUrls["Gastos"]))
        ui.dashboard_3.setUrl(QtCore.QUrl(config.AcUrls["Ventas"]))
        ui.dashboard_4.setUrl(QtCore.QUrl(config.AcUrls["Satisfaccion"]))
        ui.dashboard_5.setUrl(QtCore.QUrl(config.AcUrls["Eficiencia"]))
        ui.dashboard_6.setUrl(QtCore.QUrl(config.AcUrls["Huella"]))
        ui.dashboard_7.setUrl(QtCore.QUrl(config.AcUrls["Distancia"]))
        ui.dashboard_8.setUrl(QtCore.QUrl(config.AcUrls["Tiempos"]))


def resizewindow_dash(ui):

    heightframe = ui.tab2Analizar.geometry().height()
    widthframe = ui.tab2Analizar.geometry().width()
    widthdash = (heightframe - 32) * 16 / 9
    xdash = (widthframe - widthdash) / 2
    ui.dashboard_1.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_2.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_3.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_4.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_5.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_6.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_7.setGeometry(xdash, 0, widthdash, heightframe)
    ui.dashboard_8.setGeometry(xdash, 0, widthdash, heightframe)

