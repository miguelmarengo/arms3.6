import traceback
import sys
import time
from PyQt5.QtCore import QRunnable, QObject, pyqtSlot, pyqtSignal



# ==========================================================
class Trabajador(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Trabajador, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = TrabajadorSenales()
        # Add the callback to our kwargs
        kwargs['informa_el_progreso'] = self.signals.progreso


    @pyqtSlot()
    def run(self):
        try:
            resultado = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            time.sleep(5)
            # if resultado != False:
            #     self.signals.resultado.emit(resultado)  # Return the result of the processing
            # else:
            #     time.sleep(5)
        finally:
            self.signals.finalizado.emit()  # Done


# ==========================================================
class TrabajadorSenales(QObject):
    progreso = pyqtSignal(object)
    resultado = pyqtSignal(object)
    finalizado = pyqtSignal()
    error = pyqtSignal(tuple)
