import traceback
import os
from Base.Arcos.Service import DistanceService
import time
from PyQt5.QtCore import QObject, pyqtSignal, QRunnable, pyqtSlot, QThreadPool, QThread
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QMessageBox
import sys
from Base.GeneradorRutas.Motor.ejecucionOpta import OptaPlanner

import config
from Base.mis_trabajadores.clase_Trabajador import *

# ==========================================================================================
def fnEjecutaTrabajoMotor(informa_el_progreso):
    w = config.mainWindow
    arrayDatosX = [1,2,3,4,5,6]
    arrayDatosY = [1,2,3,4,5,6]
    try:
        correrArcos = True
        mensajeActual = ''

        w = config.mainWindow
        w.desactivabtnIniciar()

        #Verifica que sí haya un plan seleccionado
        if type(config.dfPlan) is int:
            w.activabtnIniciar()
            #QMessageBox.warning(w, "Imposible continuar", "No hay ningún plan abierto", QMessageBox.Ok)
            mydict1 = {'progressBar': 0, 'lblMensaje': 'No hay ningún plan abierto', 'TotalCosto': 0, 'Viajes': 0,
                       'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                       'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [], 'datosGraficaY': []}

            informa_el_progreso.emit(mydict1)
            return False
        elif config.dfPlan.empty:
            w.activabtnIniciar()
            #QMessageBox.warning(w, "Imposible continuar", "No hay ningún plan abierto", QMessageBox.Ok)
            mydict1 = {'progressBar': 0, 'lblMensaje': 'No hay ningún plan abierto', 'TotalCosto': 0, 'Viajes': 0,
                       'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                       'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [], 'datosGraficaY': []}

            informa_el_progreso.emit(mydict1)

            return False

        mydict1 = {'progressBar': 0, 'lblMensaje': 'Iniciando ejecución', 'TotalCosto': 0, 'Viajes': 0,
                   'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                   'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [], 'datosGraficaY': []}

        informa_el_progreso.emit(mydict1)


        # verificar que todos arcos de los destinos esten calculados
        distanceService = DistanceService(w,'SILODISA','SILODISA')
        if distanceService.validate(informa_el_progreso):
            
            if w.optaPlanner.instanciaEsActiva():
                data = w.optaPlanner.generaJson()

                if not data == None and data["status"]:
                    jsonZote = str(data["data"])

                    w.optaPlanner.ejecutarPlan(jsonZote)

                    while w.optaPlanner.planCorriendo:
                        mydict1 = w.optaPlanner.obtieneSolucion()
                        informa_el_progreso.emit(mydict1)
                        time.sleep(1)

                else:
                    print("No se pudo generar el plan para procesar")
                    w.activabtnIniciar()
                    #QMessageBox.warning(w, "Imposible continuar", "No se pudo generar el plan para procesar", QMessageBox.Ok)
                    mydict1 = {'progressBar': 0, 'lblMensaje': 'No se pudo generar el plan para procesar', 'TotalCosto': 0, 'Viajes': 0,
                               'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                               'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [],
                               'datosGraficaY': []}
                    informa_el_progreso.emit(mydict1)
                    return False
            else:
                print("La instancia del motor no se encuentra activa")
                w.activabtnIniciar()
                #QMessageBox.warning(w, "Imposible continuar", "La instancia del motor no se encuentra activa", QMessageBox.Ok)
                mydict1 = {'progressBar': 0, 'lblMensaje': 'La instancia del motor no se encuentra activa', 'TotalCosto': 0, 'Viajes': 0,
                           'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                           'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [],
                           'datosGraficaY': []}
                informa_el_progreso.emit(mydict1)
                return False

        else:
            print("Faltan arcos por calcular, intente de nuevo.")
            w.activabtnIniciar()
            #QMessageBox.warning(w, "Imposible continuar", "Faltan arcos por calcular, intente de nuevo.",  QMessageBox.Ok)
            mydict1 = {'progressBar': 0, 'lblMensaje': 'Faltan arcos por calcular, intente de nuevo.', 'TotalCosto': 0, 'Viajes': 0,
                       'KmTotales': 0, 'CostoXViaje': 0, 'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                       'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': 0, 'datosGraficaX': [],
                       'datosGraficaY': []}
            informa_el_progreso.emit(mydict1)
            return False

        return True

    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return False
