import traceback
import os

import time
from PyQt5.QtCore import QObject, pyqtSignal, QRunnable, pyqtSlot, QThreadPool, QThread
from PyQt5.QtWidgets import QMainWindow, QApplication
import sys

import config
from Base.mis_trabajadores.clase_Trabajador import *
from Base.GeneradorRutas.Optimiza.OptimizaAutomatico import *
# ==========================================================================================


def fnEjecutaTrabajoOptimizaAutomatico(informa_el_progreso):
    try:
        informa_el_progreso.emit({'progressBar': 10, 'lblMensaje': 'Optimiza Automático'})
        # aqui va el trabajo
        w = config.mainWindow
        # obten los parametros
        config.slot_editOcupacion = int(config.slot_editOcupacion)
        config.slot_editCercania = int(config.slot_editCercania)
        config.slot_editAumentarValor = int(config.slot_editAumentarValor)
        config.slot_editAumentarVentana = int(config.slot_editAumentarVentana)
        config.slot_editAumentarVol = int(config.slot_editAumentarVol)
        config.slot_editAumentarKg =  int(config.slot_editAumentarKg)
        # ejecuta vacios o cercanos
        if config.radioOptimiza == 'Vacíos':
            w.optimizaAutomatico.optimiza_vacios(informa_el_progreso)
        else:
            w.optimizaAutomatico.optimiza_cercanos(informa_el_progreso)
        return True
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return False


def fnEjecutaTrabajoAbrirPlan(informa_el_progreso):
    try:
        informa_el_progreso.emit({'progressBar': 10, 'lblMensaje': 'Abrir Plan'})
        # a = 1 / 0
        # aqui va el trabajo
        w = config.mainWindow
        fnLimpiaTableros()
        fnLimpiaTableritos()
        fnLimpiaGrids()
        informa_el_progreso.emit({'progressBar': 15, 'lblMensaje': 'Tableros Limpios...'})
        # antes de abrir pon el nombre en el combo del TAB EL PLAN
        index = w.comboNombrePlan.findText(config.nombrePlan, QtCore.Qt.MatchFixedString)
        if index >= 0:
            w.comboNombrePlan.setCurrentIndex(index)
        # abre el plan seleccionado (esta en config.nombrePlan
        w.plan.obtenerDatosDelSSPlan(informa_el_progreso)

        return True
    # ----------------------------------------------------
    except Exception as e:
        traceback.print_exc()
        exctype, value = sys.exc_info()[:2]
        informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Ocurrio un error en fnEjecutaTrabajoAbrirPlan'})
        # progress_error.emit((exctype, value, traceback.format_exc()))
        # exctype, exc_obj, exc_tb = sys.exc_info()
        # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        # error = '{} - {} - linea {}  ({})'.format(exctype, fname, exc_tb.tb_lineno, str(e))

        return False


def fnEjecutaTrabajoCrearPlan(informa_el_progreso):
    try:
        informa_el_progreso.emit({'progressBar': 2, 'lblMensaje': 'Crear plan'})
        # aqui va el trabajo
        w = config.mainWindow
        fnLimpiaTableros()
        fnLimpiaTableritos()
        fnLimpiaGrids()
        informa_el_progreso.emit({'progressBar': 4, 'lblMensaje': 'Tableros Limpios...'})

        nombreDelPlan = config.nombreGenerarPlan
        w.datos.crearPlan(informa_el_progreso)
        # mostrar seleccionado el ss del plan generado
        index = w.listAbrePlan.findItems(nombreDelPlan, QtCore.Qt.MatchWrap)
        if len(index) > 0:
            w.listAbrePlan.setCurrentItem(index[0])
        index = w.comboNombrePlan.findText(nombreDelPlan, QtCore.Qt.MatchFixedString)
        if index >= 0:
            w.comboNombrePlan.setCurrentIndex(index)

        return True
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return False
