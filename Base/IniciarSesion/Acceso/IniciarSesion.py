 # coding=utf-8
from datetime import datetime
import os
import pymysql
import sys
import json
import config
import requests
from IO.Apis.login import Login
"""
    @desc: Clase para manejar el login a ARMS 3
    @author: Erick Sandoval  

"""


class IniciarSesion:
    def __init__(self, ui):
        """Constructor sin parametros"""
        self.ui = ui
        self.ui.lineEdit_iniciar_login_cedi.setText("")
        self.ui.lineEdit_iniciar_login_usuario.setText("")
        self.ui.lineEdit_iniciar_login_clave.setText("")


    def init_components(self):
        self.ui.pushButton_Iniciar_login.clicked.connect(self.do_login)

    """
        :return True si se pudo autenticar al usuario y settea el diccionario de config.usuario con los datos del usuario logueado
        :return False si no fue posible autenticar al usuario
    """
    def authenticate(self, user, password):
        """Metodo para consultar si el usuario con el usuario y password existen, el cedi puede no existir"""

        headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/login"
        datos = {"username": user, "password": password}
        try:
            r = requests.post(url, data=json.dumps(datos), headers=headers).json()
            return r
        except requests.exceptions.HTTPError as err:
            print("Request exception")
            return False, dict({})

    def do_login(self):
        pass
        '''
        """Método para handlear el inicio de sesión, obtiene los datos de CEDI, usuario y password de la UI"""
        cedi = self.ui.lineEdit_iniciar_login_cedi.text()
        user = self.ui.lineEdit_iniciar_login_usuario.text()
        pwd = self.ui.lineEdit_iniciar_login_clave.text()
        if user == "" or pwd == "" or cedi =="":
            """Lanzar error, ningun campo puede estar vacio"""
            #self.ui.labelStatusBarMensaje.setText("Ingresa el Cedi, el usuario y la contraseña")
            print("Ingresa los campos (usuario y password) ... (Login)")
        else:
            
            response = Login.validate(self,cedi,user,pwd)
            if response["status"]:
                print("Login correcto: "+response["data"]["nombre"])
                config.usuario = response["data"]
            else:
                print("algo fallo al logear")
                self.update_ui_labels()
                self.ui.labelStatusBarMensaje.setText("Usuario o contraseña incorrectos")
        '''


    def update_ui_labels(self):
        self.ui.labelStatusBarEmpresa.setText(config.usuario["empresa"])
        self.ui.labelStatusBarCEDI.setText(config.usuario["cedi"])
        self.ui.labelStatusBarUser.setText(config.usuario["nombre"])
        self.ui.labelStatusBarMensaje.setText("Bienvenido: " + config.usuario["nombre"])

def authenticate():
    """Metodo para consultar si el usuario con el usuario y password existen, el cedi puede no existir"""

    cedi = config.slot_editCEDI
    user = config.slot_editUsuario
    pwd  = config.slot_editPassword
    print(cedi)
    print(user)
    print(pwd)



    if user == "" or pwd == "" or cedi =="":
        print("falta llenar datos del usuario en login")
    else:        
        return Login.validate(cedi,user,pwd)


def block_app(ui):
    ui.tabWidget1Principal.setCurrentIndex(0)
    ui.tabWidget2Iniciar.setCurrentIndex(0)
    stylesheet = ui.tabWidget1Principal.styleSheet()

    #"background-color: #cccccc;\n"
    #"color: #4c4c4c;\n"


    newstyle = "QTabBar::tab::disabled {\n"" \
    ""/* ---------------------------------- */\n"" \
    ""background-color: #ffffff;\n"" \
    ""color: #ffffff;\n"" \
    ""/* ---------------------------------- */    \n"" \
    ""border: 0px solid #d8c3a5;\n"" \
    ""border-radius: 0px;\n"" \
    ""padding: 0px;\n"" \
    ""margin-left: 3px;\n"" \
    ""margin-right: 3px;\n"" \
    ""/* ---------------------------------- */\n"" \
    ""font: 11pt \"Roboto\";\n"" \
    ""width:150px;\n"" \
    ""height:17;\n"" \
    ""}\n"" \
    ""\n"
    stylesheet = stylesheet + newstyle
    ui.tabWidget1Principal.setStyleSheet(stylesheet)
    number_tabs = ui.tabWidget1Principal.count()
    for i in range(1, number_tabs):
        ui.tabWidget1Principal.setTabEnabled(i, False)


def unblock_app(ui):
    ui.tabWidget1Principal.setCurrentIndex(0)
    number_tabs = ui.tabWidget1Principal.count()
    for i in range(1, number_tabs):
        ui.tabWidget1Principal.setTabEnabled(i, True)


'''





        unblock_app(self)



'''