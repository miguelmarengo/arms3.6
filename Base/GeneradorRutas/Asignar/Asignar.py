import config
import sys
import os
from PyQt5.QtCore import pyqtSlot
from    PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QTableWidget, QTableWidgetItem,QAction,QVBoxLayout
from PyQt5 import QtCore, QtGui, QtWidgets
import json
from datetime import datetime
import requests
import config
from IO.Apis.Db import *
from IO.Apis.ordenar_datos import *
from IO.Apis.spread_sheet import *
from Base.GeneradorRutas.Datos.Planes import *
from Base.GeneradorRutas.Optimiza.funciones import *
from IO.Apis.consolidar_json import ConsolidarJson

# ---------------------------------------------------------------------
# CLASES NUESTRAS
from Base.IniciarSesion.Acceso.IniciarSesion import IniciarSesion

class Asignador:
    def __init__(self, ui):
        self.ui = ui
        self.viajes = []
        self.tablaRes = 0
        self.tablaVehi = 0
        self.tablaOpe = 0
        self.abierto = 0
        self.vehiculos = []
        self.res = 0
        self.resPre = []
        self.ope = []
        self.opeBKP = []
        self.temporales = []
        self.temporalesRes = []
        self.temporalesope = []
        self.viajenuevo1 = []
        self.viajenuevo2 = []
        self.dato=[]
        self.Uno=SpreadSheet()
        self.plan=[]
        self.s = False
        self.viajes = []
        self.origen = 0
        self.Planabierto = 0
        self.borrado=[]
        self.borradonave=[]
        self.asignaResultados=[]
        self.asignaPredespegados=[]
        self.ultimoclickazo = 0


    #info de resultados plan
    def leerResultados(self):
        try:
            print("hola")
            self.res = config.dfPlan.copy()
            self.abierto = 1
            self.Planabierto = 1
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def clickRes(self,a,b,c,d):
        try:
            self.origen = 1
            temp = a
            self.temporalesRes = temp
            print(self.temporalesRes)
            self.asignaResultados.append(self.temporalesRes)
            self.idviaje = self.res['Viaje'].iloc[self.temporalesRes]
            self.VolumenMax = self.res['VolumenMax'].iloc[self.temporalesRes]
            print(self.VolumenMax)
            self.vehiculosordenados = []
            self.vehiculossinordenar = []

            nave = json.loads(self.vehiculos.to_json(orient='records'))
            for vehiculo in nave:
                if vehiculo['VolumenMax'] >= self.VolumenMax:
                    self.vehiculosordenados.append(vehiculo)
                else:
                    self.vehiculossinordenar.append(vehiculo)
            print("naaaveeesss")
            self.vehiculosordenados += self.vehiculossinordenar
            print(self.vehiculosordenados)

            self.vehiculosFrame = pd.DataFrame.from_records(self.vehiculosordenados)

            fnPutGrid(self.vehiculosFrame, self.ui.gridAsignarVeh, config.campos_ssAsignarVehiculos)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def listBoxVehiculosPlan(self):
        try:
            datosVehiculos = []
            for spread in config.lista_ss:
                if 'ARMS VEHICULOS' in spread["name"]:
                    datosVehiculos.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.comboVehAsignarPlan.clear()
            for dato in datosVehiculos:
                self.ui.comboVehAsignarPlan.addItem(dato['name'])
            #selecciona el primero
            self.ui.comboVehAsignarPlan.setCurrentRow(0)

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def leerVehiculos(self):
        try:

            status, duracion, vehi = self.Uno.abrirSS(config.slot_comboVehAsignar,"VEHICULOS")
            print(status, config.slot_comboVehAsignar)
            if status == False:
                self.vehiculos = config.dfVehiculos
            else:
                self.vehiculos = vehi


            self.cargaTablaVehiculos()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def cargaTablaVehiculos(self):
        try:

            fnPutGrid(self.vehiculos, self.ui.gridAsignarVeh, config.campos_ssAsignarVehiculos)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)


    def clickNave(self, a, b, c, d):
        try:


                row = self.ui.gridAsignarVeh.currentItem().row()
                #col = widget.currentItem().column()
                columnname = ('Placas')
                #loop through headers and find column number for given column name
                headercount = self.ui.gridAsignarVeh.columnCount()
                for x in range(0,headercount,1):
                    headertext = self.ui.gridAsignarVeh.horizontalHeaderItem(x).text()
                    if columnname == headertext:
                        matchcol = x
                        break
                print(a, matchcol)
                self.cellnave = self.ui.gridAsignarVeh.item(a,matchcol).text()   # get cell at row, col

                self.res['Placas'].iloc[self.temporalesRes] = self.cellnave
                self.ui.gridAsignar.setItem(self.temporalesRes, 2, QTableWidgetItem(self.cellnave))
                rowtabla = 0
                self.ultimoclickazo = self.temporalesRes
        #cargar placas a todos los pedidos con el mismo viajes
                self.res.set_index('Viaje', inplace=True, drop=False)
                self.res.loc[self.idviaje, 'Placas' ] = self.cellnave
            # minidf = self.res.loc[self.idviaje, :]
            # minidf['Placas'] = self.cellnave
                print (self.res.loc[self.idviaje, 'Placas' ])
                fnPutGrid(self.res, self.ui.gridAsignar, config.campos_ssAsignar)

                self.ui.gridAsignar.selectRow(self.ultimoclickazo)

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)
    def listBoxOperadores(self):

        try:
            datosOperadores = []
            for spread in config.lista_ss:
                if 'ARMS OPERADORES' in spread["name"]:
                    datosOperadores.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.comboOperadoresAsignarPlan.clear()
            for dato in datosOperadores:
                self.ui.comboOperadoresAsignarPlan.addItem(dato['name'])
            # selecciona el primero
            self.ui.comboOperadoresAsignarPlan.setCurrentRow(0)


        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def leerOperadores(self,s):
        try:
            print(self.abierto)
            if s == 1 :

                print(config.slot_comboOperadoresAsignar)
                status, duracion, ope = self.Uno.abrirSS(config.slot_comboOperadoresAsignar,"OPERADORES")
                print(status)
                if status == False:
                    self.ope = config.dfOperadores
                else:
                    self.tablaOpe = 1
                    df_operador = ope.drop_duplicates(['Operador'], keep='first')
                    operadores=json.loads(df_operador.to_json(orient='records'))
                    self.ope = operadores
                    self.cargaTablaOperadores()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def cargaTablaOperadores(self):
        try:
            operadorPreferente=[]
            operadorPreferente2=[]
            ordenados=[]
            num=0
            for operador in self.ope:
                num=num+1
                numrow=num

                self.ui.gridAsignarOperadores.setColumnCount(5)
                self.ui.gridAsignarOperadores.setRowCount(numrow)
                self.ui.gridAsignarOperadores.setHorizontalHeaderLabels(['Nombre', 'Licencia', 'Ranking','Destino Fav','Proveedor'])
                self.ui.gridAsignarOperadores.insertRow(numrow)
                self.ui.gridAsignarOperadores.setRowCount(numrow)
                row=0
            for operador in self.ope:

                self.ui.gridAsignarOperadores.setItem(row, 0, QTableWidgetItem(str(operador['Operador'])))
                self.ui.gridAsignarOperadores.setItem(row, 1, QTableWidgetItem(operador["Licencia"]))
                self.ui.gridAsignarOperadores.setItem(row, 2, QTableWidgetItem("vacio"))
                self.ui.gridAsignarOperadores.setItem(row, 3, QTableWidgetItem("vacio"))
                self.ui.gridAsignarOperadores.setItem(row, 4, QTableWidgetItem(str(operador["ProveedorFlete"])))
                row = row+1

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)

    def clickOpe(self, a, b, c, d):
        try:
            print ("click operador")
            row = self.ui.gridAsignarOperadores.currentItem().row()
            columnname = ('Nombre')
            headercount = self.ui.gridAsignarOperadores.columnCount()
            print(headercount)
            for x in range(0,headercount,1):
                headertext = self.ui.gridAsignarOperadores.horizontalHeaderItem(x).text()
                if columnname == headertext:
                    matchcol = x

                    break
            self.cell = self.ui.gridAsignarOperadores.item(a,matchcol).text()   # get cell at row, col
            self.res['Operador'].iloc[self.temporalesRes] = self.cell
            print(self.temporalesRes)
            self.ultimoclickazo = self.temporalesRes

            print("mMMMMMMMMMMMM")
            #cargar placas a todos los pedidos con el mismo viajes
            self.res.set_index('Viaje', inplace=True, drop=False)
            self.res.loc[self.idviaje, 'Operador'] = self.cell
            #minidf = self.res.loc[self.idviaje, :]
            #minidf['Operador'] = self.cell
            print(self.res['Operador'])
            fnPutGrid(self.res, self.ui.gridAsignar, config.campos_ssAsignar)
            self.ui.gridAsignar.selectRow(self.ultimoclickazo)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    #Predespagar
    def listBoxVehiculosPredespegue(self):
        try:
            datosVehiculos = []
            for spread in config.lista_ss:
                if 'ARMS VEHICULOS' in spread["name"]:
                    datosVehiculos.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.comboVehAsignarPredespegue.clear()
            for dato in datosVehiculos:
                self.ui.comboVehAsignarPredespegue.addItem(dato['name'])
            #selecciona el primero
            self.ui.comboVehAsignarPredespegue.setCurrentRow(0)


        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)


    def leerVehiculosPre(self,p):
        try:
            print(p)
            if p == 1 :

                status, duracion, vehipre = self.Uno.abrirSS(config.slot_comboVehAsignarPredespegue,"VEHICULOS")
                print(status, config.slot_comboVehAsignarPredespegue)
                if(status == True):
                    vehiculospre=json.loads(vehipre.to_json(orient='records'))
                    self.vehiculospre = vehiculospre

                    self.cargaTablaVehiculospre()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def cargaTablaVehiculospre(self):
        try:

            numrow=0
            for nave in self.vehiculospre:
                numrow=numrow+1
            self.ui.gridAsignarVehPredespegue.setColumnCount(7)
            self.ui.gridAsignarVehPredespegue.setRowCount(numrow)
            self.ui.gridAsignarVehPredespegue.setHorizontalHeaderLabels(['Vehiculo', 'Placa','Vol. Max.', 'No. Economico', 'Proveedor','Licencia.', 'Ranking'])
            self.ui.gridAsignarVehPredespegue.insertRow(numrow)
            self.ui.gridAsignarVehPredespegue.setRowCount(numrow)
            row=0
            for nave in self.vehiculospre:
                self.ui.gridAsignarVehPredespegue.setItem(row, 0, QTableWidgetItem(nave['TipoVehiculo']))
                self.ui.gridAsignarVehPredespegue.setItem(row, 1, QTableWidgetItem(nave["Placas"]))
                self.ui.gridAsignarVehPredespegue.setItem(row, 2, QTableWidgetItem(str(nave["VolumenMax"])))
                self.ui.gridAsignarVehPredespegue.setItem(row, 3, QTableWidgetItem(nave["Economico"]))
                self.ui.gridAsignarVehPredespegue.setItem(row, 4, QTableWidgetItem(str(nave['ProveedorFlete'])))
                self.ui.gridAsignarVehPredespegue.setItem(row, 5, QTableWidgetItem(nave["Licencia"]))
                self.ui.gridAsignarVehPredespegue.setItem(row, 6, QTableWidgetItem("vacio"))
                row = row+1


        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)



    def selection_changed(self, a, b, c, d):
        try:
            self.origen = 2

            self.temporales = a
            self.asignaPredespegados.append(int(self.temporales))
            print("click viaje", self.temporales)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def clickNavepre(self, a, b, c, d):
        try:
            if self.abierto == 1 :

                print ("click navepre")
                row = self.ui.gridAsignarVehPredespegue.currentItem().row()
                columnname = ('Placas')
                headercount = self.ui.gridAsignarVehPredespegue.columnCount()
                print(headercount)
                for x in range(0,headercount,1):
                    headertext = self.ui.gridAsignarVehPredespegue.horizontalHeaderItem(x).text()
                    if columnname == headertext:
                        matchcol = x
                        break
                self.cell = self.ui.gridAsignarVehPredespegue.item(a,matchcol).text()
                self.viajes['viajes'][self.temporales]['propiedadunidad']['Placas'] = self.cell

                numrow=0
                for viaje in self.viajes['viajes']:
                    numrow=numrow+1

                self.ui.gridAsignarPredespegue.setColumnCount(5)
                self.ui.gridAsignarPredespegue.setRowCount(numrow)
                self.ui.gridAsignarPredespegue.setHorizontalHeaderLabels(['Viaje', 'Placas', 'Operador' , 'Km','Destinos'])
                self.ui.gridAsignarPredespegue.insertRow(numrow)
                self.ui.gridAsignarPredespegue.setRowCount(numrow)
                row=0
                for viaje in self.viajes['viajes']:
                    self.ui.gridAsignarPredespegue.setItem(row, 0, QTableWidgetItem(str(viaje['Viaje'])))
                    self.ui.gridAsignarPredespegue.setItem(row, 1, QTableWidgetItem(viaje["propiedadunidad"]["Placas"]))
                    self.ui.gridAsignarPredespegue.setItem(row, 2, QTableWidgetItem(viaje["propiedadunidad"]["Operador"]))
                    self.ui.gridAsignarPredespegue.setItem(row, 3, QTableWidgetItem(str(viaje["Km"])))
                    self.ui.gridAsignarPredespegue.setItem(row, 4, QTableWidgetItem(str(len(viaje["destinos"]))))
                    row = row+1
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def listBoxOperadorespre(self):
        try:
            datosOperadores = []
            for spread in config.lista_ss:
                if 'ARMS OPERADORES' in spread["name"]:
                    datosOperadores.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.comboOperadoresAsignarPredespegue.clear()
            for dato in datosOperadores:
                self.ui.comboOperadoresAsignarPredespegue.addItem(dato['name'])
            # selecciona el primero
            self.ui.comboOperadoresAsignarPredespegue.setCurrentRow(0)


        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)


    def leerOperadorespre(self,o):
        try:
            print(self.abierto)
            if o == 1 :

                print(config.slot_comboOperadoresAsignarPredespegue)
                status, duracion, opepre = self.Uno.abrirSS(config.slot_comboOperadoresAsignarPredespegue,"OPERADORES")
                print(status)
                if(status == True):
                    self.tablaOpe = 1
                    df_operador = opepre.drop_duplicates(['Operador'], keep='first')
                    operadores=json.loads(df_operador.to_json(orient='records'))
                    self.opepre = operadores
                    self.cargaTablaOperadorespre()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False


    def cargaTablaOperadorespre(self):
        try:
            operadorPreferente=[]
            operadorPreferente2=[]
            ordenados=[]
            #print (self.opeBKP)
            num=0
            for operador in self.opepre:
                num=num+1
                numrow=num

            self.ui.gridAsignarOperadoresPredespegue.setColumnCount(5)
            self.ui.gridAsignarOperadoresPredespegue.setRowCount(numrow)
            self.ui.gridAsignarOperadoresPredespegue.setHorizontalHeaderLabels(['Nombre', 'Licencia', 'Ranking','Destino Fav','Proveedor'])
            self.ui.gridAsignarOperadoresPredespegue.insertRow(numrow)
            self.ui.gridAsignarOperadoresPredespegue.setRowCount(numrow)
            row=0
            for operador in self.opepre:

                self.ui.gridAsignarOperadoresPredespegue.setItem(row, 0, QTableWidgetItem(str(operador['Operador'])))
                self.ui.gridAsignarOperadoresPredespegue.setItem(row, 1, QTableWidgetItem(operador["Licencia"]))
                self.ui.gridAsignarOperadoresPredespegue.setItem(row, 2, QTableWidgetItem("vacio"))
                self.ui.gridAsignarOperadoresPredespegue.setItem(row, 3, QTableWidgetItem("vacio"))
                self.ui.gridAsignarOperadoresPredespegue.setItem(row, 4, QTableWidgetItem(str(operador["ProveedorFlete"])))
                row = row+1

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)

    def clickOpepre(self, a, b, c, d):
        try:
            if self.abierto == 1 :

                print ("click operadorpre")
                row = self.ui.gridAsignarOperadoresPredespegue.currentItem().row()
                columnname = ('Nombre')
                headercount = self.ui.gridAsignarOperadoresPredespegue.columnCount()
                print(headercount)
                for x in range(0,headercount,1):
                    headertext = self.ui.gridAsignarOperadoresPredespegue.horizontalHeaderItem(x).text()
                    if columnname == headertext:
                        matchcol = x
                        break
                self.cell = self.ui.gridAsignarOperadoresPredespegue.item(a,matchcol).text()
                self.viajes['viajes'][self.temporales]['propiedadunidad']['Operador'] = self.cell

                numrow=0
                for viaje in self.viajes['viajes']:
                    numrow=numrow+1

                self.ui.gridAsignarPredespegue.setColumnCount(5)
                self.ui.gridAsignarPredespegue.setRowCount(numrow)
                self.ui.gridAsignarPredespegue.setHorizontalHeaderLabels(['Viaje', 'Placas', 'Operador' , 'Km','Destinos'])
                self.ui.gridAsignarPredespegue.insertRow(numrow)
                self.ui.gridAsignarPredespegue.setRowCount(numrow)
                row=0
                for viaje in self.viajes['viajes']:
                    self.ui.gridAsignarPredespegue.setItem(row, 0, QTableWidgetItem(str(viaje['Viaje'])))
                    self.ui.gridAsignarPredespegue.setItem(row, 1, QTableWidgetItem(viaje["propiedadunidad"]["Placas"]))
                    self.ui.gridAsignarPredespegue.setItem(row, 2, QTableWidgetItem(viaje["propiedadunidad"]["Operador"]))
                    self.ui.gridAsignarPredespegue.setItem(row, 3, QTableWidgetItem(str(viaje["Km"])))
                    self.ui.gridAsignarPredespegue.setItem(row, 4, QTableWidgetItem(str(len(viaje["destinos"]))))


                    row = row+1
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False



    #info Predespegados
    def leeResultadosDb(self):

        print('** obteniendo viajesitos 8.5 atte: Marco Gomez **')
        headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/obtener-viajes"
        datos = {"empresaId": 1, "status": 8.5, "cedi": "SILODISA"}
        try:
            r = requests.post(url, data=json.dumps(datos), headers=headers).json()
            config.predespegados = r
            self.cargaTablaPredespegue()
        except requests.exceptions.HTTPError as err:
            print("fallo")
            print(err)
        return r

    def cargaTablaPredespegue(self):
        try:
            data = config.predespegados
            self.viajes = json.loads(data['Data'])
            self.plan = json.loads(data['Data'])

            numrow=0
            for viaje in self.viajes['viajes']:
                numrow=numrow+1

            self.ui.gridAsignarPredespegue.setColumnCount(5)
            self.ui.gridAsignarPredespegue.setRowCount(numrow)
            self.ui.gridAsignarPredespegue.setHorizontalHeaderLabels(['Viaje', 'Placas', 'Operador' , 'Km','Destinos'])
            self.ui.gridAsignarPredespegue.insertRow(numrow)
            self.ui.gridAsignarPredespegue.setRowCount(numrow)
            row=0
            for viaje in self.viajes['viajes']:
                self.ui.gridAsignarPredespegue.setItem(row, 0, QTableWidgetItem(str(viaje['Viaje'])))
                self.ui.gridAsignarPredespegue.setItem(row, 1, QTableWidgetItem(viaje["propiedadunidad"]["Placas"]))
                self.ui.gridAsignarPredespegue.setItem(row, 2, QTableWidgetItem(viaje["propiedadunidad"]["Operador"]))
                self.ui.gridAsignarPredespegue.setItem(row, 3, QTableWidgetItem(str(viaje["Km"])))
                self.ui.gridAsignarPredespegue.setItem(row, 4, QTableWidgetItem(str(len(viaje["destinos"]))))
                row = row+1


        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)

    def enviaViajes(self):
        try:
            db=DB()
        # Envia viajes predespegados
            if self.asignaPredespegados != []:
                for index in self.asignaPredespegados:
                    print("entre a predespegados")
                    if self.viajes['viajes'][index]["propiedadunidad"]['Placas'] != "" and self.viajes['viajes'][index]["propiedadunidad"]['Operador'] != "":
                        print("entre x2")
                        self.plan['viajes']=[]
                        self.plan['viajes'].append(self.viajes['viajes'][index])
                        print (self.plan)
                        status=8
                        viaje=self.plan

                        db.enviarViajes(status,viaje)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def guardaSS(self):
        try:
            ss = SpreadSheet()
            resGridrevuelto = fnGetGrid(self.ui.gridAsignar)
            print(resGridrevuelto)
            resGridOrdenado = ordenar_resultados(resGridrevuelto, completar=True)
            print(resGridOrdenado)
            res = ss.guarda_ss(resGridOrdenado, config.nombrePlan, "RESULTADOS", tipo_nombre_ss='name')
            config.dfPlan = self.res
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def refresh(self):
        try:
            self.res = config.dfPlan.copy()
            fnPutGrid(self.res, self.ui.gridAsignar, config.campos_ssAsignar)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False

    def AsignadorAutomatico (self):
        try:
            self.viajeauto = self.res.copy()
            self.id = json.loads(self.viajeauto.to_json(orient='records'))
            if self.id[0]['Viaje']:


                self.id_viaje = self.res.groupby('Viaje', as_index=False).sum()
                rowmaxplan = len(self.id_viaje)

                listrow = list (range(0, rowmaxplan))

                self.viajeordenado = json.loads(self.id_viaje.to_json(orient='records'))
                #Vehiculos ordenados
                self.vehiculosordenados = []
                self.vehiculossinordenar = []

                nave = json.loads(self.vehiculos.to_json(orient='records'))
                n=0
                for viaje in self.viajeordenado:

                    viaje['Placas']=nave[n]['Placas']
                    viaje['Operador']=self.ope[n]['Operador']

                    n = n + 1

                #cargar placas a todos los pedidos con el mismo viajes
                self.res.set_index('Viaje', inplace=True, drop=False)
                for viaje in self.viajeordenado:
                    idviaje = viaje['Viaje']
                    placas = viaje['Placas']
                    ope = viaje['Operador']
                    self.res.loc[idviaje, 'Placas' ] = placas
                    self.res.loc[idviaje, 'Operador' ] = ope

                fnPutGrid(self.res, self.ui.gridAsignar, config.campos_ssAsignar)
                # self.res['Placas'].iloc[viaje] = self.vehiculos['Placas'].iloc[viaje]
                # self.ui.gridAsignar.setItem(viaje, 2, QTableWidgetItem(self.vehiculos['Placas'].iloc[viaje]))

            else:
                print("Plan NO corrido en el motor")


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return False
