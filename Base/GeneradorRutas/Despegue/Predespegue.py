import os
import sys
import json
from IO.Apis.Db import DB
from IO.Apis.spread_sheet import SpreadSheet
from IO.Apis.consolidar_json import ConsolidarJson
from IO.Apis.json2df import *
from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem
import requests
from argparse import ArgumentParser
from PyQt5.QtCore import QDate, QTime, QDateTime, Qt
from pandas.io.json import json_normalize
from Base.GeneradorRutas.Datos.Planes import Planes
from Base.GeneradorRutas.Optimiza.funciones import *
import config
from datetime import datetime


class Predespegue:
    def __init__(self, ui):
        self.ui = ui
        
    def cargar(self):
        start_time = datetime.now()
        try:
            #res = self.leeResultadosDb()
            pl = Planes(self.ui)
            data = config.dfPlan
            config.dfPredespegue = data
            self.ui.gridPredespegar.setRowCount(data.shape[0])
            self.ui.gridPredespegar.setColumnCount(data.shape[1])
            pl.carga_grid(data, self.ui.gridPredespegar)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'No hay resultado'

    def send2db(self):
        start_time = datetime.now()
        try:
            print("Entra a send2db")
            cJSON = ConsolidarJson()
            viajes = cJSON.jsonParaDB()
            print(viajes)
            res = self.predespegamelo(viajes)
            print(res)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, res


    def predespegamelo(self, jsonPredespegue):
        start_time = datetime.now()
        try:
            url = 'https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/pre-despegar-viaje'
            payload = { 'status': '7', 'jsonViaje': jsonPredespegue }
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
            r = requests.post(url, data=json.dumps(payload), headers=headers)
            resultado = r.text
            #print(resultado)
            res = json.loads(resultado)
            print(res)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, res
            

    def selection_changed_resultados(self):
        start_time = datetime.now()
        try:
            rows = [idx.row() for idx in self.ui.gridPredespegar.selectionModel().selectedRows()]
            #print(rows)
            for value in rows:
                print(value)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'No hay resultado'



    def dataframe_generation_from_table(self):
        start_time = datetime.now()
        try:
            number_of_rows = self.ui.gridPredespegar.rowCount()
            number_of_columns = self.ui.gridPredespegar.columnCount()

            tmp_df = pd.DataFrame( 
                
                        columns=[Pedido, DestinoTR1, FechaEntregaPedido, FechaSalidaPedido, TipoPedido, TiemDescarga, Volumen, Peso, Piezas, Valor, VentanaFechaInicioPedido, VentanaHoraInicioPedido, VentanaFechaFinPedido, VentanaHoraFinPedido, TiemServicio, EntregaRecoleccion, Productos, RecolectaCEDI2, DestinoTR2, FechaEntregaTR2, RestriccionVolumen, Plan, Empresa, PrioridadVehiculo, Viaje, Tirada, Latitud, Longitud, TipoVehiculo, Refrigerado, VolumenPermitido, PesoPermitido, VolumenMax, PesoMax, ValorMax, OcupacionVolumen, OcupacionPeso, Dedicado, Cita, FechaSalida, FechaRetorno, DuracionViaje, Km, Co2, LitrosDiesel, CEDI, NumComidas, NumHoteles, NumDescansos, CostoProveedorFlete, CostoCombustible, CostoHotel, CostoComidas, CostoThermo, CostoCasetas, CostoFerry, CostoOtros, CostoTotal, NumComidasRetorno, NumHotelesRetorno, NumDescansosRetorno, Operador, Placas, ProveedorFlete, FechaEjecucionMotor, UsuarioEjecucionMotor, FechaEjecucionOpti, UsuarioEjecucionOpti, FechaEjecucionAsigna, UsuarioEjecucionAsigna, ModificadoVacios, ModificadoCercanos], # Fill columnets
                        index=range(number_of_rows) # Fill rows
                        ) 

            for i in range(number_of_rows):
                for j in range(number_of_columns):
                    tmp_df.ix[i, j] = self.ui.gridPredespegar.item(i, j).data()
            return tmp_df
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, tmp_df        