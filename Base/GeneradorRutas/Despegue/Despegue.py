import os
import sys
import json
from IO.Apis.Db import DB
from IO.Apis.spread_sheet import SpreadSheet
from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem
import requests
from argparse import ArgumentParser
from PyQt5.QtCore import QDate, QTime, QDateTime, Qt
from PyQt5 import QtGui, QtWidgets
from datetime import datetime

class Despegue:
    def __init__(self, ui):
        self.ui = ui
        self.ui.tableWidget_despegar_predespegue.itemSelectionChanged.connect(self.selection_changed_resultados)
        self.viajesArr = []
        #self.ui.btnDespegar.clicked.connect(self.despegamelo)
        self.despegarViaje = ''
        self.ui.tableWidget_despegar_predespegue.setSortingEnabled(False)
        self.ui.tableWidget_despegar_despegados.setSortingEnabled(False)
        #self.ui.tableWidget_despegar_despegados.setEditTriggers(False)

    def manejador(self):
        start_time = datetime.now()
        try:
            self.carga_tablas()
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'

    def carga_tablas(self):
        start_time = datetime.now()
        try:
            self.cargarPredespegados()
            self.cargarDespegados()
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'    


           

    def limpiar(self):
        start_time = datetime.now()
        try:
            self.ui.tableWidget_despegar_predespegue.setItem(0,0, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,1, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,2, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,3, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,4, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,5, QTableWidgetItem(""))
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'

    def cargarPredespegados(self):
        start_time = datetime.now()
        try:           
            self.ui.tableWidget_despegar_predespegue.setColumnCount(6)
            self.ui.tableWidget_despegar_predespegue.setSelectionBehavior(QTableWidget.SelectRows)
            self.ui.tableWidget_despegar_predespegue.setSelectionMode(QTableWidget.SingleSelection)
            self.ui.tableWidget_despegar_predespegue.setHorizontalHeaderLabels(['Viaje', 'Fecha de salida', 'Tipo de vehiculo', 'Placas', 'Operador', 'Fletera'])
            self.ui.tableWidget_despegar_predespegue.setItem(0,0, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,1, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setColumnWidth(1, 200)
            self.ui.tableWidget_despegar_predespegue.setColumnWidth(3, 200)
            self.ui.tableWidget_despegar_predespegue.setItem(0,2, QTableWidgetItem(""))
            self.ui.tableWidget_despegar_predespegue.setItem(0,3, QTableWidgetItem(""))
            db = DB()
            sp = SpreadSheet()
            predespegados = db.obtenerViajes(1,8.5,'SILODISA')                        
            aux = predespegados["Data"]            
            aux = json.loads(aux)
            #print(aux)
            viajes = aux['viajes']
            tamaño = len(viajes)
            self.ui.tableWidget_despegar_predespegue.setRowCount(tamaño)
            
            for idx, viaje in enumerate(viajes):
                #print(viaje)
                esto = str(viaje["Viaje"])
                self.viajesArr.append(esto)
                esto2 = str(viaje["FechaSalida"])
                esto3 = str(viaje["Placas"])
                esto4 = str(viaje["propiedadunidad"]["Operador"])
                #print(idx)
                index = idx
                self.ui.tableWidget_despegar_predespegue.setItem(index,0, QTableWidgetItem(esto))
                self.ui.tableWidget_despegar_predespegue.setItem(index,1, QTableWidgetItem(esto2))
                self.ui.tableWidget_despegar_predespegue.setItem(index,2, QTableWidgetItem(esto3))
                self.ui.tableWidget_despegar_predespegue.setItem(index,3, QTableWidgetItem(esto4))
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'

    def cargarDespegados(self):
        start_time = datetime.now()
        try:
            db = DB()
            sp = SpreadSheet()
            despegados = db.obtenerViajes(1,9,'SILODISA')
            aux2 = despegados["Data"]
            aux2 = json.loads(aux2)
            #print(aux)
            viajes2 = aux2['viajes']
            tamaño2 = len(viajes2)
            if tamaño2 > 0:
                self.ui.tableWidget_despegar_despegados.setHorizontalHeaderLabels(['Viaje', 'Fecha de salida', 'Tipo de vehiculo', 'Placas', 'Operador', 'Fletera', 'Token'])
                self.ui.tableWidget_despegar_despegados.setRowCount(tamaño2)
                self.ui.tableWidget_despegar_despegados.setColumnCount(7)
                self.ui.tableWidget_despegar_despegados.setItem(0,0, QTableWidgetItem("Viaje"))
                self.ui.tableWidget_despegar_despegados.setItem(0,1, QTableWidgetItem("Fecha Salida"))
                self.ui.tableWidget_despegar_despegados.setColumnWidth(1, 200)
                self.ui.tableWidget_despegar_despegados.setColumnWidth(3, 200)
                self.ui.tableWidget_despegar_despegados.setItem(0,2, QTableWidgetItem("Placas"))
                self.ui.tableWidget_despegar_despegados.setItem(0,3, QTableWidgetItem("Operador"))

                for idx, viaje in enumerate(viajes2):
                    #print(viaje)
                    esto = str(viaje["Viaje"])
                    esto2 = str(viaje["FechaSalida"])
                    esto3 = str(viaje["Placas"])
                    esto4 = str(viaje["propiedadunidad"]["Operador"])
                    #print(idx)
                    index = idx
                    self.ui.tableWidget_despegar_despegados.setItem(index,0, QTableWidgetItem(esto))
                    self.ui.tableWidget_despegar_despegados.setItem(index,1, QTableWidgetItem(esto2))
                    self.ui.tableWidget_despegar_despegados.setItem(index,2, QTableWidgetItem(esto3))
                    self.ui.tableWidget_despegar_despegados.setItem(index,3, QTableWidgetItem(esto4))

            else:
                self.limpiar()
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'


    def selection_changed_resultados(self):
        start_time = datetime.now()
        try:
            rows = [idx.row() for idx in self.ui.tableWidget_despegar_predespegue.selectionModel().selectedRows()]
            #print(rows)
            for value in rows:
                valor = value
                self.despegarViaje = self.viajesArr[valor]
                #print(self.despegarViaje)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'

    def despegamelo(self):
        start_time = datetime.now()     
        try:
            if self.despegarViaje is not None:
                #print(self.despegarViaje)
                url = 'https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/despegar-viaje'
                payload = { 'empresaId': '1', 'IdViaje': self.despegarViaje }
                headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
                r = requests.post(url, data=json.dumps(payload), headers=headers)
                resultado = r.text
                #print(resultado)
                cosa = json.loads(resultado)
                #print(cosa)
                self.ui.tableWidget_despegar_predespegue.clearSelection()
                self.ui.tableWidget_despegar_despegados.clearSelection()
                self.despegarViaje = None

                if cosa[0]:
                    self.ui.labelDespeguePIN.setText("PIN: " + cosa[2])
                else:
                    self.ui.labelDespeguePIN.setText(cosa[2][:1].upper() + cosa[2][1:])

                self.cargarDespegados()
                self.cargarPredespegados()

            else:
                #TODO crear aviso al ususario
                self.ui.labelDespeguePIN.setText('Seleccione algún viaje')
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, sys.modules[__name__], str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, 'ok'