# ---------------------------------------------------------------------
# provisional para ver los nombre!!!!
# Qt windows creadas en Qt Creator
# from Qt.arms import *
# ---------------------------------------------------------------------
from __future__ import print_function

import config
from Base.GeneradorRutas.Datos.Planes import *
from Base.GeneradorRutas.Optimiza.funciones import *

from datetime import datetime
from datetime import timedelta
import numpy as np
import pandas as pd
import pygsheets

import sys
import os
import time
import gc
import arrow


from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QTableWidget, QTableWidgetItem, QAction, QVBoxLayout
from PyQt5 import QtCore, QtGui, QtWidgets

from IO.Apis.calculate_metrics import *

class OptimizaAutomatico:
    def __init__(self, ui):
        # ---------------------------------------------------
        self.ui = ui



 
    def b(self, s):
        h = str(datetime.now().strftime("%d-%m-%Y %H:%M:%S.%f"))
        config.bitacora.append([h, s])

    def rutina_inicial(self):
        try:
            # ---------------------------------------------------
            self.t = datetime.now()
            config.TableroOptimizado = None
            self.gcResultados = None
            self.contador = 0
            self.contBitacora_cambios = 0
            config.bitacora = []

            # trae parametros
            self.ignoraMatriz = config.slot_checkIgnoraComp
            self.ignoraAcceso = config.slot_checkIgnoraAcceso
            self.OcupacionMenorA = int(config.slot_editOcupacion)
            self.cercania = int(config.slot_editCercania) * 1000
            self.valor = int(config.slot_editAumentarValor) * 1000
            self.ventana = int(config.slot_editAumentarVentana)
            self.volumen = 1 + int(config.slot_editAumentarVol) / 100
            self.peso = 1 + int(config.slot_editAumentarKg) / 100

            self.b('I N I C I O')
            self.b('---------------------------------------------')
            # ---------------------------------------------------
            # obten los datos del grid, por si cambio algo, no TOMAR los de dfPlan!!!!
            dfOptimizaAuto = fnGetGrid(config.mainWindow.gridElPlan)

            # PREPARO LOS 3 FRAMEWORKS (PESTAÑAS) CON INFO QUE REQUIERO DEL SS
            self.P = dfOptimizaAuto
            self.C = config.dfCompatibilidad
            self.A = config.A
            # ---------------------------------------------------
            # verifica que traiga VIAJE, o sea que hay pasado por MOTOR, sino salte
            try:
                # si trae un string, va a mandar error
                if not (int(self.P.iloc[0].Viaje) > 0):
                    print('El plan si trae viajes...')
            except:
                print('Plan sin viajes')
                self.b('Plan sin viajes')
                return False

            # ---------------------------------------------------
            # GENERA CATEGORIAS
            self.A['ORIGEN'] = self.A.ORIGEN.astype('category')
            self.A['DESTINO'] = self.A.DESTINO.astype('category')
            self.P['DestinoTR1'] = self.P.DestinoTR1.astype('category')
            # ---------------------------------------------------
            # guarda y luego vuelve a meterlos los PEDIDOS Dedicados = TRUE
            self.RDedicados = self.P.loc[(self.P.Dedicado == 'Si'), :]
            self.R = self.P.loc[(self.P.Dedicado == 'No'), :].copy()
            self.b('Se eliminaron Pedidos Dedicados (diferente de No), ya que no se pueden combinar con otros viajes')
            # ---------------------------------------------------
            if self.valida_datos() == False:
                return False
            # ----------------------------------------------------_
            # multiplica los valores por parametros deseados
            self.R.VolumenMax *= self.volumen
            self.R.PesoMax *= self.peso
            self.R.ValorMax += self.valor
            # ventanas
            if self.ventana > 0:
                for index, row in self.R.iterrows():
                    self.R.loc[index, 'VentanaHoraFinPedido'] = row.VentanaHoraFinPedido + timedelta(
                        minutes=self.ventana)
            return True
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def rutina_final(self, df):
        try:
            # ----------------------------------------------------
            # Regresa lo inflado de parametros a su lugar
            # DIVIDE los valores por parametros deseados
            df.VolumenMax /= self.volumen
            df.PesoMax /= self.peso
            df.ValorMax -= self.valor
            # ventanas
            if self.ventana > 0:
                for index, row in df.iterrows():
                    self.R.loc[index, 'VentanaHoraFinPedido'] = row.VentanaHoraFinPedido - timedelta(
                        minutes=self.ventana)
            # ----------------------------------------------------
            # borra las ultimas distancias
            if 'DISTANCIA' in df.columns:
                df.drop(['DISTANCIA', 'TIEMPO'], axis=1, inplace=True)
            # ----------------------------------------------------
            # recalcula como quedo la ocupacion y grabalo en REGRESO
            v_sum = df.groupby(df.index).sum()
            v_max = df.groupby(df.index).max()
            df.OcupacionVolumen = v_sum.Volumen / v_max.VolumenMax * 100
            df.OcupacionPeso = v_sum.Peso / v_max.PesoMax * 100
            # ----------------------------------------------------

            # ----------------------------------------------------
            #  regresa fechas y horas a como estaban
            self.transforma_fechas(df)
            # ----------------------------------------------------
            # cacatena los registros que eliminamos que venian como DEDICADOS = Si...son parte del plan
            if not self.RDedicados.empty:
                df = pd.concat([df, self.RDedicados])
                self.b('Número de Pedidos Dedicados {}'.format(self.RDedicados.shape[0]))
            # ----------------------------------------------
            # tablero
            self.b('---------------------------------------------')
            config.TableroOptimizado = calculate_metrics(df)
            self.b('Optimizado: {}'.format(config.TableroOptimizado))
            # ----------------------------------------------
            self.b('Destinos Reubicados: {}'.format(config.contadorReubicados))
            # ----------------------------------------------------
            return df
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def valida_datos(self):
        try:
            # ----------------------------------------------------
            # validaciones de que lleguen los datos bien R, C y A
            # ----------------------------------------------------
            # checa que no traiga nulls o Nan
            if (self.R.isnull().any().any()) == True:
                self.b('**** ERROR **** {}'.format('ERROR....hay NaN o nulls en Resultados'))
                return False
            # checa que no traiga nulls o Nan
            if (self.C.isnull().any().any()) == True:
                self.b('**** ERROR **** {}'.format('ERROR....hay NaN o nulls en Matriz de Complementos'))
                return False
            # checa que no traiga nulls o Nan
            if (self.A.isnull().any().any()) == True:
                self.b('**** ERROR **** {}'.format('ERROR....hay NaN o nulls en Arcos'))
                return False
            # ----------------------------------------------------
            # quitale a todos los espacio, antes y despues al nombre de columnas
            self.R.columns.str.strip()
            self.C.columns.str.strip()
            self.A.columns.str.strip()
            # ----------------------------------------------------
            # verifica que lleguen los nombres de las columnas que necesito
            if not self.R.columns.values.tolist() == config.campos_ssPlan:
                self.b('Los campos que trae el SS y No.Campos esperados = {}, config = {}'.format(len(self.R.columns.values.tolist()), len(config.campos_ssPlan)))
                #return False
            # ----------------------------------------------------
            self.C['ClaveProducto1'] = self.C['ClaveProducto1'].apply(str)
            self.C['ClaveProducto2'] = self.C['ClaveProducto2'].apply(str)
            # ----------------------------------------------------
            # cambia las columnas a tipo NUMERIC
            self.R[['Volumen', 'Peso', 'Valor', 'Piezas', 'TiemDescarga']] = self.R[
                ['Volumen', 'Peso', 'Valor', 'Piezas', 'TiemDescarga']].apply(pd.to_numeric)
            self.R[['Tirada', 'VolumenMax', 'PesoMax', 'ValorMax']] = self.R[
                ['Tirada', 'VolumenMax', 'PesoMax', 'ValorMax']].apply(pd.to_numeric)
            self.R[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']] = \
                self.R[['RestriccionVolumen', 'OcupacionVolumen', 'OcupacionPeso', 'Km', 'Co2', 'CostoTotal']].apply(
                    pd.to_numeric)
            # cambia las columnas a tipo  DATETIME
            self.R['FechaSalida'] = pd.to_datetime(self.R['FechaSalida'], dayfirst=True)
            self.R['FechaRetorno'] = pd.to_datetime(self.R['FechaRetorno'], dayfirst=True)
            self.R['FechaEntregaPedido'] = pd.to_datetime(self.R['FechaEntregaPedido'], dayfirst=True)
            self.R['FechaSalidaPedido'] = pd.to_datetime(self.R['FechaSalidaPedido'], dayfirst=True)
            self.R['VentanaFechaInicioPedido'] = pd.to_datetime(self.R['VentanaFechaInicioPedido'], dayfirst=True)
            self.R['VentanaFechaFinPedido'] = pd.to_datetime(self.R['VentanaFechaFinPedido'], dayfirst=True)
            self.R['VentanaHoraInicioPedido'] = pd.to_datetime(self.R['VentanaHoraInicioPedido'], dayfirst=True)
            self.R['VentanaHoraFinPedido'] = pd.to_datetime(self.R['VentanaHoraFinPedido'], dayfirst=True)
            self.R['FechaEntregaTR2'] = pd.to_datetime(self.R['FechaEntregaTR2'], dayfirst=True)
            self.R['FechaEjecucionOpti'] = pd.to_datetime(self.R['FechaEjecucionOpti'], dayfirst=True)

            self.R['FechaEjecucionMotor'] = pd.to_datetime(self.R['FechaEjecucionMotor'], dayfirst=True)
            # ----------------------------------------------------
            return True
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def transforma_fechas(self, df):
        try:
            # -----------------------------------------------------
            # fechas a dd/mm/aaaa
            df.VentanaFechaInicioPedido = df.VentanaFechaInicioPedido.dt.strftime('%d/%m/%Y')
            df.VentanaFechaFinPedido = df.VentanaFechaFinPedido.dt.strftime('%d/%m/%Y')


            df.FechaEntregaPedido = df.FechaEntregaPedido.dt.strftime('%d/%m/%Y %H:%M')

            df.FechaSalidaPedido = df.FechaSalidaPedido.dt.strftime('%d/%m/%Y %H:%M')
            df.FechaSalida = df.FechaSalida.dt.strftime('%d/%m/%Y')
            df.FechaRetorno = df.FechaRetorno.dt.strftime('%d/%m/%Y')
            df.FechaEntregaTR2 = df.FechaEntregaTR2.dt.strftime('%d/%m/%Y')
            df.FechaEjecucionOpti = df.FechaEjecucionOpti.dt.strftime('%d/%m/%Y')
            df.FechaEjecucionMotor = df.FechaEjecucionMotor.dt.strftime('%d/%m/%Y')

            # -----------------------------------------------------
            # horas
            df.VentanaHoraInicioPedido = df.VentanaHoraInicioPedido.dt.strftime('%H:%M')
            df.VentanaHoraFinPedido = df.VentanaHoraFinPedido.dt.strftime('%H:%M')
            # -----------------------------------------------------
            df.OcupacionVolumen = df.OcupacionVolumen.round(0)
            df.OcupacionPeso = df.OcupacionPeso.round(0)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False


    def guarda_optimiza_cercanos_SS(self):
        try:
            # guarda Resultados
            z = pd.read_csv('/Users/ARMS/optimiza_cercanos.csv')
            wks = config.sheet.worksheet_by_title('RES-OPTIMIZA')
            wks.set_dataframe(z, (5, 1), fit=True, copy_head=False)
            # guarda bitacora
            config.B.to_csv('/Users/ARMS/optimiza_cercanos_bitacora.csv')
            z = pd.read_csv('/Users/ARMS/optimiza_cercanos_bitacora.csv')
            wks = config.sheet.worksheet_by_title('BITACORA CERCANOS')
            wks.clear(start='A1', end='end')
            wks.set_dataframe(z, (1, 1), fit=True, copy_head=False)
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return True

    def guarda_optimiza_vacios_SS(self):
        try:
            # guarda Resultados
            z = pd.read_csv('/Users/ARMS/optimiza_vacios.csv')
            wks = config.sheet.worksheet_by_title('RES-OPTIMIZA')
            wks.set_dataframe(z, (5, 1), fit=True, copy_head=False)
            # guarda bitacora
            config.B.to_csv('/Users/ARMS/optimiza_vacios_bitacora.csv')
            z = pd.read_csv('/Users/ARMS/optimiza_vacios_bitacora.csv')
            wks = config.sheet.worksheet_by_title('BITACORA VACIOS')
            wks.clear(start='A1', end='end')
            wks.set_dataframe(z, (1, 1), fit=True, copy_head=False)
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return True


    def ss_abre_resultados(self, nombre_ss, nombre_wks):
        try:
            config.gcResultados = pygsheets.authorize(outh_file='../ImportarDatos/client_secret.json')
            config.sheet = config.gcResultados.open(nombre_ss)
            wks = config.sheet.worksheet_by_title(nombre_wks)
            self.P = wks.get_as_df()
            # TODO: borra los 4 primeros renglones. Siempre en todos los SS de ARMS
            self.P.drop(self.P.index[0:3], inplace=True)
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return True

    def ss_abre_compatibilidad(self, nombre_ss, nombre_wks):
        try:
            wks = config.sheet.worksheet_by_title(nombre_wks)
            config.dfCompatibilidad = wks.get_as_df()
            # TODO: borra los 4 primeros renglones. Siempre en todos los SS de ARMS
            config.dfCompatibilidad.drop(config.dfCompatibilidad.index[0:3], inplace=True)
            # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
        return True

    def cumple_6_restricciones(self, pedidos_a_reubicar, pedidos_al_viaje, esta_a_seg):
        bCumple = False
        if self.restr_acceso(pedidos_a_reubicar, pedidos_al_viaje):
            if self.restr_volumen(pedidos_a_reubicar, pedidos_al_viaje):
                if self.restr_peso(pedidos_a_reubicar, pedidos_al_viaje):
                    if self.restr_valor(pedidos_a_reubicar, pedidos_al_viaje):
                        if self.restr_matriz_comp(pedidos_a_reubicar, pedidos_al_viaje):
                            if self.restr_ventanas(pedidos_a_reubicar, pedidos_al_viaje, esta_a_seg):
                                bCumple = True
        return bCumple

    def restr_acceso(self, reubicar, al_viaje):
        try:
            # si seleccionaron "ignorar" la restriccion de acceso
            if self.ignoraAcceso: return True

            # checa que al_viaje tenga renglones
            if al_viaje.empty:
               self.b('        ...Restricción Acceso. - Al viaje -, ya está vacío .')
               return False

            # cual es la restriccion de acceso de "meter" y cual la "a_este_viaje"
            de = reubicar.RestriccionVolumen.values[0]
            a = al_viaje.RestriccionVolumen.values[0]
            if de >= a:
                ok = True
            else:
                x = 'Destino acepta MAX. {} m3, Viaje tiene {} m3'.format(de, a)
                self.b('        ...No cumple Restricción de Acceso {}.'.format(x))
                ok = False
            # ----------------------------------------------------
            return ok
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def restr_volumen(self, reubicar, al_viaje):
        try:
            # TODO meter factores de ignorar
            #  ------- obten los totales para trabajar
            reubicar_sum = reubicar.Volumen.sum()
            al_viaje_sum = al_viaje.Volumen.sum()
            al_viaje_max = al_viaje.VolumenMax.max()
            # Suma y dime si se pasa o le cabe (negativo s pasa)
            dif = al_viaje_max - al_viaje_sum - reubicar_sum
            # checa que al_viaje tenga renglones
            if al_viaje.empty:
                self.b('        ...Restricción Volumen. - Al viaje -, ya está vacío .')
                return False

            # volumen
            if dif < 0.0:
                x = 'Se Excede:{:.4f}m3, Max:{:.0f}m3. Viaje {:.4f} + Pedidos {:.4f}'.format(dif, al_viaje_max,
                                                                                             al_viaje_sum, reubicar_sum)
                self.b('        ...No cumple Restricción Volumen {}.'.format(x))
                ok = False
            else:
                ok = True
            # ----------------------------------------------------
            return ok
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def restr_peso(self, reubicar, al_viaje):
        try:
            # TODO meter factores de ignorar
            #  ------- obten los totales para trabajar
            reubicar_sum = reubicar.Peso.sum()
            al_viaje_sum = al_viaje.Peso.sum()
            al_viaje_max = al_viaje.PesoMax.max()
            # Suma y dime si se pasa o le cabe (negativo s pasa)
            dif = al_viaje_max - al_viaje_sum - reubicar_sum
            # checa que al_viaje tenga renglones
            if al_viaje.empty:
                self.b('        ...Restricción Peso. - Al viaje -, ya está vacío .')
                return False

            # volumen
            if dif < 0.0:
                x = 'Se Excede:{:.0f}kg, Max:{:.0f}kg. Viaje {:.0f} + Pedidos {:.0f}'.format(dif, al_viaje_max,
                                                                                             al_viaje_sum, reubicar_sum)
                self.b('        ...No cumple Restricción Peso {}.'.format(x))
                ok = False
            else:
                ok = True
            # ----------------------------------------------------
            return ok
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def restr_valor(self, reubicar, al_viaje):
        try:
            # TODO meter factores de ignorar
            #  ------- obten los totales para trabajar
            reubicar_sum = reubicar.Valor.sum()
            al_viaje_sum = al_viaje.Valor.sum()
            al_viaje_max = al_viaje.ValorMax.max()
            # Suma y dime si se pasa o le cabe (negativo s pasa)
            dif = al_viaje_max - al_viaje_sum - reubicar_sum
            # checa que al_viaje tenga renglones
            if al_viaje.empty:
                self.b('        ...Restricción Valor. - Al viaje -, ya está vacío .')
                return False


            # volumen
            if dif < 0.0:
                x = 'Se Excede:{:.4f} pesos, Max:{:.0f} pesos. Viaje {:.0f} + Pedidos {:.0f}'.format(dif, al_viaje_max,
                                                                                                     al_viaje_sum,
                                                                                                     reubicar_sum)
                self.b('        ...No cumple Restricción Valor {}.'.format(x))
                ok = False
            else:
                ok = True
            # ----------------------------------------------------
            return ok
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def restr_matriz_comp(self, reubicar, al_viaje):
        try:
            if self.ignoraMatriz:
                return True
            # convierte los productos a listados
            productos_a_reubicar = self.productos_a_listado(reubicar)
            productos_en_viaje = self.productos_a_listado(al_viaje)
            # checa que al_viaje tenga renglones
            if al_viaje.empty:
                self.b('        ...Restricción M. Comp. - Al viaje -, ya está vacío .')
                return False


            # has el match entre todas las parejas 'reubicar' y 'al_viaje'
            for i in productos_a_reubicar:
                for j in productos_en_viaje:
                    valor = self.C.loc[
                        (self.C.ClaveProducto2 == str(i)) & (self.C.ClaveProducto1 == str(j)), 'ValorCompatibilidad']
                    if not valor.empty:
                        if valor.values == 'NO':
                            # AQUI PORQUE NO ENTRO....[i, j, valor.values]
                            x = 'Producto {} con Producto {}'.format(str(i), str(j))
                            self.b('        ...No cumple Restricción Matriz Compatibilidad {}.'.format(x))
                            ok = False
                        else:
                            return True
                    else:
                        ok = True
            # ----------------------------------------------------
            return ok
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def restr_ventanas(self, reubicar, al_viaje, esta_a_seg):
        try:
            # checa que al_viaje tenga renglones
            if al_viaje.empty:
                self.b('        ...Restricción Ventana. - Al viaje -, ya está vacío .')
                return False


            # me traigo los horarios de destino y viaje
            horario_reubicar = self.ventana_destino(reubicar)
            horario_al_viaje = self.ventana_viaje(al_viaje)
            factor_google = 1.5
            tiempo_destino_min = esta_a_seg / 60 * factor_google
            # busca la fecha en viaje
            tirada = 0
            for key in horario_reubicar:
                if key in horario_al_viaje.keys():  # encontre un dia que deseo entregar dentro de los dias del Viaje
                    # trae su datetime
                    x = horario_al_viaje[key]
                    # separa el datetime de la tirada
                    fecha = x[0]
                    tirada = x[1]
                    # obten el horario maximo a entregar segun el DESTINO
                    horario_max_para_entregar = horario_reubicar[key].time()
                    # suma a la hora ultima del viaje....el traslado segun google (es promedio de los destinos que trae viaje!!)
                    suma_traslado_y_ultima_hora = timedelta(hours=fecha.hour) + timedelta(
                        minutes=fecha.minute) + timedelta(minutes=tiempo_destino_min)
                    tiempo_final = time.strftime('%H:%M:%S', time.gmtime(suma_traslado_y_ultima_hora.total_seconds()))
                    tiempo_final = datetime.strptime(tiempo_final, '%H:%M:%S').time()
                    if tiempo_final <= horario_max_para_entregar:
                        # dia_con_hora_entrega = datetime.combine(fecha.date(), tiempo_final)
                        # return True, dia_con_hora_entrega, tirada + 1
                        config.tiempo_final = tiempo_final
                        config.tirada = tirada + 1
                        return True
                    else:
                        # AQUI PORQUE NO ENTRO....
                        dia_con_hora_entrega = datetime.combine(key, tiempo_final)
                        self.b(
                            '        ...No cumple Restricción de Ventana {}. Max hora: {}.'.format(dia_con_hora_entrega,
                                                                                                   horario_max_para_entregar))
                        return False
                else:
                    # metelo en otro dia a las 8 am
                    # print ('** restr_ventanas else METELO OTRO DIA:...', str(datetime.now() - self.t), 'restr_ventanas')
                    t = datetime.strptime('08:00', '%H:%M').time()
                    dia_con_hora_entrega = datetime.combine(key, t)
                    self.b('            ...SI CUMPLE OTRO DIA, : {}'.format(dia_con_hora_entrega))
                    return True, dia_con_hora_entrega, tirada + 1
            self.b(' NO CUMPLE RESTRICCION VENTANA', 'Sin Horario')
            # ----------------------------------------------------
            return False
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def ventana_destino(self, df):
        try:
            destino_ventanas = {}
            # ---------------------------------------
            diff = df.VentanaFechaFinPedido.iloc[0] - df.VentanaFechaInicioPedido.iloc[0]
            # prepara el diccionario del destino
            delta = timedelta(days=1)
            for i in range(0, diff.days + 1):
                dia = df.VentanaFechaInicioPedido.iloc[0] + i * delta
                suma_dia_y_hora = datetime.combine(dia.date(), df.VentanaHoraFinPedido.iloc[0].time())
                destino_ventanas[dia.date()] = suma_dia_y_hora
            return destino_ventanas
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def ventana_viaje(self, df):
        try:
            # ---------------------------------------
            # me traigo VENTANAS del VIAJE
            viaje_ventanas = {}
            for index, row in df.iterrows():
                viaje_ventanas[row['FechaSalidaPedido'].date()] = [row['FechaSalidaPedido'].time(), row.Tirada]
            return viaje_ventanas
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def productos_a_listado(self, df):
        try:
            unicos = df.Productos.unique()
            listado_productos = []
            for prod in unicos:
                p = str(prod)
                pord_split = p.split(',')
                for i in pord_split:
                    if not (i in listado_productos):
                        listado_productos.append(i.strip())
            return listado_productos
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            self.b('**** ERROR **** {}'.format(error))
            return False

    def reubicalo(self, reubicar, al_viaje):
        try:
            # datos de viaje
            Viaje = al_viaje.iloc[0].Viaje
            TipoVehiculo = al_viaje.iloc[0].TipoVehiculo
            VolumenMax = al_viaje.iloc[0].VolumenMax
            PesoMax = al_viaje.iloc[0].PesoMax
            ValorMax = al_viaje.iloc[0].ValorMax
            Tirada = al_viaje.groupby('Viaje').Tirada.max().values[0] + 1

            # actualiza en R
            for index, row in reubicar.iterrows():
                # guarda viaje actual
                viaje_actual = row.Viaje
                # calcula la fecha
                entrega = arrow.get(row.FechaEntregaPedido)
                # entrega = arrow.get(row.FechaEntregaPedido, 'YYYY-MM-DD HH:mm:ss')
                FechaSalidaPedido = entrega.shift(minutes=+row.TiemDescarga)

                # reemplaza en R (no olvides modificar ModificadoCercanos = 1
                self.R.set_value(index, 'Viaje', Viaje, takeable=False)
                self.R.set_value(index, 'Tirada', Tirada, takeable=False)
                self.R.set_value(index, 'TipoVehiculo', TipoVehiculo, takeable=False)
                self.R.set_value(index, 'VolumenMax', VolumenMax, takeable=False)
                self.R.set_value(index, 'PesoMax', PesoMax, takeable=False)
                self.R.set_value(index, 'ValorMax', ValorMax, takeable=False)
                self.R.set_value(index, 'FechaSalidaPedido', FechaSalidaPedido.naive, takeable=False)
                self.R.set_value(index, 'Viaje', Viaje, takeable=False)

            # para el VIAJE donde lo reubicamos (va mas lleno)
            # recalcula la ocupacion
            # m3
            suma = self.R.loc[self.R.Viaje == Viaje].groupby('Viaje').Volumen.sum().values[0]
            maxi = self.R.loc[self.R.Viaje == Viaje].groupby('Viaje').Volumen.max().values[0]
            ocu = suma / maxi * 100
            self.R.loc[self.R.Viaje == Viaje, 'OcupacionVolumen'] = ocu
            # kg
            suma = self.R.loc[self.R.Viaje == Viaje].groupby('Viaje').Peso.sum().values[0]
            maxi = self.R.loc[self.R.Viaje == Viaje].groupby('Viaje').Peso.max().values[0]
            ocu = suma / maxi * 100
            self.R.loc[self.R.Viaje == Viaje, 'OcupacionPeso'] = ocu
            # informa que podificaste
            self.R.loc[self.R.Viaje == Viaje, 'ModificadoCercanos'] = 1

            # para el VIAJE donde lo sacamos (va mas vacio)
            # recalcula la ocupacion
            # m3
            try:
                suma = self.R.loc[self.R.Viaje == viaje_actual].groupby('Viaje').Volumen.sum().values[0]
            except:
                pass #el viaje ya no existe
            try:
                maxi = self.R.loc[self.R.Viaje == viaje_actual].groupby('Viaje').Volumen.max().values[0]
            except:
                pass #el viaje ya no existe
            try:
                ocu = suma / maxi * 100
                self.R.loc[self.R.Viaje == viaje_actual, 'OcupacionVolumen'] = ocu
            except:
                pass #el viaje ya no existe
            # kg
            try:
                suma = self.R.loc[self.R.Viaje == viaje_actual].groupby('Viaje').Peso.sum().values[0]
            except:
                pass #el viaje ya no existe
            try:
                maxi = self.R.loc[self.R.Viaje == viaje_actual].groupby('Viaje').Peso.max().values[0]
            except:
                pass #el viaje ya no existe
            try:
                ocu = suma / maxi * 100
                self.R.loc[self.R.Viaje == viaje_actual, 'OcupacionPeso'] = ocu
            except:
                pass #el viaje ya no existe
            # informa que podificaste
            try:
                self.R.loc[self.R.Viaje == viaje_actual, 'ModificadoCercanos'] = 1
            except:
                pass #el viaje ya no existe
            # ----------------------------------------------------
            return True
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False


    def optimiza_vacios(self, informa_el_progreso):
        try:
            print ('optimiza_vacios')
            config.contadorReubicados = 0

            if not (self.rutina_inicial()):
                self.b('**** error en rutina inicial, optimiza_vacios')
                fnGuardaBitacora('Optimiza Vacíos')
                return False
            # ----------------------------------------------------
            x = 'PARAMETROS: %Ocupación menor al {:0,.0f}%; Cercanía:{:0,.0f} km; Aumenta Vol:{:0,.0f}%; Aumenta Peso:{:0,.0f}%, Aumenta Valor:{:0,.0f} miles pesos'.format(
                self.OcupacionMenorA, self.cercania / 1000, self.volumen*100, self.peso*100, self.valor)
            y = ', Ignora Acceso:{:0,.0f}; Ignora Mat.Compatibilidad:{:0,.0f}; Aumenta Ventana: {:0,.0f} mins'.format(
                self.ignoraAcceso, self.ignoraMatriz, self.ventana)
            self.b(x + y)
            # ----------------------------------------------------
            # inicializa DATA FRAMES
            self.dfOptimizado = None
            OPTIMIZADO = self.R.copy()
            OPTIMIZADO = OPTIMIZADO[0:0]  # quita todos los renglones
            RESTO = self.R.copy()
            RESTO.set_index('DestinoTR1', inplace=True)
            # =============================================================================================================
            # Encuentra VIAJES VACIOS < porcentaje_vacio
            # =============================================================================================================
            viajes_vacios = RESTO.groupby('Viaje').OcupacionVolumen.max()
            viajes_vacios = viajes_vacios.loc[viajes_vacios <= self.OcupacionMenorA]
            viajes_vacios.sort_values(inplace=True)
            ViajesVacios = int(viajes_vacios.shape[0])
            # ***************************************************
            self.b('Número de Viajes Vacios con ocupación menor al {}% - {}. Viajes {} - Ocupación: {}'.format(
                self.OcupacionMenorA, ViajesVacios, str(viajes_vacios.index.values), str(viajes_vacios.values)))
            # =============================================================================================================
            # Para todos los VIAJES VACIOS < porcentaje_vacio
            # =============================================================================================================
            cont = 0
            for num_viaje_a_reubicar in viajes_vacios.index:
                cont += 1
                self.b('........................')
                self.b('{}. Viaje a reubicar {} '.format(cont, num_viaje_a_reubicar))
                # ----------------------------------------------------
                informa_el_progreso.emit({'progressBar': 10 + (cont / ViajesVacios * 80), 'lblMensaje': 'Reubicando...'+str(cont)+'/'+str((ViajesVacios))})
                # ----------------------------------------------------
                # destinos_del_num_viaje_a_reubicar dentro del Viaje
                destinos_del_num_viaje_a_reubicar = RESTO.loc[RESTO.Viaje == num_viaje_a_reubicar].index.unique()
                # ____________________________________________________
                # quita de RESTO el viaje vacio
                RESTO = RESTO.loc[RESTO.Viaje != num_viaje_a_reubicar].copy()
                # =============================================================================================================
                # Para todos los destinos_del_num_viaje_a_reubicar en el viaje
                # =============================================================================================================
                cuenta2 = 0
                reubique = False
                for pedidos_a_reubicar in destinos_del_num_viaje_a_reubicar:
                    if reubique: break ;
                    cuenta2 += 1
                    self.b('  {}.{}. Destino a reubicar {} '.format(cont, cuenta2, pedidos_a_reubicar))
                    # ***************************************************
                    pedidos_del_destino_a_reubicar = self.R.loc[
                        (self.R.Viaje == num_viaje_a_reubicar) & (self.R.DestinoTR1 == pedidos_a_reubicar)].copy()
                    # itero RESTO agrupado por DestinoTR1 y busco que este cercano
                    destinos_resto = RESTO.index.unique()
                    cuenta3 = 0
                    bYaPinte = False
                    if not destinos_resto.empty:
                        for destino_resto in destinos_resto:
                            cuenta3 += 1
                            # ----------------------------------------------------------------------
                            # trae la distancia y el tiempo de arcos de este destino_resto
                            xx = self.A.loc[
                                ((self.A.ORIGEN == pedidos_a_reubicar) & (self.A.DESTINO == destino_resto)), ['DISTANCIA',
                                                                                                              'TIEMPO']].values
                            config.distancia_al_destino = int(xx[0][0])

                            if (config.distancia_al_destino > self.cercania):
                                if not bYaPinte:
                                    self.b('      NO hay Viaje cercano para reubicar a {} km'.format(self.cercania / 1000))
                                    bYaPinte = True
                                break
                            bEncontreCercano = True
                            config.tiempo_google_al_destino = int(xx[0][1])
                            # ----------------------------------------------------------------------
                            # busca el viaje que tenga este destino_resto
                            viajes_que_contienen_destino_resto = RESTO[RESTO.index == destino_resto].Viaje.unique()
                            if len(viajes_que_contienen_destino_resto) > 0:
                                bYaPinte = True
                                for num_viaje in viajes_que_contienen_destino_resto:
                                    self.b('    {}.{}.{}. Viaje al que quiero reubicar {} que contiene a {} (distancia {} km)'.format(cont, cuenta2, cuenta3,
                                                                                                  num_viaje, destino_resto, str(config.distancia_al_destino/1000)))
                                    pedidos_al_viaje = self.R.loc[self.R.Viaje == num_viaje]
                                    esta_a_seg = int(xx[0][1])

                                    if self.cumple_6_restricciones(pedidos_del_destino_a_reubicar, pedidos_al_viaje, esta_a_seg):
                                        # ------------------------------------------------------------------------
                                        # cambia los datos del NUEVO VIAJE
                                        pedidos_del_destino_a_reubicar.Tirada = 99
                                        pedidos_del_destino_a_reubicar.Viaje = num_viaje
                                        pedidos_del_destino_a_reubicar.TipoVehiculo = pedidos_al_viaje.iloc[0].TipoVehiculo
                                        pedidos_del_destino_a_reubicar.VolumenMax = pedidos_al_viaje.iloc[0].VolumenMax
                                        pedidos_del_destino_a_reubicar.PesoMax = pedidos_al_viaje.iloc[0].PesoMax
                                        pedidos_del_destino_a_reubicar.ValorMax = pedidos_al_viaje.iloc[0].ValorMax
                                        # ------------------------------------------------------------------------
                                        if (not pedidos_del_destino_a_reubicar['ModificadoVacios'].values.any() == 1):
                                            self.contBitacora_cambios += 1

                                        pedidos_del_destino_a_reubicar['ModificadoVacios'] = 1
                                        self.b('            ****** REUBICADO al Viaje {} ******'.format(num_viaje))
                                        config.contadorReubicados += 1
                                        reubique = True
                                        break

                                    else:
                                        pass
                                        #self.b('            No cumple Restricc. Pedidos a reubicar{} al target {}'.format(pedidos_a_reubicar, destino_resto))
                            else:
                                self.b('...No hay viajes cercanos en plan')
                    else:
                        self.b('...Último Viaje, no hay más viajes para incorporar')

                    # copio

                    OPTIMIZADO = pd.concat([OPTIMIZADO, pedidos_del_destino_a_reubicar])
            # ----------------------------------------------------
            # quita de RESTO el viaje vacio
            RESTO = RESTO.loc[RESTO.Viaje != num_viaje_a_reubicar]
            # copio los que NO estaban vacios
            RESTO.reset_index(inplace=True)
            OPTIMIZADO = pd.concat([RESTO, OPTIMIZADO])
            #=====================================================
            # rutina FINAL
            informa_el_progreso.emit({'progressBar': 91, 'lblMensaje': 'Rutina final...'})
            OPTIMIZADO = self.rutina_final(OPTIMIZADO)
            informa_el_progreso.emit({'progressBar': 95, 'lblMensaje': 'Bitácora...'})
            #=====================================================
            # BITACORA. proceso de guardado en bitacora
            self.b('F I N')
            self.b('******************************')
            x = fnGuardaBitacora('Optimiza Vacíos')
            # CSV
            ahora = str(datetime.now())
            OPTIMIZADO.to_csv('/Users/ARMS/optimiza_vacios ' + ahora + '.csv')
            #para config
            config.dfOptimizadoAuto = OPTIMIZADO
            # SS. GUARDA
            # wks = config.sheet.worksheet_by_title('RES-OPTIMIZADO')
            # wks.set_dataframe(OPTIMIZADO, (5, 1), fit=True, copy_head=True) # lo cambie a true
            #=====================================================
            # limpia memoria
            del viajes_vacios
            del RESTO
            del OPTIMIZADO
            gc.collect()
            informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Terminado...'})
            return True, 0, 0
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False

    def optimiza_cercanos(self, informa_el_progreso):
        try:
            print('optimiza_cercanos')
            if not (self.rutina_inicial()):
                fnGuardaBitacora('Optimiza Cercanos')
                return False
            config.contadorReubicados = 0
            # ----------------------------------------------------
            x = 'PARAMETROS: %Ocupación menor al {:0,.0f}%; Cercanía:{:0,.0f}km; Aumenta Vol:{:0,.0f}%; Aumenta Peso:{:0,.0f}%, Aumenta Valor:{:0,.0f} pesos'.format(
                self.OcupacionMenorA, self.cercania / 1000, self.volumen, self.peso, self.valor)
            y = ' Ignora Acceso:{:0,.0f}; Ignora Mat.Compatibilidad:{:0,.0f}; Aumenta Ventana: {:0,.0f} mins'.format(
                self.ignoraAcceso, self.ignoraMatriz, self.ventana)
            self.b(x + y)
            # ----------------------------------------------------
            # inicializa DATA FRAMES
            self.dfOptimizado = None
            OPTIMIZADO = self.R.copy()
            OPTIMIZADO = OPTIMIZADO[0:0]
            # creo un DF con todos los viajes y destinos de R
            viajes_itera = self.R.groupby(['Viaje', 'DestinoTR1']).Pedido.count()

            # itera todos los Viajes-Destinos
            contador1 = 0
            for v, d in viajes_itera.index:
                contador1 += 1
                self.b('  {}. Viaje [{}] / Destino [{}] '.format(contador1, v, d))

                # trae todas las distancias menores a 100km ya en ORDEN menor a mayor distancia
                dist = self.A.loc[(self.A.ORIGEN == d) & (self.A.DISTANCIA > 0) & (self.A.DISTANCIA < self.cercania / 1000),
                                  ['DESTINO', 'DISTANCIA', 'TIEMPO']].sort_values('DISTANCIA')
                # ----------------------------------------------------
                informa_el_progreso.emit({'progressBar': 10 + (contador1 / viajes_itera.shape[0] * 80), 'lblMensaje': 'Reubicando...'+str(contador1)+'/'+str((viajes_itera.shape[0]))})
                # ----------------------------------------------------

                # haciendo merge entre R y posibles dist y agrupando por viaje
                viajes_target = self.R.merge(dist, left_on='DestinoTR1', right_on='DESTINO', how='inner')

                # elimina los posibles destinos del viaje propio que esta en v
                # viajes_target = viajes_target[~(viajes_target.Viaje == v)]

                # agrupa por viaje - destino
                target = viajes_target.groupby(['Viaje', 'DestinoTR1']).DISTANCIA.mean().sort_values()

                # trata de jalar todos estos destinos a mi camion
                contador2 = 0
                for v1, d1 in target.index:
                    # salte si encontraste una pareja cercana dentro del mismo camion
                    contador2 += 1
                    esta_a_km = \
                        viajes_target.loc[
                            (viajes_target.Viaje == v1) & (viajes_target.DestinoTR1 == d1)].DISTANCIA.values[
                            0] / 1000
                    esta_a_seg = \
                        viajes_target.loc[(viajes_target.Viaje == v1) & (viajes_target.DestinoTR1 == d1)].TIEMPO.values[
                            0]
                    self.b('    {}.{}. Viaje {}. [{} / en viaje *** {} ***] al {}... a {} km aprox'.format(contador1, contador2, v, d1, v1, v, esta_a_km))

                    if v == v1:
                        self.b('      ......Va en el Viaje Correcto, con su pareja más cercana')
                        break

                        # listo para ver si cabe en el VIAJE

                    # trae los pedidos del destino y los pedidos del VIAJE al que quiero reubicar
                    pedidos_a_reubicar = self.R.loc[(self.R.Viaje == v1) & (self.R.DestinoTR1 == d1)]
                    pedidos_al_viaje = self.R.loc[(self.R.Viaje == v)]

                    bCumpleTodasRestriccciones = self.cumple_6_restricciones(pedidos_a_reubicar, pedidos_al_viaje,
                                                                             esta_a_seg)
                    if bCumpleTodasRestriccciones:
                        self.reubicalo(pedidos_a_reubicar, pedidos_al_viaje)
                        self.b('            ****** Viaje {} con destino {} REUBICADO al Viaje {} ******'.format(v1, d1, v))
                        config.contadorReubicados += 1
            # ----------------------------------------------------
            informa_el_progreso.emit({'progressBar': 91, 'lblMensaje': 'Rutina final...'})
            # ----------------------------------------------------
            self.R = self.rutina_final(self.R)
            # ----------------------------------------------------
            informa_el_progreso.emit({'progressBar': 95, 'lblMensaje': 'Rutina Bitácora...'})
            # ----------------------------------------------------
            #=====================================================
            # BITACORA. proceso de guardado en bitacora
            self.b('F I N')
            self.b('******************************')
            fnGuardaBitacora('Optimiza Cercanos')
            # CSV
            ahora = str(datetime.now())
            self.R.to_csv('/Users/ARMS/optimiza_cercanos ' + ahora + '.csv')
            #para config
            config.dfOptimizadoAuto = self.R
            # SS. GUARDA
            # wks = config.sheet.worksheet_by_title('RES-OPTIMIZADO')
            # wks.set_dataframe(OPTIMIZADO, (5, 1), fit=True, copy_head=True) # lo cambie a true
            #=====================================================
            informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Finalizado...'})

            # limpia memoria
            del OPTIMIZADO
            del self.R
            gc.collect()
            return True, 0, 0
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # self.b('**** ERROR **** {}'.format(error))
            print(error)
            return False
