from Base.GeneradorRutas.Datos.Planes import *
import sys
import os
import pandas as pd
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import traceback
import datetime
from IO.Apis.ordenar_datos import *

import config
from Base.GeneradorRutas.Datos.Planes import *
from IO.Apis.calculate_metrics import calculate_metrics


# ---- otros -----------------------------------------------------------

def fnPropaga(ui, df=False, solo_df=False, grid=False):
    try:
        print('propaga...')
        # obten los datos del grid seleciconado en un df
        if not solo_df:
            df = fnGetGrid(grid)
        # si no existe el grid se toman los datos sólo del df
        config.dfPlan = df
        # remplazo los otros df
        config.dfOptimizadoAuto = df
        config.dfPredespegue = df
        config.dfAsignador = df
        config.dfOptimizadoManual = df
        # lleno todos los grids y tableros
        fnPutGridTablero(ui)

    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnGuardarResultados(df):
    try:
        pass
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnPutGridTablero(ui):
    try:
        # el Plan y sus copias
        fnPutGrid(config.dfPlan, ui.gridElPlan, config.campos_ssPlan)
        # otros
        fnPutGrid(config.dfVehiculos, ui.gridAsignarVehPredespegue, config.campos_ssAsignarVehiculos)
        fnPutGrid(config.dfOperadores, ui.gridAsignarOperadoresPredespegue, config.campos_ssAsignarOperadores)
        # SE MUESTRAN LOS DATOS EN LOS TABLEROS
        config.TableroOriginal = calculate_metrics(config.dfPlan)
        fnPutTablero(ui.tableroPlan, solo_original=True)
        # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnGuardaPlan():
    try:
        print('Guarda el Plan...')
        # ordena el resultado
        config.dfPlan = config.dfPlan[config.campos_ssOriginal]
        # config.sheet = config.gcPlan.open(config.nombrePlan)
        # Si se le cambia el nombre con el que se va a escribir el ss debe clonar el existente y sobre el escribir los resultados
        wks = config.sheet.worksheet_by_title('RESULTADOS')
        wks.set_dataframe(config.dfPlan, (5, 1), fit=True, copy_head=False)
        print('Plan guardado correctamente')
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True


# ---- colorea y pinta -----------------------------------------------------------

def fnColoreaViajes(grid):
    try:
        col = fnBuscaPosicionColEnTabla(grid, 'Viaje')
        fnPintaGridPorViaje(grid, col)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnColoreaDestinos(grid):
    try:
        col = fnBuscaPosicionColEnTabla(grid, 'DestinoTR1')
        fnPintaGridPorDestino(grid, col)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnColoreaSinColor(grid):
    try:
        fnPintaGridSinColor(grid)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnPintaGridPorViaje(grid, col):
    try:
        if col == -1: return
        dato = 0
        color = QColor(220,220,55)
        for i in range(grid.rowCount()):
            if dato != int(grid.item(i, col).text()):
                dato = int(grid.item(i, col).text())
                color = QColor(220,220,55) if color == QColor(255,255,255) else QColor(255,255,255)

            fnPintaUnRow(grid, i, color)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True

def fnPintaGridPorDestino(grid, col):
    try:
        if col == -1: return
        dato = ''
        color = QColor(106, 193, 140)
        for i in range(grid.rowCount()):
            if dato != grid.item(i, col).text():
                dato = grid.item(i, col).text()
                color = QColor(106, 193, 140) if color == QColor(255,255,255) else QColor(255,255,255)

            fnPintaUnRow(grid, i, color)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True

def fnPintaGridSinColor(grid):
    try:
        for i in range(grid.rowCount()):
            fnPintaUnRow(grid, i, QColor(255,255,255))
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True

def fnPintaUnRow(grid, rowIndex, color):
    try:
        for col in range(grid.columnCount()):
            grid.item(rowIndex, col).setForeground(color)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True


# ---- tableros -----------------------------------------------------------

def fnPutTablero(grid, solo_original=False):
    try:
        # tercer paramettro menos es mejor = -1, mas es mejor=1
        tablero = ['Costo Pieza', '{:0,.3f}', -1], ['Costo Peso', '{:0,.3f}', -1], \
                  ['Costo Volumen', '{:0,.3f}', -1], \
                  ['Costo Valor', '{:0,.3f}', -1], ['Costo Viaje', '{:0,.3f}', -1], ['Costo Total', '{:0,.3f}', -1], \
                  ['Total Km', '{:0,.3f}', -1], ['Total Co2', '{:0,.3f}', -1], \
                  ['OCUPACIONVOLUMEN/Viaje', '{:0,.3f}', 1], \
                  ['OCUPACIONPESO/Viaje', '{:0,.3f}', 1], ['Total Pedidos', '{:0,.3f}', -1], \
                  ['Total Destinos', '{:0,.3f}', -1], \
                  ['Total Viajes', '{:0,.3f}', -1], ['Total m3', '{:0,.3f}', -1], ['Total Kg', '{:0,.3f}', -1], \
                  ['Total Valor', '{:0,.3f}', -1], ['Total Tiempo', '{:0,.3f}', -1], ['Costo Km', '{:0,.3f}', -1], \
                  ['Costo Co2', '{:0,.3f}', -1], ['Costo Hr', '{:0,.3f}', -1], \
                  ['Piezas/Viaje', '{:0,.3f}', 1], ['m3/Viaje', '{:0,.3f}', 1], ['Kg/Viaje', '{:0,.3f}', 1], \
                  ['Valor/Viaje', '{:0,.3f}', 1], ['Km/Viaje', '{:0,.3f}', -1], ['Co2/Viaje', '{:0,.3f}', -1], \
                  ['Hr/Viaje', '{:0,.3f}', -1], ['No Respeto Ventana', '{:0,.3f}', 1], \
                  ['No Respeto Volumen', '{:0,.3f}', 1], \
                  ['No Respeto Peso', '{:0,.3f}', 1], ['No Respeto Valor', '{:0,.3f}', 1]

        col = 0
        for dato in tablero:
            # ORIGINAL
            s = dato[1].format(float(config.TableroOriginal[dato[0]]))
            item = QTableWidgetItem(s)
            #item.setTextAlignment(Qt.AlignCenter)
            #item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            grid.setItem(0, col, item)

            # solo el primer renglon
            if not solo_original:
                # OPTIMIZADO
                s = dato[1].format(float(config.TableroOptimizado[dato[0]]))
                item = QTableWidgetItem(s)
                #item.setTextAlignment(Qt.AlignCenter)
                #item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                grid.setItem(1, col, item)
            else:
                # vuelve a replicar el ORIGINAL
                s = dato[1].format(float(config.TableroOriginal[dato[0]]))
                item = QTableWidgetItem(s)
                #item.setTextAlignment(Qt.AlignCenter)
                #item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                grid.setItem(1, col, item)

            if not solo_original:
                # la diferencia
                # checa que traigan numeros sino ponle cero
                try:
                    op = float(config.TableroOptimizado[dato[0]])
                except:
                    op = 0.0

                try:
                    ori = float(config.TableroOriginal[dato[0]])
                except:
                    ori = 0.0

                dif = float(op) - float(ori)
                s = dato[1].format(dif)
                item = QTableWidgetItem(s)
                #item.setTextAlignment(Qt.AlignCenter)
                #item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                # mas es mejor, pinta color texto "menor" en verde
                if dato[2] == 1:
                    if dif < 0:
                        item.setForeground(QColor(222, 0, 0))
                    elif dif > 0:
                        item.setForeground(QColor(0, 222, 0))
                    grid.setItem(2, col, item)
                else:
                    if dif > 0:
                        item.setForeground(QColor(222, 0, 0))
                    elif dif < 0:
                        item.setForeground(QColor(0, 222, 0))
                    grid.setItem(2, col, item)
            col += 1
        grid.repaint()

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)

def fnPutTableritos(ui):
    try:

        # Indicadores para "EL PLAN"
        ui.lblTableroElPlanHeaderPedidos.setText('Pedidos\n{:0,.0f} '.format(config.TableroOriginal["Total Pedidos"]))
        ui.lblTableroElPlanHeaderDestinos.setText('Destinos\n{:0,.0f} '.format(config.TableroOriginal["Total Destinos"]))
        ui.lblTableroElPlanHeaderKg.setText('Kg\n{:0,.0f}'.format(config.TableroOriginal["Total Kg"]))
        ui.lblTableroElPlanHeaderPiezas.setText('Piezas\n{:0,.0f}'.format(config.TableroOriginal["Piezas"]))
        ui.lblTableroElPlanHeaderM3.setText('M3\n{:0,.2f}'.format(config.TableroOriginal["Total m3"]))

        ui.lblTableroElPlanHeaderViajes.setText('Viajes\n{:0,.0f}'.format(config.TableroOriginal["Total Viajes"]))
        ui.lblTableroElPlanHeaderCostoTotal.setText('$ Total\n{:0,.0f}'.format(config.TableroOriginal["Costo Total"]))
        ui.lblTableroElPlanHeaderKm.setText('Km\n{:0,.0f}'.format(config.TableroOriginal["Total Km"]))

        ui.lblTableroElPlanHeaderCostoViaje.setText('Ocup.M3\n{:0,.0f}%'.format(config.TableroOriginal["OCUPACIONVOLUMEN/Viaje"]))
        ui.lblTableroElPlanHeaderCostoDestino.setText('Ocup.Kg\n{:0,.0f}%'.format(config.TableroOriginal["OCUPACIONPESO/Viaje"]))
        ui.lblTableroElPlanHeaderCostoPedido.setText('$/Viaje\n{:0,.0f}'.format(config.TableroOriginal["Costo Viaje"]))
        ui.lblTableroElPlanHeaderCostoPiezas.setText('$/Pieza\n{:0,.2f}'.format(config.TableroOriginal["Costo Pieza"]))
        ui.lblTableroElPlanHeaderCostoKg.setText('$/Kg\n{:0,.2f}'.format(config.TableroOriginal["Costo Peso"]))
        ui.lblTableroElPlanHeaderCostoM3.setText('$/M3\n{:0,.2f}'.format(config.TableroOriginal["Costo Volumen"]))

        # Indicadores para el MOTOR
        ui.lblMotorPedidos.setText('Pedidos\n{:0,.0f} '.format(config.TableroOriginal["Total Pedidos"]))
        ui.lblMotorDestinos.setText('Destinos\n{:0,.0f} '.format(config.TableroOriginal["Total Destinos"]))
        ui.lblMotorKg.setText('Kg\n{:0,.0f} '.format(config.TableroOriginal["Total Kg"]))
        ui.lblMotorPiezas.setText('Piezas\n{:0,.0f} '.format(config.TableroOriginal["Piezas"]))
        ui.lblMotorM3.setText('M3\n{:0,.2f} '.format(config.TableroOriginal["Total m3"]))

        # Indicadores para "AUTO"
        ui.lblTableroAutoHeaderPedidos.setText('Pedidos\n{:0,.0f} '.format(config.TableroOriginal["Total Pedidos"]))
        ui.lblTableroAutoHeaderDestinos.setText('Destinos\n{:0,.0f} '.format(config.TableroOriginal["Total Destinos"]))
        ui.lblTableroAutoHeaderKg.setText('Kg\n{:0,.0f} '.format(config.TableroOriginal["Total Kg"]))
        ui.lblTableroAutoHeaderPiezas.setText('Piezas\n{:0,.0f} '.format(config.TableroOriginal["Piezas"]))
        ui.lblTableroAutoHeaderM3.setText('M3\n{:0,.2f} '.format(config.TableroOriginal["Total m3"]))


        # # --------- VALOR 000
        # # busco posicion de las columnas
        # col = fnBuscaPosicionColEnTabla(ui.tableroPlan, 'Valor')
        # # obtengo la suma
        # diferencia = fnSumaDiferenciaTablerito(ui.tableroPlan, col, config.valorMax_plan)
        # #pinta el resultado
        # ui.lbl_valor_plan.setText('{:0,.2f}'.format(diferencia/1000.0))
        # if diferencia<0:
        #     ui.lbl_valor_plan.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        # else:
        #     ui.lbl_valor_plan.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")
        #
        # # --------- Volumen
        # # busco posicion de las columnas
        # col = fnBuscaPosicionColEnTabla(ui.tableroPlan, 'Volumen')
        # # obtengo la suma
        # diferencia = fnSumaDiferenciaTablerito(ui.tableroPlan, col, config.volumenMax_plan)
        # #pinta el resultado
        # ui.lbl_m3_plan.setText('{:0,.2f}'.format(diferencia/1000.0))
        # if diferencia<0:
        #     ui.lbl_m3_plan.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        # else:
        #     ui.lbl_m3_plan.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")
        #
        # # --------- Peso
        # # busco posicion de las columnas
        # col = fnBuscaPosicionColEnTabla(ui.tableroPlan, 'Peso')
        # # obtengo la suma
        # diferencia = fnSumaDiferenciaTablerito(ui.tableroPlan, col, config.pesoMax_plan)
        # #pinta el resultado
        # ui.lbl_kg_plan.setText('{:0,.2f}'.format(diferencia/1000.0))
        # if diferencia<0:
        #     ui.lbl_kg_plan.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        # else:
        #     ui.lbl_kg_plan.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")
        # print('slot_manual_tablerito_plan')
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnPutTableritoV1(ui):
    try:
        # --------- VALOR 000
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV1, 'Valor')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV1, col, config.valorMax_v1)
        #pinta el resultado
        ui.lbl_valor_v1.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_valor_v1.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_valor_v1.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")

        # --------- Volumen
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV1, 'Volumen')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV1, col, config.volumenMax_v1)
        #pinta el resultado
        ui.lbl_m3_v1.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_m3_v1.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_m3_v1.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")

        # --------- Peso
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV1, 'Peso')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV1, col, config.pesoMax_v1)
        #pinta el resultado
        ui.lbl_kg_v1.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_kg_v1.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_kg_v1.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")
        print('slot_manual_tablerito_v1')
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnPutTableritoV2(ui):
    try:
        # --------- VALOR 000
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV2, 'Valor')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV2, col, config.valorMax_v2)
        #pinta el resultado
        ui.lbl_valor_v2.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_valor_v2.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_valor_v2.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")

        # --------- Volumen
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV2, 'Volumen')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV2, col, config.volumenMax_v2)
        #pinta el resultado
        ui.lbl_m3_v2.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_m3_v2.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_m3_v2.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")

        # --------- Peso
        # busco posicion de las columnas
        col = fnBuscaPosicionColEnTabla(ui.tablaV2, 'Peso')
        # obtengo la suma
        diferencia = fnSumaDiferenciaTablerito(ui.tablaV2, col, config.pesoMax_v2)
        #pinta el resultado
        ui.lbl_kg_v2.setText('{:0,.2f}'.format(diferencia/1000.0))
        if diferencia<0:
            ui.lbl_kg_v2.setStyleSheet("color:rgb(255, 164, 163); font: 10pt 'Arial Narrow';")
        else:
            ui.lbl_kg_v2.setStyleSheet("color:rgb(175, 255, 166); font: 10pt 'Arial Narrow';")
        print('slot_manual_tablerito_v2')
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return True

def fnSumaDiferenciaTablerito(grid, col, maximo):
    try:
        suma = 0.0
        for row in range(grid.rowCount()):
            suma += float(grid.item(row, col).text())
        return float(maximo) - suma
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True

def fnLimpiaTableros():
    print('limpio tableros')

    #limpia tableros
    for row in range(config.mainWindow.tableroPlan.rowCount()):
        for col in range(config.mainWindow.tableroPlan.columnCount()):
            config.mainWindow.tableroPlan.setItem(row, col, QTableWidgetItem(''))

def fnLimpiaTableritos():
    print('limpio tableritos')

    #limpia tableritos
    config.mainWindow.lblV1Kg.setText('Kg')
    config.mainWindow.lblV1M3.setText('m3')
    config.mainWindow.lblV1Valor.setText('$')
    config.mainWindow.lblV1Hrs.setText('Hrs')
    config.mainWindow.lblV1Acceso.setText('Acceso')
    config.mainWindow.lblV1Comp.setText('M. Comp')

    config.mainWindow.lblV2Kg.setText('Kg')
    config.mainWindow.lblV2M3.setText('m3')
    config.mainWindow.lblV2Valor.setText('$')
    config.mainWindow.lblV2Hrs.setText('Hrs')
    config.mainWindow.lblV2Acceso.setText('Acceso')
    config.mainWindow.lblV2Comp.setText('M. Comp')

    config.mainWindow.editV1MaxKg.setText('12000')
    config.mainWindow.editV1MaxM3.setText('12000')
    config.mainWindow.editV1MaxValor.setText('12000')
    config.mainWindow.editV1Tipo.setText('Thorton')
    config.mainWindow.editV1NumViaje.setText('0')

    config.mainWindow.editV2MaxKg.setText('12000')
    config.mainWindow.editV2MaxM3.setText('12000')
    config.mainWindow.editV2MaxValor.setText('12000')
    config.mainWindow.editV2Tipo.setText('Thorton')
    config.mainWindow.editV2NumViaje.setText('0')

def fnLimpiaGrids():
    print('limpio grids')

    #grids
    config.mainWindow.gridElPlan.setRowCount(0)
    config.mainWindow.gridAsignar.setRowCount(0)
    config.mainWindow.gridPlanManual.setRowCount(0)
    config.mainWindow.gridV1Manual.setRowCount(0)
    config.mainWindow.gridV2Manual.setRowCount(0)

    config.mainWindow.gridElPlan.setColumnCount(0)
    config.mainWindow.gridAsignar.setColumnCount(0)
    config.mainWindow.gridPlanManual.setColumnCount(0)
    config.mainWindow.gridV1Manual.setColumnCount(0)
    config.mainWindow.gridV2Manual.setColumnCount(0)

    config.mainWindow.gridElPlan.repaint()
    config.mainWindow.gridAsignar.repaint()
    config.mainWindow.gridPlanManual.repaint()
    config.mainWindow.gridV1Manual.repaint()
    config.mainWindow.gridV2Manual.repaint()



# ---- get & put -----------------------------------------------------------

def fnGuardaBitacora(mensaje):
        try:
            d = str(datetime.datetime.now())
            print('Guardando bitácora de los resultados de optimización...{} - {}'.format(mensaje, d))
            # bitacora a csv
            config.B = pd.DataFrame(config.bitacora, columns=['Hora', 'BITACORA'])
            # config.B.to_csv('/Users/ARMS/optimiza_vacios_bitacora ' + d + '.csv')

            # grid
            if mensaje == 'Optimiza Vacíos':
                grid = config.mainWindow.gridBitacoraVacios
            else:
                grid = config.mainWindow.gridBitacoraCercanos
            row = 0

            # carga al grid bitacora
            #dicc =  config.B.to_dict()
            print(33)
            fnPutGrid(config.B, grid, ordenCampos=['Hora', 'BITACORA'])
            print(331)
            return True
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            return True

def fnBuscaPosicionColEnTabla(grid, nombre_col):
    try:
        col=0
        for i in range(grid.columnCount()):
            headertext = grid.horizontalHeaderItem(i).text()
            if headertext == nombre_col:
                return col
            col +=1
        return -1
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True

def fnPutGrid(df, grid, ordenCampos):
    try:
        print('fnPutGrid')
        grid.setRowCount(0)

        df.reset_index(drop=True, inplace=True)
        grid.setSortingEnabled(False)
        # ordena el df
        df = df[ordenCampos]
        print(len(ordenCampos), df.shape)
      
        # dimension de la tabla y borra antes
        grid.setRowCount(df.shape[0])        
        grid.setColumnCount(df.shape[1])        
        # headers de las cols
        grid.setHorizontalHeaderLabels(ordenCampos)        
        grid.setSortingEnabled(False) 
        # mete celda x celda
      
        for i in range(df.shape[0]):
            for j in range(df.shape[1]):
                s = str(df.iloc[i, j])
                grid.setItem(i, j, QTableWidgetItem(s))
       
        # # finales para el grid
        #grid.resizeColumnsToContents()
        # # grid.resizeRowsToContents()
        grid.setSortingEnabled(True)
        # # repinta el GRID
        grid.repaint()
        # print("______")

    #----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        #config.b('**** ERROR **** {}'.format(error))
        print(error)
        return False, 0, 0

def fnGetGrid(grid):
    try:
        # los headers
        header = []
        for col in range(grid.columnCount()):
            header.append(grid.horizontalHeaderItem(col).text())
        # saca datos
        rows = []
        for i in range(grid.rowCount()):
             row = []
             for j in range(grid.columnCount()):
                 try:
                     row.append(grid.item(i, j).text())
                 except Exception as e:
                     print('*err fnGetGrid {} {} - {}'.format(i, j, e))
                     row.append('*err fnGetGrid {} {} - {}'.format(i, j, e))
             rows.append(row)
        return pd.DataFrame.from_records(rows, columns=header)
    # ----------------------------------------------------
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
        return False

def fnOrdenarResultados(dfModificado, completar=False):
    if completar:
        df = pd.DataFrame(columns=config.campos_ssOriginal)
        dfCompleto = pd.merge(df, dfModificado, how='outer')
    else: dfCompleto = dfModificado
    dfCompleto = dfCompleto[config.campos_ssOriginal]

    return dfCompleto

