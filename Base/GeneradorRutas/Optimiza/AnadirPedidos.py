import os
import sys
import json
import config
from IO.Apis.Db import DB
from Base.GeneradorRutas.Datos.Datos import Datos
from database_services.dynamodbmethods import DynamoDBMethods
from IO.Apis.spread_sheet import SpreadSheet
from IO.Apis.GDrive import GDrive
import pandas as pd


class AnadirPedidos:
    def __init__(self, ui):
        self.ui = ui
        self.pedidos_unlocked = False
        self.predespegue_unlocked = False

    def init_components(self):
        self.carga_tablas_spreadsheets()
        self.init_listeners()

    def carga_tablas_spreadsheets(self):
        try:
            db = DB()
            sp = Datos(self.ui)
            # Cargando list de pedidos
            datos = sp.obtenerLista("pedidos")
            for dato in datos:
                self.ui.listWidgetAnadePedidos.addItem(dato["name"])
            # Cargando list de predespegue
            predespegados = db.obtenerViajes(10, 7, config.usuario["cedi"])
            aux = predespegados["Data"]
            aux = json.loads(aux)
            viajes = aux['viajes']
            for viaje in viajes:
                self.ui.listWidgetAnadePredespegue_2.addItem(str(viaje["Viaje"]))
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def init_listeners(self):
        self.ui.pushButtonAnadePedidosQuitaPredespegue_2.clicked.connect(self.quitar_de_resultados)
        self.ui.pushButtonAnadePedidosAnadeResultados_2.clicked.connect(self.anadir_a_resultados)
        self.ui.listWidgetAnadePedidos.itemClicked.connect(self.unlock_list_pedidos)
        self.ui.listWidgetAnadePredespegue_2.itemClicked.connect(self.unlock_list_predespegue)

    def quitar_de_resultados(self):
        if self.pedidos_unlocked:
            item_to_remove = self.ui.listWidgetAnadePedidos.currentItem().text()
            pedidos = self.get_pedidos_de_spreadsheet(item_to_remove)
        if self.predespegue_unlocked:
            item_to_remove = self.ui.listWidgetAnadePredespegue_2.currentItem().text()
            pedidos = self.get_pedidos_de_predespegue(item_to_remove)
        print("Shape original de pedidos")
        print(pedidos.shape)
        print("Shape original de resultados")
        print(config.dfResultados.shape)
        pedidos = pedidos["Pedido"].values
        config.dfResultados = config.dfResultados[~config.dfResultados["Pedido"].isin(pedidos)]
        print("Shape final de resultados")
        print(config.dfResultados.shape)

    def anadir_a_resultados(self):
        if self.pedidos_unlocked:
            item_to_add = self.ui.listWidgetAnadePedidos.currentItem().text()
            pedidos = self.get_pedidos_de_spreadsheet(item_to_add)
        if self.predespegue_unlocked:
            item_to_add = self.ui.listWidgetAnadePredespegue_2.currentItem().text()
            pedidos = self.get_pedidos_de_predespegue(item_to_add)
        print("Shape original de pedidos")
        print(pedidos.shape)
        print("Shape original de resultados")
        print(config.dfResultados.shape)
        aux = config.dfResultados.append(pedidos)
        print("Shape de resultados despues de append de pedidos")
        print(aux.shape)
        config.dfResultados = aux
        print("Shape de resultados final")
        print(config.dfResultados.shape)
        config.dfResultados = config.dfResultados.fillna(0)

    def get_pedidos_de_spreadsheet(self, nombre_ss):
        print("Obteniendo pedidos del spreadsheet " + nombre_ss)
        ss = SpreadSheet()
        data = ss.abrirSS(nombre_ss, "PEDIDOS", "nombre")
        data = data[2]
        return data

    def get_pedidos_de_predespegue(self, viaje):
        print("Obteniendo pedidos del viaje predespegado: " + viaje)
        db = DB()
        data = db.getViaje(viaje)
        pedidos = list()
        isFirst = True
        dfPedidos = pd.DataFrame({}, index=[0])
        if data[0] == True:
            print("Viaje encontrado")
            viajes = json.loads(data[1][0][1])
            for viaje in viajes["viajes"]:
                for destino in viaje["destinos"]:
                    for pedido in destino["pedidos"]:
                        aux = pd.DataFrame([pedido])
                        dfPedidos = pd.concat([dfPedidos, aux])

            return dfPedidos


        else:
            print("Error al obtener el viaje predespegado")
            return pd.DataFrame({})

    def unlock_list_predespegue(self):
        print("Seleccionando predespegue")
        self.pedidos_unlocked = False
        self.predespegue_unlocked = True

    def unlock_list_pedidos(self):
        print("Seleccionando pedidos")
        self.pedidos_unlocked = True
        self.predespegue_unlocked = False


def llena_lista_pedidos(ui):

    ui.listAnadirPedidos.clear()

    for index in range(0, ui.listGenerarPlanPedidos.count()):
        ui.listAnadirPedidos.addItem(ui.listGenerarPlanPedidos.item(index).text())


def anade_pedidos(nombre_ss):

    get_pedidos_de_spreadsheet(nombre_ss)

    print(config.dfPlan['Pedido'])

    config.dfAnadirPedidos = config.dfAnadirPedidos.drop('CEDISalida', 1)
    # config.dfPlan.append(config.dfAnadirPedidos, ignore_index=True)
    config.dfPlan = pd.concat([config.dfPlan, config.dfAnadirPedidos], ignore_index=True)

    # config.dfPlan.drop_duplicates(['Pedido'], keep='first', inplace=True)
    # config.dfPlan = config.dfPlan.reset_index(drop=True)

    print(config.dfPlan['Pedido'])


def borra_pedidos(nombre_ss):

    get_pedidos_de_spreadsheet(nombre_ss)

    print(config.dfPlan.iloc[:, 0:2])
    print(config.dfAnadirPedidos.iloc[:, 0:2])

    idx1 = set(config.dfPlan.set_index(['Pedido']).index)
    idx2 = set(config.dfAnadirPedidos.set_index(['Pedido']).index)

    nuevos_pedidos = list(idx1 - idx2)

    config.dfPlan.set_index(['Pedido'], inplace=True, drop=False)
    config.dfPlan = config.dfPlan.loc[nuevos_pedidos]
    config.dfPlan.reset_index(drop=True, inplace=True)

    print(config.dfPlan.iloc[:, 0:2])


def get_pedidos_de_spreadsheet(nombre_ss):

    print("Obteniendo pedidos del spreadsheet " + nombre_ss)
    ss = SpreadSheet()
    config.dfAnadirPedidos = ss.abrirSS(nombre_ss=nombre_ss, nombre_wks="PEDIDOS")[2]



