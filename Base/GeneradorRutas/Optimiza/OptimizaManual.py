import config
import json

def verificaMedidas(df, vehiculo, ui):
    dfVehiculo = json.loads(df.to_json(orient='records'))
    
    # VALORES MAXIMOS
    maxKg = 0.0
    maxM3 = 0.0
    maxVal = 0.0
    if vehiculo == 1:
        maxKg = float(config.slot_editMaxKgManualV1)
        maxM3 = float(config.slot_editMaxM3ManualV1)
        maxVal = float(config.slot_editMaxValorManualV1)
    elif vehiculo == 2:
        maxKg = float(config.slot_editMaxKgManualV2)
        maxM3 = float(config.slot_editMaxM3ManualV2)
        maxVal = float(config.slot_editMaxValorManualV2)

    # SUMA DE VALORES POR UNIDAD
    sumaKg = 0.0
    sumaM3 = 0.0
    sumaVal = 0.0

    for pedido in dfVehiculo:
        sumaKg = sumaKg + float(pedido["Peso"])
        sumaM3 = sumaM3 + float(pedido["Volumen"])
        sumaVal = sumaVal + float(pedido["Valor"])

    # DIFERENCIA ENTRE VALORES
    difKg = maxKg - sumaKg
    difM3 = maxM3 - sumaM3
    difVal = maxVal - sumaVal

    if vehiculo == 1:
        ui.lblV1Kg.setText(str(difKg))
        if difKg >= 0:
            ui.lblV1Kg.setStyleSheet("background: green")
        else:
            ui.lblV1Kg.setStyleSheet("background: red")

        ui.lblV1M3.setText(str(difM3))
        if difM3 >= 0:
            ui.lblV1M3.setStyleSheet("background: green")
        else:
            ui.lblV1M3.setStyleSheet("background: red")

        ui.lblV1Valor.setText(str(difVal))
        if difVal >= 0:
            ui.lblV1Valor.setStyleSheet("background: green")
        else:
            ui.lblV1Valor.setStyleSheet("background: red")

    elif vehiculo == 2:
        ui.lblV2Kg.setText(str(difKg))
        if difKg >= 0:
            ui.lblV2Kg.setStyleSheet("background: green")
        else:
            ui.lblV2Kg.setStyleSheet("background: red")

        ui.lblV2M3.setText(str(difM3))
        if difM3 >= 0:
            ui.lblV2M3.setStyleSheet("background: green")
        else:
            ui.lblV2M3.setStyleSheet("background: red")

        ui.lblV2Valor.setText(str(difVal))
        if difVal >= 0:
            ui.lblV2Valor.setStyleSheet("background: green")
        else:
            ui.lblV2Valor.setStyleSheet("background: red")
    