import pymysql
import config

"""
    @desc: Clase para listar los ss en una lista
    @author: Raquel Antonio

"""
from PyQt5.QtWidgets import QListWidgetItem, QListWidget
from IO.Apis.spread_sheet import SpreadSheet
from database_services.dynamodbmethods import DynamoDBMethods
#sfrom IO.Apis.ordenar_datos import ordenar_resultados
from IO.Apis.consolidar_json import ConsolidarJson
from IO.Apis.spread_sheet import SpreadSheet
import requests
import json
import time
import os, sys
import config
from Base.GeneradorRutas.Datos.Planes import *
from IO.Apis.GDrive import GDrive
import pandas as pd
class Datos:
    def __init__(self, ui):
        self.ui = ui
        self.planes = Planes(self.ui)
        self.planes.cargaPlanesCreados()


        self.dbd = DynamoDBMethods('config.ini')
        self.spreadsheet = SpreadSheet()
        status, config.lista_ss = self.obtenerLista()

    def cargaDatos(self):
        self.cargaTablaPedidos()
        self.cargaTablaVehiculos()
        self.cargaTablaOperadores()
        self.cargaTablaCompatibilidad()
        self.cargaTablaDestinos()
        self.cargaTablaTarifario()
        self.cargaTablaPreferencias()

    def crearPlan(self, informa_el_progreso):
        try:

            if config.nombreGenerarPlan != '':

                if config.slot_listPedidos != None and config.slot_listCompatibilidad != None and config.slot_listDestinos != None and config.slot_listOperadores != None and config.slot_listPreferencias != None and config.slot_listTarifario != None and config.slot_listVeh != None:

                    ss = SpreadSheet()

                    GLOBALstatus = True

                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 5, 'lblMensaje': 'Abriendo PEDIDOS...'})
                        GLOBALstatus, duracion, dfPedidos = ss.abrirSS(nombre_ss=config.slot_listPedidos,nombre_wks="PEDIDOS", informa_el_progreso=informa_el_progreso, key=config.idPedido)

                    if GLOBALstatus:
                        '''SE COMPLETAN Y ORDENAN LOS VALORES DE LOS PEDIDOS A RESULTADOS'''
                        df = pd.DataFrame(columns=config.campos_ssOriginal)
                        dfCompleto = pd.merge(dfPedidos, df, how='outer')
                        dfPedidos = dfCompleto[config.campos_ssOriginal]
                        dfPedidos.fillna('', inplace=True)
                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 10, 'lblMensaje': 'Abriendo DESTINOS...'})
                        GLOBALstatus, duracion, dfDestinos = ss.abrirSS(nombre_ss=config.slot_listDestinos, nombre_wks="DESTINOS", encabezado=True, informa_el_progreso=informa_el_progreso, key=config.idDestino)

                    
                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 14, 'lblMensaje': 'Obteniendo CEDI...'})
                        #OBTENGO LOS DATOS DEL CEDI
                        cedi = dfDestinos.get_value(0, 'Colonia')
                        dfCedi = dfDestinos.loc[dfDestinos['Destino'] == cedi]
                        if dfCedi.shape[0] == 0:
                            GLOBALstatus = False
                            time.sleep(5)
                            informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'No se encontró el CEDI en los destinos'})
                        else:
                            #ELIMINO LAS PRIMERAS TRES FILAS DEL DF
                            dfDestinos.drop(dfDestinos.index[0:3], inplace=True)

                            informa_el_progreso.emit({'progressBar': 15, 'lblMensaje': 'Abriendo COMPATIBILIDAD...'})
                            GLOBALstatus, duracion, dfCompatibilidad = ss.abrirSS(nombre_ss=config.slot_listCompatibilidad, nombre_wks="COMPATIBILIDAD", informa_el_progreso=informa_el_progreso, key=config.idCompatibilidad)

                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 20, 'lblMensaje': 'Abriendo OPERADORES...'})
                        GLOBALstatus, duracion, dfOperadores = ss.abrirSS(nombre_ss=config.slot_listOperadores, nombre_wks="OPERADORES", informa_el_progreso=informa_el_progreso, key=config.idOperador)
                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 25, 'lblMensaje': 'Abriendo MATRIZ DE TARIFAS...'})
                        GLOBALstatus, duracion, dfTarifario = ss.abrirSS(nombre_ss=config.slot_listTarifario, nombre_wks="MATRIZ_TARIFAS", encabezado=True, informa_el_progreso=informa_el_progreso, key=config.idTarifario)
                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 30, 'lblMensaje': 'Abriendo VEHICULOS...'})
                        GLOBALstatus, duracion, dfVehiculos = ss.abrirSS(nombre_ss=config.slot_listVeh, nombre_wks="VEHICULOS", informa_el_progreso=informa_el_progreso, key=config.idVehiculo)
                    if GLOBALstatus:
                        informa_el_progreso.emit({'progressBar': 35, 'lblMensaje': 'Abriendo PREFERENCIAS...'})
                        GLOBALstatus, duracion, dfPreferencias = ss.abrirSS(nombre_ss=config.slot_listPreferencias, nombre_wks="Preferencias", informa_el_progreso=informa_el_progreso, key=config.idPreferencia)

                    

                    if GLOBALstatus:
                        #completar datos
                        cols = ['FechaEntregaPedido', 'FechaSalidaPedido']
                        dfPedidos[cols] = ["01/01/1980 07:00","01/01/1980 07:00"]
                        colsDestinos = ['TiemServicio','RestriccionVolumen']
                        dtColsFaltantes =dfDestinos[colsDestinos]
                        dfPedidos[colsDestinos]= dtColsFaltantes

                        #SE GUARDA EL NOMBRE DEL SS DEL PLAN

                        
                        informa_el_progreso.emit({'progressBar': 37, 'lblMensaje': 'Creando PLAN...'})

                        ss_target = self.spreadsheet.copiarPlantilla(config.usuario["folderResultados"])
                        if ss_target:
                            print("El spreadsheet se escribira en: " + ss_target)
                            res = ss.guarda_ss(dfPedidos,ss_target,"RESULTADOS", informa_el_progreso=informa_el_progreso)
                            informa_el_progreso.emit({'progressBar': 44, 'lblMensaje': 'Escribiendo RESULTADOS...'})
                            if res == False: res = ss.guarda_ss(dfPedidos, ss_target, "RESULTADOS")
                            print("Escritura de pedidos", res)
                            config.dfPlan = dfPedidos
                            informa_el_progreso.emit({'progressBar': 55, 'lblMensaje': 'Escribiendo DESTINOS...'})
                            res = ss.guarda_ss(dfDestinos, ss_target, "DESTINOS", informa_el_progreso=informa_el_progreso)
                            print("Escritura de destinos", res)
                            config.dfDestinos = dfDestinos
                            informa_el_progreso.emit({'progressBar': 65, 'lblMensaje': 'Escribiendo COMPATIBILIDADES...'})
                            res = ss.guarda_ss(dfCompatibilidad, ss_target, "COMPATIBILIDAD")
                            if res == False: res = ss.guarda_ss(dfCompatibilidad, ss_target, "COMPATIBILIDAD", informa_el_progreso=informa_el_progreso)
                            print("Escritura de compatibilidad", res)
                            config.dfCompatibilidad = dfCompatibilidad
                            informa_el_progreso.emit({'progressBar': 70, 'lblMensaje': 'Escribiendo OPERADORES...'})
                            res = ss.guarda_ss(dfOperadores, ss_target, "OPERADORES")
                            if res == False: res = ss.guarda_ss(dfOperadores, ss_target, "OPERADORES", informa_el_progreso=informa_el_progreso)
                            print("Escritura de operadores", res)
                            config.dfOperadores = dfOperadores
                            informa_el_progreso.emit({'progressBar': 80, 'lblMensaje': 'Escribiendo TARIFARIO...'})
                            res = ss.guarda_ss(dfTarifario, ss_target, "TARIFARIO")
                            if res == False: res = ss.guarda_ss(dfTarifario, ss_target, "TARIFARIO", informa_el_progreso=informa_el_progreso)
                            print("Escritura de tarifas", res)
                            config.dfTarifas = dfTarifario
                            informa_el_progreso.emit({'progressBar': 85, 'lblMensaje': 'Escribiendo VEHICULOS...'})
                            res = ss.guarda_ss(dfVehiculos, ss_target, "VEHICULOS")
                            if res == False: res = ss.guarda_ss(dfVehiculos, ss_target, "VEHICULOS", informa_el_progreso=informa_el_progreso)
                            print("Escritura de vehiculos", res)
                            config.dfVehiculos = dfVehiculos
                            informa_el_progreso.emit({'progressBar': 90, 'lblMensaje': 'Escribiendo información del CEDI...'})
                            res = ss.guarda_ss(dfCedi, ss_target, "CEDI")
                            if res == False: res = ss.guarda_ss(dfCedi, ss_target, "CEDI", informa_el_progreso=informa_el_progreso)
                            print("Escritura de cedi", res)
                            config.dfCedi = dfCedi
                            informa_el_progreso.emit({'progressBar': 95, 'lblMensaje': 'Escribiendo CONFIGURACIÓN...'})
                            res = ss.guarda_ss(dfPreferencias, ss_target, "CONFIGURACION", informa_el_progreso=informa_el_progreso)
                            print("Escritura de configuracion/preferencias", res)
                            config.dfConfiguracion = dfPreferencias
                            print("Proceso terminado...")
                            informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Plan creado correctamente'})
                            #RECARGAR LISTA DE PLANES
                            status, config.lista_ss = self.obtenerLista()
                            print(status, config.lista_ss)
                            self.planes.cargaPlanesCreados(informa_el_progreso=informa_el_progreso)
                            self.planes.cargar_grid_tablero(informa_el_progreso=informa_el_progreso, tableroOriginal=True)
                        else:
                            time.sleep(5)
                            informa_el_progreso.emit({'progressBar': 100,
                                                      'lblMensaje': 'No se pudo crear el plan'})

                    else:
                        time.sleep(5)
                        informa_el_progreso.emit({'progressBar': 100,
                                                  'lblMensaje': 'Hubo un error en la lectura de los Spreadsheets'})
                        return False

                else:
                    informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'No se puede generar el plan si no existen todos los datos'})

            else:
                informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Asigna un nombre al plan para poder continuar'})

        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #config.b('**** ERROR **** {}'.format(error))
            print(error)
            return False, 0, 0

    def format_columns(self, df):

        columns_float2d = ['Valor', 'ValorMax', 'DuracionViaje', 'Km', 'LitrosDiesel', 'CostoProveedorFlete',
                           'CostoCombustible', 'CostoHotel', 'CostoComidas', 'CostoThermo',
                           'CostoCasetas', 'CostoFerry', 'CostoOtros', 'CostoTotal']

        df[columns_float2d] = df[columns_float2d].apply(pd.to_numeric, errors='ignore')
        df[columns_float2d] = df[columns_float2d].applymap('{:,.2f}'.format)

        columns_float4d = ['Volumen', 'Peso', 'VolumenPermitido', 'PesoPermitido', 'VolumenMax',
                           'Co2']

        df[columns_float4d] = df[columns_float4d].apply(pd.to_numeric, errors='ignore')
        df[columns_float4d] = df[columns_float4d].applymap('{:,.4f}'.format)

        columns_dates = ['VentanaFechaInicioPedido', 'VentanaFechaFinPedido']

        for index in range(0, len(columns_dates)):
            date_a = pd.to_datetime(df[columns_dates[index]], errors='ignore')
            date_b = date_a.apply(lambda i: i.strftime('%d/%m/%Y'))
            df[columns_dates[index]] = date_b

        columns_dates_comp = ['FechaEntregaPedido', 'FechaSalidaPedido', 'FechaSalida', 'FechaRetorno',
                              'FechaEjecucionMotor', 'FechaEjecucionOpti', 'FechaEjecucionAsigna', 'FechaEntregaTR2']

        for index in range(0, len(columns_dates_comp)):
            date_a = pd.to_datetime(df[columns_dates_comp[index]], errors='ignore')
            date_b = date_a.apply(lambda x: x.strftime('%d/%m/%Y %H:%M') if not pd.isnull(x) else '')
            df[columns_dates_comp[index]] = date_b

        return df

    def cargaTablaPedidos(self):
        try:
            datosPedidos = []
            for spread in config.lista_ss:
                if 'ARMS PEDIDOS' in spread["name"]:
                    datosPedidos.append(spread)
            #para limpiar los datos previos en la lista
            self.ui.listGenerarPlanPedidos.clear()
            config.datosPedidos = pd.DataFrame(datosPedidos)
            for i in range(config.datosPedidos.shape[0]):
                self.ui.listGenerarPlanPedidos.addItem(config.datosPedidos.name[i])
            # selecciona el primero
            self.ui.listGenerarPlanPedidos.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            print(error)

    def cargaTablaVehiculos(self):
        try:
            datosVehiculos = []
            for spread in config.lista_ss:
                if 'ARMS VEHICULOS' in spread["name"]:
                    datosVehiculos.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanVehiculos.clear()
            config.datosVehiculos = pd.DataFrame(datosVehiculos)
            for dato in datosVehiculos:
                self.ui.listGenerarPlanVehiculos.addItem(dato['name'])
            #selecciona el primero
            self.ui.listGenerarPlanVehiculos.setCurrentRow(0)
            # evento producido cuando cambia el elemento seleccionado
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def cargaTablaOperadores(self):
        try:
            datosOperadores = []
            for spread in config.lista_ss:
                if 'ARMS OPERADORES' in spread["name"]:
                    datosOperadores.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanOperadores.clear()
            config.datosOperadores = pd.DataFrame(datosOperadores)
            for dato in datosOperadores:
                self.ui.listGenerarPlanOperadores.addItem(dato['name'])
            # selecciona el primero
            self.ui.listGenerarPlanOperadores.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)



    def cargaTablaCompatibilidad(self):
        try:
            datosCompatibilidad = []
            for spread in config.lista_ss:
                if 'ARMS COMPATIBILIDAD' in spread["name"]:
                    datosCompatibilidad.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanComp.clear()
            config.datosCompatibilidad = pd.DataFrame(datosCompatibilidad)
            for dato in datosCompatibilidad:
                self.ui.listGenerarPlanComp.addItem(dato['name'])
            # selecciona el primero
            self.ui.listGenerarPlanComp.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def cargaTablaTarifario(self):
        try:
            datosTarifario = []
            for spread in config.lista_ss:
                if 'ARMS TARIFARIO' in spread["name"]:
                    datosTarifario.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanTarifario.clear()
            config.datosTarifario = pd.DataFrame(datosTarifario)
            for dato in datosTarifario:
                self.ui.listGenerarPlanTarifario.addItem(dato['name'])
            # selecciona el primero
            self.ui.listGenerarPlanTarifario.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)


    def cargaTablaDestinos(self):
        try:
            datosDestinos = []
            for spread in config.lista_ss:
                if 'ARMS DESTINOS' in spread["name"]:
                    datosDestinos.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanDestinos.clear()
            config.datosDestinos = pd.DataFrame(datosDestinos)
            for dato in datosDestinos:
                self.ui.listGenerarPlanDestinos.addItem(dato['name'])
            # selecciona el primero
            self.ui.listGenerarPlanDestinos.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def cargaTablaPreferencias(self):
        try:
            datosPreferencias = []
            for spread in config.lista_ss:
                if 'ARMS PREFERENCIAS' in spread["name"]:
                    datosPreferencias.append(spread)
            # para limpiar los datos previos en la lista
            self.ui.listGenerarPlanPreferencias_2.clear()
            config.datosPreferencias = pd.DataFrame(datosPreferencias)
            for dato in datosPreferencias:
                self.ui.listGenerarPlanPreferencias_2.addItem(dato['name'])
            # selecciona el primero
            self.ui.listGenerarPlanPreferencias_2.setCurrentRow(0)
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            print(error)

    def obtenerLista(self):
        headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/listar-ss"
        datos = {
                    "tipo": "todo",
                    "folder_cedi":config.usuario["folderCedi"],
                    "folder_origen":config.usuario["folderOrigen"],
                    "folder_resultados":config.usuario["folderResultados"]
                }
        try:
            r = requests.post(url, data=json.dumps(datos), headers=headers).json()
            return True, r
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #config.b('**** ERROR **** {}'.format(error))
            print(error)
            return False, 0, 0

    def obtener_folder_empresa(self):
        try:
            criterios = [{'campo': 'idEmpresa', 'valor': str(config.empresa)}, {'campo': 'nombre', 'valor': 'Resultados'}, {'campo': 'cedi', 'valor': str(config.slot_editCEDI)}]
            status, id = self.dbd.buscar_id(criterios)
            # print (id)
            return id[0]['id']

        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #config.b('**** ERROR **** {}'.format(error))
            print(error)
            return False, 0, 0




