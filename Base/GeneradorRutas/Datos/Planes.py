
from Base.GeneradorRutas.Optimiza.funciones import *

"""
    @desc: Clase para listar los ss de planes en una lista y visualizarlos en una tabla
    @author: Raquel Antonio

"""
from PyQt5.QtWidgets import QListWidgetItem
from IO.Apis.spread_sheet import SpreadSheet
import os, sys
import config
from IO.Apis.calculate_metrics import calculate_metrics
from IO.Apis.consolidar_json import ConsolidarJson

class Planes:
    def __init__(self, ui):
        self.ui = ui
        self.cedi = config.slot_editCEDI
        self.empresa = config.empresa
        config.nombrePlan = ''
        self.spreadsheet = SpreadSheet()

    def cargaPlanesCreados(self, informa_el_progreso=False):
        try:
            if informa_el_progreso: informa_el_progreso.emit({'progressBar': 10, 'lblMensaje': 'Se estan cargando los planes creados'})
            datosPlanes = []
            for spread in config.lista_ss:
                if 'ARMS RESULTADOS' in spread["name"] or 'A.' in spread["name"]:
                    datosPlanes.append(spread)

            if len(datosPlanes) != 0:
                # para limpiar los datos previos en la lista
                self.ui.listAbrePlan.clear()
                self.ui.comboNombrePlan.clear()
                config.datosPlanes = pd.DataFrame(datosPlanes)
                for dato in datosPlanes:
                    self.ui.listAbrePlan.addItem(dato['name'])
                    self.ui.comboNombrePlan.addItem(dato['name'])
                # selecciona el primero
                self.ui.listAbrePlan.setCurrentRow(0)
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #config.b('**** ERROR **** {}'.format(error))
            print(error)
            if informa_el_progreso: informa_el_progreso.emit(
                {'progressBar': 100, 'lblMensaje': 'Hobo un error al cargar el plan'})
            return False, 0, 0


    def obtenerDatosDelSSPlan(self, informa_el_progreso):
        try:
            if config.nombrePlan != '':
                status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'RESULTADOS', key=config.idPlan)
                if status == True:
                    '''SE GUARDA EL NOMBRE DEL SS DEL PLAN'''
                    config.nombrePlan = config.nombrePlan
                    config.dfPlan = resultado.copy()
                    config.dfPlan.reset_index(drop=True, inplace=True)

                    fnPutGrid(config.dfPlan, self.ui.gridElPlan, config.campos_ssPlan)


                    # abre el resto de las pestañas
                    informa_el_progreso.emit({'progressBar': 20, 'lblMensaje': 'Abriendo DESTINOS...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'DESTINOS', abierto=True)
                    if status == True: config.dfDestinos = resultado.copy()
                    informa_el_progreso.emit({'progressBar': 25, 'lblMensaje': 'Abriendo VEHICULOS...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'VEHICULOS', abierto=True)
                    if status == True:
                        config.dfVehiculos = resultado.copy()
                        config.dfVehiculos.reset_index(drop=True, inplace=True)
                    informa_el_progreso.emit({'progressBar': 30, 'lblMensaje': 'Abriendo CEDI...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'CEDI', abierto=True)
                    if status == True:config.dfCedi = resultado.copy()
                    informa_el_progreso.emit({'progressBar': 35, 'lblMensaje': 'Abriendo CONFIGURACION...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'CONFIGURACION', abierto=True)
                    if status == True:config.dfConfiguracion = resultado.copy()
                    informa_el_progreso.emit({'progressBar': 40, 'lblMensaje': 'Abriendo COMPATIBILIDAD...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'COMPATIBILIDAD', abierto=True)
                    if status == True:config.dfCompatibilidad = resultado.copy()
                    informa_el_progreso.emit({'progressBar': 45, 'lblMensaje': 'Abriendo TARIFARIO...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'TARIFARIO', abierto=True)
                    if status == True:config.dfTarifas = resultado.copy()
                    informa_el_progreso.emit({'progressBar': 50, 'lblMensaje': 'Abriendo OPERADORES...'})
                    status, duracion, resultado = self.spreadsheet.abrirSS(config.nombrePlan, 'OPERADORES', abierto=True)
                    if status == True:
                        config.dfOperadores = resultado.copy()
                        config.dfOperadores.reset_index(drop=True, inplace=True)

                    # SE MUESTRAN LOS DATOS EN EL GRID
                    # el Plan y sus copias
                    informa_el_progreso.emit({'progressBar': 55, 'lblMensaje': 'Cargando Grid El Plan...'})
                    #fnPutGrid(config.dfPlan, self.ui.gridElPlan, config.campos_ssPlan)
                    # otros
                    # informa_el_progreso.emit({'progressBar': 60, 'lblMensaje': 'Cargando Grid Asignar Vehículos...'})
                    # fnPutGrid(config.dfVehiculos, self.ui.gridAsignarVeh, config.campos_ssAsignarVehiculos)
                    # informa_el_progreso.emit({'progressBar': 65, 'lblMensaje': 'Cargando Grid Asignar Operadores...'})
                    # fnPutGrid(config.dfOperadores, self.ui.gridAsignarOperadores, config.campos_ssAsignarOperadores)
                    # informa_el_progreso.emit({'progressBar': 70, 'lblMensaje': 'Cargando Grid Predespegue Vehículos...'})
                    # fnPutGrid(config.dfVehiculos, self.ui.gridAsignarVehPredespegue, config.campos_ssAsignarVehiculos)
                    # informa_el_progreso.emit({'progressBar': 75, 'lblMensaje': 'Cargando Grid Predespegue Operadores...'})
                    # fnPutGrid(config.dfOperadores, self.ui.gridAsignarOperadoresPredespegue, config.campos_ssAsignarOperadores)

                    # SE MUESTRAN LOS DATOS EN LOS TABLEROS
                    config.TableroOriginal = calculate_metrics(config.dfPlan)
                    informa_el_progreso.emit({'progressBar': 80, 'lblMensaje': 'Tablero Plan...'})
                    fnPutTablero(self.ui.tableroPlan, solo_original=True)
                    informa_el_progreso.emit({'progressBar': 85, 'lblMensaje': 'Tablero Optimiza Automático...'})
                    fnPutTableritos(self.ui)
                else:
                    print('Hubo un error al obtener y guardar los datos: ', config.nombrePlan)
                    return False

            else:
                'No se ha seleccionado un plan'

                # ----------------------------------------------------
        except Exception as e:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            # exc_type, exc_obj, exc_tb = sys.exc_info()
            # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            # error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # #config.b('**** ERROR **** {}'.format(error))
            # print(error)
            return False, 0, 0

    def carga_grid(self, datos, tabla, solo_struct=False):
        try:
            # borra la tabla antes de meter datos
            tabla.setColumnCount(0)
            tabla.setRowCount(0)
            # SE MUESTRAN LOS DATOS EN EL GRID
            tabla.setColumnCount(len(datos.columns))
            tabla.setHorizontalHeaderLabels(datos.columns)
            if not solo_struct:
                tabla.setRowCount(len(datos.index))
                for i in range(len(datos.index)):
                    for j in range(len(datos.columns)):
                        tabla.setItem(i, j, QTableWidgetItem(str(datos.get_value(i, datos.columns[j]))))
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {} ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print(error)
            return False, 0, 0

    def cargar_grid_tablero(self, informa_el_progreso=False, tableroOriginal=False):
        try:
            if informa_el_progreso: informa_el_progreso.emit(
                {'progressBar': 10, 'lblMensaje': 'Se estan llenando los grids'})
            # SE MUESTRAN LOS DATOS EN EL GRID
            # el Plan y sus copias
            fnPutGrid(config.dfPlan, self.ui.gridElPlan, config.campos_ssPlan)
            # otros
            fnPutGrid(config.dfVehiculos, self.ui.gridAsignarVeh, config.campos_ssAsignarVehiculos)
            fnPutGrid(config.dfOperadores, self.ui.gridAsignarOperadores, config.campos_ssAsignarOperadores)
            fnPutGrid(config.dfVehiculos, self.ui.gridAsignarVehPredespegue, config.campos_ssAsignarVehiculos)
            fnPutGrid(config.dfOperadores, self.ui.gridAsignarOperadoresPredespegue, config.campos_ssAsignarOperadores)
            # SE MUESTRAN LOS DATOS EN LOS TABLEROS
            config.TableroOriginal = calculate_metrics(config.dfPlan)
            fnPutTablero(self.ui.tableroPlan, solo_original=tableroOriginal)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {} ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print(error)
            if informa_el_progreso: informa_el_progreso.emit(
                {'progressBar': 100, 'lblMensaje': 'Hubo un error al cargar los grids'})
            return False, 0, 0
