import math

class VehicleRoutingWorldPanel():
    def __init__(self):
        self.minimumLatitude = float("inf")
        self.maximumLatitude = -float("inf")
        self.minimumLongitude = float("inf")
        self.maximumLongitude = -float("inf")

        self.latitudeLength = 0
        self.longitudeLength = 0

        self.innerWidth = 0
        self.innerHeight = 0
        self.innerWidthMargin = 0
        self.innerHeightMargin = 0

        self.imageWidth = -1
        self.imageHeight = -1
        self.MARGIN_RATIO = 0.04

    def agregarCordenadas(self, latitude, longitude):
        if (latitude < self.minimumLatitude):
            self.minimumLatitude = latitude;

        if (latitude > self.maximumLatitude):
            self.maximumLatitude = latitude;

        if (longitude < self.minimumLongitude):
            self.minimumLongitude = longitude;

        if (longitude > self.maximumLongitude):
            self.maximumLongitude = longitude;

    def prepareFor(self, width, height):
        self.latitudeLength = self.maximumLatitude - self.minimumLatitude
        self.longitudeLength = self.maximumLongitude - self.minimumLongitude
        self.innerWidthMargin = width * self.MARGIN_RATIO
        self.innerHeightMargin = height * self.MARGIN_RATIO
        self.innerWidth = width - (2.0 * self.innerWidthMargin)
        self.innerHeight = height - (2.0 * self.innerHeightMargin)

        if self.innerWidth > (self.innerHeight * self.longitudeLength / self.latitudeLength):
            self.innerWidth = self.innerHeight * self.longitudeLength / self.latitudeLength
        else:
            self.innerHeight = self.innerWidth * self.latitudeLength / self.longitudeLength

        self.imageWidth = int(math.floor((2.0 * self.innerWidthMargin) + self.innerWidth))
        self.imageHeight = int(math.floor((2.0 * self.innerHeightMargin) + self.innerHeight))

    def translateLogitudeToX(self, longitude):
        return int(
            ((longitude - self.minimumLongitude) * self.innerWidth / self.longitudeLength) + self.innerWidthMargin)

    def transladeLatitudeToY(self, latitude):
        return int(math.floor(
            ((self.maximumLatitude - latitude) * self.innerHeight / self.latitudeLength) + self.innerHeightMargin))

    def getImageWidth(self):
        return self.imageWidth

    def getImageHeight(self):
        return self.imageHeight

        # if __name__ == '__main__':
        # jsonCompleto = open('solved.json').read()
        # print(dict(json.loads(jsonCompleto)))