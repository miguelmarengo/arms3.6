import json
import threading
import os, sys, time
from random import random
from py4j.java_gateway import JavaGateway
from IO.Apis.consolidar_json import ConsolidarJson
from Base.GeneradorRutas.Motor.VehicleRoutingWorldPanel import VehicleRoutingWorldPanel
from Base.GeneradorRutas.Motor.CanvasMotor import GraphWidget, Canvas
from Base.GeneradorRutas.Motor.grafica import MyDynamicMplCanvas
from PyQt5.QtWidgets import QMainWindow, QFrame, QDialog, QGraphicsScene, QGraphicsView, QWidget
import config

class OptaPlanner:
    gateway = JavaGateway()
    rutaMotor = "arms-engine.jar"
    planCorriendo = False
    tiempoEjecucion = 0

    def __init__(self, padre):
        self.padreContainer = padre
        self.iteracion = 0
        self.datax = []
        self.datay =  []

        self.gra = MyDynamicMplCanvas(self.padreContainer.widgetGrafica, width=7.5, height=4, dpi=100)
        self.gra.values(self.datax, self.datay)
        self.gra.update_figure()

    def abreOptaplanner(self):
        try:
            if (os.path.exists(self.rutaMotor)):
                p = os.system("java -jar " + self.rutaMotor)
                print(p)  # Error no encontrado 256
                print('Instancia optaplanner lanzada')
                return True
            else:
                print("Error: No se encuentra el archivo ejecutable del motor")
                return False

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: abreOptaplanner")
            # print(error)
            return False

    def iniciaInstancia(self):
        try:
            # Antes de iniciar, verifico si ya está activa
            if (self.instanciaEsActiva() == False):
                print('Iniciando instancia optaplanner')
                t = threading.Thread(target=self.abreOptaplanner, daemon=True)
                t.start()
                return True
            else:
                # La instancia del motor ya está iniciada, entonces la reinicio
                self.finalizarInstancia()  # La detengo y me duermo 5 seg
                time.sleep(5)
                self.iniciaInstancia()  # Vuelvo a arrancar la instancia de Optaplanner
                return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: iniciaInstancia")
            # print(error)
            return False

    def instanciaEsActiva(self):
        try:
            # Verifico si ya estaba iniciada una instancia de Optaplanner
            activo = self.gateway.entry_point.motorActivo()
            print("¿El motor está activo? R = '" + str(activo) + "'")
            return activo
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: instanciaEsActiva")
            # print(error)
            return False

    def finalizarInstancia(self):
        # Finaliza la instancia de ejecución
        try:
            self.gateway.entry_point.finalizarInstancia()
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: finalizarInstancia")
            # print(error)
            return False

    def generaJson(self):
        try:
            #if not os.path.exists('data/arms/planes/'):
            #    os.makedirs('data/arms/planes/')

            consolidar = ConsolidarJson()
            data = consolidar.getJson()
            return data
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: generaJson")
            print(error)
            data = dict()
            data["status"] = False
            data["data"] = "Error"
            return data

    def ejecutarPlan(self, jsonZote):
        print('ejecutarPlan')
        fullPath = 'data/arms/planes/plan.json'
        self.tiempoEjecucion = 0
        try:
        #     if (os.path.exists(fullPath)):
        #         jsonCompleto = open(fullPath).read()
        #         print('Termina cargaJson')
        #     else:
        #         print('El archivo plan.json NO EXISTEEE!!! ')
        #         return False

            #ReiniciaCanvas
            #self.reiniciaCanvas()
            jsonCompleto = jsonZote

            # Carga un plan mandandole el json
            self.gateway.entry_point.cargandoPlanFromText(jsonCompleto)
            print('Plan cargado')

            # Ejecuta el plan que este cargado
            self.gateway.entry_point.iniciarEjecucion()
            print('iniciarEjecucion iniciado')

            self.planCorriendo = True

            # Inicia timer para obtener solución
            # timer = threading.Thread(target=self.obtieneSolucion)
            # timer.start()

            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: ejecutarPlan")
            print(error)
            return False

    def obtieneSolucion(self):
        print('obtenerSolucion')
        mensajeActual = 'Buscando una solución...'
        try:
            solution = self.gateway.entry_point.getSolution()
            jjson = json.loads(solution)
            viajes = jjson['viajes']

            self.tiempoEjecucion += 1

            if jjson['scores']['hardScore'] == 0 and len(viajes) > 0:

                self.actualizaGUI(solution)
                #Prepara datos para indicadores
                numViajes = len(viajes)
                totalDestinos = config.TableroOriginal["Total Destinos"]
                totalPedidos = config.TableroOriginal["Total Pedidos"]
                totalKg = config.TableroOriginal["Total Kg"]
                totalPiezas= config.TableroOriginal["Piezas"]
                totalM3 = config.TableroOriginal["Total m3"]

                costoTotal = jjson['scores']['costoTotal']
                kmTotal = jjson['scores']['kmTotal']
                CostoXViaje = costoTotal / numViajes
                CostoXDestino = costoTotal / totalDestinos
                CostoXPedido = costoTotal / totalPedidos
                CostoXKg = costoTotal / totalKg
                CostoXPieza = costoTotal / totalPiezas
                CostoXM3 = costoTotal / totalM3

                self.datax.append(self.tiempoEjecucion)
                self.datay.append(round(costoTotal, 2))
                self.gra.values(self.datax, self.datay)
                self.gra.update_figure()

                return {'progressBar': 100, 'lblMensaje': 'Motor en ejecución...', 'TotalCosto': round(costoTotal, 2),
                        'Viajes': round(numViajes, 0), 'KmTotales': round(kmTotal, 2),
                        'CostoXViaje': round(CostoXViaje, 2),
                        'CostoXDestino': round(CostoXDestino, 2), 'CostoXPedido': round(CostoXPedido, 2),
                        'CostoXKg': round(CostoXKg, 2), 'CostoXPieza': round(CostoXPieza, 2),
                        'CostoXM3': round(CostoXM3, 2),
                        'tiempoEjecucion': self.tiempoEjecucion, 'datosGraficaX': [self.datax],
                        'datosGraficaY': [self.datay]}

            else:
                print("Aún no hay solución")

                if(self.tiempoEjecucion < 10):
                    mensajeActual = 'Buscando una solución...'
                elif(self.tiempoEjecucion <20):
                    mensajeActual = 'Buscando arduamente una solución...'
                elif (self.tiempoEjecucion < 30):
                    mensajeActual = 'Me estoy concentrando en buscar una solución... ¿Ya revisaste tus datos?'
                else:
                    mensajeActual = 'No creo poder encontrar una solución, verifica tus datos e intenta de nuevo...'


                return {'progressBar': 0, 'lblMensaje': mensajeActual, 'TotalCosto': 0, 'Viajes': 0,
                        'KmTotales': 0, 'CostoXViaje': 0,
                        'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                        'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': self.tiempoEjecucion, 'datosGraficaX': [], 'datosGraficaY': []}

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: obtieneSolucion")
            print(error)
            return {'progressBar': 0, 'lblMensaje': mensajeActual, 'TotalCosto': 0, 'Viajes': 0,
                    'KmTotales': 0, 'CostoXViaje': 0,
                    'CostoXDestino': 0, 'CostoXPedido': 0, 'CostoXKg': 0,
                    'CostoXPieza': 0, 'CostoXM3': 0, 'tiempoEjecucion': self.tiempoEjecucion, 'datosGraficaX': [], 'datosGraficaY': []}

    def guardaSolucion(self):
        print('guardaSolucion')
        data = dict()
        try:
            solution = self.gateway.entry_point.getSolution()
            jjson = json.loads(solution)
            viajes = jjson['viajes']

            if jjson['scores']['hardScore'] == 0 and len(viajes) > 0:
                self.actualizaGUI(solution)
            else:
                print("Aún no hay solución")
                data["status"] = False
                data["data"] = "Aún no hay solución"
                return data

            print("Este es el JSOOOOOOOOOON para guardar!!")
            jsonGuardado = jjson #json.dumps(jjson)
            print(jsonGuardado)

            # if not os.path.exists('data/arms/resultados/'):
            #     os.makedirs('data/arms/resultados/')
            #
            # fullPath = 'data/arms/resultados/plan.json'
            # with open(fullPath, 'w') as f:
            #     f.write(json.dumps(jjson))

            #print("¡¡¡Json Guardado Correctamente!!")

            data["status"] = True
            data["data"] = jsonGuardado
            return data
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: guardaSolucion")
            print(error)
            data["status"] = False
            data["data"] = ""
            return data

    def detenerEjecucion(self):
        print('detenerEjecucion')
        self.planCorriendo = False
        try:
            self.gateway.entry_point.terminarEjecucion()
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: detenerEjecucion")
            # print(error)
            return False

    def generaDirectorios(self):
        try:
            if not os.path.exists('data/arms/resultados/'):
                print("No existe resultados, lo creo")
                os.makedirs('data/arms/resultados')

            if not os.path.exists('data/arms/planes/'):
                print("No existe planes, lo creo")
                os.makedirs('data/arms/planes')

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: generaDirectorios")
            print(error)

    def actualizaGUI(self, jsonator):
        try:
            translator = VehicleRoutingWorldPanel()
            solution = json.loads(jsonator)

            locations = []
            viajes = []
            for viaje in solution['viajes']:
                # print(viaje)
                for destino in viaje['destinos']:
                    location = destino['Latitud'], destino['Longitud']
                    locations.append(location)
                    translator.agregarCordenadas(location[0], location[1])

            depot = solution['cedi'][0]
            depotLocation = float(depot['Latitud']), float(depot['Longitud'])
            translator.agregarCordenadas(depotLocation[0], depotLocation[1])

            w = self.padreContainer.graphicsViewMotor.height() -10 #self.padreContainer.graphicsViewMotor.width() -10
            h = self.padreContainer.graphicsViewMotor.height() -15
            # print('w:{} h:{}'.format(w, h))
            im = Canvas(w, h)
            width = im.width()
            height = im.height()
            translator.prepareFor(width, height - 10)

            for viaje in solution['viajes']:
                vi = []
                i = 1
                for destino in viaje['destinos']:
                    location = destino['Latitud'], destino['Longitud']
                    x = translator.translateLogitudeToX(location[1])
                    y = translator.transladeLatitudeToY(location[0])
                    locTranslated = x, y, i, location
                    i = i + 1
                    im.drawCircle(locTranslated[0], locTranslated[1])
                    vi.append(locTranslated)
                viajes.append(vi)

            # depot
            depx = translator.translateLogitudeToX(depotLocation[1])
            depy = translator.transladeLatitudeToY(depotLocation[0])
            depox = depx, depy, 0
            im.drawDepot(depox[0], depox[1])

            for viaje in viajes:
                # print(viaje)
                previo = depox
                color = int(random() * 255), int(random() * 255), int(random() * 255)
                i = 1
                for destino in viaje:
                    im.drawLineLoc(previo, destino, color)
                    previo = destino

                    if i == 1:
                        latitudC = destino[3][0]  # (depotLocation[0] + destino[3][0]) / 2
                        longitudC = destino[3][1]  # (depotLocation[1] + destino[3][1]) / 2
                        xL = translator.translateLogitudeToX(longitudC)
                        yL = translator.transladeLatitudeToY(latitudC)
                        im.drawCamion(xL, yL)
                    if len(viaje) == i:
                        im.drawLineLoc(depox, destino, color, True)

                    i = i + 1

            zoomN = 1
            scene = QGraphicsScene()
            scene.setItemIndexMethod(QGraphicsScene.NoIndex)
            scene.setSceneRect(0, 0, width, height)
            scene.addPixmap(im)
            self.padreContainer.graphicsViewMotor.setScene(scene)


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            # config.b('**** ERROR **** {}'.format(error))
            print("Error método: actualizaGUI")
            print(error)

    def reiniciaCanvas(self):

        w = self.padreContainer.graphicsViewMotor.height() - 10 #self.padreContainer.graphicsViewMotor.width() - 10
        h = self.padreContainer.graphicsViewMotor.height() - 10
        # print('w:{} h:{}'.format(w, h))
        im = Canvas(w, h)
        width = im.width()
        height = im.height()

        zoomN = 1
        scene = QGraphicsScene()
        scene.setItemIndexMethod(QGraphicsScene.NoIndex)
        scene.setSceneRect(0, 0, width, height)
        scene.addPixmap(im)
        self.padreContainer.graphicsViewMotor.setScene(scene)


    def resizeEvent(self, *args, **kwargs):
        # print('resizing')
        self.actualizaGUI(None)