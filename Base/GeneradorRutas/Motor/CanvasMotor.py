import os
from random import random

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtGui import QPen
from PyQt5.QtWidgets import *


class Canvas(QtGui.QPixmap):
    """ Canvas for drawing"""

    def __init__(self, w, h):
        QtGui.QPixmap.__init__(self, w, h)
        # self.parent = parent 
        # self.imH = 0 
        # self.imW = 0 
        self.fill(QtGui.QColor(0,0,0,0))
        self.color = QtGui.QColor(1, 55, 0)
        self.solid = QtCore.Qt.SolidLine;
        self.dotline = QtCore.Qt.DotLine;

    def paintEvent(self, point=False):
        print('paintevent')
        if point:
            p = QtGui.QPainter(self)
            p.setPen(QtGui.QPen(self.color, 2, self.solid))
            p.drawPoints(point)


            # def drawLine(self, x1=0,y1=10, x2=200, y2=200):

    #     p=QtGui.QPainter(self)
    #     # p.setPen(QtGui.QPen(self.color, 2, self.solid)) 
    #     # p.setBrush(QtGui.QBrush(self.color, 2, self.solid)) 
    #     # p.drawLine(x1, y1, x2, y2); 

    def drawCircle(self, x=0, y=10):
        # print('circulo:x:{} y:{}'.format(x,y)) 
        p = QtGui.QPainter(self)
        QtGui.QColor(178, 89, 89)
        p.setPen(QtGui.QPen(QtGui.QColor(160, 160, 160), 2, self.solid))
        p.drawArc(QtCore.QRectF(x, y, 2, 2), 20, 5760);

    def drawCamion(self, x=10, y=10):
        # print('dibujando camioncito') 
        p = QtGui.QPainter(self)
        # rnd = int(random() * 4) + 1
        # if rnd == 1:
        #     pixmap = QtGui.QPixmap('images/vehicleButter.png')
        # if rnd == 2:
        #     pixmap = QtGui.QPixmap('images/vehicleChameleon.png')
        # if rnd == 3:
        #     pixmap = QtGui.QPixmap('images/vehicleChocolate.png')
        # if rnd == 4:
        #     pixmap = QtGui.QPixmap('images/vehiclePlum.png')
        # if rnd == 5:
        #     pixmap = QtGui.QPixmap('images/vehicleSkyBlue.png')

        pixmap = QtGui.QPixmap('images/vehicleChameleon.png')
        p.drawPixmap(x, y, pixmap)

    def drawDepot(self, x=10, y=10):
        # print('dibujando depot')
        p = QtGui.QPainter(self)

        pixmap = QtGui.QPixmap('images/depot.png')
        # p.drawPixmap(x, y, pixmap)
        p.drawPixmap(x - 30, y - 30, pixmap)

    def clic(self, mouseX, mouseY):
        print('click punto')
        self.paintEvent(QtCore.QPoint(mouseX, mouseY))

    def drawLineLoc(self, loc1, loc2, rgb, subida=False):
        p = QtGui.QPainter(self)
        color = QtGui.QColor(rgb[0], rgb[1], rgb[2])

        if subida == False:
            p.setPen(QtGui.QPen(color, 0, QtCore.Qt.SolidLine))
        else:
            p.setPen(QtGui.QPen(color, 0, QtCore.Qt.DotLine))
        p.drawLine(loc1[0], loc1[1], loc2[0], loc2[1]);


class GraphWidget(QGraphicsView):
    """ Display, zoom, pan..."""

    def __init__(self, parent=None):
        QGraphicsView.__init__(self)
        self.im = Canvas(self.width(), self.height())
        self.imH = self.im.height()
        self.imW = self.im.width()
        self.zoomN = 1
        self.scene = QGraphicsScene(self)
        self.scene.setItemIndexMethod(QGraphicsScene.NoIndex)

        # self.scene.setSceneRect(0, 0, self.imW, self.imH) 

        self.scene.addPixmap(self.im)
        self.setScene(self.scene)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)
        self.setMinimumSize(880, 880)
        self.setWindowTitle("pix")

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            pos = self.mapToScene(event.pos())
            self.im.clic(pos.x(), pos.y())
            # ~ self.scene.update(0,0,64,64)
            # ~ self.updateScene([QtCore.QRectF(0,0,64,64)])
            self.scene.addPixmap(self.im)
            # print('items')
            # print(self.scene.items())
        else:
            return QtGui.QGraphicsView.mousePressEvent(self, event)

    def resizeEvent(self, QResizeEvent):
        pass
        # print('resize event')
        # self.im.width(900) 
        # self.im.height(900) 

    def wheelEvent(self, event):
        if event.delta() > 0:
            self.scaleView(2)
        elif event.delta() < 0:
            self.scaleView(0.5)

    def scaleView(self, factor):
        n = self.zoomN * factor
        if n < 1 or n > 16:
            return
        self.zoomN = n
        self.scale(factor, factor)

    def resize(self, event):
        pass
        # print('resized')


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    widget = GraphWidget()
    widget.show()
    sys.exit(app.exec_())