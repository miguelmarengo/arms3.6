# -*- coding: utf-8 -*-
import requests
import json
from datetime import datetime
import os
import sys
from argparse import ArgumentParser
from PyQt5.QtCore import QDate, QTime, QDateTime, Qt
from IO.Apis.Db import DB
from IO.Apis.spread_sheet import SpreadSheet
from PyQt5.QtWidgets import QTableWidget,QTableWidgetItem
import pandas as pd

class InterfazSilodisa:
    def __init__(self, ui):
        self.ui = ui
        self.ui.pushButtonInterfaseSilodisaGeneraPedido.clicked.connect(self.TraerPedidos)
        # self.ui.pushButtonAsignaPredespegaResultados.clicked.connect()
        #self.CrearSS('P.01-04092017-L')
        self.destinos = []
        self.SSdesde = {}
        self.SShacia = {}
        self.listarDestinos()
        self.ui.tableWidgetSilodisaAnadeDe.itemSelectionChanged.connect(self.selection_changed)
        self.ui.tableWidgetSilodisaAnadeA.itemSelectionChanged.connect(self.selection_changed)
        self.ui.pushButtonInterfaseSilodisaAnadeDir.clicked.connect(self.concatSS)
        

    def TraerPedidos(self):
       
        nombrePlan = 'P.O1-'
        fec =self.ui.dateEditInterfaseSilodisaFecha.date().toPyDate()
        fech = str(fec)
        fecha = fech.split("-")

        nombrePlan += fecha[2] + fecha[1] + fecha[0] 
        #print(fecha)

        if self.ui.radioButtonInterfaseSilodisaLocal.isChecked() == True:
            nombrePlan += '-L'
            self.ui.listWidgetSilodisaPedidos.addItem(nombrePlan)
            if nombrePlan is not 'P.O1-':
                #print("Ejecutando para obtener el plan " + nombrePlan)
                ok = self.CrearSS(nombrePlan)
                print(ok)
        elif self.ui.radioButtonInterfaseSilodisaForaneo.isChecked() == True:
            nombrePlan += '-F'
            self.ui.listWidgetSilodisaPedidos.addItem(nombrePlan)
            if nombrePlan is not 'P.O1-':
                #print("Ejecutando para obtener el plan " + nombrePlan)
                ok = self.CrearSS(nombrePlan)
                print(ok)


    
    def escribirSS(self, plan):
        # RADIO BUTTONS
        # radioButtonInterfasePochtecaPedidos
        # radioButtonInterfasePochtecaDestinos
        # radioButtonInterfasePochtecaOperadores
        # radioButtonInterfasePochtecaTodos
        # radioButtonInterfasePochtecaOrdinarios
        # radioButtonInterfasePochtecaCancelados
        # radioButtonInterfasePochtecaUrgentes
        
        try:
            #self.ui.listWidgetSilodisaPedidos.addItem("Item")
            '''parser = ArgumentParser(description='ejecucion manual')
            parser.add_argument('cedi', type=str, metavar='<cedi>', default='',  help='')
            args = parser.parse_args()'''
            start_time = datetime.now()

            url = 'https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/silodisa-crear-planes'
            payload = { 'cedi': plan}
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}

            r = requests.post(url, data=json.dumps(payload), headers=headers)
            self.ui.tabWidget_4.setCurrentIndex(1)
            self.ui.tabWidget_4.setCurrentIndex(0)
            resultado = r.text
            #print(resultado)
            # self.ui.pushButtonInterfaseSilodisaGeneraPedido.clicked.connect(a_btn.generarPedido)
            # self.ui.listViewInterfasePochtecaActual.addItems("Un Item")
            return True
        except Exception as e:
            raise e




    def CrearSS(self, plannn):
        start_time = datetime.now()
        try:
            url = 'https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/silodisa-crear-planes'
            payload = {"plan": plannn}
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}

            r = requests.post(url, data=json.dumps(payload), headers=headers)
            resultado = r.text

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            return False, error, False
        duracion = datetime.now() - start_time
        return True, duracion, resultado




    def listarDestinos(self):
        try:
            sp = SpreadSheet()
            cosas = sp.listarSS()
            tamaño = len(cosas) +1
            self.destinos = cosas
            self.ui.tableWidgetSilodisaAnadeDe.setRowCount(tamaño)
            self.ui.tableWidgetSilodisaAnadeDe.setColumnCount(2)
            '''self.ui.tableWidgetSilodisaAnadeDe.setItem(0,0, QTableWidgetItem("Nombre"))
                                    self.ui.tableWidgetSilodisaAnadeDe.setItem(0,1, QTableWidgetItem("ID"))'''
            self.ui.tableWidgetSilodisaAnadeDe.setColumnWidth(0, 300)
            self.ui.tableWidgetSilodisaAnadeDe.setColumnWidth(1, 400)
            self.ui.tableWidgetSilodisaAnadeDe.setSelectionBehavior(QTableWidget.SelectRows)
            self.ui.tableWidgetSilodisaAnadeDe.setSelectionMode(QTableWidget.SingleSelection)


            self.ui.tableWidgetSilodisaAnadeA.setRowCount(tamaño)
            self.ui.tableWidgetSilodisaAnadeA.setColumnCount(2)
            '''self.ui.tableWidgetSilodisaAnadeA.setItem(0,0, QTableWidgetItem("Nombre"))
                                    self.ui.tableWidgetSilodisaAnadeA.setItem(0,1, QTableWidgetItem("ID"))'''
            self.ui.tableWidgetSilodisaAnadeA.setColumnWidth(0, 300)
            self.ui.tableWidgetSilodisaAnadeA.setColumnWidth(1, 400)
            self.ui.tableWidgetSilodisaAnadeA.setSelectionBehavior(QTableWidget.SelectRows)
            self.ui.tableWidgetSilodisaAnadeA.setSelectionMode(QTableWidget.SingleSelection)
            for idx, cosa in enumerate(cosas):
                esto = str(cosa["name"])
                esto2 = str(cosa["id"])
                print(esto2)
                index = idx
                self.ui.tableWidgetSilodisaAnadeDe.setItem(index,0, QTableWidgetItem(esto))
                self.ui.tableWidgetSilodisaAnadeDe.setItem(index,1, QTableWidgetItem(esto2))

                self.ui.tableWidgetSilodisaAnadeA.setItem(index,0, QTableWidgetItem(esto))
                self.ui.tableWidgetSilodisaAnadeA.setItem(index,1, QTableWidgetItem(esto2))
        except Exception as e:
            raise e

    def coordenadas(self, direccion):
        val = direccion.replace(" ", "+")
        key="AIzaSyCb_y3NUbBPZskgpFKE9hywELUY1YsTIvE"
        buffer = StringIO()
        c = pycurl.Curl()
        c.setopt(c.URL, 'https://maps.googleapis.com/maps/api/geocode/json?address='+val+'&key='+key)
        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        c.close()

        body = buffer.getvalue()
        j = json.loads(body)
        if j['status'] != "ZERO_RESULTS":
            latitud = str(j['results'][0]['geometry']['location']['lat'])
            longitud = str(j['results'][0]['geometry']['location']['lng'])

            key="AIzaSyCb_y3NUbBPZskgpFKE9hywELUY1YsTIvE"
            buffer = StringIO()
            c = pycurl.Curl()
            c.setopt(c.URL, 'https://roads.googleapis.com/v1/nearestRoads?points='+latitud+','+longitud+'&key='+key)
            c.setopt(c.WRITEDATA, buffer)
            c.perform()
            c.close()

            body = buffer.getvalue()
            j = json.loads(body)
            lat =str(j['snappedPoints'][0]['location']['latitude'])
            lon =str(j['snappedPoints'][0]['location']['longitude'])

            return True, lat, lon

        else:
            return False, 0, 0




    def selection_changed(self):
        rows = [idx.row() for idx in self.ui.tableWidgetSilodisaAnadeDe.selectionModel().selectedRows()]
        #print(rows)
        for value in rows:
            if value < len(self.destinos):
                value = value
                self.SSdesde = self.destinos[value]
                print("Lo que hay en desde " + self.SSdesde["id"])

        rows2 = [idx.row() for idx in self.ui.tableWidgetSilodisaAnadeA.selectionModel().selectedRows()]
        #print(rows)
        for value in rows2:
            if value < len(self.destinos):
                value = value
                self.SShacia = self.destinos[value]
                print("Lo que hay en hacia " + self.SShacia["id"])





    def concatSS(self):
        sp = SpreadSheet()
        status, duracion, resultado = sp.abrirSS(self.SSdesde["id"],'DESTINOS','key')
        status2, duracion2, resultado2 = sp.abrirSS(self.SShacia["id"],'DESTINOS','key')
        print(resultado2)
        concatenados = pd.concat([resultado2, resultado])
        print(concatenados)
        sp.salvarDFtoSS(concatenados)
