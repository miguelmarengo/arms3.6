# -*- coding: utf-8 -*-
import requests
import json
from datetime import datetime
import os
import sys
from argparse import ArgumentParser

class InterfazPochteca:
    def __init__(self, ui):
        self.ui = ui
        
        self.ui.pushButtonInterfasePochtecaGenera.clicked.connect(self.manejador)

    def manejador(self):
        ###aqui va loq ue maneja el pochteca
        self.cedi = 'cedi1'
        dato = self.leerTSVdesdeFTP(self.cedi)
        resultado = json.loads(dato)
        #print (resultado)
        self.ui.listWidgetPochtecaFTP.clear()
        if self.ui.radioButtonInterfasePochtecaPedidos.isChecked() == True:
            if self.ui.radioButtonInterfasePochtecaTodos.isChecked() == True:
                for cosa in resultado["pedidos"]["normales"]:
                    self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])
                for cosa in resultado["pedidos"]["urgentes"]:
                    self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])
                for cosa in resultado["pedidos"]["otros"]:
                    self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])

            if self.ui.radioButtonInterfasePochtecaOrdinarios.isChecked() == True:
                for cosa in resultado["pedidos"]["normales"]:
                    self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])

            if self.ui.radioButtonInterfasePochtecaUrgentes.isChecked() == True:
                for cosa in resultado["pedidos"]["urgentes"]:
                    self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])

        elif self.ui.radioButtonInterfasePochtecaDestinos.isChecked() == True:
            for cosa in resultado["destinos"]:
                self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])

        elif self.ui.radioButtonInterfasePochtecaOperadores.isChecked() == True:
            for cosa in resultado["operadores"]:
                self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])
    
    def leerTSVdesdeFTP(self, cedi):
        # RADIO BUTTONS
        # radioButtonInterfasePochtecaPedidos
        # radioButtonInterfasePochtecaDestinos
        # radioButtonInterfasePochtecaOperadores
        # radioButtonInterfasePochtecaTodos
        # radioButtonInterfasePochtecaOrdinarios
        # radioButtonInterfasePochtecaCancelados
        # radioButtonInterfasePochtecaUrgentes
        
        try:
            
            '''parser = ArgumentParser(description='ejecucion manual')
            parser.add_argument('cedi', type=str, metavar='<cedi>', default='',  help='')
            args = parser.parse_args()'''
            start_time = datetime.now()

            url = 'https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/list-files-ftp'
            payload = { "cedi": cedi}
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}

            r = requests.post(url, data=json.dumps(payload), headers=headers)
            resultado = json.loads(r.text)
            #for cosa in resultado[1][0]["destinos"]:
            #self.ui.listWidgetPochtecaFTP.addItem(cosa["nombre"])
            #print(res)
            # self.ui.pushButtonInterfaseSilodisaGeneraPedido.clicked.connect(a_btn.generarPedido)
            # self.ui.listViewInterfasePochtecaActual.addItems("Un Item")
            '''url = 'https://o0480vy3fe.executest-1.amazonaws.com/dev/silodisa-crear-planes'
            payload = {"plan": 'P.O1-11092017-L'}
            headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
            try:
                r = requests.post(url, data=json.dumps(payload), headers=headers)
                resultado2 = r.text
                print(resultado2)
            except Exception as e:
                raise e'''
            
            return resultado
        except Exception as e:
            raise e