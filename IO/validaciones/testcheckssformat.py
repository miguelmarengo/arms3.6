
# -*- coding: utf-8 -*-
from __future__ import print_function
import httplib2
import os
import re

from apiclient import discovery
from oauth2client import file, client, tools
from oauth2client.file import Storage
from httplib2 import Http
import string
from itertools import product as prod2
import copy
import checkssformat

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def main():

    alphabet = string.ascii_uppercase
    length = 2
    columnnames = []
    for letter in alphabet:
        columnnames.append(letter)
    for comb in prod2(alphabet, repeat=length):
        columnnames.append(''.join(comb))

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discovery_url = ('https://sheets.googleapis.com/$discovery/rest?'
                     'version=v4')

    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discovery_url)

    # spreadsheet_id = '108-I9BiwrqeWvzj-iy1WYPgpMBFLwvkEe_KftyeG7P0'  # Plantilla USUARIOS
    # spreadsheet_id = '10gV7WiVI4JxAZcyyeFSZQw99f9ePaq2zVQc2jMfnAe0'  # Plantilla EMPRESAS
    spreadsheet_id = '1gvxM9hA4J5pV5V5Q34ulpr4Jx__vOBMVYzMzLWNu3Is'  # Plantilla OPERADORES
    # spreadsheet_id = '1abxTKiax3SERjelcwA7E7yKUnIPACns5w-oqKaBIa6o'  # Plantilla PEDIDOS
    # spreadsheet_id = '1ae-rYxf8vx_rt-XvlRAmzGCm_ryt0sYO3cyxQUzTUDI'  # Plantilla COMPATIBILIDAD
    # spreadsheet_id = '1jTvsmJilBhuZxlM6tNCKggaqBA4ohSaB4C3TxVQbPx8'  # Plantilla DESTINOS
    # spreadsheet_id = '1e9NgTaOfG0RrZh51dsfVYr_2JCtuqZeSFsGkaWu5WmA'  # Plantilla PREFERENCIAS
    # spreadsheet_id = '1TlxL2CW8Baj7AiSKyKHeEmMWZfbrdJ_W1vXtItOlHDI'  # Plantilla VEHICULOS

    ss_type = 'OPERADORES'

    sheet_metadata = service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
    sheet_handler = sheet_metadata.get('sheets', '')

    for i in range(0, 1):
        title = sheet_handler[i].get("properties", {}).get("title", '')

        range_name = title
        result = service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id, range=range_name).execute()
        values = result.get('values', [])
        last_row = len(values)
        last_col = len(values[0])

        print(title, " #Filas = ", last_row,
              " #Columnas = ", last_col, '(', columnnames[last_col-1], ')')

        row_no = 0
        all_values = []
        fields = []
        if not values:
            print('No data found.')
        else:
            for row in values:
                row_no = row_no + 1

                if row_no == 1:
                    if ss_type == 'COMPATIBILIDAD':
                        fields = row[1:]
                    else:
                        fields = row
                elif row_no >= 5:

                    if (len(row) < len(fields)) and (len(row) > 1):
                        row.extend(([''] * (len(fields)-len(row))))

                    if ss_type == 'COMPATIBILIDAD':
                        row = row[1:]
                    row_dictionary = dict(zip(fields, row))
                    all_values.append(row_dictionary)

        while {} in all_values:
            all_values.remove({})

        format_checker = checkssformat.CheckFormat()
        errors = format_checker.check_format_ss(all_values, ss_type)

        if errors != {}:
            print('SS Type: ', ss_type, '# Errors: ', len(errors['Errors']))
            for error_dictionary in errors['Errors']:
                list_errors = ""
                for key in error_dictionary.keys():
                    list_errors = list_errors + "   " + key + ":" + error_dictionary[key]
                print(list_errors)
        else:
            print('SS Type: ', ss_type, '# Errors: ', 0)


if __name__ == '__main__':
    main()
