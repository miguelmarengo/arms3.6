
# -*- coding: utf-8 -*-

import httplib2
import os
import json
import copy
import re

from apiclient import discovery
from oauth2client import file, client, tools
from oauth2client.file import Storage
from httplib2 import Http
from IO.Apis.spread_sheet import SpreadSheet
import string
from itertools import combinations_with_replacement as cwr


def alphabet_columns():

    alphabet = string.ascii_uppercase
    length = 2
    column_names = []
    for letter in alphabet:
        column_names.append(letter)
    for comb in cwr(alphabet, length):
        column_names.append(''.join(comb))

    return column_names


def check_plan_format(NameSS):

    # ss_list = ['RESULTADOS', 'DESTINOS', 'VEHICULOS', 'CEDI', 'CONFIGURACION', 'COMPATIBILIDAD', 'TARIFARIO', 'OPERADORES']
    ss_list = ['RESULTADOS', 'DESTINOS', 'VEHICULOS', 'CEDI', 'CONFIGURACION', 'COMPATIBILIDAD']
    ss = SpreadSheet()
    column_names = alphabet_columns()
    statusfinal = True
    errors_final = {}

    try:
        if NameSS != '':
            for sheet_name in ss_list:
                print("Antes leyendo .......................................................", sheet_name, NameSS)
                status, duracion, dataframe = ss.abrirSS(NameSS, sheet_name)
                statusfinal = statusfinal and status
                print("After leyendo .......................................................")
                print(statusfinal, status, dataframe.shape)
                if statusfinal is True:
                    json_sheet = json.loads(dataframe.to_json(orient='records'))
                    errors_sheet = check_format_ss(json_sheet, sheet_name, column_names)
                    errors_final.update(errors_sheet)
                else:
                    break

            print("Fin For ", statusfinal)

        else:
            statusfinal = False

        for sheet_name in ss_list:
            print("Errores ", sheet_name)
            for i in errors_final[sheet_name]:
                print(i)

        return statusfinal, 0, errors_final

    except Exception as e:
        return False, 0, 0


def check_format_ss(data, ss_name, column_names):

    """

    Inputs:
    :param data = JSON with the records to be written in SS:

    example:
    [
        {
        "Operador": "Marcelino Gracia Perez",
        "Correo": "abc@arete.ws",
        "Placas": "139AR5",
        "Licencia": "E",
        "ProveedorFlete": "708054"
        },
        {
        "Operador": "Antonio Garcia Perez",
        "Correo": "operador1@arete.ws",
        "Placas": "139AR5",
        "Licencia": "E",
        "ProveedorFlete": "700776"
        },
        {
        "Operador": "Martin Garcia",
        "Correo": "operador2@arete.ws",
        "Placas": "139AR5",
        "Licencia": "E",
        "ProveedorFlete": "700776"
        }
    ]

    :param ss_name: Name of the SS to be written:
    one of the following:

    'USUARIOS'
    'EMPRESAS'
    'OPERADORES'
    'PEDIDOS'
    'COMPATIBILIDAD'
    'DESTINOS'
    'PREFERENCIAS'
    'VEHICULOS'

    Outputs:

    self.errors = JSON with errors found in the regular expression match

    The JSON has the following fields:
        Column: Field name of the value with error
        ExpectedRegEx: Regular expression expected for the field
        ActualValue: Value tried to be written in the SS
        Row: Row number of the SS

    example (if there is no errors):
   {} =
   example (if there is errors in 3 different rows)

  {
   "ss_name": [
        {
        "Row": "1",
        "Columns: [A, B, C, AJ]",
        }
        {
        "Row": "6",
        "Columns: [D, E, Z]",
        }
        {
        "Row": "19",
        "Columns: [A]",
        }
    ]
    }

    """

    error_ss = {
        ss_name: [
            {
                'Row': '',
                'Column': []
            }
       ]
    }

    acdir = os.path.dirname(__file__)
    file_validation = acdir + '/VALIDACION_'+ss_name+'.json'
    print(file_validation)
    with open(file_validation) as data_file:
        validation_data = json.loads(data_file.read())

    count_wrong = 0
    count_row = 0
    for row in data:
        count_row = count_row + 1
        count_wrong_row = 0
        count_col = 0
        for field in row:
            if field in validation_data:
                count_col = count_col + 1
                # print(validation_data[field]['RegEx'])
                # print(row[field])
                match_object = re.match(str(validation_data[field]['RegEx']), str(row[field]))

                if not (hasattr(match_object, 'group')):
                    count_wrong_row = count_wrong_row + 1

                    if count_wrong > 0 and count_wrong_row == 1:
                        error_ss[ss_name].append({'Row': '', 'Column': []})

                    error_ss[ss_name][count_wrong]['Row'] = str(count_row)
                    error_ss[ss_name][count_wrong]['Column'].append(column_names[count_col])

        if count_wrong_row > 0:
            count_wrong = count_wrong + 1

    if count_wrong == 0:
        errors = {}
    else:
        errors = error_ss

    return errors



class CheckFormat:
    def __init__(self):
        self.ss_name = []
        self.spreadsheet_id = ''
        self.data = {}
        self.errors = {}
        

        """
        to use  check_format_ss function:
        errors = format_checker.check_format_ss(data, ss_type)

        """





    def check_format_ss(self, data, ss_name):

        """

        Inputs:
        :param data = JSON with the records to be written in SS:

        example:
        [
            {
            "Operador": "Marcelino Gracia Perez",
            "Correo": "abc@arete.ws",
            "Placas": "139AR5",
            "Licencia": "E",
            "ProveedorFlete": "708054"
            },
            {
            "Operador": "Antonio Garcia Perez",
            "Correo": "operador1@arete.ws",
            "Placas": "139AR5",
            "Licencia": "E",
            "ProveedorFlete": "700776"
            },
            {
            "Operador": "Martin Garcia",
            "Correo": "operador2@arete.ws",
            "Placas": "139AR5",
            "Licencia": "E",
            "ProveedorFlete": "700776"
            }
        ]

        :param ss_name: Name of the SS to be written:
        one of the following:

        'USUARIOS'
        'EMPRESAS'
        'OPERADORES'
        'PEDIDOS'
        'COMPATIBILIDAD'
        'DESTINOS'
        'PREFERENCIAS'
        'VEHICULOS'

        Outputs:

        self.errors = JSON with errors found in the regular expression match

        The JSON has the following fields:
            Column: Field name of the value with error
            ExpectedRegEx: Regular expression expected for the field
            ActualValue: Value tried to be written in the SS
            Row: Row number of the SS

        example (if there is no errors):
       {} =
       example (if there is 3 errors)

      {
       "Errors": [
            {
            "Column": "Licencia",
            "ExpectedRegEx": "^(A|B|C|D|E)$",
            "ActualValue": "F",
            "Row": 7
            },
            {
            "Column": "ProveedorFlete",
            "ExpectedRegEx": "^^([A-Za-z0-9]+)$",
            "ActualValue": "",
            "Row": 7
            },
            {
            "Column": "Placas",
            "ExpectedRegEx": "(^$|^([A-Za-z0-9]+)$)",
            "ActualValue": "!",
            "Row": "9"
            }
        ]
        }

        """



        self.data = data
        self.ss_name = ss_name

        error_ss = {
            'Errors': [
                {
                    'Row': '',
                    'Column': '',
                    'ExpectedRegEx': '',
                    'ActualValue': ''
                }
           ]
        }
        dir = os.path.dirname(__file__)
        print(dir)
        FILE = dir + '/VALIDACION_'+ss_name+'.json'
        print(FILE)
        with open(FILE) as data_file:
            ok = data_file.read()
            okk = json.loads(ok)
            print(okk["Pedido"])
            validation_data = json.loads(ok)

        count_wrong = 0
        count_row = 0
        for row in data:
            count_row = count_row + 1
            for field in row:
                if field in validation_data:
                    print(validation_data[field]['RegEx'])
                    print(row[field])
                    match_object = re.match(str(validation_data[field]['RegEx']), str(row[field]))

                    if not (hasattr(match_object, 'group')):
                        # if count_row < 5:
                            # print(field, row[field], validation_data[field]['RegEx'],  "No cumple")

                        if count_wrong > 0:
                            error_ss['Errors'].append(copy.deepcopy(error_ss['Errors'][count_wrong - 1]))

                        error_ss['Errors'][count_wrong]['Row'] = str(count_row)
                        error_ss['Errors'][count_wrong]['Column'] = field
                        error_ss['Errors'][count_wrong]['ExpectedRegEx'] = validation_data[field]['RegEx']
                        error_ss['Errors'][count_wrong]['ActualValue'] = row[field]
                        count_wrong = count_wrong + 1
                    # else:
                        # if count_row < 5:
                            # print(row[field], validation_data[field]['RegEx'], "Si cumple")

        if count_wrong == 0:
            self.errors = {}
        else:
            self.errors = error_ss

        return self.errors

    def auto_format_ss(self, spreadsheet_id, ss_name):

        self.spreadsheet_id = spreadsheet_id
        self.ss_name = ss_name

        DATAFormat = {'requests': [
            {'repeatCell': {
                'range': {
                    'sheetId': 0,
                    'startRowIndex': 1,
                    'endRowIndex': 1,
                    'startColumnIndex': 1,
                    'endColumnIndex': 1},
                "cell": {
                    "userEnteredFormat": {
                        "numberFormat": {
                            "type": "DATE",
                            "pattern": "hh:mm:ss am/pm, ddd mmm dd yyyy"
                        }
                    }
                },
                "fields": "userEnteredFormat.numberFormat"
            }}
        ]}

        with open('VALIDACION_'+ss_name+'.json') as data_file:
            validation_data = json.load(data_file)

        scopes = 'https://www.googleapis.com/auth/spreadsheets'
        store = file.Storage('storage.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, scopes)
            creds = tools.run_flow(flow, store)
        service = discovery.build('sheets', 'v4', http=creds.authorize(Http()))

        sheet_metadata = service.spreadsheets().get(spreadsheetId=self.spreadsheet_id).execute()
        sheet_handler = sheet_metadata.get('sheets', '')

        for i in range(0, len(sheet_handler) - 1):
            title = sheet_handler[i].get("properties", {}).get("title", '')
            sheet_id = sheet_handler[i].get("properties", {}).get("sheetId", '')

            if title == ss_name:
                break

        result = list(service.spreadsheets().values()).get(
            spreadsheetId=self.spreadsheet_id, range=self.ss_name).execute()
        values = result.get('values', [])

        lastrow = len(values)
        fields_ss = values[0]
        contcol = 0
        countcolformat = 0
        for field in fields_ss:

            print(field)

            contcol = contcol + 1

            if validation_data[field]['Type'] != 'STRING':

                if countcolformat > 0:
                    DATAFormat['requests'].append(copy.deepcopy(DATAFormat['requests'][countcolformat - 1]))

                DATAFormat['requests'][countcolformat]['repeatCell']['range']['sheetId'] = sheet_id
                DATAFormat['requests'][countcolformat]['repeatCell']['range']['endRowIndex'] = lastrow
                DATAFormat['requests'][countcolformat]['repeatCell']['range']['startColumnIndex'] = \
                    contcol - 1
                DATAFormat['requests'][countcolformat]['repeatCell']['range']['endColumnIndex'] = \
                    contcol
                DATAFormat['requests'][countcolformat]['repeatCell']['cell']['userEnteredFormat'] \
                    ['numberFormat']['type'] = validation_data[field]['Type']
                DATAFormat['requests'][countcolformat]['repeatCell']['cell']['userEnteredFormat'] \
                    ['numberFormat']['pattern'] = validation_data[field]['Pattern']
                countcolformat = countcolformat + 1

        if countcolformat > 0:
            service.spreadsheets().batchUpdate(
                spreadsheetId=self.spreadsheet_id, body=DATAFormat).execute()















