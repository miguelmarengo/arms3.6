
import pandas as pd
import config

def ordenar_resultados(dfModificado, completar=False):
    if completar:
        df = pd.DataFrame(columns=config.campos_ssOriginal)
        dfCompleto = pd.merge(df, dfModificado, how='outer')
    else: dfCompleto = dfModificado
    dfCompleto = dfCompleto[config.campos_ssOriginal]

    return dfCompleto