import config
import json

def fnObtenerDestinos():
    completo = 1
    # ------------------------------- #
    if type(config.dfPlan) is int:
        print ("dfPlan vacio")
        completo = 0
    else:
        if config.dfPlan.empty:
            print ("dfPlan vacio")
            completo = 0
        else:
            resultados = json.loads(config.dfPlan.to_json(orient='records'))
    # ------------------------------- #
    if type(config.dfDestinos) is int:
        print ("dfDestinos vacio")
        completo = 0
    else:
        if config.dfDestinos.empty:
            print ("dfDestinos vacio")
            completo = 0
        else:
            destinos = json.loads(config.dfDestinos.to_json(orient='records'))
    # ------------------------------- #
    if type(config.dfCedi) is int:
        print ("dfCedi vacio")
        completo = 0
    else:
        if config.dfCedi.empty:
            print ("dfCedi vacio")
            completo = 0
        else:
            cedi = json.loads(config.dfCedi.to_json(orient='records'))

    if completo == 1:
        respuesta = {}
        respuesta["data"] = []

        for pedido in resultados:
            for destino in destinos:
                # if (pedido["DestinoTR1"] == destino["Destino"] or pedido["CEDISalida"] == destino["Destino"] or pedido["DestinoTR2"] == destino["Destino"]):
                if pedido["DestinoTR1"] == destino["Destino"]:
                    if respuesta["data"]:
                        encontrado = 0
                        for res in respuesta["data"]:
                            if res["iddestino"] == destino["Destino"]:
                                encontrado = 1
                        if encontrado == 0:
                            temp = {
                                "iddestino": destino["Destino"],
                                "latitud": destino["Latitud"],
                                "longitud": destino["Longitud"],
                                "status": destino["RecalcularDestino"]
                            }
                            respuesta["data"].append(temp)
                    else:
                        temp = {
                            "iddestino": destino["Destino"],
                            "latitud": destino["Latitud"],
                            "longitud": destino["Longitud"],
                            "status": destino["RecalcularDestino"]
                        }
                        respuesta["data"].append(temp)
        
        # SOLO PARA EL CEDI
        for destino in destinos:
            if cedi[0]["Destino"] == destino["Destino"]:
                if respuesta["data"]:
                    encontrado = 0
                    for res in respuesta["data"]:
                        if res["iddestino"] == destino["Destino"]:
                            encontrado = 1
                    if encontrado == 0:
                        temp = {
                            "iddestino": destino["Destino"],
                            "latitud": destino["Latitud"],
                            "longitud": destino["Longitud"],
                            "status": destino["RecalcularDestino"]
                        }
                        respuesta["data"].append(temp)
                else:
                    temp = {
                        "iddestino": destino["Destino"],
                        "latitud": destino["Latitud"],
                        "longitud": destino["Longitud"],
                        "status": destino["RecalcularDestino"]
                    }
                    respuesta["data"].append(temp)

        return respuesta
    else:
        return "Algo falló"