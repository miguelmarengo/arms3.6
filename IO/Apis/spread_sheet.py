# -*- coding: utf-8 -*-
import requests
import json
import sys
import os
import gc
import config
import pygsheets
from datetime import datetime
from pygsheets.exceptions import *
import pandas as pd

class SpreadSheet:

    # def AbrirSS(self, id, sheetname, tipo):
    #     start_time = datetime.now()
    #     try:
    #         url = 'https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/leer-spreadsheet'
    #         payload = {"spreadsheetId": id, "sheetName": sheetname, "tipo": tipo}
    #         headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
    #
    #         r = requests.post(url, data=json.dumps(payload), headers=headers)
    #         resultado = r.text
    #         duracion = datetime.now() - start_time
    #         return True, duracion, resultado
    #
    #     except Exception as e:
    #         file_name = os.path.basename(sys.argv[0])
    #         this_function_name = sys._getframe().f_code.co_name
    #         error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
    #         return False, error, False
    #
    # def AbrirSSResultados(self, spreadID, sheetName):
    #     start_time = datetime.now()
    #     print('Obteniendo viajes desde el SSResultados ' + str(spreadID) + ": " + str(datetime.now()))
    #     headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
    #     url = "https://b2uiut18x9.execute-api.us-east-1.amazonaws.com/dev/obtener-viajes"
    #     datos = {"spreadsheetId":spreadID,"sheetName":sheetName,"tipo":"resultados/optimizador"}
    #     try:
    #         r = requests.post(url, data=json.dumps(datos), headers=headers).json()
    #         print("termina lectura de viajes en ssResultados")
    #     except requests.exceptions.HTTPError as err:
    #         print("fallo")
    #         print(err)
    #     duracion = datetime.now() - start_time
    #     print(duracion)
    #     return r


    def abrirSS(self, nombre_ss, nombre_wks, abierto=False, encabezado=False, informa_el_progreso=False, key=False):
        try:
            # TODO: pide autorizacion
            if not abierto:
                config.gcPlan = pygsheets.authorize(service_file='IO/ARMS 2 Legacy-327b3624f992.json')
                if key:
                    config.sheet = config.gcPlan.open_by_key(key)
                else:
                    config.sheet = config.gcPlan.open(nombre_ss)
            # TODO: selecciona el wks
            wks = config.sheet.worksheet_by_title(nombre_wks)
            df = wks.get_as_df()
            # TODO: borra los 4 primeros renglones. Siempre en todos los SS de ARMS
            if not encabezado:
                df.drop(df.index[0:3], inplace=True)
            return True, 0, df
        # ----------------------------------------------------
        except PyGsheetsException as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            error = str(exc_type)
            print(error)
            if error == "<class 'pygsheets.exceptions.SpreadsheetNotFound'>": lblMensaje = 'Spreadsheet {0} no encontrado'.format(nombre_ss)
            if error == "<class 'pygsheets.exceptions.WorksheetNotFound'>": lblMensaje = 'Worksheet  {0} no encontrado'.format(nombre_wks)
            if informa_el_progreso: informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': lblMensaje})
            return False, 0, error


    def listarSS(self):
        start_time = datetime.now()
        destinos = []
        try:
            
            # TODO: pide autorizacion
            config.gc = pygsheets.authorize(outh_file='IO/client_secret.json')
            # TODO: lista los SS
            todas = config.gc.list_ssheets()
            #print(todas)
            for algo in todas:
                #print(algo)

                if 'Destinos' in algo["name"]:
                    destinos.append(algo)
            return destinos
                    
           

            # guarda como json
            # df.to_json('Datos/resultados.json')
            #

        # ----------------------------------------------------
        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
            return False, error, False

    def listarTodosSS(self):
        start_time = datetime.now()
        try:

            # TODO: pide autorizacion
            config.gc = pygsheets.authorize(outh_file='IO/client_secret.json')
            # TODO: lista los SS
            todas = config.gc.list_ssheets()
            return todas

        except Exception as e:
            file_name = os.path.basename(sys.argv[0])
            this_function_name = sys._getframe().f_code.co_name
            error = '{} - {} - {}'.format(file_name, this_function_name, str(e))
            return False, error, False


    def guarda_ss(self, df, nombre_ss, nombre_wks, tipo_nombre_ss='key', borra_wks=False, folder_id=None,nuevo_ss=False, nuevo_wks=False, informa_el_progreso=False):
        # corre cronometro
        start_time = datetime.now()
        try:
            # checa si hubo error...si viene df = False, si lo hubo
            # TODO: pide autorizacion
            gc = pygsheets.authorize(outh_file='client_secret.json')
            # TODO: es un nuevo ss?
            if nuevo_ss == True:
                sheet = gc.create(nombre_ss, parent_id=folder_id)
                wks = sheet.worksheet('index', 0)
            else:
                # TODO: abre el ss
                if tipo_nombre_ss == 'key':
                    self.sheet = gc.open_by_key(nombre_ss)
                elif tipo_nombre_ss == 'url':
                    self.sheet = gc.open_by_url(nombre_ss)
                else:
                    self.sheet = gc.open(nombre_ss)

                # TODO: genera un nuevo wks si fue pedido
                if nuevo_wks == True:
                    self.sheet.add_worksheet(nombre_wks)
                    wks = self.sheet.worksheet_by_title(nombre_wks)
                else:
                    wks = self.sheet.worksheet_by_title(nombre_wks)
            # TODO: mete el df al wks

            # borr wks
            df2 = df.astype(str)
            if (borra_wks == True):
                wks.clear()
                wks.set_dataframe(df2, (1, 1), copy_head=True)
            else:
                wks.set_dataframe(df2, (5, 1), copy_head=False)
            return True
        # ----------------------------------------------------
        except Exception as e:
            print(e)
            if informa_el_progreso: informa_el_progreso.emit({'progressBar': 100, 'lblMensaje': 'Hubo un error en la escritura del '+nombre_wks})
            return False


    def salvarDFtoSS(self, df):
        gc = pygsheets.authorize(outh_file='IO/client_secret.json')

        df = df.tolist()

        #open the google spreadsheet 
        sh = gc.open_by_keyl('1X5XC0d1fEEvpBu_oojrUoKxSNj5JGWkViUHxQXGboCQ')

        #select the first sheet 
        wks = sh[0]

        #update the first sheet with df, starting at cell B2. 
        wks.update_cells(crange='A1',values = df)


    def copiarPlantilla(self, destino):
        headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/copiar-plantilla"
        datos = {
                  "id_plantilla": config.ss_resultados_template,
                  "nombre_archivo": config.nombrePlan,
                  "folder_destino": destino
                }
        try:
            r = requests.post(url, data=json.dumps(datos), headers=headers).json()
            if r['status']:
                return r['result']
            else: return False
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            print(error)
            return False


    def obtenerLista(self):
        headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}
        url = "https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/listar-ss"
        datos = {
                    "tipo": "todo",
                    "folder_cedi":config.usuario["folderCedi"],
                    "folder_origen":config.usuario["folderOrigen"],
                    "folder_resultados":config.usuario["folderResultados"]
                }
        try:
            r = requests.post(url, data=json.dumps(datos), headers=headers).json()
            return True, r
        # ----------------------------------------------------
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
            #config.b('**** ERROR **** {}'.format(error))
            print(error)
            return False, 0, 0


