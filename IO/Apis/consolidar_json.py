# coding: utf-8
import requests
import json
from datetime import datetime, timedelta
from time import gmtime, strftime
from random import randint
import time
import config
from pathlib import Path

"""
    @desc: Clase para hacer el JSON-ZOTE
    @author: Eduardo Giles
"""

class ConsolidarJson:
    def getJson(self):
        completo = 1
        # ------------------------------- #
        if type(config.dfPlan) is int:
            print ("dfPlan vacio")
            completo = 0
        else:
            if config.dfPlan.empty:
                print ("dfPlan vacio")
                completo = 0
            else:
                resultados = json.loads(config.dfPlan.to_json(orient='records'))
                for res in resultados:
                    if res["RecolectaCEDI2"] == "Sí" or res["RecolectaCEDI2"] == "Si":
                        res["RecolectaCEDI2"] = True
                    else:
                        res["RecolectaCEDI2"] = False
        # ------------------------------- #
        if type(config.dfDestinos) is int:
            print ("dfDestinos vacio")
            completo = 0
        else:
            if config.dfDestinos.empty:
                print ("dfDestinos vacio")
                completo = 0
            else:
                destinos = json.loads(config.dfDestinos.to_json(orient='records'))
                for destino in destinos:
                    if destino["L"] == "Sí" or destino["L"] == "Si":
                        destino["L"] = True
                    else:
                        destino["L"] = False
                    if destino["M"] == "Sí" or destino["M"] == "Si":
                        destino["M"] = True
                    else:
                        destino["M"] = False
                    if destino["Mi"] == "Sí" or destino["Mi"] == "Si":
                        destino["Mi"] = True
                    else:
                        destino["Mi"] = False
                    if destino["J"] == "Sí" or destino["J"] == "Si":
                        destino["J"] = True
                    else:
                        destino["J"] = False
                    if destino["V"] == "Sí" or destino["V"] == "Si":
                        destino["V"] = True
                    else:
                        destino["V"] = False
                    if destino["S"] == "Sí" or destino["S"] == "Si":
                        destino["S"] = True
                    else:
                        destino["S"] = False
                    if destino["D"] == "Sí" or destino["D"] == "Si":
                        destino["D"] = True
                    else:
                        destino["D"] = False
        # ------------------------------- #
        if type(config.dfVehiculos) is int:
            print ("dfVehiculos vacio")
            completo = 0
        else:
            if config.dfVehiculos.empty:
                print ("dfVehiculos vacio")
                completo = 0
            else:
                vehiculos = json.loads(config.dfVehiculos.to_json(orient='records'))
                # print ("dfVehiculos")
                # print (config.dfVehiculos)
                # SACA EL PROVEEDOR PREFERENTE
                proveedor = str(vehiculos[0]["ProveedorFlete"])
                # VERIFICA DISPONIBILIDAD Y CAMBIA LOS INTEGER EN STRINGS
                unidadesDisponibles = list()
                for vehiculo in vehiculos:
                    if vehiculo["Disponibilidad"] == "Disponible":
                        del vehiculo["ProveedorFlete"]
                        del vehiculo["Placas"]
                        vehiculo["PesoPermitido"] = str(vehiculo["PesoPermitido"])
                        vehiculo["VolumenPermitido"] = str(vehiculo["VolumenPermitido"])
                        vehiculo["Rendimiento"] = str(vehiculo["Rendimiento"])
                        vehiculo["HuellaCarbono"] = str(vehiculo["HuellaCarbono"])
                        vehiculo["Ejes"] = str(vehiculo["Ejes"])
                        vehiculo["PesoMax"] = str(vehiculo["PesoMax"])
                        vehiculo["VolumenMax"] = str(vehiculo["VolumenMax"])
                        vehiculo["PrioridadVehiculo"] = str(vehiculo["PrioridadVehiculo"])
                        vehiculo["Proveedor"] = proveedor
                        if vehiculo["Refrigerado"] == "Sí" or vehiculo["Refrigerado"] == "Si":
                            vehiculo["Refrigerado"] = True
                        else:
                            vehiculo["Refrigerado"] = False
                        unidadesDisponibles.append(vehiculo)
        # ------------------------------- #
        if type(config.dfCedi) is int:
            print ("dfCedi vacio")
            completo = 0
        else:
            if config.dfCedi.empty:
                print ("dfCedi vacio")
                completo = 0
            else:
                cedi = json.loads(config.dfCedi.to_json(orient='records'))
        # ------------------------------- #
        if type(config.dfConfiguracion) is int:
            print ("dfConfiguracion vacio")
            completo = 0
        else:
            if config.dfConfiguracion.empty:
                print ("dfConfiguracion vacio")
                completo = 0
            else:
                configuracion = json.loads(config.dfConfiguracion.to_json(orient='records'))
                for element in configuracion:
                    pass
                    if element["FacVolumen"] == "Sí" or element["FacVolumen"] == "Si":
                        element["FacVolumen"] = True
                    else:
                        element["FacVolumen"] = False
                    if element["FacPeso"] == "Sí" or element["FacPeso"] == "Si":
                        element["FacPeso"] = True
                    else:
                        element["FacPeso"] = False
                    if element["FacValor"] == "Sí" or element["FacValor"] == "Si":
                        element["FacValor"] = True
                    else:
                        element["FacValor"] = False
                    if element["FacVentanaEntrega"] == "Sí" or element["FacVentanaEntrega"] == "Si":
                        element["FacVentanaEntrega"] = True
                    else:
                        element["FacVentanaEntrega"] = False
                    if element["FacRestriccionVolumen"] == "Sí" or element["FacRestriccionVolumen"] == "Si":
                        element["FacRestriccionVolumen"] = True
                    else:
                        element["FacRestriccionVolumen"] = False
                    if element["FacCompatibilidad"] == "Sí" or element["FacCompatibilidad"] == "Si":
                        element["FacCompatibilidad"] = True
                    else:
                        element["FacCompatibilidad"] = False
                    if element["FacHoteles"] == "Sí" or element["FacHoteles"] == "Si":
                        element["FacHoteles"] = True
                    else:
                        element["FacHoteles"] = False
                    if element["FacDescansos"] == "Sí" or element["FacDescansos"] == "Si":
                        element["FacDescansos"] = True
                    else:
                        element["FacDescansos"] = False
                    if element["FacLocal"] == "Sí" or element["FacLocal"] == "Si":
                        element["FacLocal"] = True
                    else:
                        element["FacLocal"] = False
                    if element["FacComidas"] == "Sí" or element["FacComidas"] == "Si":
                        element["FacComidas"] = True
                    else:
                        element["FacComidas"] = False
                    if element["FacOtros"] == "Sí" or element["FacOtros"] == "Si":
                        element["FacOtros"] = True
                    else:
                        element["FacOtros"] = False
        # ------------------------------- #
        if type(config.dfCompatibilidad) is int:
            print ("dfCompatibilidad vacio")
            completo = 0
        else:
            if config.dfCompatibilidad.empty:
                print ("dfCompatibilidad vacio")
                completo = 0
            else:
                compatibilidad = json.loads(config.dfCompatibilidad.to_json(orient='records'))
                for element in compatibilidad:
                    element['ClaveProducto1'] = str(element['ClaveProducto1'])
                    element['ClaveProducto2'] = str(element['ClaveProducto2'])
        # ------------------------------- #
        if type(config.dfTarifas) is int:
            print ("dfTarifas vacio")
            completo = 0
        else:
            if config.dfTarifas.empty:
                print ("dfTarifas vacio")
                completo = 0
            else:
                tarifas = json.loads(config.dfTarifas.to_json(orient='records'))
                tarifasRes = list()
                # print ("-------")
                # print (json.dumps(tarifas, indent=4))
                # print ("-------")
                # OBTIENE LOS VALORES DE TARIFAS Y LOS FORMATEA
                for index, element in enumerate(tarifas):
                    if index == 0:
                        tarifasRes.append({"repartosincluidos": str(element["RangoInferior"])})
                        tarifasRes[0]["data"] = list()
                    elif index == 1:
                        tarifasRes[0]["repartoadicional"] = str(element["RangoInferior"])
                    elif index == 2:
                        pass
                    else:
                        # CAMBIA LOS INTEGER EN STRINGS
                        element["RangoInferior"] = str(element["RangoInferior"])
                        element["RangoSuperior"] = str(element["RangoSuperior"])
                        element["6"] = str(element["6"])
                        element["12"] = str(element["12"])
                        element["18"] = str(element["18"])
                        element["28"] = str(element["28"])
                        element["30"] = str(element["30"])
                        element["90"] = str(element["90"])
                        tarifasRes[0]["data"].append(element)
        # ------------------------------- #
        if completo == 1:
            # AGRUPACION
            pedidosGlobal = list()
            destinosGlobal = list()
            errores = list()

            for index, value in enumerate(sorted(resultados, key=lambda x: float(x['Volumen']), reverse=True)):
                falta = []
                agrupacionPedidos = list()
                restriccionVehiculo = ""
                encontrado = 0

                # print (json.dumps(value, indent=4))
                # value[]
                # print ("******")

                for val in destinos: # LLENA DESTINOS CON EL ID QUE HAY EN PEDIDOS
                    if value['DestinoTR1'] == val['Destino']:
                        restriccionVehiculo = val['RestriccionVolumen'] # ESTO ES DEL DESTINO
                        encontrado = 1
                        yacontado = 0
                        pesoMax = 0.0
                        volMax = 0.0
                        sumaVolumen = 0
                        sumaPeso = 0
                        sumaValor = 0
                        compatible = 0
                        resta = 0
                        temp = 0

                        for unidad in unidadesDisponibles:
                            if float(unidad['VolumenMax']) <= float(restriccionVehiculo):
                                resta = float(restriccionVehiculo) - float(unidad['VolumenMax'])
                            if resta == 0: # SI LA RESTA ES 0 ESTABLECE LOS VALORES MAXIMOS Y CORTA LA INSTRUCCION
                                pesoMax = float(unidad['PesoPermitido'])
                                volMax = float(unidad['VolumenPermitido'])
                                break # CORTO LA INSTRUCCION PARA QUE TOME LOS VALORES DE LA PRIMER COINCIDENCIA (TODAS LAS DEMAS COINCIDENCIAS SON IGUALES)
                            else: # LA RESTA NO HA DADO 0, AUN SE DEBE VERIFICAR EL VALOR MAS CERCANO
                                if temp == 0: # SE INICIALIZA CON EL VALOR QUE TENGA resta
                                    temp = resta
                                    # SE ESTABLECEN LOS VALORES AQUI POR SI SOLO HAY 1 VEHICULO Y LA RESTA NO DA EXACTA, 
                                    pesoMax = float(unidad['PesoPermitido'])
                                    volMax = float(unidad['VolumenPermitido'])
                                else: # TEMP YA NO VALE 0
                                    if resta <= temp: # SI LA RESTA ES MENOR O IGUAL, TEMP SE QUEDA CON EL VALOR MINIMO
                                        temp = resta
                                        # LOS VALORES MAXIMOS SE ESTABLECEN CON EL MINIMO ACTUAL
                                        pesoMax = float(unidad['PesoPermitido'])
                                        volMax = float(unidad['VolumenPermitido'])

                        if pedidosGlobal: # EL ARRAY DE PEDIDOS NO ESTA VACIO, HAY QUE VERIFICAR QUE SE PUEDA AGRUPAR
                            yaTieneDestino = 0
                            for pedidoArray in pedidosGlobal: # SE CORRE ESTE FOR PARA VER SI SE PUEDE AGRUPAR EN UNO YA EXISTENTE, ESTE ES EL ARRAY GLOBAL (PRACTICAMENTE UN DESTINO)
                                # INICIALIZA LA SUMA DE VOLUMEN Y PESO CON LOS DATOS DEL PEDIDO CANDIDATO
                                sumaVolumen = float(value['Volumen']) # EL VOLUMEN DEL PEDIDO CANDIDATO
                                sumaPeso = float(value['Peso']) # EL PESO DEL PEDIDO CANDIDATO
                                sumaValor = float(value['Valor']) # EL VALOR DEL PEDIDO CANDIDATO
                                
                                """ TODO ESTO HAY EN UN SOLO NODO COMO SI FUERA EL DESTINO """
                                for pedido in pedidoArray: # ESTOS SON LOS ELEMENTOS DE UNA POSICION DEL ARRAY GLOBAL
                                    if value['DestinoTR1'] == pedido['DestinoTR1']: # VERIFICA QUE EL DESTINO CANDIDATO Y EL DEL ARRAY ACTUAL SEA EL MISMO
                                        # INICIALIZA LA SUMA DE VOLUMEN Y PESO CON LOS DATOS DEL PEDIDO CANDIDATO
                                        fechaIni1 = value['VentanaFechaInicioPedido']
                                        fechaIni2 = pedido['VentanaFechaInicioPedido']
                                        fechaFin1 = value['VentanaFechaFinPedido']
                                        fechaFin2 = pedido['VentanaFechaFinPedido']
                                        horaIni1 = value['VentanaHoraInicioPedido']
                                        horaIni2 = pedido['VentanaHoraInicioPedido']
                                        horaFin1 = value['VentanaHoraFinPedido']
                                        horaFin2 = pedido['VentanaHoraFinPedido']

                                        # SI LAS HORAS VIENEN EN CEROS, HEREDAN LA HORA INICIO Y FIN DEL DESTINO
                                        if horaIni1 == "00:00":
                                            horaIni1 = val['VentanaInicioDestino']
                                        if horaIni2 == "00:00":
                                            horaIni2 = val['VentanaInicioDestino']
                                        if horaFin1 == "23:59":
                                            horaFin1 = val['VentanaFinDestino']
                                        if horaFin2 == "23:59":
                                            horaFin2 = val['VentanaFinDestino']
                                        
                                        if fechaIni1 == fechaIni2 and fechaFin1 == fechaFin2 and horaIni1 == horaIni2 and horaFin1 == horaFin2: # VERIFICA FECHAS
                                            yacontado = 1 # SE ACTUALIZA PARA QUE NO ENTRE A ABRIR UN NUEVO ELEMENTO
                                            compatible = 1 # INICIALIZA LA COMPATIBILIDAD
                                            for comp in compatibilidad: # OBTIENE EL VALOR DE LA COMPATIBILIDAD (SI / NO)
                                                if comp["ClaveProducto1"] == value["Productos"] and comp["ClaveProducto2"] == pedido["Productos"]:
                                                    if comp["ValorCompatibilidad"] == "NO":
                                                        compatible = 0 # NO ES COMPATIBLE CON ESTE PRODUCTO QUE YA ESTA AGREGADO A ESTE DESTINO
                                            if compatible == 0: # YA VI QUE NO ES COMPATIBLE CON UN PEDIDO, ASI QUE NO SE PUEDE METER AHI
                                                pass
                                            else: # SI ES COMPATIBLE, SIGUE VERIFICAR VOLUMENES
                                                sumaVolumen += float(pedido['Volumen']) # SE LE SUMA EL VOLUMEN DE LOS DEMAS PEDIDOS DE LA MISMA AGRUPACION
                                                sumaPeso += float(pedido['Peso']) # SE LE SUMA EL PESO DE LOS DEMAS PEDIDOS DE LA MISMA AGRUPACION
                                                sumaValor += float(pedido['Valor']) # SE LE SUMA EL PESO DE LOS DEMAS PEDIDOS DE LA MISMA AGRUPACION
                                """ TODO ESTO HAY EN UN SOLO NODO COMO SI FUERA EL DESTINO """
                                # FIN DEL CICLO DE CADA ELEMENTO EN EL ARRAY
                                if sumaVolumen <= volMax and sumaPeso <= pesoMax and compatible == 1 and yacontado == 1: # SI LA SUMA DE VOLUMENES Y PESOS ES MENOR A LOS MAXIMOS, CONTINUA
                                    pedidoArray.append(value)
                                    yaTieneDestino = 1
                                    break # SALE DEL FOR DE pedidosGlobal
                                else: # NO SALE DEL FRO GLOBAL PARA VER SI SE PUEDE ACOMODAR EN OTRO NODO DE pedidosGlobal
                                    yacontado = 0
                            # AQUI LLEGA DESPUES DEL BREAK O CUANDO YA SE ACABARON LOS ELEMENTOS EN pedidosGlobal
                        if yacontado == 0:
                            agrupacionPedidos.append(value)
                            variableCompleta = {"Pedidos": agrupacionPedidos}
                            pedidosGlobal.append(agrupacionPedidos)
                            # CADA QUE ENTRA AQUI, SE CREA UN DESTINO NUEVO
                            destinoNuevo = val
                            destinosGlobal.append(destinoNuevo)
                if encontrado == 0:
                    falta.append("No se encuentra el destino " + value['DestinoTR1'] + " de este pedido")
                    errores.append({"row": index, "falta": falta})
            if errores:
                data = dict()
                data["status"] = False
                data["data"] = errores

                return data
            else:
                # print ("destinos / pedidos")
                # resultado = self.acomodarPedidosEnDestinos(destinosGlobal, pedidosGlobal)
                resultadoAcomodado = self.acomodarPedidosEnDestinos(destinosGlobal, pedidosGlobal)
                
                # print ("-----")
                # print (json.dumps(resultadoAcomodado, indent=4))
                # print ("-----")

                jsonZote = self.makeJson(resultadoAcomodado, unidadesDisponibles, cedi, configuracion, compatibilidad, tarifasRes, proveedor, 1)

                # with open('data/arms/planes/' + config.nombrePlan + '.json', 'w') as outfile:
                with open('data/arms/planes/plan.json', 'w') as outfile:
                    json.dump(jsonZote, outfile)

                data = dict()
                data["status"] = True
                data["data"] = jsonZote
                    
                return data
    
    def getJsonPorViaje(self, df, tipo):
        # ------------------------------- #
        resultados = json.loads(df.to_json(orient='records'))
        # ------------------------------- #
        destinos = json.loads(config.dfDestinos.to_json(orient='records'))
        # ------------------------------- #
        vehiculos = json.loads(config.dfVehiculos.to_json(orient='records'))
        # SACA EL PROVEEDOR PREFERENTE
        proveedor = str(vehiculos[0]["ProveedorFlete"])
        # VERIFICA DISPONIBILIDAD Y CAMBIA LOS INTEGER EN STRINGS
        unidadesDisponibles = list()
        for vehiculo in vehiculos:
            if vehiculo["Disponibilidad"] == "Disponible":
                del vehiculo["ProveedorFlete"]
                del vehiculo["Placas"]
                vehiculo["PesoPermitido"] = str(vehiculo["PesoPermitido"])
                vehiculo["VolumenPermitido"] = str(vehiculo["VolumenPermitido"])
                vehiculo["Rendimiento"] = str(vehiculo["Rendimiento"])
                vehiculo["HuellaCarbono"] = str(vehiculo["HuellaCarbono"])
                vehiculo["Ejes"] = str(vehiculo["Ejes"])
                vehiculo["PesoMax"] = str(vehiculo["PesoMax"])
                vehiculo["VolumenMax"] = str(vehiculo["VolumenMax"])
                vehiculo["PrioridadVehiculo"] = str(vehiculo["PrioridadVehiculo"])
                vehiculo["Proveedor"] = str(vehiculo["Proveedor"])
                unidadesDisponibles.append(vehiculo)
        # ------------------------------- #
        cedi = json.loads(config.dfCedi.to_json(orient='records'))
        # ------------------------------- #
        configuracion = json.loads(config.dfConfiguracion.to_json(orient='records'))
        # ------------------------------- #
        compatibilidad = json.loads(config.dfCompatibilidad.to_json(orient='records'))
        # CAMBIA LOS INTEGER EN STRINGS
        for element in compatibilidad:
            element['ClaveProducto1'] = str(element['ClaveProducto1'])
            element['ClaveProducto2'] = str(element['ClaveProducto2'])
        # ------------------------------- #
        tarifas = json.loads(config.dfTarifas.to_json(orient='records'))
        tarifasRes = list()
        # OBTIENE LOS VALORES DE TARIFAS Y LOS FORMATEA
        for index, element in enumerate(tarifas):
            if index == 0:
                tarifasRes.append({"repartosincluidos": str(element["RangoInferior"])})
                tarifasRes[0]["data"] = list()
            elif index == 1:
                tarifasRes[0]["repartoadicional"] = str(element["RangoInferior"])
            elif index == 2:
                pass
            else:
                # CAMBIA LOS INTEGER EN STRINGS
                element["RangoInferior"] = str(element["RangoInferior"])
                element["RangoSuperior"] = str(element["RangoSuperior"])
                element["6"] = str(element["6"])
                element["12"] = str(element["12"])
                element["18"] = str(element["18"])
                element["28"] = str(element["28"])
                element["30"] = str(element["30"])
                element["90"] = str(element["90"])
                tarifasRes[0]["data"].append(element)
        # ------------------------------- #
        
        # AGRUPACION

        pedidosPorViaje = list()

        for pedido in resultados:
            agrupacionPedidos = list()
            if pedidosPorViaje: # SI LA VARIABLE TIENE ALGUN PEDIDO, SI NO LO AGREGA AUTOMATICAMENTE
                agrupado = 0
                for pedidosArray in pedidosPorViaje:
                    agregar = 0
                    for pedidoIndividual in pedidosArray:
                        if pedidoIndividual["Viaje"] == pedido["Viaje"]:
                            agregar = 1
                    if agregar == 1:
                        pedidosArray.append(pedido)
                        agrupado = 1
                        break
                if agrupado == 0: # NO LO METIO A ALGUN ARRAY ENTONCES ABRE OTRO
                    agrupacionPedidos.append(pedido)
                    pedidosPorViaje.append(agrupacionPedidos)
            else: # PRIMER ITERACION
                agrupacionPedidos.append(pedido)
                pedidosPorViaje.append(agrupacionPedidos)
        
        viajes = list()
        for pedidosPV in pedidosPorViaje:
            pedidosGlobal = list()
            destinosGlobal = list()

            for pedido in pedidosPV:
                agrupacionPedidos = list()
                if pedidosGlobal:
                    agrupado = 0
                    for pedidosArray in pedidosGlobal:
                        agregar = 0
                        for pedidoIndividual in pedidosArray:
                            if pedidoIndividual["DestinoTR1"] == pedido["DestinoTR1"]:
                                agregar = 1
                        if agregar == 1:
                            pedidosArray.append(pedido)
                            agrupado = 1
                            break
                    if agrupado == 0:
                        agrupacionPedidos.append(pedido)
                        pedidosGlobal.append(agrupacionPedidos)
                        for destino in destinos:
                            if pedido["DestinoTR1"] == destino["Destino"]:
                                destinosGlobal.append(destino)
                                break
                    
                else:
                    agrupacionPedidos.append(pedido)
                    pedidosGlobal.append(agrupacionPedidos)
                    for destino in destinos:
                        if pedido["DestinoTR1"] == destino["Destino"]:
                            destinosGlobal.append(destino)
                            break

            resultadoAcomodado = self.acomodarPedidosEnDestinos(destinosGlobal, pedidosGlobal)
            # print ("----")
            # print (json.dumps(resultadoAcomodado))
            # SELECCIONA UN SOLO VEHICULO, PRIMERO HAY QUE SACAR LA RESTRICCION MAS PEQUEÑA
            volumenMax = 0.0
            for index, res in enumerate(resultadoAcomodado):
                if index == 0: # EL PRIMER INDICE LO PONE POR DEFAULT
                    volumenMax = float(res["RestriccionVolumen"])
                else:
                    if float(res["RestriccionVolumen"]) < volumenMax: # SI EL VOLUMEN MAXIMO DEL DESTINO ACTUAL ES MENOR QUE EL GUARDADO, ESCOGE EL ACTUAL
                        volumenMax = float(res["RestriccionVolumen"])

            # print (volumenMax)

            unidadSeleccionadaTemp = list()
            for unidad in unidadesDisponibles:
                # print (json.dumps(unidad))
                if float(unidad["VolumenPermitido"]) == volumenMax: # SI ENCUENTRA UNA UNIDAD QUE TENGA EL MISMO VALOR, SELECCIONA ESA Y SALE
                    unidadSeleccionadaTemp = unidad
                    break
                else:
                    if float(unidad["VolumenPermitido"]) < volumenMax: # NO ES IGUAL, SI LA UNIDAD ES MENOR AL VOLUMENMAX
                        if unidadSeleccionadaTemp: # SI YA HAY UNA UNIDAD SELECCIONADA, VERIFICA QUE LA UNIDAD ACTUAL SEA MAYOR QUE LA SELECCIONADA (PARA QUE LE QUEPA MAS)
                            if float(unidad["VolumenPermitido"]) > float(unidadSeleccionadaTemp["VolumenPermitido"]):
                                unidadSeleccionadaTemp = unidad
                        else: # NO HAY UNIDAD SELECCIONADA, ASIGNA ESTA UNIDAD QUE ES LA PRIMERA QUE ENTRO
                            unidadSeleccionadaTemp = unidad
            # print ("----")
            unidadSeleccionada = [unidadSeleccionadaTemp]
            jsonZote = self.makeJson(resultadoAcomodado, unidadSeleccionada, cedi, configuracion, compatibilidad, tarifasRes, proveedor, tipo)
            viajes.append(jsonZote)

        return viajes

    def acomodarPedidosEnDestinos(self, destinos, pedidos):
        acomodado = []
        for index, destino in enumerate(destinos):
            PesoAcumulado = 0
            VolumenAcumulado = 0
            ValorAcumulado = 0
            TiempoServicioAcumulado = int(float(destinos[index]['TiemServicio']))

            for pedido in pedidos[index]:
                PesoAcumulado += float(pedido['Peso'])
                VolumenAcumulado += float(pedido['Volumen'])
                ValorAcumulado += float(pedido['Valor'])
                TiempoServicioAcumulado += int(float(pedido['TiemDescarga']))
                if pedido["FechaEntregaPedido"]:
                    pedido["FechaEntregaPedido"] = pedido["FechaEntregaPedido"].replace('-', '/')
                if pedido["FechaSalidaPedido"]:
                    pedido["FechaSalidaPedido"] = pedido["FechaSalidaPedido"].replace('-', '/')
                if pedido["VentanaFechaInicioPedido"]:
                    pedido["VentanaFechaInicioPedido"] = pedido["VentanaFechaInicioPedido"].replace('-', '/')
                if pedido["VentanaFechaFinPedido"]:
                    pedido["VentanaFechaFinPedido"] = pedido["VentanaFechaFinPedido"].replace('-', '/')
                if pedido["FechaEjecucionMotor"]:
                    pedido["FechaEjecucionMotor"] = pedido["FechaEjecucionMotor"].replace('-', '/')
                if pedido["FechaEjecucionOpti"]:
                    pedido["FechaEjecucionOpti"] = pedido["FechaEjecucionOpti"].replace('-', '/')
                if pedido["FechaEjecucionAsigna"]:
                    pedido["FechaEjecucionAsigna"] = pedido["FechaEjecucionAsigna"].replace('-', '/')

            temp = {"PesoAcumulado": str(PesoAcumulado), "VolumenAcumulado": str(VolumenAcumulado), "ValorAcumulado": str(ValorAcumulado), "TiempoServicioAcumulado": str(TiempoServicioAcumulado),"pedidos": pedidos[index]}
            temp.update(destinos[index])
            acomodado.append(temp)

        # CAMBIA LOS INTEGER EN STRINGS DE DESTINOS
        for item in acomodado:
            item["CodigoPostal"] = str(item["CodigoPostal"])
            item["Subregion"] = str(item["Subregion"])
            item["Latitud"] = str(item["Latitud"])
            item["Longitud"] = str(item["Longitud"])
            item["RestriccionVolumen"] = str(item["RestriccionVolumen"])
            item["Dedicado"] = str(item["Dedicado"])
            item["DesfaseHorario"] = str(item["DesfaseHorario"])
            item["TiemServicio"] = str(item["TiemServicio"])
            item["Telefono1"] = str(item["Telefono1"])
            item["Telefono2"] = str(item["Telefono2"])
            for pedido in item["pedidos"]:
                pedido["TiemDescarga"] = str(pedido["TiemDescarga"])
                pedido["Volumen"] = str(pedido["Volumen"])
                pedido["Peso"] = str(pedido["Peso"])
                pedido["Piezas"] = str(pedido["Piezas"])
                pedido["Valor"] = str(pedido["Valor"])
                pedido["Productos"] = str(pedido["Productos"])


        return acomodado

    def makeJson(self,destinos, vehiculos, cedi, configuracion, compatibilidad, tarifas, proveedor, tipo):
        # print (config.usuario)
        path = str(Path.home())
        pathFolder = path+"/apps/arms/SILODISA/SILODISA/arcos"
        pathFile   = pathFolder+"/arcos.txt"

        data = dict()
        data["nombreusuario"] = config.slot_editUsuario
        data["fechaActual"] = strftime("%Y/%m/%d %H:%M", gmtime())
        data["compatibilidad"] = compatibilidad
        data["proveedor"] = proveedor
        data["destinos"] = destinos
        data["cedi"] = cedi
        data["unidades"] = vehiculos
        data["tarifas"] = tarifas
        configuracion[0]["IdEmpresa"] = config.empresa
        data["configuracion"] = configuracion
        data["keyarcos"] = pathFile
        data["nombreSS"] = config.nombrePlan
        if tipo == 1:
            data["tipoOptimizacion"] = "completa" # Cuando es una ejecución normal del motor
        elif tipo == 2:
            data["tipoOptimizacion"] = "optimizacion" # Cuando en el optimizador mandas todo un viaje y te *mueve* de orden las tiradas
        elif tipo == 3:
            data["tipoOptimizacion"] = "recalculo" # Cuando en el optimizador mandas todo un viaje y te *respeta* el orden de las tiradas

        # print (json.dumps(vehiculos, indent=4))

        return data

    def jsonParaDB(self):
        # def getJson(self):
        # ------------------------------- #

        resultados = json.loads(config.dfPlan.to_json(orient='records'))
        destinos = json.loads(config.dfDestinos.to_json(orient='records'))
        vehiculos = json.loads(config.dfVehiculos.to_json(orient='records'))
        cedi = json.loads(config.dfCedi.to_json(orient='records'))

        # ------------------------------- #

        # AGRUPACION

        pedidosPorViaje = list()

        for pedido in resultados:
            agrupacionPedidos = list()
            if pedidosPorViaje:  # SI LA VARIABLE TIENE ALGUN PEDIDO, SI NO LO AGREGA AUTOMATICAMENTE
                agrupado = 0
                for pedidosArray in pedidosPorViaje:
                    agregar = 0
                    for pedidoIndividual in pedidosArray:
                        if pedidoIndividual["Viaje"] == pedido["Viaje"]:
                            agregar = 1
                    if agregar == 1:
                        pedidosArray.append(pedido)
                        agrupado = 1
                        break
                if agrupado == 0:  # NO LO METIO A ALGUN ARRAY ENTONCES ABRE OTRO
                    agrupacionPedidos.append(pedido)
                    pedidosPorViaje.append(agrupacionPedidos)
            else:  # PRIMER ITERACION
                agrupacionPedidos.append(pedido)
                pedidosPorViaje.append(agrupacionPedidos)

        viajes = list()
        for pedidosPV in pedidosPorViaje:
            pedidosGlobal = list()
            destinosGlobal = list()

            for pedido in pedidosPV:
                agrupacionPedidos = list()
                if pedidosGlobal:
                    agrupado = 0
                    for pedidosArray in pedidosGlobal:
                        agregar = 0
                        for pedidoIndividual in pedidosArray:
                            if pedidoIndividual["DestinoTR1"] == pedido["DestinoTR1"]:
                                agregar = 1
                        if agregar == 1:
                            pedidosArray.append(pedido)
                            agrupado = 1
                            break
                    if agrupado == 0:
                        agrupacionPedidos.append(pedido)
                        pedidosGlobal.append(agrupacionPedidos)
                        for destino in destinos:
                            if pedido["DestinoTR1"] == destino["Destino"]:
                                destinosGlobal.append(destino)
                                break

                else:
                    agrupacionPedidos.append(pedido)
                    pedidosGlobal.append(agrupacionPedidos)
                    for destino in destinos:
                        if pedido["DestinoTR1"] == destino["Destino"]:
                            destinosGlobal.append(destino)
                            break

            resultadoAcomodado = self.acomodarPedidosEnDestinos(destinosGlobal, pedidosGlobal)
            jsonZote = self.makeJsonDB(resultadoAcomodado)
            viajes.append(jsonZote)
        # Para asignar los datos de los destinos y viajes
        for viaje in viajes:
            NumComidasV = 0
            NumDescansosV = 0
            NumHotelesV = 0
            for destinos in viaje['destinos']:
                destinos['NumComidas'] = destinos['pedidos'][0]['NumComidas']
                destinos['NumDescansos'] = destinos['pedidos'][0]['NumDescansos']
                destinos['NumHoteles'] = destinos['pedidos'][0]['NumHoteles']
                destinos['FechaEntregaPedido'] = destinos['pedidos'][0]['FechaEntregaPedido']
                destinos['FechaSalidaPedido'] = destinos['pedidos'][0]['FechaSalidaPedido']
                destinos['Tirada'] = destinos['pedidos'][0]['Tirada']
                destinos['Cita'] = destinos['pedidos'][0]['Cita']
                destinos['Inicio'] = destinos['VentanaInicioDestino']
                destinos['Fin'] = destinos['VentanaFinDestino']
                NumComidasV = NumComidasV + destinos['NumComidas']
                NumDescansosV = NumDescansosV + destinos['NumDescansos']
                NumHotelesV = NumHotelesV + destinos['NumHoteles']
            viaje['NumComidas'] = NumComidasV
            viaje['NumDescansos'] = NumDescansosV
            viaje['NumHoteles'] = NumHotelesV
            viaje['Viaje'] = viaje['destinos'][0]['pedidos'][0]['Viaje']
            viaje['Km'] = viaje['destinos'][0]['pedidos'][0]['Km']
            viaje['Refrigerado'] = viaje['destinos'][0]['pedidos'][0]['Refrigerado']
            viaje['FechaSalida'] = viaje['destinos'][0]['pedidos'][0]['FechaSalida']
            viaje['FechaRetorno'] = viaje['destinos'][0]['pedidos'][0]['FechaRetorno']
            viaje['Co2'] = viaje['destinos'][0]['pedidos'][0]['Co2']
            viaje['CostoProveedorFlete'] = viaje['destinos'][0]['pedidos'][0]['CostoProveedorFlete']
            viaje['CostoFerry'] = viaje['destinos'][0]['pedidos'][0]['CostoFerry']
            viaje['CostoHotel'] = viaje['destinos'][0]['pedidos'][0]['CostoHotel']
            viaje['CostoOtros'] = viaje['destinos'][0]['pedidos'][0]['CostoOtros']
            viaje['CostoComidas'] = viaje['destinos'][0]['pedidos'][0]['CostoComidas']
            viaje['CostoThermo'] = viaje['destinos'][0]['pedidos'][0]['CostoThermo']
            viaje['CostoCasetas'] = viaje['destinos'][0]['pedidos'][0]['CostoCasetas']
            viaje['CostoCombustible'] = viaje['destinos'][0]['pedidos'][0]['CostoCombustible']
            placas = viaje['destinos'][0]['pedidos'][0]['Placas']
            viaje['propiedadunidad'] = [{}]
            viaje['propiedadunidad'][0]['Operador'] = viaje['destinos'][0]['pedidos'][0]['Operador']
            viaje['propiedadunidad'][0]['Placas'] = viaje['destinos'][0]['pedidos'][0]['Placas']
            viaje['propiedadunidad'][0]['TipoVehiculo'] = viaje['destinos'][0]['pedidos'][0]['TipoVehiculo']
            viaje['propiedadunidad'][0]['VolumenPermitido'] = viaje['destinos'][0]['pedidos'][0]['VolumenPermitido']
            viaje['propiedadunidad'][0]['PesoPermitido'] = viaje['destinos'][0]['pedidos'][0]['PesoPermitido']
            viaje['propiedadunidad'][0]['CostoCombustible'] = viaje['destinos'][0]['pedidos'][0]['CostoCombustible']
            viaje['propiedadunidad'][0]['ProveedorFlete'] = viaje['destinos'][0]['pedidos'][0]['ProveedorFlete']
            viaje['propiedadunidad'][0]['VolumenMax'] = viaje['destinos'][0]['pedidos'][0]['VolumenMax']
            if placas != '':
                Rendimiento = self.dfVehiculos.loc[self.dfVehiculos['Placas'] == placas]['Rendimiento'].tolist()
                viaje['propiedadunidad'][0]['Rendimiento'] = Rendimiento[0]
            else:
                viaje['propiedadunidad'][0]['Rendimiento'] = ''
        json_base = {"viajes": viajes, "cedi": cedi, "idempresa": viajes[0]['destinos'][0]['pedidos'][0]['Empresa']}
        return json_base

    def makeJsonDB(self, destinos):
        # print (config.usuario)
        data = dict()
        data["destinos"] = destinos

        return data