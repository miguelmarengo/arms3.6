# coding: utf-8
import requests
import json
from datetime import datetime
from time import gmtime, strftime
from random import randint
import time
import config
from IO.Apis.json2df import *
from IO.Apis.consolidar_json import ConsolidarJson
from py4j.java_gateway import JavaGateway
import pandas as pd

"""
    @desc: Clase para llamar al motor por viaje
    @author: Eduardo Giles
"""

class CallMotorOptimizado:

    def callMotor(self, df, tipo):
        print ("Llamar al motor aqui")
        completo = 1
        if type(df) is int:
            print ("Plan vacio")
            completo = 0
        else:
            if df.empty:
                print ("Plan vacio")
                completo = 0
        
        if completo == 1:
            consolidar = ConsolidarJson()
            viajes = consolidar.getJsonPorViaje(df, tipo)

            # print ("*************")
            # print (json.dumps(viajes))
            # print ("*************")

            dfOpt = list
            # LLAMADA AL MOTOR
            for index, viaje in enumerate(viajes):
                gateway = JavaGateway()
                solution = gateway.entry_point.cargandoPlanFromText(json.dumps(viaje))
                gateway.entry_point.iniciarEjecucion()
                time.sleep(9)
                gateway.entry_point.terminarEjecucion()
                time.sleep(1)
                solution = gateway.entry_point.getSolution()
                res = json.loads(solution)
                
                if index == 0:
                    dfOpt = res
                else:
                    res["viajes"][0]["Viaje"] = index + 1
                    dfOpt["viajes"].append(res["viajes"][0])

                # print ("/////////////")
                # print (json.dumps(res, indent=4))
                # print ("/////////////")

            # print ("/////////////")
            # print (json.dumps(dfOpt))
            # print ("/////////////")

            dfRes = json2df(dfOpt)
            return dfRes
        else:
            dfEmpty = pd.DataFrame()
            return dfEmpty
        
        
        # LLAMADA AL MOTOR

        # # print ("-------")
        # # print (json.dumps(viajes, indent=4))
        # # print ("-------")
        # # INICIA LLAMADA AL MOTOR
        # gateway = JavaGateway()
        # # fullPath = 'data/arms/planes/plan.json'
        # # jsonCompleto= open(fullPath).read()

        # # print ("--------------")
        # # print (jsonCompleto)
        # # print (type(jsonCompleto))
        # print ("**************")
        # print (json.dumps(viajes[0]))
        # # print (type(json.dumps(viajes[0])))
        # print ("--------------")


        # solution = gateway.entry_point.cargandoPlanFromText(json.dumps(viajes[0]))
        # # solution = gateway.entry_point.cargandoPlanFromText(jsonCompleto)
        # # solution = gateway.entry_point.cargarFile(fullPath)
        # gateway.entry_point.iniciarEjecucion()
        # time.sleep(20)
        # #Obtiene la mejor solucion actual, puede ser ejecutada en cualquier momento despues de haber sido ejecutado el plan no es necesario detener
        

        # # solution = gateway.entry_point.getSolution()
        # # jjson = json.loads(solution)
        # # dict = jjson['contenido']['jsonGrafica']
        # # print(json.dumps(dict))
        # gateway.entry_point.terminarEjecucion()

        # print ("---------*********---------")
        # # solution = gateway.entry_point.getSolution()
        # # jjson = json.loads(solution)
        # # dict = jjson['viajes']
        # # print(json.dumps(dict))
        # time.sleep(1)
        # print (gateway.entry_point.getSolution())
        # print ("---------*********---------")

        







            

    # def callMotor(self):
    #     # ok, duracion, resultado = self.obtenerJsonPorViaje(resultadosId, resultadosSheetName, tarifasId, tarifasSheetName)
    #     consolidar = ConsolidarJson()
    #     viajes = consolidar.getJsonPorViaje()
    #     # print (json.dumps(viajes, indent=4))
    #     # ok, duracion, resultado = self.obtenerJsonPorViaje()
    #     # print (ok)
    #     for viaje in viajes:
    #         viaje["bucketname"] = "arete.arms3.arcos.dev"
    #         viaje["keyarcos"] = "SILODISA/ARMS DESTINOS SILODISA/arcos.txt"
    #         plan = viaje["configuracion"][0]["Plan"] + "~Viaje" + str(randint(0, 1000))
    #         viaje["configuracion"][0]["Plan"] = plan
    #         resMotor = self.iniciarMotor(viaje)
    #         if resMotor == True:
    #             time.sleep(5)
    #             resSolucion = self.obtenerSolucion(1, plan)
    #             if resSolucion == True:
    #                 resDetenido = self.detenerMotor(1, plan)

    # def obtenerJsonPorViaje(self, resultadosId, resultadosSheetName, tarifasId, tarifasSheetName):
    #     start_time = datetime.now()
    #     try:
    #         url = 'https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/leer-ss-json-optimizador'
    #         payload = {"resultadosId": resultadosId, "sheetName1": resultadosSheetName, "tarifasId": tarifasId, "sheetName2": tarifasSheetName}
    #         headers = {'content-type': 'application/json', 'X-API-KEY': 'mtS5hV3rl53tBeGeWnfWq2pqxSRDjOvJW6L7hUb9'}

    #         r = requests.post(url, data=json.dumps(payload), headers=headers)
    #         resultado = json.loads(r.text)
            
    #         # resultado[1][0]["bucketname"] = "arete.arms3.arcos.dev"
    #         # resultado[1][0]["keyarcos"] = "SILODISA/ARMS DESTINOS SILODISA/arcos.txt"
    #         # resultado[1][0]["nombreusuario"] = "EL LALOS"
    #         # resultado[1][0]["fechaActual"] = strftime("%Y/%m/%d %H:%M", gmtime())
    #         # resultado[1][0]["NombreEmpresa"] = "SILODISA"
    #         # resultado[1][0]["NombreArchivo"] = "ARMS DESTINOS SILODISA"
    #         # resultado[1][0]["tipoOptimizacion"] = "Optimizar"
    #         # resultado[1][0]["configuracion"][0]["IdEmpresa"] = 1
    #         # plan = resultado[1][0]["configuracion"][0]["Plan"] + "~Viaje" + str(randint(0, 1000))
    #         # resultado[1][0]["configuracion"][0]["Plan"] = plan

    #         # for i in viajes:
    #         #     for j in i['destinos']:
    #         #         for k in j['pedidos']:
    #         #             VentanaFechaInicioPedido = str(k['VentanaFechaInicioPedido']).strip()
    #         #             VentanaFechaFinPedido = str(k['VentanaFechaFinPedido']).strip()
    #         #             #print('{}, inicio'.format(VentanaFechaInicioPedido))
    #         #             #print('{}, fin'.format(VentanaFechaFinPedido))
    #         #             inicio = datetime.datetime.strptime(VentanaFechaInicioPedido, '%d-%m-%y').date().strftime('%d/%m/%Y')
    #         #             fin = datetime.datetime.strptime(VentanaFechaFinPedido, '%d-%m-%Y').date().strftime('%d/%m/%Y')
    #         #             k['VentanaFechaInicioPedido'] =inicio
    #         #             k['VentanaFechaFinPedido'] = fin






    #         fileName='ARMS DESTINOS SILODISA'
    #         empresa='SILODISA'
    #         data=[]

    #         for i in resultado[1]:
    #             for j in i['destinos']:
    #                 data.append(dict(latitud=j['Latitud'], longitud=j['Longitud'],iddestino= j['Destino'],status=1))

    #         payloadDestinos= {'fileName': fileName,'idempresa': empresa,'data':data,'status': 'true'}
            
    #         headers = {'content-type': 'application/json'}
    #         s = requests.post('http://34.234.222.132/api/arcostime/',json.dumps(payloadDestinos),headers=headers)
    #         datos=s.json()
    #         try:
    #             if len(datos['data'][0])<3:
    #                 print('Faltan arcos')
    #                 p = requests.post('http://34.234.222.132/api/arcos/', json.dumps(payloadDestinos), headers=headers)
    #                 print(p.text)
    #                 exit('CALCULANDO ARCOS')

    #         except Exception as e:
    #             print(e)
    #             print('NO HAY FALTANTES')





            
    #         for viaje in resultado[1]:
    #             viaje["bucketname"] = "arete.arms3.arcos.dev"
    #             viaje["keyarcos"] = "SILODISA/ARMS DESTINOS SILODISA/arcos.txt"
    #             viaje["nombreusuario"] = "EL LALOS"
    #             viaje["fechaActual"] = strftime("%Y/%m/%d %H:%M", gmtime())
    #             viaje["NombreEmpresa"] = "SILODISA"
    #             viaje["NombreArchivo"] = "ARMS DESTINOS SILODISA"
    #             viaje["tipoOptimizacion"] = "Optimizar"
    #             viaje["configuracion"][0]["IdEmpresa"] = 1
    #             viaje["configuracion"][0]["FacVentanaEntrega"] = False
    #             plan = viaje["configuracion"][0]["Plan"] + "~Viaje" + str(randint(0, 1000))
    #             viaje["configuracion"][0]["Plan"] = plan
    #             for j in viaje["destinos"]:
    #                 # CORRIGE LA FECHA
    #                 for k in j['pedidos']:
    #                     VentanaFechaInicioPedido = str(k['VentanaFechaInicioPedido']).strip()
    #                     VentanaFechaFinPedido = str(k['VentanaFechaFinPedido']).strip()
    #                     if '-' in VentanaFechaInicioPedido:
    #                         inicio = VentanaFechaInicioPedido.replace('-', '/')
    #                         # inicio = datetime.strptime(VentanaFechaInicioPedido, '%d-%m-%Y').date().strftime('%d/%m/%Y')
    #                         k['VentanaFechaInicioPedido'] = inicio
    #                     if '-' in VentanaFechaFinPedido:
    #                         fin = VentanaFechaFinPedido.replace('-', '/')
    #                         # fin = datetime.strptime(VentanaFechaFinPedido, '%d-%m-%Y').date().strftime('%d/%m/%Y')
    #                         k['VentanaFechaFinPedido'] = fin

    #             resMotor = self.iniciarMotor(viaje)
    #             if resMotor == True:
    #                 time.sleep(5)
    #                 resSolucion = self.obtenerSolucion(1, plan)
    #                 if resSolucion == True:
    #                     resDetenido = self.detenerMotor(1, plan)


    #     except Exception as e:
    #         file_name = os.path.basename(sys.argv[0])
    #         this_function_name = sys._getframe().f_code.co_name
    #         error = '{} - {} - {}'.format(file_name,this_function_name,str(e))
    #         return False, error, False
    #     duracion = datetime.now() - start_time
    #     return True, duracion, 0

    # def iniciarMotor(self, jsonViaje):
    #     print ("INICIANDO EL MOTOR")

    #     r = requests.post('http://arms3-engine.us-east-1.elasticbeanstalk.com/rest/arms/solution/recalculoviaje',json.dumps(jsonViaje))
    #     solucion = r.json()

    #     print ("---- RESPUESTA  MOTOR ----")
    #     print(solucion)
    #     print ("----")

    #     return True

    # def obtenerSolucion(self, idEmpresa, idPlan):
    #     print ("OBTENIENDO SOLUCION")

    #     args = {"ambiente": 0, "idEmpresa": idEmpresa, "idPlan": idPlan}

    #     r = requests.post('http://arms3-engine.us-east-1.elasticbeanstalk.com/rest/arms/solution/getBestJSONOptimizador',json.dumps(args))
    #     solucion = r.json()

    #     print ("---- SOLUCION  MOTOR ----")
    #     print(solucion)
    #     print ("----")

    #     return True

    # def detenerMotor(self, idEmpresa, idPlan):
    #     print ("DETENIENDO MOTOR")

    #     args = {"ambiente": 0, "idEmpresa": idEmpresa, "idPlan": idPlan}

    #     r = requests.post('http://arms3-engine.us-east-1.elasticbeanstalk.com/rest/arms/solution/terminateEarly',json.dumps(args))
    #     solucion = r.json()

    #     print ("---- DETENER  MOTOR ----")
    #     print(solucion)
    #     print ("----")

    #     return True

    
    # def getBest():
    #     # ARCOS FALTANTES
    #     {"data":[{"tiempo":"0.0 min"},{"faltantes":"2"},{"ticket":"-KwqlCw0I38eF5ZhMUcw"}]}
    #     fileName='ARMS DESTINOS SILODISA'
    #     empresa='SILODISA'

    #     payload={
    #     "sheetName1": "RESULTADOS",
    #     "resultadosId": "1tLkiVHjj9ImwXMOHxMbI7UvavGExsm1OLpiVPj8JhuE",
    #     "tarifasId": "1HGRQajI2bzZppMuVonjk4h2hjXug3xttvFzx7xUJ2JE",
    #     "sheetName2": "MATRIZ_TARIFAS"
    #     }
    #     r = requests.post('https://o0480vy3fe.execute-api.us-east-1.amazonaws.com/dev/leer-ss-json-optimizador',json.dumps(payload))
    #     solucion=r.json()
    #     viajes=solucion[1]
    #     data=[]
    #     #pprint()
    #     for i in viajes:
    #         for j in i['destinos']:
    #             data.append(dict(latitud=j['Latitud'], longitud=j['Longitud'],iddestino= j['Destino'],status=1))

    #     payloadDestinos= {
    #             'fileName': fileName,
    #             'idempresa': empresa,
    #             'data':data,
    #             'status': 'true'
    #         }
    #     print(payloadDestinos)
    #     headers = {'content-type': 'application/json'}
    #     s = requests.post('http://34.234.222.132/api/arcostime/',json.dumps(payloadDestinos),headers=headers)
    #     datos=s.json()
    #     print(s.text)
    #     print(len(datos['data']))
    #     try:
    #         if len(datos['data'])<3:
    #             print('Faltan arcos')
    #             p = requests.post('http://34.234.222.132/api/arcos/', json.dumps(payloadDestinos), headers=headers)
    #             print(p.text)
    #             exit('CALCULANDO ARCOS')
    #     except Exception as e:
    #         print(e)
    #         print('NO HAY FALTANTES')
    #     idViaje = 1
    #     for i in viajes:
    #         for j in i['destinos']:
    #             for k in j['pedidos']:
    #                 VentanaFechaInicioPedido = str(k['VentanaFechaInicioPedido']).strip()
    #                 VentanaFechaFinPedido = str(k['VentanaFechaFinPedido']).strip()
    #                 #print('{}, inicio'.format(VentanaFechaInicioPedido))
    #                 #print('{}, fin'.format(VentanaFechaFinPedido))
    #                 inicio = datetime.datetime.strptime(VentanaFechaInicioPedido, '%d-%m-%y').date().strftime('%d/%m/%Y')
    #                 fin = datetime.datetime.strptime(VentanaFechaFinPedido, '%d-%m-%Y').date().strftime('%d/%m/%Y')
    #                 k['VentanaFechaInicioPedido'] =inicio
    #                 k['VentanaFechaFinPedido'] = fin

    #         plan=i['configuracion'][0]['Plan']
    #         try:
    #             i['keyarcos'] = KEYARCOS
    #             i['nombreusuario'] = NOMBREUSUARIO
    #             i['fechaActual'] = datetime.datetime.fromtimestamp(time.time()).strftime('%d/%m/%Y %H:%M:%S')
    #             i['proveedor'] = 'AS000104'
    #             i['configuracion'][0]['IdEmpresa'] = 1
    #             i['configuracion'][0]['idspreadsheetpedidos'] = spreadsheetIdPedidos
    #             i['configuracion'][0]['NombreEmpresa'] = empresa
    #             i['configuracion'][0]['NombreArchivo'] = fileName
    #             i['tipoOptimizacion'] = 'Optimizar'
    #             i['configuracion'][0]['FacCompatibilidad'] = False
    #             i['configuracion'][0]['NombreArchivo'] = False
    #             i['configuracion'][0]['FacRestriccionVolumen'] = False
    #             i['configuracion'][0]['FacPeso'] = False
    #             i['configuracion'][0]['FacValor'] = False
    #             i['configuracion'][0]['FacVolumen'] = False
    #             i['configuracion'][0]['FactorLocal'] = False
    #             i['configuracion'][0]['NombreArchivo'] = False
    #             i['configuracion'][0]['Plan'] = plan + '~Viaje_{}'.format(str(idViaje))

    #             print(json.dumps(i))
    #             idViaje=idViaje+1
    #             iniciarMotor(i);
    #         except Exception as e:
    #             print(e)
    # def iniciarMotor(payload):
    #     r = requests.post('http://localhost:8080/rest/arms/solution/recalculoviaje',json.dumps(payload))
    #     solucion = r.json()
    #     print(solucion)