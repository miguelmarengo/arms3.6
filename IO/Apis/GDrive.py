from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

class GDrive:

    def __init__(self):
        gauth = GoogleAuth()
        gauth.LocalWebserverAuth()
        self.drive = GoogleDrive(gauth)

    def copy_file_to_folder(self, source_id, dest_folder_id,dest_title):
        try:
            f = self.drive.CreateFile()
            service = self.drive.auth.service
            copied_file = {'title': dest_title}
            f = service.files().copy(fileId=source_id, body=copied_file).execute()
            parents = f.get('parents')
            if parents == None:
                file = service.files().update(fileId=f["id"], addParents=dest_folder_id,fields='id, parents').execute()
            else:
                file = service.files().update(fileId=f["id"], addParents=dest_folder_id,removeParents=f["parents"][0]["id"], fields='id, parents').execute()
            return file['id']
        except Exception as e:
            print("Error en la copia del plan")
            print(e)
            exit(0)




    def search_folder(self,folderName):
        if folderName.lower() == "pochteca":
            return ""
        if folderName.lower() == "silodisa":
            return "0B-JkJEb5mwhhaDZVUGNwWVN2Umc"

