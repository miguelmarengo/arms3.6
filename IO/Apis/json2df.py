
import json
import copy
import pandas as pd
import config
from IO.Apis.ordenar_datos import *
from IO.Apis.spread_sheet import SpreadSheet
from Base.GeneradorRutas.Datos.Planes import *
from IO.Apis.consolidar_json import ConsolidarJson
import os
from Base.GeneradorRutas.Optimiza.funciones import *


def json2df(json_file):

    df_plan = json2df_fun(json_file)
    df_plan_ordenado = ordenar_resultados(df_plan, completar=True)
    df_plan_ordenado2 = format_columns(df_plan_ordenado)
    df_plan_ordenado2.fillna('', inplace=True)

    return df_plan_ordenado2


def json2df_motor(jsonGuardado, save=False):

    #cwd = os.getcwd()

    #datafile = cwd + '/data/arms/resultados/' + filename
    #json_file = load_json(datafile)
    df_plan = json2df_fun(jsonGuardado)

    df_plan_ordenado = fnOrdenarResultados(df_plan, completar=True)
    df_plan_ordenado2 = format_columns(df_plan_ordenado)
    df_plan_ordenado2.fillna('', inplace=True)


    # print(df_plan_ordenado2)

    if save:
        ss = SpreadSheet()
        res = ss.guarda_ss(df_plan_ordenado2, config.nombrePlan, "RESULTADOS", tipo_nombre_ss='name')
        # LLAMADA PARA CREAR EL JSON-ZOTE
        consolidar = ConsolidarJson()
        viajes = consolidar.jsonParaDB()
        print(viajes)

    return df_plan_ordenado2

def load_json(name_file):

    with open(name_file) as data_file:
        data = json.load(data_file)

    # json_file = data['json_write']['plan']
    json_file = data

    return json_file


def json2df_fun(json_file):

    # carga el df del motor
    dataframe_motor = pd.io.json.json_normalize(json_file, ['viajes'], ['nombreusuario', 'idempresa'])

    # Renombra columnas para que coincidan con el ss
    dataframe_motor = dataframe_motor.rename(columns={'nombreusuario': 'UsuarioEjecucionMotor', 'idempresa': 'Empresa'})

    # expande el diccionario de "propiedadunidad" en el df
    dataframe_motor_unidad = expand_dictionary_df(dataframe_motor, 'propiedadunidad')

    # expande la lista de cada destino para que sea una fila del df
    dataframe_motor_destinos = expand_list_df(dataframe_motor_unidad, 'destinos')

    # expande el diccionario de destinos a una columna del df
    dataframe_motor_destinos = expand_dictionary_df(dataframe_motor_destinos, 'destinos')

    # expande la lista de cada pedido para que sea una fila del df
    dataframe_motor_pedidos = expand_list_df(dataframe_motor_destinos, 'pedidos')

    # expande el diccionario de pedidos a una columna del df
    dataframe_motor_pedidos = expand_dictionary_df(dataframe_motor_pedidos, 'pedidos')

    dataframe_motor_pedidos = dataframe_motor_pedidos.rename(columns={'VentanaInicioDestino': 'VentanaHoraInicioPedido',
                                                                      'VentanaFinDestino': 'VentanaHoraFinPedido'})

    dataframe_motor_pedidos['ValorMax'] = 18000000

    return dataframe_motor_pedidos


def expand_dictionary_df(df, loc):

    df = pd.concat([df.drop([loc], axis=1),
                    df[loc].apply(pd.Series)], axis=1)
    df = df.ix[:, ~df.columns.duplicated()]

    return df


def expand_list_df_minus1(df, loc):

    rows = []
    for i, row in df.iterrows():
        vs = row.at[loc]
        new = row.copy()
        i2 = 0
        list_length = len(vs)
        for v in vs:
            if i2 < list_length - 1:
                new.at[loc] = v
                rows.append(copy.deepcopy(new))
            i2 = i2 + 1

    return pd.DataFrame(rows)


def expand_list_df(df, loc):

    rows = []
    for i, row in df.iterrows():
        vs = row.at[loc]
        new = row.copy()
        for v in vs:
            new.at[loc] = v
            rows.append(copy.deepcopy(new))

    return pd.DataFrame(rows)


def format_columns(df):

    columns_si_no = ['Refrigerado', 'Dedicado', 'Cita']

    df[columns_si_no] = df[columns_si_no].replace({False: 'No'}, regex=True)
    df[columns_si_no] = df[columns_si_no].replace({True: 'Sí'}, regex=True)

    df['EntregaRecoleccion'] = df['EntregaRecoleccion'].replace({False: 'Entrega'}, regex=True)
    df['EntregaRecoleccion'] = df['EntregaRecoleccion'].replace({True: 'Recolección'}, regex=True)

    columns_float2d = ['Valor', 'ValorMax', 'DuracionViaje', 'Km', 'LitrosDiesel', 'CostoProveedorFlete',
                       'CostoCombustible', 'CostoHotel', 'CostoComidas', 'CostoThermo',
                       'CostoCasetas', 'CostoFerry', 'CostoOtros', 'CostoTotal']

    df[columns_float2d] = df[columns_float2d].apply(pd.to_numeric, errors='ignore')
    df[columns_float2d] = df[columns_float2d].applymap('{:.2f}'.format)

    columns_float4d = ['Volumen', 'Peso', 'VolumenPermitido', 'PesoPermitido', 'VolumenMax', 'PesoMax',
                       'Co2', 'OcupacionVolumen', 'OcupacionPeso']

    df[columns_float4d] = df[columns_float4d].apply(pd.to_numeric, errors='ignore')
    df[columns_float4d] = df[columns_float4d].applymap('{:.4f}'.format)

    columns_dates = ['VentanaHoraInicioPedido', 'VentanaHoraFinPedido']

    for index in range(0, len(columns_dates)):
        date_a = pd.to_datetime(df[columns_dates[index]], errors='ignore')
        date_b = date_a.apply(lambda i: i.strftime('%H:%M'))
        df[columns_dates[index]] = date_b

    columns_dates = ['VentanaFechaInicioPedido', 'VentanaFechaFinPedido']

    for index in range(0, len(columns_dates)):
        date_a = pd.to_datetime(df[columns_dates[index]], errors='ignore')
        date_b = date_a.apply(lambda i: i.strftime('%d/%m/%Y'))
        df[columns_dates[index]] = date_b

    columns_dates_comp = ['FechaEntregaPedido', 'FechaSalidaPedido', 'FechaSalida', 'FechaRetorno',
                          'FechaEjecucionMotor', 'FechaEjecucionOpti', 'FechaEjecucionAsigna', 'FechaEntregaTR2']

    for index in range(0, len(columns_dates_comp)):
        date_a = pd.to_datetime(df[columns_dates_comp[index]], errors='ignore')
        date_b = date_a.apply(lambda x: x.strftime('%d/%m/%Y %H:%M')if not pd.isnull(x) else '')
        df[columns_dates_comp[index]] = date_b

    return df


def list_duplicates(seq):

    seen = set()
    seen_add = seen.add
    # adds all elements it doesn't know yet to seen and all other to seen_twice
    seen_twice = set(x for x in seq if x in seen or seen_add(x))
    # turn the set into a list (as requested)
    return list(seen_twice)


