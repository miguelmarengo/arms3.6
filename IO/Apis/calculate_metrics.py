
# -*- coding: utf-8 -*-
import pandas as pd
import sys
import os


def calculate_metrics_empty(df_pedido):

    # Etiqueta de las métricas
    metrics_labels = ['Costo Pieza', 'Costo Peso', 'Costo Volumen', 'Costo Valor', 'Costo Viaje',
                      'Costo Total', 'Total Km', 'Total Co2', 'OCUPACIONVOLUMEN/Viaje', 'OCUPACIONPESO/Viaje',
                      'Total Pedidos', 'Total Destinos', 'Total Viajes', 'Total m3', 'Total Kg', 'Total Valor',
                      'Total Tiempo', 'Costo Km', 'Costo Co2', 'Costo Hr', 'Piezas/Viaje', 'm3/Viaje',
                      'Kg/Viaje', 'Valor/Viaje', 'Km/Viaje', 'Co2/Viaje', 'Hr/Viaje', 'No Respeto Ventana',
                      'No Respeto Volumen', 'No Respeto Peso', 'No Respeto Valor', 'Piezas']

    # Columnas que se van a sumar en el dataframe de pedido
    columns2_sum_df_pedido = ['Volumen', 'Peso', 'Piezas', 'Valor']

    # Suma las columnas del df de pedido correspondiente
    df_sum_pedido = df_pedido[columns2_sum_df_pedido].sum(axis=0)

    # Crea un df de destino para contabilizar los destinos
    df_destinos = df_pedido.drop_duplicates(['DestinoTR1'], keep='first')
    df_destinos = df_destinos.DestinoTR1

    # Calcula el número de pedidos, destinos y viajes
    no_pedidos = df_pedido.shape[0]
    no_destinos = len(df_destinos)

    # Crea una lista con todas las fórmulas de las métricas
    metric_list = [0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   no_pedidos,
                   no_destinos,
                   0.0,
                   df_sum_pedido['Volumen'],
                   df_sum_pedido['Peso'],
                   df_sum_pedido['Valor'],
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   0.0,
                   df_sum_pedido['Piezas']
                   ]

    # Crea un diccionario con las métricas y las etiquetas
    # metrics = dict(zip(metrics_labels, metric_list))
    metrics = pd.Series(metric_list, index=metrics_labels)

    return metrics


def calculate_metrics_full(df_pedido):
    try:

        # Etiqueta de las métricas
        metrics_labels = ['Costo Pieza', 'Costo Peso', 'Costo Volumen', 'Costo Valor', 'Costo Viaje',
                          'Costo Total', 'Total Km', 'Total Co2', 'OCUPACIONVOLUMEN/Viaje', 'OCUPACIONPESO/Viaje',
                          'Total Pedidos', 'Total Destinos', 'Total Viajes', 'Total m3', 'Total Kg', 'Total Valor',
                          'Total Tiempo', 'Costo Km', 'Costo Co2', 'Costo Hr', 'Piezas/Viaje', 'm3/Viaje',
                          'Kg/Viaje', 'Valor/Viaje', 'Km/Viaje', 'Co2/Viaje', 'Hr/Viaje', 'No Respeto Ventana',
                          'No Respeto Volumen', 'No Respeto Peso', 'No Respeto Valor', 'Piezas']

        # Columnas que se van a sumar en el dataframe de pedido
        columns2_sum_df_pedido = ['Volumen', 'Peso', 'Piezas', 'Valor']
        # Columnas que se van a promediar en el dataframe de pedido
        columns2_mean_df_pedido = ['NoRespetoVentana']
        # Columnas que se van a sumar en el dataframe de viaje
        columns2_sum_df_viaje = ['Km', 'Co2', 'LitrosDiesel', 'CostoCombustible', 'CostoTotal', 'DuracionViaje']
        # Columnas que se van a promediar en el dataframe de pedido
        columns2_mean_df_viaje = ['OcupacionVolumenAc', 'OcupacionPesoAc',
                                  'NoRespetoPeso', 'NoRespetoVolumen', 'NoRespetoValor']

        # Transforma la columna de VentanaFechaInicioPedido a flotante
        df_pedido['VentanaFechaInicioPedidoDate'] = pd.to_datetime(df_pedido['VentanaFechaInicioPedido'],
                                                                   format='%d/%m/%Y')

        # Transforma la columna de VentanaFechaFinPedido a flotante
        df_pedido['VentanaFechaFinPedidoDate'] = pd.to_datetime(df_pedido['VentanaFechaFinPedido'],
                                                                format='%d/%m/%Y')
        # df_pedido['VentanaFechaFinPedidoDate'].dt.hour = 23
        # df_pedido['VentanaFechaFinPedidoDate'].dt.minute = 59

        # Transforma la columna de VentanaHoraInicioPedido a flotante
        hour_col = pd.to_datetime(df_pedido['VentanaHoraInicioPedido'], format='%H:%M')
        df_pedido['VentanaHoraInicioPedidoFloat'] = hour_col.dt.hour + hour_col.dt.minute / 60

        # Transforma la columna de VentanaHoraFinPedido a flotante
        hour_col = pd.to_datetime(df_pedido['VentanaHoraFinPedido'], format='%H:%M')
        df_pedido['VentanaHoraFinPedidoFloat'] = hour_col.dt.hour + hour_col.dt.minute / 60

        # Transforma la parte de la hora de la columna de FechaEntregaPedido a flotante
        hour_col = pd.to_datetime(df_pedido['FechaEntregaPedido'], format='%d/%m/%Y %H:%M')
        df_pedido['HoraEntregaPedidoFloat'] = hour_col.dt.hour + hour_col.dt.minute / 60
        hour_col2 = hour_col.copy()
        hour_col2 = hour_col2.apply(lambda dt: dt.replace(hour=0, minute=0))
        df_pedido['FechaEntregaPedidoDate'] = hour_col2

        # Crea la columna de NoRespetoVentanaHora (100 si no respeta la ventana de acceso en horas, 0 si si la respeta)
        df_pedido['NoRespetoVentanaHora'] = 100
        df_pedido.loc[(df_pedido['HoraEntregaPedidoFloat'] >= df_pedido['VentanaHoraInicioPedidoFloat']) &
                      (df_pedido['HoraEntregaPedidoFloat'] <= df_pedido['VentanaHoraFinPedidoFloat']),
                      'NoRespetoVentanaHora'] = 0

        # Crea la columna de NoRespetoVentanaFecha (100 si no respeta la ventana de acceso en fecha, 0 si si la respeta)
        df_pedido['NoRespetoVentanaFecha'] = 100
        df_pedido.loc[(df_pedido['FechaEntregaPedidoDate'] >= df_pedido['VentanaFechaInicioPedidoDate']) &
                      (df_pedido['FechaEntregaPedidoDate'] <= df_pedido['VentanaFechaFinPedidoDate']),
                      'NoRespetoVentanaFecha'] = 0

        # Crea la columna de NoRespetoVentana (100 no respeta ya sea la ventana de hora o fecha, 0 si si la respeta)
        df_pedido['NoRespetoVentana'] = 100
        df_pedido.loc[(df_pedido['NoRespetoVentanaHora'] == 0) &
                      (df_pedido['NoRespetoVentanaFecha'] == 0),
                      'NoRespetoVentana'] = 0

        # Suma las columnas del df de pedido correspondiente
        df_sum_pedido = df_pedido[columns2_sum_df_pedido].sum(axis=0)
        # promedia las columnas del df de pedido correspondiente
        df_mean_pedido = df_pedido[columns2_mean_df_pedido].mean(axis=0)

        # Copia el df de pedido
        df_pedido2 = df_pedido.copy()
        # Suma las columnas de volumen, peso, piezas y valor por viaje (este df se concatenará al df por viaje)
        df_sum_pedidoxviaje = df_pedido2.groupby('Viaje')[columns2_sum_df_pedido].sum(axis=0)

        # Crea el df por viaje
        df_viaje = df_pedido.copy()
        # Elimina los duplicados (para obtener solo una fila por viaje)
        df_viaje.drop_duplicates(['Viaje'], keep='first', inplace=True)
        # Hace el indice por viaje y lo ordena
        df_viaje.set_index(['Viaje'], inplace=True)
        df_viaje.sort_index(inplace=True)
        # Elimina las columnas que se van a añadir de sumas por viaje
        df_viaje.drop(columns2_sum_df_pedido, axis=1, inplace=True)
        # Concatena los dos df de viaje (el de registros únicos y el de suma)
        df_viaje = pd.concat([df_viaje, df_sum_pedidoxviaje], axis=1)

        # Crea la columna de NoRespetoPeso (100 si no respeta el peso máximo 0 si si)
        df_viaje['NoRespetoPeso'] = 0
        df_viaje.loc[df_viaje['Peso'] > df_viaje['PesoMax'], 'NoRespetoPeso'] = 100

        # Crea la columna de NoRespetoVolumen (100 si no respeta el volumen máximo 0 si si)
        df_viaje['NoRespetoVolumen'] = 0
        df_viaje.loc[df_viaje['Volumen'] > df_viaje['VolumenMax'], 'NoRespetoVolumen'] = 100

        # Crea la columna de NoRespetoValor (100 si no respeta el valor máximo 0 si si)
        df_viaje['NoRespetoValor'] = 0
        df_viaje.loc[df_viaje['Valor'] > df_viaje['ValorMax'], 'NoRespetoValor'] = 100

        # Crea la columna de OcupacionVolumenAc para recalcular la ocupacion volumen
        df_viaje['OcupacionVolumenAc'] = (df_viaje['Volumen']*100 / df_viaje['VolumenMax'])

        # Crea la columna de OcupacionPesoAc para recalcular la ocupacion volumen
        df_viaje['OcupacionPesoAc'] = (df_viaje['Peso']*100 / df_viaje['PesoMax'])

        # Crea un df de destino para contabilizar los destinos
        df_destinos = df_pedido.drop_duplicates(['DestinoTR1'], keep='first')
        df_destinos = df_destinos.DestinoTR1

        # Suma las columnas del df de viaje correspondiente
        df_sum_viaje = df_viaje[columns2_sum_df_viaje].sum(axis=0)
        # Promedia las columnas del df de viaje correspondiente
        df_mean_viaje = df_viaje[columns2_mean_df_viaje].mean(axis=0)

        # Calcula el número de pedidos, destinos y viajes
        no_pedidos = df_pedido.shape[0]
        no_viajes = df_viaje.shape[0]
        no_destinos = len(df_destinos)

        # no pueden venir en CERO pues lo dividimos...divide by cero
        piezas = 1 if df_sum_pedido['Piezas'] == 0 else df_sum_pedido['Piezas']
        Peso = 1 if df_sum_pedido['Peso'] == 0 else df_sum_pedido['Peso']
        Volumen = 1 if df_sum_pedido['Volumen'] == 0 else df_sum_pedido['Volumen']
        Valor = 1 if df_sum_pedido['Valor'] == 0 else df_sum_pedido['Valor']
        no_viajes1 = 1 if no_viajes == 0 else no_viajes
        Km = 1 if df_sum_viaje['Km'] == 0 else df_sum_viaje['Km']
        Co2 = 1 if df_sum_viaje['Co2'] == 0 else df_sum_viaje['Co2']
        DuracionViaje = 1 if df_sum_viaje['DuracionViaje'] == 0 else df_sum_viaje['DuracionViaje']

        # Crea una lista con todas las fórmulas de las métricas
        metric_list = [df_sum_viaje['CostoTotal'] / piezas,
                       df_sum_viaje['CostoTotal'] / Peso,
                       df_sum_viaje['CostoTotal'] / Volumen,
                       df_sum_viaje['CostoTotal'] / Valor,
                       df_sum_viaje['CostoTotal'] / no_viajes1,
                       df_sum_viaje['CostoTotal'],
                       df_sum_viaje['Km'],
                       df_sum_viaje['Co2'],
                       df_mean_viaje['OcupacionVolumenAc'],
                       df_mean_viaje['OcupacionPesoAc'],
                       no_pedidos,
                       no_destinos,
                       no_viajes,
                       df_sum_pedido['Volumen'],
                       df_sum_pedido['Peso'],
                       df_sum_pedido['Valor'],
                       df_sum_viaje['DuracionViaje'],
                       df_sum_viaje['CostoTotal'] / Km,
                       df_sum_viaje['CostoTotal'] / Co2,
                       df_sum_viaje['CostoTotal'] / DuracionViaje,
                       df_sum_pedido['Piezas'] / no_viajes1,
                       df_sum_pedido['Volumen'] / no_viajes1,
                       df_sum_pedido['Peso'] / no_viajes1,
                       df_sum_pedido['Valor'] / no_viajes1,
                       df_sum_viaje['Km'] / no_viajes1,
                       df_sum_viaje['Co2'] / no_viajes1,
                       df_sum_viaje['DuracionViaje'] / no_viajes1,
                       df_mean_pedido['NoRespetoVentana'],
                       df_mean_viaje['NoRespetoVolumen'],
                       df_mean_viaje['NoRespetoPeso'],
                       df_mean_viaje['NoRespetoValor'],
                       df_sum_pedido['Piezas']
                       ]

        # Crea un diccionario con las métricas y las etiquetas
        # metrics = dict(zip(metrics_labels, metric_list))
        metrics = pd.Series(metric_list, index=metrics_labels)

        return metrics

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error = '{} - {} - linea {}  ({})'.format(exc_type, fname, exc_tb.tb_lineno, str(e))
        print(error)
    return True


def calculate_metrics(df_pedido):
    df_pedido_copy = df_pedido.copy()
    df_pedido_copy = df_pedido_copy.apply(pd.to_numeric, errors='ignore')
    df_pedido_copy.fillna('', inplace=True)

    if df_pedido_copy['FechaEntregaPedido'].iloc[0] == '':
        metrics = calculate_metrics_empty(df_pedido_copy)
    else:
        metrics = calculate_metrics_full(df_pedido_copy)

    return metrics
