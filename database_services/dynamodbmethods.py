#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, os
import logging
import json
from database_services.dynamodb_service import DynamoDBConnectionARMS
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger(__name__)


class DynamoDBMethods(DynamoDBConnectionARMS):
    """Clase de servicios de ARMS. Esta clase devuelve los objetos de la base de datos. No parsea los resultados"""

    def __init__(self, configfile):
        super(DynamoDBMethods, self).__init__(configfile)

    def guardar_ids_drive(self, acciones):
        """Metodo que guarda la informacion en la bitacora"""
        logger.info('guardar_bitacora')
        try:
            with self.ids.batch_writer() as batch:
                for key in acciones:
                    batch.put_item(Item=key)
            return True, 'Informacion actualizada'

        except Exception as ex:
            return False, 'Fallo al guardar la informacion'

    def buscar_id(self, criterios):
        """Metodo que obtiene de la bitacora la info solicitada"""
        logger.info('buscar_bitacora')
        try:
            filtros = None
            for idx, i in enumerate(criterios):
                if filtros is None:
                    filtros = Attr(criterios[idx]['campo']).eq(criterios[idx]['valor'])
                else:
                    filtros = filtros & Attr(criterios[idx]['campo']).eq(criterios[idx]['valor'])

            response = self.ids.scan(FilterExpression=filtros)
            print("dinamo response")
            print(response)
            data = response['Items']

            return True, data

        except Exception as ex:
            return False, 'Fallo al consultar la info'