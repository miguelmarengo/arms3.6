#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Archivo que maneja las funciones de conexion a la base de datos DynamoDB"""
import os, sys
import logging
import configparser
import boto3

logger = logging.getLogger(__name__)

class DynamoDBConnectionARMS(object):
    """Clase de conexion a DynamoDB. Todas las clases que se conectan a DynamoDB deben heredar de esta clase"""
    def __init__(self, configfile):
        config = self.parseconfig(configfile)

        self.conn = boto3.resource(
                'dynamodb'
                ,region_name=config['region']
                ,aws_access_key_id=config['acc_key_id']
                ,aws_secret_access_key=config['sec_access_key'])
        self.ids = self.conn.Table(config['database'])

    @staticmethod
    def parseconfig(configfile):
        """Archivo que regresa la configuracion"""
        config = configparser.ConfigParser()
        config.read(configfile)
        region = config.get('DynamoDB', 'region')
        aws_acc_key = config.get('DynamoDB', 'aws_access_key_id')
        aws_sec_key = config.get('DynamoDB', 'aws_secret_access_key')
        database = config.get('DynamoDB', 'database')
        return {'region': region,
                'acc_key_id': aws_acc_key,
                'sec_access_key': aws_sec_key,
                'database': database}